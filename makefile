### Definitions for linux FORTRAN compiler

F95      = /opt/intel/composer_xe_2013_sp1.1.106/bin/intel64/ifort -cpp -override-limits -diag-disable 8290,8291

#alba
FFLAGS   = -fpe0 -i-static -traceback #-check bounds -check uninit 

#------

profCDM: library.f90 tracksCDM.f90 deltaprotoCDM.f90       
	$(F95) $(FFLAGS) library.f90 tracksCDM.f90 deltaprotoCDM.f90 -o zz_hprof_plnk_hm2

