# CUSP

This README explains how to use the CUSP pack to compute the density profiles of halos with different masses and the parameter
values giving their respective NFW and Einasto best fits. The redshift z of the density profiles to be calculated as well as the
parameters defining the CDM cosmology adopted are introduced through the param_wmap.par and (WMAP7) param_plnk.par (Planck14) files.

The code is written in FORTRAN 95 and comprises the following programs:

- deltaprotoCDM.f90 
- tracksCDM.f90
- library.f90  

The different options are set in deltaprotoCDM.f90. Taking option=1, the program fits the halo density profile to the NFW (and Einasto) analytic functions fits. One can select the definition of the halo mass (mdef), the cosmology (cosmo) and whether the NFW/Einasto fits are constrained (the halo mass is enforced) or not (prof_cons). 

The main program (tracksCDM.f90) computes the peak trajectory corresponding to a halo with M at z, deconvolves it to find the density contrast profile of the protohalo and finds the halo density profile. Multiparametric fits to the NFW/Einasto profiles  are performed by χ2 minimisation over the radial range from R to 10^−2 R. The density profiles for halo masses spanning over 22 orders of magnitud are computed in two separate ranges: from 10^-6 to 10^7 Mo (low mass, lm) and from 10^8 to 10^16 Mo (high mass, hm). To select the halo mass range, one should comment/uncomment the corresponding code lines in the massrange module at the top of the main program and compile. For a given redshift z, the outputs are written in NFW_params_hm/lm.dat [columns: (1) halo mass, (2) rho_s, (3) r_s, (4) c, (5) M_s) )and Einasto_params_hm/lm.dat (columns: (1) halo mass, (2) rho_s, (3) r_s, (4) c, (5) M_s, (6) alpha).

library.f90 includes all the subroutines used to perform the numerical calculations.

The CUSP pack also includes:

- makefile: built for users of intel FORTRAN 95 compiler. Change it if you use other compiler.
- file moments_nn_lm.dat: contains a table with the CDM 0th order moments as a funtion of the smoothing radius for halos within the low mass range (required in tracksCDM.f90 to save computation time).
- directory profiles: contains output data files with CUSP profiles for halos of different masses at a given z.  
- directory tracks: data needed to built protohalo profiles from peak trajectories are written in/read from data files stored in this directory. 

