program deltaprotoCDM
  USE math_lib, ONLY : spline,spl,minim
  USE massrange
  USE delta_track, ONLY : delta 
  USE global_defs, ONLY : hm1,rvir,alph,r_2,rhos,pi43,a1,a2,b1,b2,bd1,bd2,tol,amin,erra,pi,nitt,xtrm,rep,halom,intmass,qapprox,deltapk,option,mdef,prof_cons,param_d,param_s0,param_s1,param_s2,param_A,cosmo

  IMPLICIT NONE
  INTEGER i,nps,nr,npg1(5),nit,k,ncomp,zz,jj,imass,imr
  
  double precision bd11(2),bd12(2),tol1(2),amin1(2),erra1(2),bestq,q0,delta_0,qmatrix(14)
  LOGICAL :: xtrm1,rep1
  CHARACTER(LEN=2) :: mrng
 
!-- Select flag 
  option=1            !0: adjust the halo-peak relation; 1: generate density profiles and find NFW/Einasto parameters; 2: generate the halo mass function   
  mdef=0              !0: spherical overdesinty with \Delta_vir; 1:M_200
  cosmo=0             !0: WMAP7, 1:Planck (2014)
  prof_cons=1         !0: fits unconstrained NFW and Einasto profiles, 1: fits constrained NFW and Einasto profiles
  
  IF(option==0)THEN
     OPEN(50,file='spec_moments.dat')
  ELSE
     IF(cosmo==0)THEN
        IF(mdef==0)THEN
           param_d=1.06d0
           param_s0=0.0422d0
           param_s1=0.0375d0
           param_s2=0.0318d0
           param_A=25.7d0
        ELSE
           param_d=1.06d0
           param_s0=0.0148d0
           param_s1=0.0630d0
           param_s2=0.0132d0
           param_A=12.4d0
        END IF
     ELSE
        IF(mdef==0)THEN
           param_d=0.928
           param_s0=0.0226
           param_s1=0.0610
           param_s2=0.0156
           param_A=11.7
        ELSE
           param_d=0.928
           param_s0=0.0341
           param_s1=0.0684
           param_s2=0.0239
           param_A=6.87
        END IF
     END IF
     mrng=MERGE('lm','hm',lowm)
     OPEN(74,file='NFW_params_'//mrng//'.dat')
     OPEN(84,file='Einasto_params_'//mrng//'.dat')
  END IF

!-- Select the halo mass range 

  IF(lowm)THEN  
     DO imass=-6,7              ! M=jmass*10^imass
        CALL delta(imass)
     END DO
     STOP
  ELSE
      DO imass=8,16              ! M=jmass*10^imass
        CALL delta(imass)
     END DO
     STOP
  END IF
END program deltaprotoCDM
