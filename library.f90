!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FILE library.f90

!   contains:

!     module tools
!     module math_lib

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

MODULE tools

IMPLICIT NONE
INTEGER :: inpt,ilog,kcum
REAL, PARAMETER :: rzero=1.e-7
!REAL, PARAMETER :: rzero=epsilon(1.)
REAL(KIND=8) :: eps
REAL(KIND=8), PARAMETER :: zero=1.d-13
REAL(KIND=8), PARAMETER :: accur8=1.d-13 ! real*8 accuracy (slightly larger than epsilon(1.d0))
!REAL(KIND=8), PARAMETER :: zero=epsilon(1.d0)
REAL(KIND=8), PARAMETER :: fpzero=tiny(1.d0)/zero
REAL(KIND=8), PARAMETER :: lfpzero=LOG(fpzero)
REAL(KIND=8), DIMENSION(500) :: lvx,lvy,mlvy
LOGICAL :: rnew,MIRAR=.false.,mirar2=.FALSE.

!  contains:

!     function arth
!     function cumsum
!     subroutine diagad
!     function geop
!     function imaxloc
!     function iminloc
!     function nneg
!     function outerprod
!     function outerdiff
!     subroutine swap
!     function upper_triangle

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INTERFACE arth
   MODULE PROCEDURE arth_d,arth_r,arth_i
END INTERFACE

INTERFACE cumsum
   MODULE PROCEDURE cumsum_d,cumsum_i
END INTERFACE

INTERFACE diagad
   MODULE PROCEDURE diagad_d,diagad_r
END INTERFACE

INTERFACE geop
   MODULE PROCEDURE geop_d,geop_i
END INTERFACE

INTERFACE imaxloc
   MODULE PROCEDURE imaxloc_d,imaxloc_r
END INTERFACE

INTERFACE iminloc
   MODULE PROCEDURE iminloc_d,iminloc_r
END INTERFACE

INTERFACE nneg
   MODULE PROCEDURE nneg_d,nneg_i
END INTERFACE

INTERFACE outerdiff
   MODULE PROCEDURE outerdiff_d,outerdiff_i
END INTERFACE

INTERFACE outerprod
   MODULE PROCEDURE outerprod_d,outerprod_i
END INTERFACE

INTERFACE swap
   MODULE PROCEDURE swap_d,swap_dv,swap_r,swap_i
END INTERFACE

CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION arth(first,increment,n,i)

!    Computes arithmetic series

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION arth_d(first,increment,n,i)
REAL(KIND=8), INTENT(IN) :: first,increment
INTEGER, INTENT(IN) :: n
INTEGER, INTENT(IN), OPTIONAL :: i
REAL(KIND=8), DIMENSION(n) :: arth_d
INTEGER :: k
IF(PRESENT(i))THEN
   IF(i /= -1)STOP' Problem with optional arg in arth_d'
   arth_d(n)=first
   DO k=n-1,1,-1
      arth_d(k)=arth_d(k+1)-increment
   END DO
ELSE
   arth_d(1)=first
   DO k=2,n
      arth_d(k)=arth_d(k-1)+increment
   END DO
END IF
END FUNCTION arth_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION arth_r(first,increment,n,i)
REAL, INTENT(IN) :: first,increment
INTEGER, INTENT(IN) :: n
INTEGER, INTENT(IN), OPTIONAL :: i
REAL, DIMENSION(n) :: arth_r
INTEGER :: k
IF(PRESENT(i))THEN
   IF(i /= -1)STOP' Problem with optional arg in arth'
   arth_r(n)=first
   DO k=n-1,1,-1
      arth_r(k)=arth_r(k+1)-increment
   END DO
ELSE
   arth_r(1)=first
   DO k=2,n
      arth_r(k)=arth_r(k-1)+increment
   END DO
END IF
END FUNCTION arth_r

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION arth_i(first,increment,n,i)
INTEGER, INTENT(IN) :: first,increment,n
INTEGER, INTENT(IN), OPTIONAL :: i
INTEGER, DIMENSION(n) :: arth_i
INTEGER :: k
IF(PRESENT(i))THEN
   IF(i /= -1)STOP' Problem with optional arg in arth'
   arth_i(n)=first
   DO k=n-1,1,-1
      arth_i(k)=arth_i(k+1)-increment
   END DO
ELSE
   arth_i(1)=first
   DO k=2,n
      arth_i(k)=arth_i(k-1)+increment
   END DO
END IF
END FUNCTION arth_i

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! RECURSIVE FUNCTION cumsum(arr,seed) RESULT(ans)

!    Fills array with arithmetic series

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

RECURSIVE FUNCTION cumsum_d(arr,seed) RESULT(ans)
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: arr
REAL(KIND=8), OPTIONAL, INTENT(IN) :: seed
REAL(KIND=8) :: sd
REAL(KIND=8), DIMENSION(size(arr)) :: ans
INTEGER :: n,j
n=SIZE(arr)
IF(n == 0)RETURN
sd=0
IF(PRESENT(seed))sd=seed
ans(1)=arr(1)+sd
IF(n < 16)THEN
   DO j=2,n
      ans(j)=ans(j-1)+arr(j)
   END DO
ELSE
   ans(2:n:2)=cumsum_d(arr(2:n:2)+arr(1:n-1:2),sd)
   ans(3:n:2)=ans(2:n-1:2)+arr(3:n:2)
END IF
END FUNCTION cumsum_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

RECURSIVE FUNCTION cumsum_i(arr,seed) RESULT(ans)
INTEGER, DIMENSION(:), INTENT(IN) :: arr
INTEGER, OPTIONAL, INTENT(IN) :: seed
INTEGER, DIMENSION(size(arr)) :: ans
INTEGER :: n,j,sd
n=SIZE(arr)
IF(n == 0)RETURN
sd=0
IF(PRESENT(seed))sd=seed
ans(1)=arr(1)+sd
IF(n < 16)THEN
   DO j=2,n
      ans(j)=ans(j-1)+arr(j)
   END DO
ELSE
   ans(2:n:2)=cumsum_i(arr(2:n:2)+arr(1:n-1:2),sd)
   ans(3:n:2)=ans(2:n-1:2)+arr(3:n:2)
END IF
END FUNCTION cumsum_i

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE diagad(mat,diag)

!    Computes the trace of a squared matrix

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SUBROUTINE diagad_d(mat,diag)
REAL(KIND=8), DIMENSION(:,:), INTENT(INOUT) :: mat
REAL(KIND=8), INTENT(IN) :: diag
INTEGER :: j,n
n=MIN(SIZE(mat,1),SIZE(mat,2))
DO j=1,n
   mat(j,j)=mat(j,j)+diag
END DO
END SUBROUTINE diagad_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE diagad_r(mat,diag)
REAL, DIMENSION(:,:), INTENT(INOUT) :: mat
REAL, INTENT(IN) :: diag
INTEGER :: j,n
n=MIN(SIZE(mat,1),SIZE(mat,2))
DO j=1,n
   mat(j,j)=mat(j,j)+diag
END DO
END SUBROUTINE diagad_r

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION geop(first,factor,n)

!    Computes arithmetic series

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION geop_d(first,factor,n)
REAL(KIND=8), INTENT(IN) :: first,factor
INTEGER, INTENT(IN) :: n
REAL(KIND=8), DIMENSION(n) :: geop_d
INTEGER :: k
geop_d(1)=first
DO k=2,n
   geop_d(k)=geop_d(k-1)*factor
END DO
END FUNCTION geop_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION geop_i(first,factor,n)
INTEGER, INTENT(IN) :: first,factor,n
INTEGER, DIMENSION(n) :: geop_i
INTEGER :: k
geop_i(1)=first
DO k=2,n
   geop_i(k)=geop_i(k-1)*factor
END DO
END FUNCTION geop_i

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION imaxloc_d(arr)

!     Finds index of the maximum value in an array

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION imaxloc_d(arr)
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: arr
INTEGER :: imaxloc_d
INTEGER, DIMENSION(1) :: imax
imax=MAXLOC(arr(:))
imaxloc_d=imax(1)
END FUNCTION imaxloc_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION imaxloc_r(arr)
REAL, DIMENSION(:), INTENT(IN) :: arr
INTEGER :: imaxloc_r
INTEGER, DIMENSION(1) :: imax
imax=MAXLOC(arr(:))
imaxloc_r=imax(1)
END FUNCTION imaxloc_r

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION imimloc(arr)

!     Finds index of the minimum value in an array

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION iminloc_d(arr)
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: arr
INTEGER, DIMENSION(1) :: imin
INTEGER :: iminloc_d
imin=MINLOC(arr(:))
iminloc_d=imin(1)
END FUNCTION iminloc_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION iminloc_r(arr)
REAL, DIMENSION(:), INTENT(IN) :: arr
INTEGER, DIMENSION(1) :: imin
INTEGER :: iminloc_r
imin=MINLOC(arr(:))
iminloc_r=imin(1)
END FUNCTION iminloc_r

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION nneg(arr)

!     Finds first non-negative value in an array

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------!FUNCTION nneg_d(arr)

FUNCTION nneg_d(arr)
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: arr
INTEGER :: nneg_d,k,n
n=SIZE(arr)
DO k=1,n
   IF(arr(k) > 0.d0)EXIT
END DO
nneg_d=MIN(k,n) 
END FUNCTION nneg_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION nneg_i(arr)
INTEGER, DIMENSION(:), INTENT(IN) :: arr
INTEGER :: nneg_i,k,n
n=SIZE(arr)
DO k=1,n
   IF(arr(k) > 0.d0)EXIT
END DO
nneg_i=MIN(k,n) 
END FUNCTION nneg_i

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION outerdiff(a,b)

!     Finds the outerdifference of two arrays

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION outerdiff_d(a,b)
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a,b
REAL(KIND=8), DIMENSION(SIZE(a),SIZE(b)) :: outerdiff_d
outerdiff_d=SPREAD(a,dim=2,ncopies=SIZE(b))-SPREAD(b,dim=1,ncopies=SIZE(a))
END FUNCTION outerdiff_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION outerdiff_i(a,b)
INTEGER, DIMENSION(:), INTENT(IN) :: a,b
INTEGER, DIMENSION(SIZE(a),SIZE(b)) :: outerdiff_i
outerdiff_i=SPREAD(a,dim=2,ncopies=SIZE(b))-SPREAD(b,dim=1,ncopies=SIZE(a))
END FUNCTION outerdiff_i

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION outerprod(a,b)

!     Finds the outerdifference of two arrays

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION outerprod_d(a,b)
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a,b
REAL(KIND=8), DIMENSION(SIZE(a),SIZE(b)) :: outerprod_d
outerprod_d= SPREAD(a,dim=2,ncopies=SIZE(b))*SPREAD(b,dim=1,ncopies=SIZE(a))
END FUNCTION outerprod_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION outerprod_i(a,b)
INTEGER, DIMENSION(:), INTENT(IN) :: a,b
INTEGER, DIMENSION(SIZE(a),SIZE(b)) :: outerprod_i
outerprod_i= SPREAD(a,dim=2,ncopies=SIZE(b))*SPREAD(b,dim=1,ncopies=SIZE(a))
END FUNCTION outerprod_i

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE swap(a,b)

!     Interchanges the contents of two generic variables

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE swap_d(a,b)
  IMPLICIT NONE
  REAL(KIND=8), INTENT(INOUT) :: a,b
  REAL(KIND=8) :: TEMP
  temp=a
  a=b
  b=temp
END SUBROUTINE swap_d

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE swap_dv(a,b)
  IMPLICIT NONE
  REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: a,b
  REAL(KIND=8), DIMENSION(SIZE(a)) :: TEMP
  temp=a
  a=b
  b=temp
END SUBROUTINE swap_dv

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE swap_r(a,b)
  IMPLICIT NONE
  REAL, INTENT(INOUT) :: a,b
  REAL :: TEMP
  temp=a
  a=b
  b=temp
END SUBROUTINE swap_r

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE swap_i(a, b)
  IMPLICIT NONE
  INTEGER, INTENT(INOUT) :: a,b
  INTEGER :: temp
  temp=a
  a=b
  b=temp
END SUBROUTINE swap_i

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION upper_triangle(j,k,extra)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INTEGER, INTENT(IN) :: j,k
INTEGER, OPTIONAL, INTENT(IN) :: extra
LOGICAL, DIMENSION(j,k) :: upper_triangle
INTEGER :: n
n=0
IF(PRESENT(extra))n=extra
upper_triangle=(outerdiff(arth(1,1,j),arth(1,1,k)) < n)
END FUNCTION upper_triangle

END MODULE tools

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

MODULE math_lib

USE tools

!  contains:

!     function betacf
!     function betai
!     subroutine bline
!     function bessi0
!     function bessi1
!     function bessk0
!     function bessk1
!     subroutine det5
!     subroutine errep
!     subroutine fit
!     subroutine lfit
!     subroutine covsrt
!     subroutine gaussj
!     subroutine funcp
!     function gammln
!     function gammp
!     function gammq
!     subroutine gcf
!     subroutine gser
!     subroutine hunt
!     function lint
!     function linte
!     function lnbchar
!     subroutine lubksb
!     subroutine ludcmp
!     subroutine midexp
!     subroutine midsql
!     subroutine midpnt
!     subroutine oderomb
!     subroutine odetrap_inv
!     subroutine odeint
!     subroutine polint
!     subroutine pzextr
!     function poly
!     subroutine qrmspl
!     function qromb
!     function eqromb
!     function qromo
!     subroutine drkck
!     subroutine rkqs
!     function ran2
!     subroutine stifbsh
!     subroutine simpr
!     subroutine sort
!     function sp21
!     function sp22
!     function spl
!     subroutine splie2
!     subroutine spline
!     function sply
!     subroutine trapzd
!     subroutine trapzv
!     subroutine zbrac
!     subroutine zbrac2
!     subroutine zbrak
!     subroutine zbrak2
!     function zbrent
!     function zbrent2
!     subroutine four1
!     subroutine minim
!     subroutine coord
!     subroutine fourier
!     subroutine fft
!     function ibitr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INTERFACE bessi0
   MODULE PROCEDURE bessi0_s
END INTERFACE

INTERFACE bessi1
   MODULE PROCEDURE bessi1_s
END INTERFACE

INTERFACE bessk0
   MODULE PROCEDURE bessk0_s
END INTERFACE

INTERFACE bessk1
   MODULE PROCEDURE bessk1_s
END INTERFACE

INTERFACE gammln
   MODULE PROCEDURE gammln_s,gammln_v
END INTERFACE

INTERFACE gammp
   MODULE PROCEDURE gammp_s,gammp_v
END INTERFACE

INTERFACE gammq
   MODULE PROCEDURE gammq_s,gammq_v
END INTERFACE

INTERFACE hunt
   MODULE PROCEDURE rhunt,dhunt
END INTERFACE

INTERFACE lint
   MODULE PROCEDURE lint,dlint,lint_s,dlint_s
END INTERFACE

INTERFACE midpnt
   MODULE PROCEDURE dmidpnt,dmidpnt_s
END INTERFACE

INTERFACE odeint
   MODULE PROCEDURE dodeint_s,dodeint_v,dodeint_jac
END INTERFACE

INTERFACE polint
   MODULE PROCEDURE rpolint,dpolint,dpolint_s
END INTERFACE

INTERFACE qromb
   MODULE PROCEDURE rqromb,dqromb,dqromb_s,dqromb_v
END INTERFACE

INTERFACE eqromb
   MODULE PROCEDURE reqromb,deqromb
END INTERFACE

INTERFACE qromo
   MODULE PROCEDURE dqromo,dqromo_v,dqromo_s
END INTERFACE

INTERFACE rkqs
   MODULE PROCEDURE drkqs,drkqs_v
END INTERFACE

INTERFACE sort
   MODULE PROCEDURE dsort,dsort2,isort
END INTERFACE

INTERFACE spl
   MODULE PROCEDURE rspl,rspl_s,dspl,dspl_s
END INTERFACE

INTERFACE spline
   MODULE PROCEDURE rspline,dspline,dspline_d
END INTERFACE

INTERFACE trapzd
   MODULE PROCEDURE rtrapzd,dtrapzd,dtrapzd_s
END INTERFACE

INTERFACE zbrak
   MODULE PROCEDURE rzbrak,dzbrak
END INTERFACE

INTERFACE zbrent
   MODULE PROCEDURE rzbrent,dzbrent
END INTERFACE

CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION betacf(a,b,x)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,b,x
INTEGER :: m,m2
REAL(KIND=8) :: betacf,aa,c,d,del,h,qab,qam,qap
INTEGER, PARAMETER :: maxit=100
REAL(KIND=8), PARAMETER :: EPS=epsilon(x)
REAL(KIND=8), PARAMETER :: FPMIN=tiny(x)/EPS

qab=a+b 
qap=a+1.D0
qam=a-1.D0
c=1.D0
d=1.D0-qab*x/qap
IF(ABS(d) < FPMIN)d=FPMIN
d=1.D0/d
h=d
DO m=1,maxit
   m2=2*m
   aa=m*(b-m)*x/((qam+m2)*(a+m2))
   d=1.D0+aa*d
   IF(ABS(d) < FPMIN)d=FPMIN
   c=1.D0+aa/c
   IF(ABS(c) < FPMIN)c=FPMIN
   d=1.D0/d
   h=h*d*c
   aa=-(a+m)*(qab+m)*x/((a+m2)*(qap+m2))
   d=1.D0+aa*d
   IF(ABS(d) < FPMIN)d=FPMIN
   c=1.D0+aa/c
   IF(ABS(c) < FPMIN)c=FPMIN
   d=1.D0/d
   del=d*c
   h=h*del
   IF(ABS(del-1.D0) <= EPS)EXIT
END DO
if (m > maxit)STOP' Problem in betacf: a or b too big, or maxit too small'
betacf=h
END FUNCTION betacf

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION betai(a,b,x)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,b,x
REAL(KIND=8) :: betai,bt

IF(x < 0.D0.OR.x > 1.D0)STOP' Problem in betai: bad argument x'
IF(x == 0.0.OR.x == 1.0)THEN
   bt=0.0
ELSE
   bt=EXP(gammln_s(a+b)-gammln_s(a)-gammln_s(b)+a*LOG(x)+b*log(1.D0-x))
END IF
IF(x < (a+1.D0)/(a+b+2.D0))THEN 
   betai=bt*betacf(a,b,x)/a
ELSE 
   betai=1.D0-bt*betacf(b,a,1.D0-x)/b
END IF
END FUNCTION betai

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE bline(n)

!     Fills in the screen with n blank lines.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, INTENT(IN) :: n
INTEGER :: i

DO i=1,n
   PRINT*,' '
END DO
END SUBROUTINE bline

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION bessi0(x)

!     Adapted from Numerical Recipes. For abs(x)<3.75 returns the
!     modified Bessel function, I_0(x), otherwise it returns the product
!     sqr(x)*exp(-x)*I_0(x) with x=abs(x).

!     CAUTION: this function can only be used when computing the
!              product I_0(x)*K_0(x)

!     Uses: poly

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION bessi0_s(x)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: bessi0_s,ax
REAL(KIND=8), DIMENSION(7) :: p = (/1.d0,3.5156229D0,3.0899424D0,1.2067492D0,0.2659732D0,0.360768D-1,0.45813D-2/) 
REAL(KIND=8), DIMENSION(9) :: q = (/0.39894228D0,0.1328592D-1,0.225319D-2,-0.157565D-2,0.916281D-2,-0.2057706D-1,0.2635537D-1,-0.1647633D-1,0.392377D-2/)

ax=ABS(x)
IF(ax < 3.75D0)THEN
   bessi0_s=poly((x/3.75D0)**2,p)
ELSE
   bessi0_s=(EXP(ax)/SQRT(ax))*poly(3.75d0/ax,q)
END IF
END FUNCTION bessi0_s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION bessi1(x)

!     Adapted from Numerical Recipes. For abs(x)<3.75 returns the
!     modified Bessel function, I_1(x), otherwise it returns the product
!     sqr(x)*exp(-x)*I_1(x) with x=abs(x).

!     CAUTION: this function can only be used when computing the
!              product I_1(x)*K_1(x)

!     Uses: poly

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION bessi1_s(x)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: bessi1_s,ax
REAL(KIND=8), DIMENSION(7) :: p = (/0.5D0,0.87890594D0,0.51498869D0,0.15084934D0,0.2658733D-1,0.301532D-2,0.32411D-3/)
REAL(KIND=8), DIMENSION(9) :: q = (/0.39894228D0,-0.3988024D-1,-0.362018D-2,0.163801D-2,-0.1031555D-1,0.2282967D-1,-0.2895312D-1,0.1787654D-1,-0.420059D-2/)

ax=ABS(x)
IF(ax < 3.75)THEN
   bessi1_s=ax*poly((x/3.75D0)**2,p)
ELSE
   bessi1_s=(EXP(ax)/SQRT(ax))*poly(3.75D0/ax,q)
END IF
IF(x < 0.0)bessi1_s=-bessi1_s
END FUNCTION bessi1_s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION bessk0(x)

!     Adapted from Numerical Recipes. For 0<x<3.75 returns the
!     modified Bessel function, K_0(x), otherwise it returns the product
!     exp(x)*K_0(x)/sqr(x).

!     CAUTION: this function can only be used when computing the
!              product I_0(x)*K_0(x)

!      Uses: poly

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION bessk0_s(x)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: bessk0_s,y 
REAL(KIND=8), DIMENSION(7) :: p = (/-0.57721566D0,0.42278420D0,0.23069756D0,0.3488590D-1,0.262698D-2,0.10750D-3,0.74D-5/)
REAL(KIND=8), DIMENSION(7) :: q = (/1.25331414D0,-0.7832358D-1,0.2189568D-1,-0.1062446D-1,0.587872D-2,-0.251540D-2,0.53208D-3/)

IF(x < 0.D0)STOP' Problem in bessk0_s: bad argument x'
IF(x <= 2.D0)THEN 
   y=x*x/4.D0
   bessk0_s=(-LOG(x/2.D0)*bessi0_s(x))+poly(y,p)
ELSE
   y=(2.D0/x)
   bessk0_s=(EXP(-x)/SQRT(x))*poly(y,q)
END IF
END FUNCTION bessk0_s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION bessk1(x)

!     Adapted from Numerical Recipes. For 0<x<3.75 returns the
!     modified Bessel function, K_1(x), otherwise it returns the product
!     exp(x)*K_1(x)/sqr(x).

!     CAUTION: this function can only be used when computing the
!              product I_1(x)*K_1(x)

!     Uses: poly

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION bessk1_s(x)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: bessk1_s,y 
REAL(KIND=8), DIMENSION(7) :: p = (/1.0D0,0.15443144D0,-0.67278579D0,-0.18156897D0,-0.1919402D-1,-0.110404D-2,-0.4686D-4/)
REAL(KIND=8), DIMENSION(7) :: q = (/1.25331414D0,0.23498619D0,-0.3655620D-1,0.1504268D-1,-0.780353D-2,0.325614D-2,-0.68245D-3/)

IF(x < 0.D0)STOP' Problem in bessk1_s: bad argument x'
IF(x <= 2.D0)then
   y=x*x/4.D0
   bessk1_s=(LOG(x/2.D0)*bessi1_s(x))+(1.D0/x)*poly(y,p)
ELSE
   y=2.D0/x
   bessk1_s=(EXP(-x)/SQRT(x))*poly(y,q)
END IF
END FUNCTION bessk1_s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE det5(h,y,z)

!     Calculates the derivative z of function y sampled at ndim points evenly separated by a constant stepsize h.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE det5(h,y,z)
IMPLICIT NONE
INTEGER :: ndim,i
REAL(KIND=8) :: h,hh,yy,a,b,c
REAL(KIND=8), DIMENSION(:) :: y,z

ndim=SIZE(y)
!--derivation loop
hh=.08333333333333333D0/h
yy=y(ndim-4)
b=hh*(-25.d0*y(1)+48.d0*y(2)-36.d0*y(3)+16.d0*y(4)-3.d0*y(5))
c=hh*(-3.d0*y(1)-10.d0*y(2)+18.d0*y(3)-6.d0*y(4)+y(5))
DO i=5,ndim
   a=b
   b=c
   c=hh*(y(i-4)-y(i)+8.d0*(y(i-1)-y(i-3)))
   z(i-4)=a
END DO
a=hh*(-yy+6.d0*y(ndim-3)-18.d0*y(ndim-2)+10.d0*y(ndim-1)+3.d0*y(ndim))
z(ndim)=hh*(3.d0*yy-16.d0*y(ndim-3)+36.d0*y(ndim-2)-48.d0*y(ndim-1)+25.d0*y(ndim))
z(ndim-1)=a
z(ndim-2)=c
z(ndim-3)=b
END SUBROUTINE det5

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE errep(message)

!     Stops the process and writes the message "message".

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
CHARACTER (LEN=75) :: message

PRINT*
PRINT*,'Abnormal ending: ',message
PRINT*
STOP
END SUBROUTINE errep

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE fit(x,y,ndata,sig,mwt,a,b,siga,sigb,chi2,q)

!     Fit a set of ndata points x, y to a straight line Y=A+BX
!     Adapted from Numerical Recipes

!     Uses: gammq

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER :: mwt,i,ndata
REAL(KIND=8) :: a,b,siga,sigb,chi2,q,sigdat,ss,st2,sx,sxoss,sy,t,wt
REAL(KIND=8), DIMENSION(:) :: x,y,sig

sx=0.d0
sy=0.d0
st2=0.d0
b=0.d0
IF(mwt /= 0)THEN
   ss=0.d0
   DO i=1,ndata
      wt=1.d0/(sig(i)**2)
      ss=ss+wt
      sx=sx+x(i)*wt
      sy=sy+y(i)*wt
   END DO
ELSE
   DO i=1,ndata
      sx=sx+x(i)
      sy=sy+y(i)
   END DO
   ss=DBLE(ndata)
END IF
sxoss=sx/ss
IF(mwt /= 0)THEN
   DO i=1,ndata
      t=(x(i)-sxoss)/sig(i)
      st2=st2+t*t
      b=b+t*y(i)/sig(i)
   END DO
ELSE
   DO i=1,ndata
      t=x(i)-sxoss
      st2=st2+t*t
      b=b+t*y(i)
   END DO
END IF
b=b/st2
a=(sy-sx*b)/ss
siga=SQRT((1.d0+sx*sx/(ss*st2))/ss)
sigb=SQRT(1.d0/st2)
chi2=0.d0
IF(mwt == 0)THEN
   DO i=1,ndata
      chi2=chi2+(y(i)-a-b*x(i))**2
   END DO
   q=1.d0
   sigdat=SQRT(chi2/(ndata-2))
   siga=siga*sigdat
   sigb=sigb*sigdat
ELSE
   DO i=1,ndata
      chi2=chi2+((y(i)-a-b*x(i))/sig(i))**2
   END DO
   q=gammq_s(0.5D0*(ndata-2),0.5D0*chi2)
END IF
RETURN
END SUBROUTINE fit

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE lfit(x,y,sig,ndat,a,ia,ma,covar,npc,chisq,funcs)

!     Adapted from Numerical Recipes. General linear fitting
!     Uses: covsrt,gaussj

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

iMPLICIT NONE
INTEGER :: ma,npc,ndat,i,j,k,l,m,mfit
INTEGER, PARAMETER ::  mmax=50
INTEGER, DIMENSION(ma) :: ia
REAL(KIND=8) :: chisq,sig2i,sum,wt,ym
REAL(KIND=8) :: a(ma),covar(npc,npc)
REAL(KIND=8),DIMENSION(ndat) :: sig,x,y
REAL(KIND=8),DIMENSION(mmax) ::  afunc,beta

INTERFACE
   SUBROUTINE funcs(x,fx,ma)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(ma), INTENT(OUT) :: fx
     INTEGER, INTENT(OUT) :: ma
   END SUBROUTINE funcs
END INTERFACE

mfit=0
DO j=1,ma
   if(ia(j) /= 0) mfit=mfit+1
END DO
if(mfit == 0) PAUSE 'lfit: no parameters to be fitted'
DO j=1,mfit
   DO k=1,mfit
      covar(j,k)=0.d0
   END DO
   beta(j)=0.d0
END DO
DO i=1,ndat
   CALL funcs(x(i),afunc,ma)
   ym=y(i)
   IF(mfit < ma)THEN
      DO j=1,ma
         IF(ia(j) == 0) ym=ym-a(j)*afunc(j)
      END DO
   END IF
   sig2i=1.d0/sig(i)**2
   j=0
   DO l=1,ma
      IF(ia(l)  /= 0)THEN
         j=j+1
         wt=afunc(l)*sig2i
         k=0
         DO m=1,l
            IF(ia(m) /= 0)THEN
               k=k+1
               covar(j,k)=covar(j,k)+wt*afunc(m)
            END IF
         END DO
         beta(j)=beta(j)+ym*wt
      END IF
   END DO
END DO
DO j=2,mfit
   DO k=1,j-1
      covar(k,j)=covar(j,k)
   END DO
END DO
CALL gaussj(covar,mfit,npc,beta,1,1)
j=0
DO l=1,ma
   IF(ia(l) /= 0)THEN
      j=j+1
      a(l)=beta(j)
   END IF
END DO
chisq=0.d0
DO i=1,ndat
   call funcs(x(i),afunc,ma)
   sum=0.d0
   DO j=1,ma
      sum=sum+a(j)*afunc(j)
   END DO
   chisq=chisq+((y(i)-sum)/sig(i))**2
END DO
call covsrt(covar,npc,ma,ia,mfit)
END SUBROUTINE lfit

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE covsrt(covar,npc,ma,ia,mfit)

!     Adapted from Numerical Recipes. 

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER :: i,j,k,ma,mfit,npc,ia(ma)
REAL(KIND=8) :: swap
REAL(KIND=8) :: covar(npc,npc)

DO i=mfit+1,ma
   DO j=1,i
      covar(i,j)=0.d0
      covar(j,i)=0.d0
   END DO
END DO
k=mfit
DO j=ma,1,-1
   IF(ia(j) /= 0)THEN
      DO i=1,ma
         swap=covar(i,k)
         covar(i,k)=covar(i,j)
         covar(i,j)=swap
      END DO
      DO i=1,ma
         swap=covar(k,i)
         covar(k,i)=covar(j,i)
         covar(j,i)=swap
      END DO
      k=k-1
   END IF
END DO   
END SUBROUTINE covsrt

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE gaussj(a,n,np,b,m,mp)

!     Adapted from numerical recipes. 

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
INTEGER :: m,mp,n,np,i,icol,irow,j,k,l,ll
INTEGER, PARAMETER :: nmax=50
INTEGER, DIMENSION(nmax):: indxc,indxr,ipiv
REAL(KIND=8) :: big,dum,pivinv
REAL(KIND=8) :: a(np,np),b(np,mp)

DO j=1,n
   ipiv(j)=0
END DO
DO i=1,n
   big=0.d0
   DO j=1,n
      IF(ipiv(j) /= 1)THEN
         DO k=1,n
            IF(ipiv(k) == 0)THEN
               IF(ABS(a(j,k)) >= big)THEN
                  big=ABS(a(j,k))
                  irow=j
                  icol=k
               END IF
            ELSE IF(ipiv(k).gt.1)THEN
               PAUSE 'singular matrix in gaussj'
            END IF
         END DO
      END IF
   END DO
   ipiv(icol)=ipiv(icol)+1
   IF(irow.ne.icol)THEN
      DO l=1,n
         dum=a(irow,l)
         a(irow,l)=a(icol,l)
         a(icol,l)=dum
      END DO
      DO l=1,m
         dum=b(irow,l)
         b(irow,l)=b(icol,l)
         b(icol,l)=dum
      END DO
   END IF
   indxr(i)=irow
   indxc(i)=icol
   if (a(icol,icol).eq.0.d0) PAUSE 'singular matrix in gaussj'
   pivinv=1./a(icol,icol)
   a(icol,icol)=1.
   DO l=1,n
      a(icol,l)=a(icol,l)*pivinv
   END DO
   DO l=1,m
      b(icol,l)=b(icol,l)*pivinv
   END DO
   DO ll=1,n
      IF(ll /= icol)THEN
         dum=a(ll,icol)
         a(ll,icol)=0.d0
         DO l=1,n
            a(ll,l)=a(ll,l)-a(icol,l)*dum
         END DO
         DO l=1,m
            b(ll,l)=b(ll,l)-b(icol,l)*dum
         END DO
      END IF
   END DO
END DO
DO l=n,1,-1
   IF(indxr(l) /= indxc(l))THEN
      DO k=1,n
         dum=a(k,indxr(l))
         a(k,indxr(l))=a(k,indxc(l))
         a(k,indxc(l))=dum
      END DO
   END IF
END DO
END SUBROUTINE gaussj

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
!FUNCTION erf(x)

!  Computes the error function (adapted from Numerical Recipes)
!
!     Uses: gammp

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

!IMPLICIT NONE
!REAL(KIND=8), INTENT(IN) :: x
!REAL(KIND=8) :: erf
 
!IF(x.lt.0.d0)THEN
!   erf=-gammp(5.d-1,x**2)
!ELSE
!   erf=gammp(5.d-1,x**2)
!END IF
!END FUNCTION erf

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION gammln(xx)

!     Adapted from Numerical Recipes (in some Fortran compilers this function is implemented as an intrinsic procedure).

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION gammln_s(xx)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: xx
REAL(KIND=8) :: gammln_s
REAL(KIND=8) :: tmp,x
REAL(KIND=8) :: stp = 2.5066282746310005d0
REAL(KIND=8), DIMENSION(6) :: coef = (/76.18009172947146d0,-86.50532032941677d0,24.01409824083091d0,-1.231739572450155d0,0.1208650973866179d-2,-0.5395239384953d-5/)

IF(xx <= 0.d0)STOP' Problem in gammln_s: bad argument xx'
x=xx
tmp=x+5.5d0
tmp=(x+0.5d0)*LOG(tmp)-tmp
gammln_s=tmp+LOG(stp*(1.000000000190015d0+SUM(coef(:)/arth(x+1.d0,1.d0,SIZE(coef))))/x)
END FUNCTION gammln_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION gammln_v(xx)
IMPLICIT NONE
INTEGER :: i
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xx
REAL(KIND=8), DIMENSION(size(xx)) :: gammln_v
REAL(KIND=8), DIMENSION(size(xx)) :: ser,tmp,x,y
REAL(KIND=8) :: stp = 2.5066282746310005d0
REAL(KIND=8), DIMENSION(6) :: coef = (/76.18009172947146d0,-86.50532032941677d0,24.01409824083091d0,-1.231739572450155d0,0.1208650973866179d-2,-0.5395239384953d-5/)

IF(SIZE(xx) == 0)RETURN
IF(ALL(xx > 0.0))STOP' Problem in gammln_v: bad argument xx'
x=xx
tmp=x+5.5d0
tmp=(x+0.5d0)*LOG(tmp)-tmp
ser=1.000000000190015d0
y=x
DO i=1,SIZE(coef)
   y=y+1.d0
   ser=ser+coef(i)/y
END DO
gammln_v=tmp+LOG(stp*ser/x)
END FUNCTION gammln_v

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION gammp_s(a,x)

!     Returns the incomplete gamma function P(a,x). Adapted from Numerical Recipes

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION gammp_s(a,x)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,x
REAL(KIND=8) :: gammp_s

IF( x < 0.d0 .AND. a <= 0.d0)STOP' Problem in gammp_s: bad argument a or x'
IF(x<a+1.d0)THEN
   gammp_s=gser_s(a,x)
ELSE
   gammp_s=1.d0-gcf_s(a,x)
END IF
END FUNCTION gammp_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION gammp_v(a,x)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a,x
REAL(KIND=8), DIMENSION(SIZE(x)) :: gammp_v
LOGICAL, DIMENSION(SIZE(x)) :: mask

IF(ANY(x < 0.d0).OR.ANY(a <= 0.d0))STOP' Problem in gammp_v: bad argument a or x'
mask = (x<a+1.d0)
gammp_v=MERGE(gser_v(a,MERGE(x,0.d0,mask)),1.d0-gcf_v(a,MERGE(x,0.d0,.NOT. mask)),mask)
END FUNCTION gammp_v

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION gammq(a,x)

!     Returns the incomplete gamma function Q(a,x). Adapted from Numerical Recipes

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION gammq_s(a,x)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,x
REAL(KIND=8) :: gammq_s

IF( x < 0.d0 .OR. a < 0.d0)STOP' Problem in gammq_s: bad argument a or x'
IF(x<a+1.d0)THEN
   gammq_s=1.d0-gser_s(a,x)
ELSE
   gammq_s=gcf_s(a,x)
END IF
END FUNCTION gammq_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION gammq_v(a,x)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a,x
REAL(KIND=8), DIMENSION(size(a)) :: gammq_v
LOGICAL, DIMENSION(size(x)) :: mask

IF(ANY(x >= 0.d0).OR.ANY(a > 0.d0))STOP' Problem in gammq_v: bad argument a or x'
mask=(x<a+1.d0)
gammq_v=MERGE(1.d0-gser_v(a,MERGE(x,0.d0,mask)),gcf_v(a,MERGE(x,0.d0,.NOT.mask)),mask)
END FUNCTION gammq_v

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION gcf(a,x,gln)

!     Returns the incomplete gamma function Q(a,x) evaluated by its continued fraction representation as gammcf.
!     Also returns ln of Gamma(a) as gln (adapted from Numerical Recipes)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION gcf_s(a,x,gln)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,x
REAL(KIND=8), OPTIONAL, INTENT(OUT) :: gln
INTEGER :: i
REAL(KIND=8) :: gcf_s,an,b,c,d,del,h
INTEGER, PARAMETER :: ITMAX=1000
REAL(KIND=8), PARAMETER :: EPS=epsilon(x)
REAL(KIND=8), PARAMETER :: FPMIN=tiny(x)/EPS

IF(x == 0.d0)THEN
   gcf_s=1.d0
   RETURN
END IF
b=x+1.d0-a
c=1.d0/FPMIN
d=1.d0/b
h=d
DO i=1,ITMAX
   an=-i*(i-a)
   b=b+2.d0
   d=an*d+b
   IF(ABS(d) < FPMIN)d=FPMIN
   c=b+an/c
   IF(ABS(c) < FPMIN)c=FPMIN
   d=1.d0/d
   del=d*c
   h=h*del
   IF(ABS(del-1.d0) <= EPS)EXIT
END DO
IF(i > ITMAX)STOP' Problem in gcf_s a too large, ITMAX too small'
IF(PRESENT(gln))THEN
   gln=gammln_s(a)
   gcf_s=EXP(-x+a*LOG(x)-gln)*h
ELSE
   gcf_s=EXP(-x+a*log(x)-gammln_s(a))*h
END IF
END FUNCTION gcf_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION gcf_v(a,x,gln)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a,x
REAL(KIND=8), DIMENSION(:), OPTIONAL, INTENT(OUT) :: gln
REAL(KIND=8), DIMENSION(SIZE(a)) :: gcf_v
INTEGER, PARAMETER :: ITMAX=100
REAL(KIND=8), PARAMETER :: EPS=epsilon(x)
REAL(KIND=8), PARAMETER :: FPMIN=tiny(x)/EPS
INTEGER :: i
REAL(KIND=8), DIMENSION(SIZE(a)) :: an,b,c,d,del,h
LOGICAL, DIMENSION(SIZE(a)) :: converged,zero

i=SIZE(a)
zero=(x == 0.d0)
WHERE(zero)
   gcf_v=1.d0
ELSEWHERE
   b=x+1.d0-a
   c=1.d0/FPMIN
   d=1.d0/b
   h=d
END WHERE
converged=zero
DO i=1,ITMAX
   WHERE(.NOT.converged)
      an=-i*(i-a)
      b=b+2.d0
      d=an*d+b
      d=MERGE(FPMIN,d,ABS(d)<FPMIN)
      c=b+an/c
      c=MERGE(FPMIN,c,ABS(c)<FPMIN)
      d=1.d0/d
      del=d*c
      h=h*del
      converged=(ABS(del-1.d0)<=EPS)
   END WHERE
   IF(ALL(converged))EXIT
END DO
IF(i > ITMAX)STOP' Problem in gcf_v: a too large, ITMAX too small'
IF(PRESENT(gln))THEN
   IF(SIZE(gln) < SIZE(a))STOP' Problem in gcf_v: not enough space for gln'
   gln=gammln_v(a)
   WHERE(.NOT.zero)gcf_v=EXP(-x+a*LOG(x)-gln)*h
ELSE
   WHERE(.NOT.zero)gcf_v=EXP(-x+a*log(x)-gammln_v(a))*h
END IF
END FUNCTION gcf_v

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION gser(a,x,gln)

!     Returns the incomplete gamma function P(a,x) evaluated by its series representation as gamser. Also returns ln Gamma(a) as gln. Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION gser_s(a,x,gln)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,x
REAL(KIND=8), OPTIONAL, INTENT(OUT) :: gln
REAL(KIND=8) :: gser_s
INTEGER, PARAMETER :: ITMAX=100
REAL, PARAMETER :: EPS=epsilon(x)
INTEGER :: n
REAL(KIND=8) :: ap,del,summ

IF(x == 0.d0)THEN
   gser_s=0.d0
   RETURN
END IF
ap=a
summ=1.d0/a
del=summ
DO n=1,ITMAX
   ap=ap+1.d0
   del=del*x/ap
   summ=summ+del
   IF(ABS(del) < ABS(summ)*EPS)EXIT
END DO
IF(n > ITMAX)STOP' Problem in gser_s: a too large, ITMAX too small'
IF(PRESENT(gln))THEN
   gln=gammln_s(a)
   gser_s=summ*EXP(-x+a*LOG(x)-gln)
ELSE
   gser_s=summ*EXP(-x+a*LOG(x)-gammln_s(a))
END IF
END FUNCTION gser_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION gser_v(a,x,gln)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a,x
REAL(KIND=8), DIMENSION(:), OPTIONAL, INTENT(OUT) :: gln
REAL(KIND=8), DIMENSION(SIZE(a)) :: gser_v
INTEGER, PARAMETER :: ITMAX=100
REAL, PARAMETER :: EPS=epsilon(x)
INTEGER :: n
REAL(KIND=8), DIMENSION(SIZE(a)) :: ap,del,summ
LOGICAL, DIMENSION(SIZE(a)) :: converged,zero

n=SIZE(a)
zero=(x == 0.d0)
WHERE (zero) gser_v=0.d0
ap=a
summ=1.d0/a
del=summ
converged=zero
DO n=1,ITMAX
WHERE (.NOT. converged)
   ap=ap+1.d0
   del=del*x/ap
   summ=summ+del
   converged=(ABS(del) < ABS(summ)*EPS)
END WHERE
IF(ALL(converged)) EXIT
END DO
IF(n > ITMAX)STOP' Problem in gser_v: a too large, ITMAX too small'
IF(PRESENT(gln)) THEN
   IF (SIZE(gln) < SIZE(a))STOP' Problem in gser_v: not enough space for gln'
   gln=gammln_v(a)
   WHERE(.NOT.zero)gser_v=summ*EXP(-x+a*log(x)-gln)
ELSE
   WHERE(.NOT.zero)gser_v=summ*EXP(-x+a*LOG(x)-gammln_v(a))
END IF
END FUNCTION gser_v

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE hunt(xx,x,jlo)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE rhunt(xx,x,jlo)
IMPLICIT NONE
INTEGER, INTENT(INOUT) :: jlo
REAL, INTENT(IN) :: x
REAL, DIMENSION(:), INTENT(IN) :: xx
INTEGER :: n,inc,jhi,jm
LOGICAL :: ascnd

n=SIZE(xx)
ascnd = (xx(n) >= xx(1))
inc=1
IF(x >= xx(jlo) .EQV. ascnd)THEN
   DO
      jhi=jlo+inc
      IF(jhi > n)THEN
         jhi=n+1
         EXIT
      ELSE
         IF(x < xx(jhi).EQV.ascnd)EXIT
         jlo=jhi
         inc=inc+inc
      END IF
   END DO
ELSE
   jhi=jlo
   DO
      jlo=jhi-inc
      IF(jlo < 1)THEN
         jlo=0
         EXIT
      ELSE
         IF(x >= xx(jlo).EQV.ascnd)EXIT
         jhi=jlo
         inc=inc+inc
      END IF
   END DO
END IF
DO
   IF(jhi-jlo <= 1)THEN
      IF(ABS(x-xx(1)) < ABS(rzero*xx(1)))THEN
         jlo=1
         EXIT
      ELSEIF(ABS(x-xx(n)) < ABS(rzero*xx(n)))THEN
         jlo=n-1
         EXIT
      END IF
      EXIT
   ELSE
      jm=(jhi+jlo)/2
      IF(x >= xx(jm).EQV.ascnd)THEN
         jlo=jm
      ELSE
         jhi=jm
      END IF
   END IF
END DO
IF(jlo == 0.OR.jlo == n)THEN
   PRINT*,xx(1),xx(n),x,n
   STOP' Problem in rhunt: x out of range'
END IF
END SUBROUTINE rhunt

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dhunt(xx,x,jlo)
IMPLICIT NONE
INTEGER, INTENT(INOUT) :: jlo
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xx
INTEGER :: n,inc,jhi,jm
LOGICAL :: ascnd

n=SIZE(xx)
jlo=MIN(jlo,n-1)
ascnd = (xx(n) >= xx(1))
inc=1
IF(x >= xx(jlo) .EQV. ascnd)THEN
   DO
      jhi=jlo+inc
      IF(jhi > n)THEN
         jhi=n+1
         EXIT
      ELSE
         IF (x < xx(jhi) .EQV. ascnd)EXIT
         jlo=jhi
         inc=inc+inc
      END IF
   END DO
ELSE
   jhi=jlo
   DO
      jlo=jhi-inc
      IF(jlo < 1)THEN
         jlo=0
         EXIT
      ELSE
         IF(x >= xx(jlo) .EQV. ascnd)EXIT
         jhi=jlo
         inc=inc+inc
      END IF
   END DO
END IF
DO
   IF(jhi-jlo <= 1)THEN
      IF(ABS(x-xx(1)) < ABS(zero*xx(1)))THEN
         jlo=1
         EXIT
      ELSEIF(ABS(x-xx(n)) < ABS(zero*xx(n)))THEN
         jlo=n-1
         EXIT
      END IF
      EXIT
   ELSE
      jm=(jhi+jlo)/2
      IF(x >= xx(jm) .EQV. ascnd)THEN
         jlo=jm
      ELSE
         jhi=jm
      END IF
   END IF
END DO
IF(jlo == 0.OR.jlo == n)THEN
   IF(n > 1)THEN
      PRINT*,xx(1),xx(n),x,n
      print*,1/0.d0
      STOP'  Problem in dhunt: x out of range'
   END IF
END IF
END SUBROUTINE dhunt

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION lint(xa,ya,x)

!     Linear interpolation.

!     Uses: hunt

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION lint(xa,ya,x)
IMPLICIT NONE
REAL, DIMENSION(:), INTENT(IN) :: xa,ya
REAL, DIMENSION(:), INTENT(IN) :: x
REAL, DIMENSION(SIZE(x)) :: lint
INTEGER :: n,klo,khi,k

n=SIZE(xa)
klo=0
DO k=1,SIZE(x)
   IF(ABS(xa(1)-x(k)) < ABS(rzero*xa(1)))THEN
      lint(k)=ya(1)
      CYCLE       
   ELSE IF(ABS(xa(n)-x(k)) < ABS(rzero*xa(n)))THEN
      lint(k)=ya(n)
      CYCLE
   ELSE
      IF(klo == 0)THEN
         klo=1
         CALL hunt(xa,x(k),klo)
         khi=klo+1
      ELSE
         DO WHILE(khi < n .AND. xa(khi) <= x(k))
            klo=khi
            khi=klo+1
         END DO
      END IF
      lint(k)=(ya(khi)-ya(klo))*(x(k)-xa(klo))/(xa(khi)-xa(klo))+ya(klo)
   END IF
END DO
END FUNCTION lint

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dlint(xa,ya,x)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xa,ya
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: dlint
INTEGER :: n,klo,khi,k
LOGICAL :: ascna,ascnx

n=SIZE(xa)
klo=0
DO k=1,SIZE(x)
   IF(ABS(xa(1)-x(k)) < ABS(zero*xa(1)))THEN
      dlint(k)=ya(1)
      CYCLE       
   ELSE IF(ABS(xa(n)-x(k)) < ABS(zero*xa(n)))THEN
      dlint(k)=ya(n)
      CYCLE
   ELSE
      ascna=(xa(1) >= xa(n))             ! increasing xa
      ascnx=(x(1) >= x(SIZE(x)))         ! increasing x
      IF(klo == 0)THEN
         klo=1
         CALL hunt(xa,x(k),klo)
         khi=klo+1
      ELSE
         IF(ascna.EQV.ascnx)THEN
            DO WHILE(khi < n .AND. xa(khi) <= x(k))
               klo=khi
               khi=klo+1
            END DO
         ELSE
            DO WHILE(klo > 1 .AND. xa(klo) >= x(k))
               khi=klo
               klo=khi-1
            END DO
         END IF
      END IF
      dlint(k)=(ya(khi)-ya(klo))*(x(k)-xa(klo))/(xa(khi)-xa(klo))+ya(klo)
   END IF
END DO
END FUNCTION dlint

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION lint_s(xa,ya,x,klo)
IMPLICIT NONE
REAL, DIMENSION(:), INTENT(IN) :: xa,ya
REAL, INTENT(IN) :: x
INTEGER, INTENT(INOUT) :: klo
REAL :: lint_s
INTEGER :: n,khi

n=SIZE(xa)
klo=MIN(klo,n-1)
CALL hunt(xa,x,klo)
khi=klo+1
lint_s=(ya(khi)-ya(klo))*(x-xa(klo))/(xa(khi)-xa(klo))+ya(klo)
END FUNCTION lint_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dlint_s(xa,ya,x,klo)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xa,ya
REAL(KIND=8), INTENT(IN) :: x
INTEGER, INTENT(INOUT) :: klo
REAL(KIND=8) :: dlint_s
INTEGER :: n,khi

n=SIZE(xa)
CALL hunt(xa,x,klo)
khi=klo+1
dlint_s=(ya(khi)-ya(klo))*(x-xa(klo))/(xa(khi)-xa(klo))+ya(klo)
END FUNCTION dlint_s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION linte(xa,ya,x,klo)

!     Linear interpolation/extrapolation

!     Uses: hunt

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: xa,ya
REAL(KIND=8), INTENT(IN) :: x
INTEGER, INTENT(INOUT) :: klo
INTEGER :: n,n1,khi
REAL(KIND=8) :: linte

n=SIZE(xa)
IF(ABS(xa(1)-x) < ABS(zero*xa(1)))THEN
   linte=ya(1)
   klo=1
ELSE IF(ABS(xa(n)-x) < ABS(zero*xa(n)))THEN
   linte=ya(n)
   klo=n-1
ELSE IF(x < xa(1))THEN
   linte=(ya(2)-ya(1))*(x-xa(1))/(xa(2)-xa(1))+ya(1)
ELSE IF(x > xa(n))THEN
   n1=n-1
   linte=(ya(n)-ya(n1))*(x-xa(n))/(xa(n)-xa(n1))+ya(n)
ELSE
   CALL hunt(xa,x,klo)
   khi=klo+1
   linte=(ya(khi)-ya(klo))*(x-xa(klo))/(xa(khi)-xa(klo))+ya(klo)
END IF
END FUNCTION linte

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION lnbchar(string)

!     Finds the length of the non-blank portion of a character string.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER :: j,i,lnbchar
CHARACTER (LEN=1) :: string*(*)

j=LEN(string)
DO i=1,j
  IF(string(i:i) == ' ')THEN
    lnbchar=i-1
    RETURN
  END IF
END DO
lnbchar=j
RETURN
END FUNCTION lnbchar

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE ludcmp(a,indx,d)

!       Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:,:), INTENT(INOUT) :: a
INTEGER, DIMENSION(:), INTENT(OUT) :: indx
REAL(KIND=8), INTENT(OUT) :: d
REAL(KIND=8), DIMENSION(size(a,1)) :: vv
REAL(KIND=8), PARAMETER :: TINY=1.0d-20
INTEGER :: j,n,imax

n=SIZE(a,1)
d=1.d0
vv=MAXVAL(ABS(a),dim=2)
IF(ANY(vv == 0.d0))STOP' Problem in ludcmp: singular matrix'
vv=1.d0/vv
DO j=1,n
   imax=(j-1)+imaxloc(vv(j:n)*ABS(a(j:n,j)))
   IF(j /= imax)THEN
      CALL swap(a(imax,:),a(j,:))
      d=-d 
      vv(imax)=vv(j)
   END IF
   indx(j)=imax
   IF(a(j,j) == 0.0)a(j,j)=TINY
   a(j+1:n,j)=a(j+1:n,j)/a(j,j)
   a(j+1:n,j+1:n)=a(j+1:n,j+1:n)-outerprod(a(j+1:n,j),a(j,j+1:n))
END DO
END SUBROUTINE ludcmp

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE lubksb(a,indx,b)

!       Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: a
INTEGER, DIMENSION(:), INTENT(IN) :: indx
REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: b
INTEGER :: i,n,ii,ll
REAL(KIND=8) :: summ

n=SIZE(a,1)
ii=0
DO i=1,n
   ll=indx(i)
   summ=b(ll)
   b(ll)=b(i)
   IF(ii /= 0)THEN
      summ=summ-DOT_PRODUCT(a(i,ii:i-1),b(ii:i-1))
   ELSE IF (summ /= 0.0)THEN
      ii=i
   END IF
   b(i)=summ
END DO
DO i=n,1,-1
   b(i) = (b(i)-DOT_PRODUCT(a(i,i+1:n),b(i+1:n)))/a(i,i)
END DO
END SUBROUTINE lubksb

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE midexp(funk,aa,bb,s,n)

!     Adapted from Numerical Recipes' midexp for a finite upper limit bb.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8)          :: aa
REAL(KIND=8)          :: bb
REAL(KIND=8)             :: s
INTEGER               :: n
INTEGER :: it,j
REAL(KIND=8) :: ddel,del,sum,tnm,x,a,b

INTERFACE
   FUNCTION funk(x)
     REAL(KIND=8) :: x,funk
   END FUNCTION funk
END INTERFACE

b=EXP(-aa)
a=EXP(-bb)
!-----the rest is exactly like subroutine midpnt
IF(n == 1)THEN
!  s=(b-a)*func(0.5D0*(a+b))
   s=(b-a)*funk(-dlog(0.5D0*(a+b)))/(0.5D0*(a+b))
ELSE
  it=3**(n-2)
  tnm=DBLE(it)
  del=(b-a)/(3.d0*tnm)
  ddel=del+del
  x=a+0.5D0*del
  sum=0.d0
  DO j=1,it
    sum=sum+funk(-DLOG(x))/x
    x=x+ddel
    sum=sum+funk(-DLOG(x))/x
    x=x+del
  END DO
  s=(s+(b-a)*sum/tnm)/3.d0
END IF
RETURN
END SUBROUTINE midexp

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE midsql(funk,aa,bb,s,n)

!     Based upon subroutine midpnt of Numerical Recipes. Deals with integrals with power law singularities at x=0 that diverge as much as 1/sqrt(x). 

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: aa,bb
REAL(KIND=8), INTENT(INOUT) :: s
INTEGER, INTENT(IN) :: n
REAL(KIND=8) :: a,b,del
INTEGER :: it
REAL(KIND=8), DIMENSION(2*3**(n-2)) :: x

INTERFACE
   FUNCTION funk(x)
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(SIZE(x)) :: funk
   END FUNCTION funk
END INTERFACE

b=SQRT(bb-aa)
a=0.d0
IF(n == 1)THEN
   s=(b-a)*SUM(func( (/0.5d0*(a+b)/) ))
ELSE
   it=3**(n-2)
   del=(b-a)/(3.d0*it)
   x(1:2*it-1:2)=arth(a+0.5d0*del,3.d0*del,it)
   x(2:2*it:2)=x(1:2*it-1:2)+2.d0*del
   s=s/3.d0+del*SUM(func(x))
END IF
CONTAINS
FUNCTION func(x)
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
REAL(KIND=8), DIMENSION(size(x)) :: func
func=2.d0*x*funk(aa+x**2)
END FUNCTION func
END SUBROUTINE midsql

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE midpnt(func,a,b,s,n)

!     Subroutine adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE dmidpnt(func,a,b,s,n)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,b
REAL(KIND=8), INTENT(INOUT) :: s
INTEGER, INTENT(IN) :: n
INTEGER :: it
REAL(KIND=8) :: del
REAL(KIND=8), DIMENSION(2*3**(n-2)) :: x

INTERFACE
   FUNCTION func(x)
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(SIZE(x)) :: func
   END FUNCTION func
END INTERFACE

IF(n == 1)THEN
   s=(b-a)*SUM(func( (/0.5d0*(a+b)/) ))
ELSE
   it=3**(n-2)
   del=(b-a)/(3.d0*it)
   x(1:2*it-1:2)=arth(a+0.5d0*del,3.d0*del,it)
   x(2:2*it:2)=x(1:2*it-1:2)+2.d0*del
   s=s/3.d0+del*SUM(func(x))
END IF
END SUBROUTINE dmidpnt

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dmidpnt_s(func,a,b,s,n)
IMPLICIT NONE
REAL(KIND=8) :: a,b
REAL(KIND=8) :: s
INTEGER :: n
INTEGER :: it,j
REAL(KIND=8) :: ddel,del,sum,tnm,x

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8) :: x,func
   END FUNCTION func
END INTERFACE

IF(n == 1)THEN
  s=(b-a)*func(0.5D0*(a+b))
ELSE
  it=3**(n-2)
  tnm=DBLE(it)
  del=(b-a)/(3.d0*tnm)
  ddel=del+del
  x=a+0.5D0*del
  sum=0.d0
  DO j=1,it
    sum=sum+func(x)
    x=x+ddel
    sum=sum+func(x)
    x=x+del
  END DO
  s=(s+(b-a)*sum/tnm)/3.d0
END IF
RETURN
END SUBROUTINE dmidpnt_s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE oderomb(yio,x1,x2,fdydx)

!     Solves an ordinary differential equation dy/dx=f(x) from x1 to x2 through Romberg integration.  

!     USES: qromb

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, PARAMETER :: npart=6
INTEGER :: i
REAL(KIND=8):: yio,x1,x2,xi1,xi2,stp,sromb,sum

INTERFACE 
   FUNCTION fdydx(x)
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(SIZE(x)) :: fdydx
   END FUNCTION fdydx
END INTERFACE

if(x1 == x2)RETURN
stp=(x2/x1)**(1.d0/DBLE(npart-1))
xi1=x1
sum=0.d0
DO i=1,npart-1
   xi2=xi1*stp
   sromb=qromb(fdydx,xi1,xi2)
   sum=sum+sromb
   xi1=xi1*stp
ENDDO
yio=yio+sum
END SUBROUTINE oderomb

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE odetrap_inv(yio,x1,x2,invdydx)

!     Solves an ordinary differential equation dy/dx=f(y) from x1 to x2 through trapezoidal integration.  

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER :: k,KK
INTEGER, PARAMETER :: KMAX=300
REAL(KIND=8), INTENT(INOUT) :: yio
REAL(KIND=8), INTENT(IN) :: x1,x2
REAL(KIND=8):: a,b,stp,sum,delx,hstp,ymax,stpy,y

INTERFACE 
   FUNCTION invdydx(x)
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8) :: invdydx
   END FUNCTION invdydx
END INTERFACE

if(x1 == x2)RETURN
a=invdydx(yio)
ymax=yio+1.d0
b=invdydx(ymax)
if(a == b)RETURN
delx=x2-x1
kk=1
stpy=4.D0*delx/KMAX/MIN(a,b)
hstp=.5d0*stpy
y=yio+stpy
b=invdydx(y)
sum=hstp*(a+b)
a=b
k=0
DO WHILE(sum < delx)
   k=k+1
   yio=y
   y=y+stpy
   IF(y > ymax)THEN
      yio=ymax
      RETURN
   END IF
   b=invdydx(y)
   sum=sum+hstp*(a+b)
   a=b
!   if(k == KMAX)stop'too many steps in odetrap_inv'
END DO
IF(k > 1000) PRINT*,'k> 1000',k
END SUBROUTINE odetrap_inv

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE odeint(ystart,x1,x2,eps,h1,hmin,derivs,dbsstep)

!     Subroutine adapted from Numerical Recipes.
!     Runge-Kutta driver with adaptative stepsize control to integrate
!     ordinary differential equations.

!     Uses: derivs,dbsstep

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE dodeint_s(ystart,x1,x2,eps,h1,hmin,derivs,dbsstep)
IMPLICIT NONE
REAL(KIND=8), INTENT(INOUT) :: ystart
REAL(KIND=8), INTENT(IN) :: x1,x2,eps,h1,hmin
INTEGER :: nstp
REAL(KIND=8) :: h,hdid,hnext,x
REAL(KIND=8) :: dydx,y,yscal
INTEGER, PARAMETER :: MAXSTP=10000
REAL(KIND=8), PARAMETER :: TINY=1.0d-30

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), INTENT(IN) :: y
     REAL(KIND=8), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
   SUBROUTINE dbsstep(y,dydx,x,htry,eps,yscal,hdid,hnext,derivs)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(INOUT) :: y
     REAL(KIND=8), INTENT(IN) :: dydx,yscal
     REAL(KIND=8), INTENT(INOUT) :: x
     REAL(KIND=8), INTENT(IN) :: htry,eps
     REAL(KIND=8), INTENT(OUT) :: hdid,hnext
     INTERFACE
        SUBROUTINE derivs(x,y,dydx)
          IMPLICIT NONE
          REAL(KIND=8), INTENT(IN) :: x
          REAL(KIND=8), INTENT(IN) :: y
          REAL(KIND=8), INTENT(OUT) :: dydx
        END SUBROUTINE derivs
     END INTERFACE
   END SUBROUTINE dbsstep
END INTERFACE

x=x1
h=SIGN(h1,x2-x1)
y=ystart
DO nstp=1,MAXSTP
   CALL derivs(x,y,dydx)
   yscal=ABS(y)+ABS(h*dydx)+TINY
   IF((x+h-x2)*(x+h-x1) > 0.d0)h=x2-x   
   CALL dbsstep(y,dydx,x,h,eps,yscal,hdid,hnext,derivs)
   IF((x-x2)*(x2-x1) >= 0.d0)THEN
      ystart=y
      RETURN
   END IF
   IF(ABS(hnext) < hmin)STOP' Problem in dodeint_s: stepsize smaller than minimum'
   h=hnext
END DO
STOP' Problem in dodeint_s: too many steps'
END SUBROUTINE dodeint_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dodeint_v(ystart,x1,x2,eps,h1,hmin,derivs,dbsstep)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: ystart
REAL(KIND=8), INTENT(IN) :: x1,x2,eps,h1,hmin
INTEGER :: nstp
REAL(KIND=8) :: h,hdid,hnext,x
REAL(KIND=8), DIMENSION(SIZE(ystart)) :: dydx,y,yscal,dfdx
REAL(kind=8), DIMENSION(SIZE(ystart),SIZE(ystart)):: dfdy
INTEGER, PARAMETER :: MAXSTP=10000
REAL(KIND=8), PARAMETER :: TINY=1.0d-30

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
   SUBROUTINE dbsstep(y,dydx,x,htry,eps,yscal,hdid,hnext,derivs)
     IMPLICIT NONE
     REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: dydx,yscal
     REAL(KIND=8), INTENT(INOUT) :: x
     REAL(KIND=8), INTENT(IN) :: htry,eps
     REAL(KIND=8), INTENT(OUT) :: hdid,hnext
     INTERFACE
        SUBROUTINE derivs(x,y,dydx)
          IMPLICIT NONE
          REAL(KIND=8), INTENT(IN) :: x
          REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
          REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dydx
        END SUBROUTINE derivs
     END INTERFACE
   END SUBROUTINE dbsstep
END INTERFACE

x=x1
h=SIGN(h1,x2-x1)
IF(x1 == x2)RETURN
y(:)=ystart(:)
DO nstp=1,MAXSTP
  CALL derivs(x,y,dydx)
  yscal(:)=ABS(y(:))+ABS(h*dydx(:))+TINY
  IF((x+h-x2)*(x+h-x1) > 0.d0)h=x2-x
  CALL dbsstep(y,dydx,x,h,eps,yscal,hdid,hnext,derivs)
  IF((x-x2)*(x2-x1) >= 0.d0)THEN
     ystart(:)=y(:)
     RETURN
  END IF
  IF(ABS(hnext) < hmin)STOP' Problem in dodeint_v: stepsize smaller than minimum'
  h=hnext
END DO
STOP' Problem in dodeint_v: too many steps'
END SUBROUTINE dodeint_v

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dodeint_jac(ystart,x1,x2,eps,h1,hmin,derivs,dbsstep,jacob)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: ystart
REAL(KIND=8), INTENT(IN) :: x1,x2,eps,h1,hmin
INTEGER :: nstp
REAL(KIND=8) :: h,hdid,hnext,x
REAL(KIND=8), DIMENSION(SIZE(ystart)) :: dydx,y,yscal,dfdx
REAL(kind=8), DIMENSION(SIZE(ystart),SIZE(ystart)):: dfdy
INTEGER, PARAMETER :: MAXSTP=10000
REAL(KIND=8), PARAMETER :: TINY=1.0d-30

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
   SUBROUTINE dbsstep(y,dydx,x,htry,eps,yscal,hdid,hnext,derivs,jacob)
     IMPLICIT NONE
     REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: dydx,yscal
     REAL(KIND=8), INTENT(INOUT) :: x
     REAL(KIND=8), INTENT(IN) :: htry,eps
     REAL(KIND=8), INTENT(OUT) :: hdid,hnext
     INTERFACE
        SUBROUTINE derivs(x,y,dydx)
          IMPLICIT NONE
          REAL(KIND=8), INTENT(IN) :: x
          REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
          REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dydx
        END SUBROUTINE derivs
        SUBROUTINE jacob(x,y,dfdx,dfdy)
          IMPLICIT NONE
          REAL(KIND=8), INTENT(IN) :: x
          REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
          REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dfdx
          REAL(KIND=8),DIMENSION(:,:), INTENT(OUT) :: dfdy
        END SUBROUTINE jacob
     END INTERFACE
   END SUBROUTINE dbsstep
   SUBROUTINE jacob(x,y,dfdx,dfdy)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dfdx
     REAL(KIND=8),DIMENSION(:,:), INTENT(OUT) :: dfdy
   END SUBROUTINE jacob
END INTERFACE

x=x1
h=SIGN(h1,x2-x1)
IF(x1 == x2)RETURN
y(:)=ystart(:)
DO nstp=1,MAXSTP
  CALL derivs(x,y,dydx)
  yscal(:)=ABS(y(:))+ABS(h*dydx(:))+TINY
  IF((x+h-x2)*(x+h-x1) > 0.d0)h=x2-x
  CALL dbsstep(y,dydx,x,h,eps,yscal,hdid,hnext,derivs,jacob)
  IF((x-x2)*(x2-x1) >= 0.d0)THEN
     ystart(:)=y(:)
     RETURN
  END IF
  IF(ABS(hnext) < hmin)STOP' Problem in dodeint_jac: stepsize smaller than minimum'
  h=hnext
END DO
STOP' Problem in dodeint_jac: too many steps'
END SUBROUTINE dodeint_jac

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE polint(xa,ya,x,y,dy)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE rpolint(xa,ya,x,y,dy)
IMPLICIT NONE
REAL, DIMENSION(:), INTENT(IN) :: xa,ya
REAL, INTENT(IN) :: x
REAL, INTENT(OUT) :: y,dy
INTEGER :: m,n,ns
REAL, DIMENSION(SIZE(xa)) :: c,d,den,ho

n=SIZE(xa)
c=ya
d=ya
ho=xa-x
ns=iminloc(ABS(x-xa))
y=ya(ns)
ns=ns-1
DO m=1,n-1
   den(1:n-m)=ho(1:n-m)-ho(1+m:n)
   IF(ANY(den(1:n-m) == 0.0))STOP' Problem in rpolint: calculation failure'
   den(1:n-m)=(c(2:n-m+1)-d(1:n-m))/den(1:n-m)
   d(1:n-m)=ho(1+m:n)*den(1:n-m)
   c(1:n-m)=ho(1:n-m)*den(1:n-m)
   IF(2*ns < n-m)THEN
      dy=c(ns+1)
   ELSE
      dy=d(ns)
      ns=ns-1
   END IF
   y=y+dy
END DO
END SUBROUTINE rpolint

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dpolint(xa,ya,x,y,dy)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xa,ya
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8), INTENT(OUT) :: y,dy
INTEGER :: m,n,ns
REAL(KIND=8), DIMENSION(SIZE(xa)) :: c,d,den,ho

n=SIZE(xa)
c=ya
d=ya
ho=xa-x
ns=iminloc(ABS(x-xa))
y=ya(ns)
ns=ns-1
DO m=1,n-1
   den(1:n-m)=ho(1:n-m)-ho(1+m:n)
   IF(ANY(den(1:n-m) == 0.d0))STOP' Problem in dpolint: calculation failure'
   den(1:n-m)=(c(2:n-m+1)-d(1:n-m))/den(1:n-m)
   d(1:n-m)=ho(1+m:n)*den(1:n-m)
   c(1:n-m)=ho(1:n-m)*den(1:n-m)
   IF(2*ns < n-m)THEN
      dy=c(ns+1)
   ELSE
      dy=d(ns)
      ns=ns-1
   END IF
   y=y+dy
END DO
END SUBROUTINE dpolint

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dpolint_s(xa,ya,n,x,y,dy)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:):: xa,ya
INTEGER :: n
REAL(KIND=8) :: x
REAL(KIND=8) :: y
REAL(KIND=8) :: dy
INTEGER, PARAMETER :: nmax=25
INTEGER :: i,m,ns
REAL(KIND=8) :: den,dif,dift,ho,hp,w,c(nmax),d(nmax)

ns=1
dif=ABS(x-xa(1))
DO i=1,n
  dift=ABS(x-xa(i))
  IF(dift < dif)THEN
    ns=i
    dif=dift
  END IF
  c(i)=ya(i)
  d(i)=ya(i)
END DO
y=ya(ns)
ns=ns-1
DO m=1,n-1
  DO i=1,n-m
    ho=xa(i)-x
    hp=xa(i+m)-x
    w=c(i+1)-d(i)
    den=ho-hp
    IF(den == 0.d0)STOP'failure in dpolint_s'
    den=w/den
    d(i)=hp*den
    c(i)=ho*den
  END DO
  IF(2*ns < n-m)THEN
    dy=c(ns+1)
  ELSE
    dy=d(ns)
    ns=ns-1
  END IF
  y=y+dy
END DO
RETURN
END SUBROUTINE dpolint_s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION poly(x,coeffs)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: coeffs
REAL(KIND=8) :: poly,pow
REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: vec
INTEGER :: i,n,nn

n=SIZE(coeffs)
IF(n <= 0)THEN
   poly=0.d0
ELSE IF(n < 8)THEN
   poly=coeffs(n)
   DO i=n-1,1,-1
      poly=x*poly+coeffs(i)
   END DO
ELSE
   ALLOCATE(vec(n+1))
   pow=x
   vec(1:n)=coeffs
   DO
      vec(n+1)=0.d0
      nn=ISHFT(n+1,-1)
      vec(1:nn)=vec(1:n:2)+pow*vec(2:n+1:2)
      IF(nn == 1)EXIT
      pow=pow*pow
      n=nn
   END DO
   poly=vec(1)
   DEALLOCATE(vec)
END IF
END FUNCTION poly

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE pzextr(iest,xest,yest,yz,dy)

!        Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, INTENT(IN) :: iest
REAL(KIND=8), INTENT(IN) :: xest
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: yest
REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: yz,dy
INTEGER, PARAMETER :: IEST_MAX=16
INTEGER :: j,nv
INTEGER, SAVE :: nvold=-1
REAL(KIND=8) :: delta,f1,f2
REAL(KIND=8), DIMENSION(size(yz)) :: d,tmp,q
REAL(KIND=8), DIMENSION(IEST_MAX), SAVE :: x
REAL(KIND=8), DIMENSION(:,:), ALLOCATABLE, SAVE :: qcol

nv=SIZE(yz)
IF(iest > IEST_MAX)STOP' Problem in pzextr: probable misuse, too much extrapolation'
IF(nv /= nvold)THEN
   IF(ALLOCATED(qcol))DEALLOCATE(qcol)
   ALLOCATE(qcol(nv,IEST_MAX))
   nvold=nv
END IF
x(iest)=xest
dy(:)=yest(:)
yz(:)=yest(:)
IF(iest == 1)THEN
   qcol(:,1)=yest(:)
ELSE
   d(:)=yest(:)
   DO j=1,iest-1
      delta=1.d0/(x(iest-j)-xest)
      f1=xest*delta
      f2=x(iest-j)*delta
      q(:)=qcol(:,j)
      qcol(:,j)=dy(:)
      tmp(:)=d(:)-q(:)
      dy(:)=f1*tmp(:)
      d(:)=f2*tmp(:)
      yz(:)=yz(:)+dy(:)
   END DO
   qcol(:,iest)=dy(:)
END IF
END SUBROUTINE pzextr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION qrmspl(vlx,vy,iln)

!     Integrates a splined function sply.

!     Uses: qromb,spline,sply

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: vlx,vy ! array with integration variable (eventually in natural log) and  with the integrant (including change to log(x) if the case)
INTEGER, INTENT(IN) :: iln ! 0 for interpolation performed directly from vlx and vy; > 0 for passage to natural log of vy prior to interpolation
INTEGER :: n
REAL(KIND=8) :: qrmspl

n=SIZE(vlx)
IF(n-2 < 0)THEN
   qrmspl=0.d0
ELSEIF(n-2 == 0)THEN
   qrmspl=.5D0*(vy(2)+vy(1))*(vlx(2)-vlx(1)) ! n=2  trivial trapezoidal rule
ELSE
   inpt=n
   ilog=iln
   lvx(1:n)=vlx(1:n)
   lvy(1:n)=MERGE(vy(1:n),LOG(vy(1:n)),iln == 0)
   CALL spline(lvx(1:n),lvy(1:n),mlvy(1:n))
   qrmspl=qromb(sply,vlx(1),vlx(n))
END IF
END FUNCTION qrmspl

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION sply(x)

!     Used by function qrmspl.

!     Uses: spl

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: ly,sply

ly=spl(lvx(1:inpt),lvy(1:inpt),mlvy(1:inpt),x)
sply=MERGE(ly,EXP(ly),ilog == 0)
END FUNCTION sply

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION qromb(func,a,b)

!     Adapted from Numerical Recipes.

!     Uses: polint,trapzd
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION rqromb(func,a,b)
IMPLICIT NONE
REAL, INTENT(IN) :: a,b
REAL :: rqromb
INTEGER, PARAMETER :: JMAX=20,JMAXP=JMAX+1,K=5,KM=K-1
REAL, DIMENSION(JMAXP) :: h,s
REAL :: pqromb
INTEGER :: j

INTERFACE 
   FUNCTION func(x)
     REAL, DIMENSION(:), INTENT(IN) :: x
     REAL, DIMENSION(SIZE(x)) :: func
   END FUNCTION func
END INTERFACE

rqromb=0.
IF(a == b)RETURN
h(1)=1.0
DO j=1,JMAX
   CALL trapzd(func,a,b,s(j),j)
   IF(j >= K)THEN
      CALL polint(h(j-KM:j),s(j-KM:j),0.0,rqromb,pqromb)
      IF(ABS(pqromb) <= EPS*ABS(rqromb))RETURN
   END IF
   s(j+1)=s(j)
   h(j+1)=0.25*h(j)
END DO
STOP' Problem in rqromb:too many steps'
END FUNCTION rqromb

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dqromb(func,a,b)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,b
REAL(KIND=8) :: dqromb
INTEGER, PARAMETER :: JMAX=20,JMAXP=JMAX+1,K=5,KM=K-1
REAL(KIND=8), DIMENSION(JMAXP) :: h,s
REAL(KIND=8) :: pdqromb
INTEGER :: j

INTERFACE 
   FUNCTION func(x)
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(SIZE(x)) :: func
   END FUNCTION func
END INTERFACE

dqromb=0.d0
IF(a == b)RETURN
h(1)=1.d0
DO j=1,JMAX
   CALL trapzd(func,a,b,s(j),j)
   IF(j >= K)THEN
      CALL polint(h(j-KM:j),s(j-KM:j),0.d0,dqromb,pdqromb)
      IF(ABS(pdqromb) <= EPS*ABS(dqromb))RETURN
   END IF
   s(j+1)=s(j)
   h(j+1)=0.25d0*h(j)
END DO
PRINT*,1.d0/0.d0
STOP' Problem in dqromb:too many steps'
END FUNCTION dqromb

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dqromb_s(func,a,b)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,b
REAL(KIND=8) :: dqromb_s
INTEGER, PARAMETER :: JMAX=45,JMAXP=JMAX+1,K=5,KM=K-1
REAL(KIND=8), DIMENSION(JMAXP) :: h,s
REAL(KIND=8) :: pdqromb
INTEGER :: j

INTERFACE 
   FUNCTION func(x)
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8) :: func
   END FUNCTION func
END INTERFACE

dqromb_s=0.d0
IF(a == b)RETURN
h(1)=1.d0
DO j=1,JMAX
   CALL trapzd(func,a,b,s(j),j)
   IF(j >= K)THEN
      CALL polint(h(j-KM:j),s(j-KM:j),0.d0,dqromb_s,pdqromb)
      IF(ABS(pdqromb) <= EPS*ABS(dqromb_s))RETURN
   END IF
   s(j+1)=s(j)
   h(j+1)=0.25d0*h(j)
END DO
print*,1.d0/0.d0
STOP' Problem in dqromb_s:too many steps'
END FUNCTION dqromb_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dqromb_v(func,a,b)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: a,b
REAL(KIND=8), DIMENSION(SIZE(a)) :: dqromb_v

INTERFACE 
   FUNCTION func(x)
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(SIZE(x)) :: func
   END FUNCTION func
END INTERFACE

DO kcum=1,SIZE(a)
   dqromb_v(kcum)=dqromb(func,a(kcum),b(kcum))
END DO
END FUNCTION dqromb_v

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION eqromb(x1,x2,x,y)

!     Trapezoidal rule for a function vector in uneven steps.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION reqromb(x1,x2,x,y)
IMPLICIT NONE
REAL, INTENT(IN) :: x1,x2
REAL, DIMENSION(:), INTENT(IN) :: x,y
INTEGER :: n,j
REAL :: y1,y2,reqromb

reqromb=0.
n=SIZE(x)
IF(x1 == x2 .OR. n == 1)RETURN
DO j=1,n-1
  IF(x(j) >= x1.AND.x(j+1) <= x2)THEN
    reqromb=reqromb+0.5*(x(j+1)-x(j))*(y(j)+y(j+1))
  ELSE IF(x(j) < x1.AND.x(j+1) > x1.AND.x(j+1) <= x2)THEN
    y1=(y(j+1)-y(j))/(x(j+1)-x(j))*(x1-x(j))+y(j)
    reqromb=reqromb+0.5*(x(j+1)-x1)*(y1+y(j+1))
  ELSE IF(x(j) >= x1.AND.x(j) < x2.AND.x(j+1) > x2)THEN
    y2=(y(j+1)-y(j))/(x(j+1)-x(j))*(x2-x(j))+y(j)
    reqromb=reqromb+0.5*(x2-x(j))*(y2+y(j))
  ELSE IF(x(j) < x1.AND.x(j+1) > x2)THEN
    y1=(y(j+1)-y(j))/(x(j+1)-x(j))*(x1-x(j))+y(j)
    y2=(y(j+1)-y(j))/(x(j+1)-x(j))*(x2-x(j))+y(j)
    reqromb=0.5*(x2-x1)*(y2+y1)
  END IF
END DO
END FUNCTION reqromb

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION deqromb(x1,x2,x,y)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x1,x2
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x,y
INTEGER :: n,j
REAL(KIND=8) :: y1,y2,deqromb

deqromb=0.d0
n=SIZE(x)
IF(x1 == x2 .OR. n == 1)RETURN
DO j=1,n-1
  IF(x(j) >= x1.AND.x(j+1) <= x2)THEN
    deqromb=deqromb+0.5D0*(x(j+1)-x(j))*(y(j)+y(j+1))
  ELSE IF(x(j) < x1.AND.x(j+1) <= x2.AND.x(j+1) >= x1)THEN
    y1=(y(j+1)-y(j))/(x(j+1)-x(j))*(x1-x(j))+y(j)
    deqromb=deqromb+0.5D0*(x(j+1)-x1)*(y1+y(j+1))
  ELSE IF(x(j) <= x2.AND.x(j) >= x1.AND.x(j+1) > x1.AND.x(j+1) <= x2)THEN
    y2=(y(j+1)-y(j))/(x(j+1)-x(j))*(x2-x(j))+y(j)
    deqromb=deqromb+0.5D0*(x2-x(j))*(y2+y(j))
  ELSE IF(x(j) < x1.AND.x(j+1) > x2)THEN
    y1=(y(j+1)-y(j))/(x(j+1)-x(j))*(x1-x(j))+y(j)
    y2=(y(j+1)-y(j))/(x(j+1)-x(j))*(x2-x(j))+y(j)
    deqromb=0.5D0*(x2-x1)*(y2+y1)
  END IF
END DO
END FUNCTION deqromb

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION qromo(func,a,b,choose)

!     Subroutine adapted from Numerical Recipes.

!     Uses: polint,choose

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dqromo(func,a,b,choose)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: a,b
REAL(KIND=8) :: dqromo
INTEGER, PARAMETER :: JMAX=24,JMAXP=JMAX+1,K=5,KM=K-1
REAL(KIND=8), DIMENSION(JMAXP) :: h,s
REAL(KIND=8) :: ddqromo
INTEGER :: j

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(SIZE(x)) :: func
   END FUNCTION func
   SUBROUTINE choose(funk,aa,bb,s,n)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: aa,bb
     REAL(KIND=8), INTENT(INOUT) :: s
     INTEGER, INTENT(IN) :: n
     INTERFACE
        FUNCTION funk(x)
          IMPLICIT NONE
          REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
          REAL(KIND=8), DIMENSION(SIZE(x)) :: funk
        END FUNCTION funk
     END INTERFACE
   END SUBROUTINE choose
END INTERFACE

dqromo=0.d0
IF(a == b)RETURN
h(1)=1.d0
DO j=1,JMAX
   CALL choose(func,a,b,s(j),j)
   IF(j >= K)THEN
      CALL polint(h(j-KM:j),s(j-KM:j),0.d0,dqromo,ddqromo)
      IF(ABS(ddqromo) <= EPS*ABS(dqromo)) RETURN
   END IF
   s(j+1)=s(j)
   h(j+1)=h(j)/9.d0
END DO 
PRINT*,1.d0/0.d0
STOP' Problem in dqromo: too many steps'
END FUNCTION dqromo

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dqromo_v(func,a,b,choose)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: a,b
REAL(KIND=8), DIMENSION(SIZE(b)) :: dqromo_v

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(SIZE(x)) :: func
   END FUNCTION func
   SUBROUTINE choose(funk,aa,bb,s,n)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: aa,bb
     REAL(KIND=8), INTENT(INOUT) :: s
     INTEGER, INTENT(IN) :: n
     INTERFACE
        FUNCTION funk(x)
          IMPLICIT NONE
          REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
          REAL(KIND=8), DIMENSION(SIZE(x)) :: funk
        END FUNCTION funk
     END INTERFACE
   END SUBROUTINE choose
END INTERFACE

DO kcum=1,SIZE(a)
   dqromo_v(kcum)=dqromo(func,a(kcum),b(kcum),choose)
END DO
END FUNCTION dqromo_v

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dqromo_s(func,a,b,eps,choose)
IMPLICIT NONE
REAL(KIND=8) :: a
REAL(KIND=8) :: b
REAL(KIND=8) :: eps
REAL(KIND=8) :: dqromo_s 
INTEGER :: j,i
INTEGER, PARAMETER :: jmax=25
INTEGER, PARAMETER :: jmaxp=jmax+1
INTEGER, PARAMETER :: k=5
INTEGER, PARAMETER :: km=k-1
REAL(KIND=8) :: ddqromo_s,h(jmaxp),s(jmaxp)

INTERFACE
   function func(x)
     implicit none
     real(kind=8) :: x,func
   end function func
   subroutine choose(funk,aa,bb,s,n)
     IMPLICIT NONE
     real(kind=8) :: aa,bb,s
     integer :: n
     INTERFACE
        function funk(x)
          implicit none
          real(kind=8) :: x,funk
        end function funk
     end INTERFACE
   end subroutine choose
end INTERFACE

dqromo_s=0.d0
IF(a == b)RETURN
h(1)=1.d0
DO j=1,jmax
  CALL choose(func,a,b,s(j),j)
  IF(j >= k)THEN
    CALL polint(h(j-km:j),s(j-km:j),k,0.d0,dqromo_s,ddqromo_s)
    IF(ABS(ddqromo_s) <= eps*ABS(dqromo_s))RETURN
  END IF
  s(j+1)=s(j)
  h(j+1)=h(j)/9.d0
END DO
print*,1.d0/0.d0
STOP'too many steps in dqromo_s'
END FUNCTION dqromo_s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE rkck(y,dydx,x,h,yout,yerr,derivs)

!     Subroutine adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE drkck(y,dydx,x,h,yout,yerr,derivs)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: y,dydx
REAL(KIND=8), INTENT(IN) :: x,h
REAL(KIND=8), INTENT(OUT) :: yout,yerr
REAL(KIND=8) :: ak2,ak3,ak4,ak5,ak6,ytemp
REAL(KIND=8), PARAMETER :: A2=0.2d0,A3=0.3d0,A4=0.6d0,A5=1.d0,A6=0.875d0,B21=0.2d0,B31=3.d0/40.d0,B32=9.d0/40.d0,B41=0.3d0,B42=-0.9d0,B43=1.2d0,B51=-11.d0/54.d0,B52=2.5d0, &
          B53=-70.d0/27.d0,B54=35.d0/27.d0,B61=1631.d0/55296.d0,B62=175.d0/512.d0,B63=575.d0/13824.d0,B64=44275.d0/110592.d0,B65=253.d0/4096.d0,C1=37.d0/378.d0, &
          C3=250.d0/621.d0,C4=125.d0/594.d0,C6=512.d0/1771.d0,DC1=C1-2825.d0/27648.d0,DC3=C3-18575.d0/48384.d0,DC4=C4-13525.d0/55296.d0,DC5=-277.d0/14336.d0,DC6=C6-0.25d0

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), INTENT(IN) :: y
     REAL(KIND=8), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
END INTERFACE

ytemp=y+B21*h*dydx
CALL derivs(x+A2*h,ytemp,ak2)
ytemp=y+h*(B31*dydx+B32*ak2)
CALL derivs(x+A3*h,ytemp,ak3)
ytemp=y+h*(B41*dydx+B42*ak2+B43*ak3)
CALL derivs(x+A4*h,ytemp,ak4)
ytemp=y+h*(B51*dydx+B52*ak2+B53*ak3+B54*ak4)
CALL derivs(x+A5*h,ytemp,ak5)
ytemp=y+h*(B61*dydx+B62*ak2+B63*ak3+B64*ak4+B65*ak5)
CALL derivs(x+A6*h,ytemp,ak6)
yout=y+h*(C1*dydx+C3*ak3+C4*ak4+C6*ak6)
yerr=h*(DC1*dydx+DC3*ak3+DC4*ak4+DC5*ak5+DC6*ak6)
END SUBROUTINE drkck

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE drkck_v(y,dydx,x,h,yout,yerr,derivs)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y,dydx
REAL(KIND=8), INTENT(IN) :: x,h
REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: yout,yerr
REAL(KIND=8), DIMENSION(size(y)) :: ak2,ak3,ak4,ak5,ak6,ytemp
REAL(KIND=8), PARAMETER :: A2=0.2d0,A3=0.3d0,A4=0.6d0,A5=1.d0,A6=0.875d0,B21=0.2d0,B31=3.d0/40.d0,B32=9.d0/40.d0,B41=0.3d0,B42=-0.9d0,B43=1.2d0,B51=-11.d0/54.d0,B52=2.5d0, &
          B53=-70.d0/27.d0,B54=35.d0/27.d0,B61=1631.d0/55296.d0,B62=175.d0/512.d0,B63=575.d0/13824.d0,B64=44275.d0/110592.d0,B65=253.d0/4096.d0,C1=37.d0/378.d0, &
          C3=250.d0/621.d0,C4=125.d0/594.d0,C6=512.d0/1771.d0,DC1=C1-2825.d0/27648.d0,DC3=C3-18575.d0/48384.d0,DC4=C4-13525.d0/55296.d0,DC5=-277.d0/14336.d0,DC6=C6-0.25d0

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
END INTERFACE

ytemp=y+B21*h*dydx
CALL derivs(x+A2*h,ytemp,ak2)
ytemp=y+h*(B31*dydx+B32*ak2)
CALL derivs(x+A3*h,ytemp,ak3)
ytemp=y+h*(B41*dydx+B42*ak2+B43*ak3)
CALL derivs(x+A4*h,ytemp,ak4)
ytemp=y+h*(B51*dydx+B52*ak2+B53*ak3+B54*ak4)
CALL derivs(x+A5*h,ytemp,ak5)
ytemp=y+h*(B61*dydx+B62*ak2+B63*ak3+B64*ak4+B65*ak5)
CALL derivs(x+A6*h,ytemp,ak6)
yout=y+h*(C1*dydx+C3*ak3+C4*ak4+C6*ak6)
yerr=h*(DC1*dydx+DC3*ak3+DC4*ak4+DC5*ak5+DC6*ak6)
END SUBROUTINE drkck_v

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE rkqs(y,dydx,x,htry,eps,yscal,hdid,hnext,derivs)

!     Subroutine adapted from Numerical Recipes.

!     Uses: derivs,drkck

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE drkqs(y,dydx,x,htry,eps,yscal,hdid,hnext,derivs)
IMPLICIT NONE
REAL(KIND=8), INTENT(INOUT) :: y
REAL(KIND=8), INTENT(IN) :: dydx,yscal
REAL(KIND=8), INTENT(INOUT) :: x
REAL(KIND=8), INTENT(IN) :: htry,eps
REAL(KIND=8), INTENT(OUT) :: hdid,hnext
REAL(KIND=8) :: errmax,h,htemp,xnew
REAL(KIND=8) :: yerr,ytemp
REAL(KIND=8), PARAMETER :: SAFETY=0.9d0,PGROW=-0.2d0,PSHRNK=-0.25d0,ERRCON=1.89d-4

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), INTENT(IN) :: y
     REAL(KIND=8), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
END INTERFACE

h=htry
DO
   CALL drkck(y,dydx,x,h,ytemp,yerr,derivs)
   errmax=0.d0
   errmax=MAX(errmax,ABS(yerr/yscal))/eps
   IF(errmax <= 1.d0)EXIT
   htemp=SAFETY*h*(errmax**PSHRNK)
   h=SIGN(MAX(ABS(htemp),0.1d0*ABS(h)),h)
   xnew=x+h
   IF(xnew == x)STOP' Problem in drkqs: stepsize underflow'
END DO
IF(errmax > ERRCON)THEN
   hnext=SAFETY*h*(errmax**PGROW)
ELSE 
   hnext=5.d0*h
END IF
hdid=h
x=x+h
y=ytemp
END SUBROUTINE drkqs

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE drkqs_v(y,dydx,x,htry,eps,yscal,hdid,hnext,derivs)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: y
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: dydx,yscal
REAL(KIND=8), INTENT(INOUT) :: x
REAL(KIND=8), INTENT(IN) :: htry,eps
REAL(KIND=8), INTENT(OUT) :: hdid,hnext
REAL(KIND=8) :: errmax,h,htemp,xnew
REAL(KIND=8), DIMENSION(size(y)) :: yerr,ytemp
REAL(KIND=8), PARAMETER :: SAFETY=0.9d0,PGROW=-0.2d0,PSHRNK=-0.25d0,ERRCON=1.89d-4

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
END INTERFACE

h=htry
DO
   CALL drkck_v(y,dydx,x,h,ytemp,yerr,derivs)
   errmax=MAXVAL(ABS(yerr(:)/yscal(:)))/eps
   IF(errmax <= 1.d0)EXIT
   htemp=SAFETY*h*(errmax**PSHRNK)
   h=SIGN(MAX(ABS(htemp),0.1d0*ABS(h)),h)
   xnew=x+h
   IF(xnew == x)STOP' Problem in drkqs_v: stepsize underflow'
END DO
IF(errmax > ERRCON)THEN
   hnext=SAFETY*h*(errmax**PGROW)
ELSE 
   hnext=5.d0*h
END IF
hdid=h
x=x+h
y(:)=ytemp(:)
END SUBROUTINE drkqs_v

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION ran2(idum)

!     Portable randon number generator. Call with idum a negative integer
!     to initialize. Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER :: idum
REAL(KIND=8) :: ran2
INTEGER, PARAMETER :: im1=2147483563
INTEGER, PARAMETER :: im2=2147483399
REAL(KIND=8), PARAMETER :: am=1.d0/im1
INTEGER, PARAMETER :: imm1=im1-1
INTEGER, PARAMETER :: ia1=40014
INTEGER, PARAMETER :: ia2=40692
INTEGER, PARAMETER :: iq1=53668
INTEGER, PARAMETER :: iq2=52774
INTEGER, PARAMETER :: ir1=12211
INTEGER, PARAMETER :: ir2=3791
INTEGER, PARAMETER :: ntab=32
INTEGER, PARAMETER :: ndiv=1+imm1/ntab
REAL(KIND=8), PARAMETER :: eps=3.d-16
REAL(KIND=8), PARAMETER :: rnmx=1.d0-eps
INTEGER :: idum2,j,k,iv(ntab),iy
SAVE iv,iy,idum2
DATA idum2/123456789/, iv/ntab*0/, iy/0/

IF (idum <= 0) THEN
  idum=MAX(-idum,1)
  idum2=idum
  DO  j=ntab+8,1,-1
    k=idum/iq1
    idum=ia1*(idum-k*iq1)-k*ir1
    IF (idum < 0) idum=idum+im1
    IF (j <= ntab) iv(j)=idum
  END DO
  iy=iv(1)
END IF
k=idum/iq1
idum=ia1*(idum-k*iq1)-k*ir1
IF (idum < 0) idum=idum+im1
k=idum2/iq2
idum2=ia2*(idum2-k*iq2)-k*ir2
IF (idum2 < 0) idum2=idum2+im2
j=1+iy/ndiv
iy=iv(j)-idum2
iv(j)=idum
IF(iy < 1)iy=iy+imm1
ran2=MIN(am*iy,rnmx)
END FUNCTION ran2

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
SUBROUTINE stifbsh(y,dydx,x,htry,epsi,yscal,hdid,hnext,derivs,jacob)

!     Subroutine adapted from Numerical Recipes.

!     Uses: derivs,jacob,pzextr,simpr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(INOUT) :: y
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: dydx,yscal
REAL(KIND=8), INTENT(IN) :: htry,epsi
REAL(KIND=8), INTENT(INOUT) :: x
REAL(KIND=8), INTENT(OUT) :: hdid,hnext
INTEGER, PARAMETER :: IMAX=8, KMAXX=IMAX-1
REAL(KIND=8), PARAMETER :: SAFE1=0.25d0,SAFE2=0.7d0,REDMAX=1.0d-5,REDMIN=0.7d0,TINY=1.0d-30,SCALMX=0.1d0
INTEGER :: k,km
INTEGER, DIMENSION(IMAX) :: nseq = (/ 2,6,10,14,22,34,50,70 /)
INTEGER, SAVE :: kopt,kmax,nvold=-1
REAL(KIND=8), DIMENSION(KMAXX,KMAXX), SAVE :: alf
REAL(KIND=8), DIMENSION(KMAXX) :: err
REAL(KIND=8), DIMENSION(IMAX), SAVE :: a
REAL(KIND=8), SAVE :: epsold = -1.0
REAL(KIND=8) :: eps1,errmax,fact,h,red,scale,wrkmin,xest
REAL(KIND=8), SAVE :: xnew
REAL(KIND=8), DIMENSION(size(y)) :: dfdx,yerr,ysav,yseq
REAL(KIND=8), DIMENSION(size(y),size(y)) :: dfdy
LOGICAL :: reduct
LOGICAL, SAVE :: first=.true.

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
   SUBROUTINE jacob(x,y,dfdx,dfdy)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dfdx
     REAL(KIND=8), DIMENSION(:,:), INTENT(OUT) :: dfdy
   END SUBROUTINE jacob
END INTERFACE

IF(rnew)THEN
   hnext=-1.0d29
   rnew=.FALSE.
END IF
IF(epsi /= epsold.OR.nvold /= SIZE(y))THEN
   hnext=-1.0d29
   xnew=-1.0d29
   eps1=SAFE1*epsi
   a(:)=cumsum(nseq,1)
   WHERE(upper_triangle(KMAXX,KMAXX))alf=eps1**(outerdiff(a(2:),a(2:))/outerprod(arth(3.d0,2.d0,KMAXX),(a(2:)-a(1)+1.d0)))
   epsold=epsi
   nvold=SIZE(y)
   a(:)=cumsum(nseq,1+nvold)
   DO kopt=2,KMAXX-1
      IF(a(kopt+1) > a(kopt)*alf(kopt-1,kopt))EXIT
   END DO
   kmax=kopt
END IF
h=htry
ysav(:)=y(:)
CALL jacob(x,y,dfdx,dfdy)
IF(h /= hnext .OR. x /= xnew)then
   first=.TRUE.
   kopt=kmax
END IF
reduct=.FALSE.
main_loop: do
   DO k=1,kmax
      xnew=x+h
      IF(xnew == x)STOP' Problem in stifbsh: step size underflow'
      CALL simpr(ysav,dydx,dfdx,dfdy,x,h,nseq(k),yseq,derivs)
      xest=(h/nseq(k))**2
      CALL pzextr(k,xest,yseq,y,yerr)
      IF(k /= 1)THEN
         errmax=MAXVAL(ABS(yerr(:)/yscal(:)))
         errmax=MAX(TINY,errmax)/epsi
         km=k-1
         err(km)=(errmax/SAFE1)**(1.d0/(2*km+1))
      END IF
      IF(k /= 1 .AND. (k >= kopt-1 .or. first))THEN
         IF(errmax < 1.d0)EXIT main_loop
         IF(k == kmax .OR. k == kopt+1)THEN
            red=SAFE2/err(km)
            EXIT
         ELSE IF(k == kopt)THEN
            IF(alf(kopt-1,kopt) < err(km))THEN
               red=1.d0/err(km)
               EXIT
            END IF
         ELSE IF(kopt == kmax)THEN
            IF(alf(km,kmax-1) < err(km))THEN
               red=alf(km,kmax-1)*SAFE2/err(km)
               EXIT
            END IF
         ELSE IF(alf(km,kopt) < err(km))THEN
            red=alf(km,kopt-1)/err(km)
            EXIT
         END IF
      END IF
   END DO
   red=MAX(MIN(red,REDMIN),REDMAX)
   h=h*red
   reduct=.TRUE.
end do main_loop
x=xnew
hdid=h
first=.FALSE.
kopt=1+iminloc(a(2:km+1)*MAX(err(1:km),SCALMX))
scale=MAX(err(kopt-1),SCALMX)
wrkmin=scale*a(kopt)
hnext=h/scale
IF(kopt >= k .AND. kopt /= kmax .AND. .NOT. reduct)THEN
   fact=MAX(scale/alf(kopt-1,kopt),SCALMX)
   IF(a(kopt+1)*fact <= wrkmin)THEN
      hnext=h/fact
      kopt=kopt+1
   END IF
END IF
END SUBROUTINE stifbsh

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE simpr(y,dydx,dfdx,dfdy,xs,htot,nstep,yout,derivs)

!        Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: xs,htot
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y,dydx,dfdx
REAL(KIND=8), DIMENSION(:,:), INTENT(IN) :: dfdy
INTEGER, INTENT(IN) :: nstep
REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: yout
INTEGER :: ndum,nn
INTEGER, DIMENSION(size(y)) :: indx
REAL(KIND=8) :: d,h,x
REAL(KIND=8), DIMENSION(size(y)) :: del,ytemp
REAL(KIND=8), DIMENSION(size(y),size(y)) :: a

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: y
     REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
END INTERFACE

ndum=size(y)
h=htot/nstep
a(:,:)=-h*dfdy(:,:)
CALL diagad(a,1.d0)
CALL ludcmp(a,indx,d)
yout=h*(dydx+h*dfdx)
call lubksb(a,indx,yout)
del=yout
ytemp=y+del
x=xs+h
CALL derivs(x,ytemp,yout)
DO nn=2,nstep
   yout=h*yout-del
   CALL lubksb(a,indx,yout)
   del=del+2.d0*yout
   ytemp=ytemp+del
   x=x+h
   CALL derivs(x,ytemp,yout)
END DO
yout=h*yout-del
CALL lubksb(a,indx,yout)
yout=ytemp+yout
END SUBROUTINE simpr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE sort(n,arr)

!     Adapted from numerical recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE isort(n,arr)
IMPLICIT NONE
INTEGER :: n
INTEGER, DIMENSION(:) :: arr
INTEGER, PARAMETER :: m=7
INTEGER, PARAMETER :: nstack=50
INTEGER :: i,ir,j,jstack,k,l,istack(nstack)
INTEGER :: a,temp

jstack=0
l=1
ir=n
1     IF(ir-l < m)THEN
  DO  j=l+1,ir
    a=arr(j)
    DO  i=j-1,1,-1
      IF(arr(i) <= a)GO TO 2
      arr(i+1)=arr(i)
    END DO
    i=0
    2         arr(i+1)=a
  END DO
  IF(jstack == 0)RETURN
  ir=istack(jstack)
  l=istack(jstack-1)
  jstack=jstack-2
ELSE
  k=(l+ir)/2
  temp=arr(k)
  arr(k)=arr(l+1)
  arr(l+1)=temp
  IF(arr(l+1) > arr(ir))THEN
    temp=arr(l+1)
    arr(l+1)=arr(ir)
    arr(ir)=temp
  END IF
  IF(arr(l) > arr(ir))THEN
    temp=arr(l)
    arr(l)=arr(ir)
    arr(ir)=temp
  END IF
  IF(arr(l+1) > arr(l))THEN
    temp=arr(l+1)
    arr(l+1)=arr(l)
    arr(l)=temp
  END IF
  i=l+1
  j=ir
  a=arr(l)
  3       CONTINUE
  i=i+1
  IF(arr(i) < a)GO TO 3
  4       CONTINUE
  j=j-1
  IF(arr(j) > a)GO TO 4
  IF(j < i)GO TO 5
  temp=arr(i)
  arr(i)=arr(j)
  arr(j)=temp
  GO TO 3
  5       arr(l)=arr(j)
  arr(j)=a
  jstack=jstack+2
  IF(jstack > nstack)STOP' Problem in sort: nstack too small'
  IF(ir-i+1 >= j-l)THEN
    istack(jstack)=ir
    istack(jstack-1)=i
    ir=j-1
  ELSE
    istack(jstack)=j-1
    istack(jstack-1)=l
    l=i
  END IF
END IF
GO TO 1
END SUBROUTINE isort

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dsort(n,arr)
IMPLICIT NONE
INTEGER :: n
REAL, DIMENSION(:) :: arr
INTEGER, PARAMETER :: m=7
INTEGER, PARAMETER :: nstack=50
INTEGER :: i,ir,j,jstack,k,l,istack(nstack)
REAL :: a,temp

jstack=0
l=1
ir=n
1     IF(ir-l < m)THEN
  DO  j=l+1,ir
    a=arr(j)
    DO  i=j-1,1,-1
      IF(arr(i) <= a)GO TO 2
      arr(i+1)=arr(i)
    END DO
    i=0
    2         arr(i+1)=a
  END DO
  IF(jstack == 0)RETURN
  ir=istack(jstack)
  l=istack(jstack-1)
  jstack=jstack-2
ELSE
  k=(l+ir)/2
  temp=arr(k)
  arr(k)=arr(l+1)
  arr(l+1)=temp
  IF(arr(l+1) > arr(ir))THEN
    temp=arr(l+1)
    arr(l+1)=arr(ir)
    arr(ir)=temp
  END IF
  IF(arr(l) > arr(ir))THEN
    temp=arr(l)
    arr(l)=arr(ir)
    arr(ir)=temp
  END IF
  IF(arr(l+1) > arr(l))THEN
    temp=arr(l+1)
    arr(l+1)=arr(l)
    arr(l)=temp
  END IF
  i=l+1
  j=ir
  a=arr(l)
  3       CONTINUE
  i=i+1
  IF(arr(i) < a)GO TO 3
  4       CONTINUE
  j=j-1
  IF(arr(j) > a)GO TO 4
  IF(j < i)GO TO 5
  temp=arr(i)
  arr(i)=arr(j)
  arr(j)=temp
  GO TO 3
  5       arr(l)=arr(j)
  arr(j)=a
  jstack=jstack+2
  IF(jstack > nstack)PAUSE 'nstack too small in dsort'
  IF(ir-i+1 >= j-l)THEN
    istack(jstack)=ir
    istack(jstack-1)=i
    ir=j-1
  ELSE
    istack(jstack)=j-1
    istack(jstack-1)=l
    l=i
  END IF
END IF
GO TO 1
END SUBROUTINE dsort

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dsort2(n,arr,brr)
IMPLICIT NONE
INTEGER, PARAMETER :: nstack=50
INTEGER :: n,i,ir,j,jstack,k,l,istack(nstack)
INTEGER, PARAMETER :: m=7
REAL(KIND=8) :: a,b,temp
REAL(KIND=8), DIMENSION(:) :: arr,brr

jstack=0
l=1
ir=n
10   IF(ir-l < m)THEN
  DO j=l+1,ir
    a=arr(j)
    b=brr(j)
    DO i=j-1,1,-1
      IF(arr(i) <= a)GO TO 20
      arr(i+1)=arr(i)
      brr(i+1)=brr(i)
    END DO
    i=0
    20         arr(i+1)=a
    brr(i+1)=b
  END DO
  IF(jstack == 0)RETURN
  ir=istack(jstack)
  l=istack(jstack-1)
  jstack=jstack-2
ELSE
  k=(l+ir)/2
  temp=arr(k)
  arr(k)=arr(l+1)
  arr(l+1)=temp
  temp=brr(k)
  brr(k)=brr(l+1)
  brr(l+1)=temp
  IF(arr(l+1) > arr(ir))THEN
    temp=arr(l+1)
    arr(l+1)=arr(ir)
    arr(ir)=temp
    temp=brr(l+1)
    brr(l+1)=brr(ir)
    brr(ir)=temp
  END IF
  IF(arr(l) > arr(ir))THEN
    temp=arr(l)
    arr(l)=arr(ir)
    arr(ir)=temp
    temp=brr(l)
    brr(l)=brr(ir)
    brr(ir)=temp
  END IF
  IF(arr(l+1) > arr(l))THEN
    temp=arr(l+1)
    arr(l+1)=arr(l)
    arr(l)=temp
    temp=brr(l+1)
    brr(l+1)=brr(l)
    brr(l)=temp
  END IF
  i=l+1
  j=ir
  a=arr(l)
  b=brr(l)
  30      i=i+1
  IF(arr(i) < a)GO TO 30
  40      j=j-1
  IF(arr(j) > a)GO TO 40
  IF(j < i)GO TO 50
  temp=arr(i)
  arr(i)=arr(j)
  arr(j)=temp
  temp=brr(i)
  brr(i)=brr(j)
  brr(j)=temp
  GO TO 30
  50      arr(l)=arr(j)
  arr(j)=a
  brr(l)=brr(j)
  brr(j)=b
  jstack=jstack+2
  IF(jstack > nstack)STOP' Problem dsort2: nstack too small in'
  IF(ir-i+1 >= j-l)THEN
    istack(jstack)=ir
    istack(jstack-1)=i
    ir=j-1
  ELSE
    istack(jstack)=j-1
    istack(jstack-1)=l
    l=i
  END IF
END IF
GO TO 10
END SUBROUTINE dsort2

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION sp21(x1a,x2a,ya,y2a,x1,x2)

!     Adapted from Numerical Recipes' splin2.

!     Uses: hunt,spl,spline

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x1a,x2a
REAL(KIND=8), INTENT(IN), DIMENSION(:,:) :: ya,y2a
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x1
REAL(KIND=8), INTENT(IN) :: x2
INTEGER :: k2lo,khi,n
REAL(KIND=8) :: h,a,b
REAL(KIND=8), DIMENSION(SIZE(x1))  :: sp21
REAL(KIND=8), DIMENSION(SIZE(x1a)) :: yy,myy

n=SIZE(x2a)
k2lo=1
CALL hunt(x2a,x2,k2lo)
khi=k2lo+1
h=x2a(khi)-x2a(k2lo)
a=(x2a(khi)-x2)/h
b=(x2-x2a(k2lo))/h
yy=a*ya(:,k2lo)+b*ya(:,khi)+((a**3-a)*y2a(:,k2lo)+(b**3-b)*y2a(:,khi))*(h**2)/6.d0
CALL spline(x1a,yy,myy)
sp21=spl(x1a,yy,myy,x1)
END FUNCTION sp21

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION sp22(x1a,x2a,ya,y2a,x1,x2)

!     Adapted from Numerical Recipes' splin2.

!     Uses: hunt,spl,spline

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x1a,x2a
REAL(KIND=8), INTENT(IN), DIMENSION(:,:) :: ya,y2a
REAL(KIND=8), INTENT(IN) :: x1
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x2
INTEGER :: k1lo,k2lo,khi,n,k
REAL(KIND=8) :: h,a,b
REAL(KIND=8), DIMENSION(SIZE(x2))  :: sp22
REAL(KIND=8), DIMENSION(SIZE(x1a)) :: yy,myy

n=SIZE(x2a)
k1lo=1
k2lo=0
DO k=1,SIZE(x2)
   IF(ABS(1.d0-x2(k)/x2a(1)) < zero)THEN
      yy=ya(:,1)
   ELSE IF(ABS(1.d0-x2(k)/x2a(n)) < zero)THEN
      yy=ya(:,n)
   ELSE
      IF(k2lo == 0)THEN
         k2lo=1
         CALL hunt(x2a,x2(k),k2lo)
         khi=k2lo+1
      ELSE
         DO WHILE(khi < n .AND. x2a(khi) <= x2(k))
            k2lo=khi
            khi=k2lo+1
         END DO
      END IF
      h=x2a(khi)-x2a(k2lo)
      a=(x2a(khi)-x2(k))/h
      b=(x2(k)-x2a(k2lo))/h
      yy=a*ya(:,k2lo)+b*ya(:,khi)+((a**3-a)*y2a(:,k2lo)+(b**3-b)*y2a(:,khi))*(h**2)/6.d0
   END IF
   CALL spline(x1a,yy,myy)
   sp22(k)=spl(x1a,yy,myy,x1,k1lo)
END DO
END FUNCTION sp22

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION sp22s(x1a,x2a,ya,y2a,x1,x2)
!     Adapted from Numerical Recipes' splin2.

!     Uses: hunt,spl,spline

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x1a,x2a
REAL(KIND=8), INTENT(IN), DIMENSION(:,:) :: ya,y2a
REAL(KIND=8), INTENT(IN) :: x1
REAL(KIND=8), INTENT(IN) :: x2
INTEGER :: k1lo,k2lo,khi,n,k
REAL(KIND=8) :: sp22s,h,a,b
REAL(KIND=8), DIMENSION(SIZE(x1a)) :: yy,myy

n=SIZE(x2a)
k1lo=1
k2lo=0
IF(ABS(1.d0-x2/x2a(1)) < zero)THEN
   yy=ya(:,1)
ELSE IF(ABS(1.d0-x2/x2a(n)) < zero)THEN
   yy=ya(:,n)
ELSE
   IF(k2lo == 0)THEN
      k2lo=1
      CALL hunt(x2a,x2,k2lo)
      khi=k2lo+1
   ELSE
      DO WHILE(khi < n .AND. x2a(khi) <= x2)
         k2lo=khi
         khi=k2lo+1
      END DO
   END IF
   h=x2a(khi)-x2a(k2lo)
   a=(x2a(khi)-x2)/h
   b=(x2-x2a(k2lo))/h
   yy=a*ya(:,k2lo)+b*ya(:,khi)+((a**3-a)*y2a(:,k2lo)+(b**3-b)*y2a(:,khi))*(h**2)/6.d0
END IF
CALL spline(x1a,yy,myy)
sp22s=spl(x1a,yy,myy,x1,k1lo)
END FUNCTION sp22s

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION spl(xa,ya,y2a,x,klo)

!     Adapted from Numerical Recipes' splint.

!     Uses: hunt,lint

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION rspl_s(xa,ya,y2a,x,klo)
IMPLICIT NONE
REAL, DIMENSION(:), INTENT(IN) :: xa,ya,y2a
REAL, INTENT(IN) :: x 
INTEGER, INTENT(INOUT) :: klo
INTEGER :: n,khi
REAL :: rspl_s,a,b,h

n=SIZE(xa)
IF(ABS(x-xa(1)) < ABS(rzero*xa(1)))THEN
   rspl_s=ya(1)
   RETURN
ELSEIF(ABS(x-xa(n)) < ABS(rzero*xa(n)))THEN
   rspl_s=ya(n)
   RETURN
ELSEIF(xa(1) == 0.0.AND.ABS(x) < rzero)THEN
   rspl_s=ya(1)
   RETURN
ELSEIF(xa(n) == 0.0.AND.ABS(x) < rzero)THEN
   rspl_s=ya(n)
   RETURN
ELSE
   IF(n <= 3.OR.y2a(1) == 1.)THEN
      rspl_s=lint(xa,ya,x,klo)
   ELSE
      klo=MIN(klo,n-1)
      CALL hunt(xa,x,klo)
      khi=klo+1
      h=xa(khi)-xa(klo)
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      rspl_s=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
   END IF
END IF
END FUNCTION rspl_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dspl_s(xa,ya,y2a,x,klo)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xa,ya,y2a
REAL(KIND=8), INTENT(IN) :: x 
INTEGER, INTENT(INOUT) :: klo
INTEGER :: n,khi
REAL(KIND=8) :: dspl_s,a,b,h

n=SIZE(xa)
IF(ABS(xa(1)-x) < ABS(zero*xa(1)))THEN
   dspl_s=ya(1)
   RETURN
ELSEIF(ABS(xa(n)-x) < ABS(zero*xa(n)))THEN
   dspl_s=ya(n)
   RETURN
ELSEIF(xa(1) == 0.D0.AND.ABS(x) < zero)THEN
   dspl_s=ya(1)
   RETURN
ELSEIF(xa(n) == 0.D0.AND.ABS(x) < zero)THEN
   dspl_s=ya(n)
   RETURN
ELSE
   IF(n <= 3.OR.y2a(1) == 1.d0)THEN
      dspl_s=lint(xa,ya,x,klo)
   ELSE
      CALL hunt(xa,x,klo) ! returns klo such that xa(klo)<x<=xa(klo+1)
      khi=klo+1
      h=xa(khi)-xa(klo)
      a=(xa(khi)-x)/h
      b=(x-xa(klo))/h
      dspl_s=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
   END IF
END IF
END FUNCTION dspl_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION rspl(xa,ya,y2a,x)
IMPLICIT NONE
REAL, DIMENSION(:), INTENT(IN) :: xa,ya,y2a
REAL, DIMENSION(:), INTENT(IN) :: x 
INTEGER :: n,khi,klo,k
REAL :: a,b,h
REAL, DIMENSION(SIZE(x)) :: rspl
LOGICAL lin

n=SIZE(xa)
IF(n < 3.OR.y2a(1) == 1.)THEN
   rspl=lint(xa,ya,x)
ELSE
   klo=0
   DO k=1,SIZE(x)
      IF(ABS(xa(1)-x(1)) < ABS(rzero*xa(1)))THEN
         rspl(k)=ya(1)
         CYCLE
      ELSE IF(ABS(xa(1)-x(n)) < ABS(rzero*xa(n)))THEN
         rspl(k)=ya(n)
         CYCLE
      ELSE
         IF(klo == 0)THEN
            klo=1
            CALL hunt(xa,x(k),klo)
            khi=klo+1
         ELSE
            DO WHILE(khi < n .AND. xa(khi) <= x(k))
               klo=khi
               khi=klo+1
            END DO
         END IF
         h=xa(khi)-xa(klo)
         a=(xa(khi)-x(k))/h
         b=(x(k)-xa(klo))/h
         rspl(k)=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
      END IF
   END DO
END IF
END FUNCTION rspl

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dspl(xa,ya,y2a,x)
IMPLICIT NONE
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: xa,ya,y2a
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x 
INTEGER :: n,khi,klo,k
REAL(KIND=8) :: a,b,h
LOGICAL :: ascna,ascnx
REAL(KIND=8), DIMENSION(SIZE(x)) :: dspl

n=SIZE(xa)
IF(n <= 3.OR.y2a(1) == 1.d0)THEN
   dspl=lint(xa,ya,x)
ELSE
   klo=0
   DO k=1,SIZE(x)
      IF(ABS(xa(1)-x(k)) < ABS(zero*xa(1)))THEN
         dspl(k)=ya(1)
         CYCLE       
      ELSE IF(ABS(xa(n)-x(k)) < ABS(zero*xa(n)))THEN
         dspl(k)=ya(n)
         CYCLE
      ELSE
         ascna=(xa(1) >= xa(n))             ! increasing xa
         ascnx=(x(1) >= x(SIZE(x)))         ! increasing x
         IF(klo == 0)THEN
            klo=1
            CALL hunt(xa,x(k),klo)
            khi=klo+1
         ELSE
            IF(ascna.EQV.ascnx)THEN
               DO WHILE(khi < n .AND. xa(khi) <= x(k))
                  klo=khi
                  khi=klo+1
               END DO
            ELSE
               DO WHILE(klo > 1 .AND. xa(klo) >= x(k))
                  khi=klo
                  klo=khi-1
               END DO
            END IF
         END IF
         h=xa(khi)-xa(klo)
         a=(xa(khi)-x(k))/h
         b=(x(k)-xa(klo))/h
         dspl(k)=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
      ENDIF
   END DO
END IF
END FUNCTION dspl

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE splie2(x2a,ya,y2a)

!     Adapted from Numerical Recipes.  (For natural spline only)

!     Uses: spline

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER j
REAL(KIND=8), DIMENSION(:) :: x2a
REAL(KIND=8), DIMENSION(:,:) :: ya,y2a
REAL(KIND=8), DIMENSION(SIZE(x2a)) :: y2tmp,ytmp

DO j=1,SIZE(ya(:,1))
   CALL spline(x2a,ya(j,:),y2a(j,:))
END DO
END SUBROUTINE splie2

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE spline(x,y,y2)

!     Adapted from Numerical Recipes. (For natural spline only)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE rspline(x,y,y2)
IMPLICIT NONE
REAL, INTENT(IN), DIMENSION(:) :: x,y
REAL, INTENT(OUT), DIMENSION(:) :: y2
INTEGER :: n,i,k
REAL :: p,sig,y2a,y2aa
REAL, DIMENSION(SIZE(x)) :: u
LOGICAL :: lint

n=SIZE(x)
IF(n < 3)RETURN
lint=.false.
y2(1)=0.
u(1)=0.
DO i=2,n-1
   sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
   p=sig*y2(i-1)+2.
   y2(i)=(sig-1.)/p
   u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig* u(i-1))/p
END DO
y2(n)=0.
y2a=0.
y2aa=0.
DO k=n-1,1,-1
   y2(k)=y2(k)*y2(k+1)+u(k)
   IF(y2(k)*y2a < 0.)THEN
      IF(.not.lint.and.(y2a*y2aa < 0..OR.n <= 4))lint=.true.
   END IF
   y2aa=y2a
   y2a=y2(k)
END DO
IF(lint)y2(1)=1.
END SUBROUTINE rspline

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dspline(x,y,y2)
IMPLICIT NONE
INTEGER :: n,i,k
REAL(KIND=8) :: p,sig,y2a,y2aa
REAL(kind=8), DIMENSION(:)  :: x,y,y2
REAL(KIND=8), DIMENSION(SIZE(x)) :: u
LOGICAL :: lint

n=SIZE(x)

IF(n < 3)RETURN
lint=.false.
y2(1)=0.d0
u(1)=0.d0
DO i=2,n-1
   sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
   p=sig*y2(i-1)+2.d0
   y2(i)=(sig-1.d0)/p
   u(i)=(6.d0*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
END DO
y2(n)=0.d0
y2a=0.d0
y2aa=0.d0
DO k=n-1,1,-1
   y2(k)=y2(k)*y2(k+1)+u(k)
   IF(y2(k)*y2a < 0.d0)THEN
      IF((y2a*y2aa < 0.d0.OR.n <= 4).AND..NOT.lint)lint=.true.
   END IF
   y2aa=y2a
   y2a=y2(k)
END DO
IF(lint)y2(1)=1.d0
END SUBROUTINE dspline

SUBROUTINE dspline_d(x,y,yp1,ypn,y2)
IMPLICIT NONE
INTEGER :: n,i,k
INTEGER, PARAMETER :: NMAX=500
REAL(kind=8), DIMENSION(:)  :: x,y,y2
REAL(KIND=8), DIMENSION(SIZE(x)) :: u
REAL(KIND=8) :: p,sig,qn,un,yp1,ypn
      !REAL p,qn,sig,un,u(NMAX)

n=SIZE(x)
IF(yp1 > 0.99d30)THEN
   y2(1)=0.d0
   u(1)=0.d0
ELSE
   y2(1)=-0.5d0
   u(1)=(3.d0/(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
END IF
DO i=2,n-1
   sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
   p=sig*y2(i-1)+2.
   y2(i)=(sig-1.)/p
   u(i)=(6.d0*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
END DO
IF(ypn < 0.99d30) then
   qn=0.d0
   un=0.d0
ELSE
   qn=0.5d0
   un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
END IF
y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
DO k=n-1,1,-1
   y2(k)=y2(k)*y2(k+1)+u(k)
END DO
END SUBROUTINE dspline_d

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE trapzd(func,a,b,s,n)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE rtrapzd(func,a,b,s,n)
IMPLICIT NONE
REAL :: a,b,s,del,fsum,x
INTEGER :: n,it,j

INTERFACE
  FUNCTION func(x)
     real, dimension(:) :: x
     real, dimension(size(x)) :: func
   END FUNCTION func
END INTERFACE

IF(n == 1)THEN
   s=0.5d0*(b-a)*SUM(func((/ a,b /)))
ELSE
  it=2**(n-2)
  del=(b-a)/it
  x=a+0.5*del
  fsum=SUM(func(arth(a+0.5*del,del,it)))
  s=0.5*(s+del*fsum)
END IF
END SUBROUTINE rtrapzd

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dtrapzd(func,a,b,s,n)
IMPLICIT NONE
REAL(KIND=8) :: a,b,s,del,fsum,x
INTEGER :: n,it,j

INTERFACE
   FUNCTION func(x)
     REAL(KIND=8), DIMENSION(:), INTENT(IN) :: x
     REAL(KIND=8), DIMENSION(SIZE(x)) :: func
   END FUNCTION func
END INTERFACE

IF(n == 1)THEN
   s=0.5D0*(b-a)*SUM(func((/ a,b /)))
ELSE
  it=2**(n-2)
  del=(b-a)/it
  x=a+0.5d0*del
  fsum=SUM(func(arth(a+0.5d0*del,del,it)))
  s=0.5d0*(s+del*fsum)
END IF
END SUBROUTINE dtrapzd

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dtrapzd_s(func,a,b,s,n)
IMPLICIT NONE
REAL(KIND=8) :: a,b,s,del,fsum,x
INTEGER :: n,it,j

INTERFACE
   FUNCTION func(x)
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8) :: func
   END FUNCTION func
END INTERFACE

IF(n == 1)THEN
   s=0.5D0*(b-a)*(func(a)+func(b))
ELSE
  it=2**(n-2)
  del=(b-a)/it
  x=a+0.5d0*del
  fsum=0.d0
  DO j=1,it
     fsum=fsum+func(x)
     x=x+del
  END DO
  s=0.5d0*(s+(b-a)*fsum/DBLE(it))
END IF
END SUBROUTINE dtrapzd_s

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE trapzv(x,y,s)
IMPLICIT NONE
REAL, INTENT(IN), DIMENSION(:) :: x,y ! vectors with the integration variable and integrant vector f(x)
REAL, INTENT(OUT) :: s
INTEGER :: n
REAL :: fsum
REAL, DIMENSION(SIZE(x)) :: x1

n=SIZE(x)
IF(n-2 < 0) THEN
  s=0.
ELSE IF (n-2 == 0) THEN
  s=.5*(y(2)+y(1))*(x(2)-x(1))
ELSE
  x1=EOSHIFT(x,-1,x(1)+.5*(x(1)-x(2)))
  s=SUM(y*(x-x1))
  s=s-0.5*y(n)*(x(n)-x1(n))
END IF
END SUBROUTINE trapzv

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE zbrac(func,x1,x2,success)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(INOUT) :: x1,x2
INTEGER :: j
REAL(KIND=8) :: f1,f2
LOGICAL success
REAL(KIND=8), PARAMETER :: factor=1.3d0
INTEGER, PARAMETER :: ntry=50

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8) :: func
   END FUNCTION func
END INTERFACE

success=.TRUE.
f1=func(x1)
f2=func(x2)
DO j=1,ntry
   IF((f1 > 0.D0 .AND. f2 < 0.D0) .OR. (f1 < 0.D0 .AND. f2 > 0.D0)) RETURN
   IF(ABS(f1) < ABS(f2))THEN
      x1=x1+factor*(x1-x2)
      f1=func(x1)
   ELSE
      x2=x2+FACTOR*(x2-x1)
      f2=func(x2)
   END IF
END DO
success=.FALSE.
END SUBROUTINE zbrac

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE zbrac2(func,x1,x2)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(INOUT) :: x1,x2
INTEGER :: j
REAL(KIND=8) :: f1,f2
LOGICAL success
REAL(KIND=8), PARAMETER :: factor=1.6d0
INTEGER, PARAMETER :: ntry=50

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8) :: func
   END FUNCTION func
END INTERFACE

success=.TRUE.
f1=func(x1)
f2=func(x2)
DO j=1,ntry
   IF((f1 > 0.D0 .AND. f2 < 0.D0) .OR. (f1 < 0.D0 .AND. f2 > 0.D0)) RETURN
   IF(ABS(f1) < ABS(f2))THEN
      x1=x1+factor*(x1-x2)
      f1=func(x1)
   ELSE
      x2=x2+FACTOR*(x2-x1)
      f2=func(x2)
   END IF
END DO
success=.FALSE.
END SUBROUTINE zbrac2

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! SUBROUTINE zbrak(func,x1,x2,n,xb1,xb2,nb)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE rzbrak(func,x1,x2,n,xb1,xb2,nb)
IMPLICIT NONE
INTEGER, INTENT(IN) :: n
INTEGER, INTENT(OUT) :: nb
REAL, INTENT(IN) :: x1,x2
REAL, DIMENSION(:), POINTER :: xb1,xb2
INTEGER :: i
REAL :: dx
REAL, DIMENSION(0:n) :: f,x
LOGICAL, DIMENSION(1:n) :: mask
LOGICAL, SAVE :: init=.true.

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL, INTENT(IN) :: x
     REAL :: func
   END FUNCTION func
END INTERFACE

IF(init)THEN
   INIT=.false.
   nullify(xb1,xb2)
END IF
IF(ASSOCIATED(xb1))DEALLOCATE(xb1)
IF(ASSOCIATED(xb2))DEALLOCATE(xb2)
dx=(x2-x1)/n
x=x1+dx*arth(0,1,n+1)
DO i=0,n
   f(i)=func(x(i))
END DO
mask=f(1:n)*f(0:n-1) <= 0.0
nb=COUNT(mask)
ALLOCATE(xb1(nb),xb2(nb))
xb1(1:nb)=PACK(x(0:n-1),mask)
xb2(1:nb)=PACK(x(1:n),mask)
END SUBROUTINE rzbrak

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

SUBROUTINE dzbrak(func,x1,x2,n,xb1,xb2,nb)
IMPLICIT NONE
INTEGER, INTENT(IN) :: n
INTEGER, INTENT(OUT) :: nb
REAL(KIND=8), INTENT(IN) :: x1,x2
REAL(KIND=8), DIMENSION(:), POINTER :: xb1,xb2
INTEGER :: i
REAL(KIND=8) :: dx
REAL(KIND=8), DIMENSION(0:n) :: f,x
LOGICAL, DIMENSION(1:n) :: mask
LOGICAL, SAVE :: init=.true.

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8) :: func
   END FUNCTION func
END INTERFACE

IF(init)THEN
   INIT=.false.
   nullify(xb1,xb2)
END IF
IF(ASSOCIATED(xb1))DEALLOCATE(xb1)
IF(ASSOCIATED(xb2))DEALLOCATE(xb2)
dx=(x2-x1)/n
x=x1+dx*arth(0,1,n+1)
DO i=0,n
   f(i)=func(x(i))
END DO
mask=f(1:n)*f(0:n-1) <= 0.d0
nb=COUNT(mask)
ALLOCATE(xb1(nb),xb2(nb))
xb1(1:nb)=PACK(x(0:n-1),mask)
xb2(1:nb)=PACK(x(1:n),mask)
END SUBROUTINE dzbrak

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE dzbrak2(func,x1,x2,n,xb1,xb2,nb)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, INTENT(IN) :: n
INTEGER, INTENT(OUT) :: nb
REAL(KIND=8), INTENT(IN) :: x1,x2
REAL(KIND=8), DIMENSION(:), POINTER :: xb1,xb2
INTEGER :: i
REAL(KIND=8) :: dx
REAL(KIND=8), DIMENSION(0:n) :: f,x
LOGICAL, DIMENSION(1:n) :: mask
LOGICAL, SAVE :: init=.true.

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8) :: func
   END FUNCTION func
END INTERFACE

IF(init)THEN
   INIT=.false.
   nullify(xb1,xb2)
END IF
IF(ASSOCIATED(xb1))DEALLOCATE(xb1)
IF(ASSOCIATED(xb2))DEALLOCATE(xb2)
dx=(x2-x1)/n
x=x1+dx*arth(0,1,n+1)
DO i=0,n
   f(i)=func(x(i))
END DO
mask=f(1:n)*f(0:n-1) <= 0.d0
nb=COUNT(mask)
ALLOCATE(xb1(nb),xb2(nb))
xb1(1:nb)=PACK(x(0:n-1),mask)
xb2(1:nb)=PACK(x(1:n),mask)
END SUBROUTINE dzbrak2

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION zbrent(func,x1,x2,tol)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION rzbrent(func,x1,x2,tol)
IMPLICIT NONE
REAL :: x1
REAL :: x2
REAL :: tol
INTEGER :: iter
REAL :: rzbrent
REAL :: a,b,c,d,e,fa,fb,fc,p,q,r,s,tol1,xm
INTEGER, PARAMETER :: itmax=10000

INTERFACE
   FUNCTION func(x)
     REAL :: x,func
   END FUNCTION func
END INTERFACE

a=x1
b=x2
fa=func(a)
fb=func(b)
IF((fa > 0.d0.AND.fb > 0.d0).OR.(fa < 0.d0.AND.fb < 0.d0))  &
    STOP'root must be bracketed for rzbrent'
c=b
fc=fb
DO iter=1,itmax
  IF((fb > 0..AND.fc > 0.).OR.(fb < 0..AND.fc < 0.))THEN
    c=a
    fc=fa
    d=b-a
    e=d
  END IF
  IF(ABS(fc) < ABS(fb))THEN
    a=b
    b=c
    c=a
    fa=fb
    fb=fc
    fc=fa
  END IF
  tol1=2.*rzero*ABS(b)+0.5*tol
  xm=.5*(c-b)
  IF(ABS(xm) <= tol1.OR.fb == 0.)THEN
    rzbrent=b
    RETURN
  END IF
  IF(ABS(e) >= tol1.AND.ABS(fa) > ABS(fb))THEN
    s=fb/fa
    IF(a == c)THEN
      p=2.*xm*s
      q=1.-s
    ELSE
      q=fa/fc
      r=fb/fc
      p=s*(2.*xm*q*(q-r)-(b-a)*(r-1.))
      q=(q-1.)*(r-1.)*(s-1.)
    END IF
    IF(p > 0.)q=-q
    p=ABS(p)
    IF(2.*p < MIN1(3.*xm*q-ABS(tol1*q),ABS(e*q)))THEN
      e=d
      d=p/q
    ELSE
      d=xm
      e=d
    END IF
  ELSE
    d=xm
    e=d
  END IF
  a=b
  fa=fb
  IF(ABS(d) > tol1)THEN
    b=b+d
  ELSE
    b=b+SIGN(tol1,xm)
  END IF
  fb=func(b)
END DO
STOP'rzbrent exceeding maximum iterations'
END FUNCTION rzbrent

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION dzbrent(func,x1,x2,tol)
IMPLICIT NONE
REAL(KIND=8):: x1,x2
REAL(KIND=8)          :: tol
INTEGER :: iter
REAL(KIND=8) :: a,b,c,d,e,fa,fb,fc,p,q,r,s,tol1,xm,dzbrent
INTEGER, PARAMETER :: itmax=10000

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8) :: x,func
   END FUNCTION func
END INTERFACE

a=x1
b=x2
fa=func(a)
fb=func(b)
IF((fa > 0.d0.AND.fb > 0.d0).OR.(fa < 0.d0.AND.fb < 0.d0))THEN
  PRINT*,'Problem in dzbrent:',a,b,fa,fb
  print*,LOG(-1.d0)
  STOP'root must be bracketed for dzbrent'
END IF
c=b
fc=fb
DO iter=1,itmax
  IF((fb > 0.d0.AND.fc > 0.d0).OR.(fb < 0.d0.AND.fc < 0.d0)) THEN
    c=a
    fc=fa
    d=b-a
    e=d
  END IF
  IF(ABS(fc) < ABS(fb))THEN
    a=b
    b=c
    c=a
    fa=fb
    fb=fc
    fc=fa
  END IF
  tol1=(2.d0*zero+0.5D0*tol)*ABS(b)
  xm=.5D0*(c-b)
  IF(ABS(xm) <= tol1.OR.fb == 0.d0)THEN
    dzbrent=b
    RETURN
  END IF
  IF(ABS(e) >= tol1.AND.ABS(fa) > ABS(fb))THEN
    s=fb/fa
    IF(a == c)THEN
      p=2.d0*xm*s
      q=1.d0-s
    ELSE
      q=fa/fc
      r=fb/fc
      p=s*(2.d0*xm*q*(q-r)-(b-a)*(r-1.d0))
      q=(q-1.d0)*(r-1.d0)*(s-1.d0)
    END IF
    IF(p > 0.d0)q=-q
    p=ABS(p)
    IF(2.d0*p < DMIN1(3.d0*xm*q-ABS(tol1*q),ABS(e*q)))THEN
      e=d
      d=p/q
    ELSE
      d=xm
      e=d
    END IF
  ELSE
    d=xm
    e=d
  END IF
  a=b
  fa=fb
  IF(ABS(d) > tol1)THEN
    b=b+d
  ELSE
    b=b+SIGN(tol1,xm)
  END IF
  fb=func(b)
END DO
STOP'dzbrent exceeding maximum iterations'
END FUNCTION dzbrent

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION zbrent2(func,x1,x2,tol)

!     Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
REAL(KIND=8) :: x1
REAL(KIND=8) :: x2
REAL(KIND=8) :: tol
INTEGER :: iter
REAL(KIND=8) :: a,b,c,d,e,fa,fb,fc,p,q,r,s,tol1,xm,zbrent2
INTEGER, PARAMETER :: itmax=10000

INTERFACE
   FUNCTION fx(x)
     IMPLICIT NONE
     REAL(KIND=8) :: x,fx
   END FUNCTION fx
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8) :: x,func
   END FUNCTION func
END INTERFACE

a=x1
b=x2
fa=func(a)
fb=func(b)
IF((fa > 0.d0.AND.fb > 0.d0).OR.(fa < 0.d0.AND.fb < 0.d0))THEN
  PRINT*,'Problem in zbrent2:',a,b,fa,fb
  STOP'root must be bracketed for zbrent2'
END IF
c=b
fc=fb
DO iter=1,itmax
  IF((fb > 0.d0.AND.fc > 0.d0).OR.(fb < 0.d0.AND.fc < 0.d0)) THEN
    c=a
    fc=fa
    d=b-a
    e=d
  END IF
  IF(ABS(fc) < ABS(fb))THEN
    a=b
    b=c
    c=a
    fa=fb
    fb=fc
    fc=fa
  END IF
  tol1=(2.d0*zero+0.5D0*tol)*ABS(b)
  xm=.5D0*(c-b)
  IF(ABS(xm) <= tol1.OR.fb == 0.d0)THEN
    zbrent2=b
    RETURN
  END IF
  IF(ABS(e) >= tol1.AND.ABS(fa) > ABS(fb))THEN
    s=fb/fa
    IF(a == c)THEN
      p=2.d0*xm*s
      q=1.d0-s
    ELSE
      q=fa/fc
      r=fb/fc
      p=s*(2.d0*xm*q*(q-r)-(b-a)*(r-1.d0))
      q=(q-1.d0)*(r-1.d0)*(s-1.d0)
    END IF
    IF(p > 0.d0)q=-q
    p=ABS(p)
    IF(2.d0*p < DMIN1(3.d0*xm*q-ABS(tol1*q),ABS(e*q)))THEN
      e=d
      d=p/q
    ELSE
      d=xm
      e=d
    END IF
  ELSE
    d=xm
    e=d
  END IF
  a=b
  fa=fb
  IF(ABS(d) > tol1)THEN
    b=b+d
  ELSE
    b=b+DSIGN(tol1,xm)
  END IF
  fb=func(b)
END DO
STOP'zbrent2 exceeding maximum iterations'
END FUNCTION zbrent2

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE four1(tfour,nn,isign)

!  Fast Fourier Transform. Adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
INTEGER, INTENT(IN) :: isign,nn
REAL(KIND=8), DIMENSION(:),INTENT(INOUT) :: tfour
INTEGER :: i,istep,j,m,mmax,n
REAL(KIND=8) :: tempi,tempr
REAL(KIND=8) :: theta,wi,wpi,wpr,wr,wtemp

n=2*nn
j=1
DO i=1,n,2
   IF(j > i)THEN
      tempr=tfour(j)
      tempi=tfour(j+1)
      tfour(j)=tfour(i)
      tfour(j+1)=tfour(i+1)
      tfour(i)=tempr
      tfour(i+1)=tempi
   END IF
   m=n/2
1  IF((m >= 2).AND.(j > m))THEN
      j=j-m
      m=m/2
      GOTO 1
   END IF
   j=j+m
END DO
mmax=2
DO WHILE(n > mmax)
!2 IF(n > mmax)THEN
   istep=2*mmax
   theta=6.28318530717959d0/(isign*mmax)
   wpr=-2.d0*sin(0.5d0*theta)**2
   wpi=SIN(theta)
   wr=1.d0
   wi=0.d0
   DO m=1,mmax,2
      DO i=m,n,istep
         j=i+mmax
         tempr=sngl(wr)*tfour(j)-sngl(wi)*tfour(j+1)
         tempi=sngl(wr)*tfour(j+1)+sngl(wi)*tfour(j)
         tfour(j)=tfour(i)-tempr
         tfour(j+1)=tfour(i+1)-tempi
         tfour(i)=tfour(i)+tempr
         tfour(i+1)=tfour(i+1)+tempi
      END DO
      wtemp=wr
      wr=wr*wpr-wi*wpi+wr
      wi=wi*wpr+wtemp*wpi+wi
   END DO
   mmax=istep
!   GOTO 2
!END IF
END DO
END SUBROUTINE four1

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE minim(nd,neg,bd1,bd2,tol,amin,delta,nit,xtrm,rep,func)   

!     Given a double precision parametric function func with nd
!     free parameters a with values in a range bracketed by the user, 
!     this routine isolates the local minimum of func.
!     The minimum is searched by calculating the value of func  
!     in neg(1)*...*neg(nd) points of a nd-imensional grid of 
!     parameter values. This is done iteratively by reducing at each 
!     step the grid boundaries until the desired accuracy is reached.
!     The minimum function value is saved as fmin. 
!     The nd parameter values at the minimum and their accuracy 
!     are returned by the vectors amin and delta, respectively. 
!     nit returns the number of iterations required.

!     Input:
!        nd: number of dimensions of the grid, i.e., number of free
!            parameters of func (nitmx is the max. expected number 
!            of iterations)
!        neg(i): number of grid elements along each dim. (>=4)
!        ainf0(i) = min(bd1(n),bd2(n))
!        asup0(i) = max(bd1(n),bd2(n)): boundaries of parameter i,
!                   delta(i)=(asup0(i)-ainf0(i))/(neg(i)-1) is the grid
!                   element size along each dimension
!        tol(i): upper limit to the precision of the parameter values. 

!     Uses coord

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
INTEGER, INTENT(IN) :: nd
INTEGER, DIMENSION(:),INTENT(IN) :: neg
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: bd1,bd2,tol 
REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: amin,delta
INTEGER, INTENT(OUT) :: nit
LOGICAL, INTENT(OUT) :: rep,xtrm
INTEGER, PARAMETER :: ndmx=3,        &       ! expected no. of params
                      ncmx=200000*ndmx, &       ! max. expected size of icoord
                      nitmx=5,      &       ! max. expected no. of iterations
                      nptmx=ndmx*nitmx*ncmx  ! max. expected no. of points where the minimum is calculated
INTEGER :: ngp,npt,iflag,n,i,j,negrid(0:ndmx),imin(ndmx),icoord(ncmx)
REAL(KIND=8) ::  f,fmin
REAL(KIND=8), DIMENSION(ndmx) :: a,ainf,ainf0,asup0,delta0(ndmx)
REAL(KIND=8), DIMENSION(nptmx) :: fsave
REAL(KIND=8), DIMENSION(nptmx,ndmx) :: asave
LOGICAL :: out,equal,verbose
DATA fmin,negrid(0)/1.d+300,1/

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8), DIMENSION(:) :: x
     REAL(KIND=8) :: func
   END FUNCTION func
END INTERFACE

verbose=.TRUE.
DO n=1,nd
   ainf0(n)=min(bd1(n),bd2(n))
   asup0(n)=max(bd1(n),bd2(n))
   negrid(n)=neg(n)
   delta0(n)=(asup0(n)-ainf0(n))/dble(negrid(n)-1)
END DO
DO n=1,nd
   delta(n)=delta0(n)
   ainf(n)=ainf0(n)
   amin(n)=0.d0
   imin(n)=1
END DO

!-----determines the coordinates of the grid
CALL coord(nd,negrid,ngp,icoord)

!-----calculates the minimum of the values taken by func  
!     at the points of the grid. Repeats, reducing delta(i), until 
!     tol(i) is larger than delta(i).
npt=0
nit=1
200 IF(nit.gt.nitmx)THEN
   nit=nit-1
   goto 99
END IF
!verbose=.false.
IF(verbose)THEN
   IF(nit > 1)THEN
      DO n=1,nd
         print*,'parameter no. ',n,': ',REAL(amin(n)),' (tolerance: ',REAL(delta0(n)),')'
      END DO
      print*,'Minimum value of the function:',fmin
   END IF
   write(*,'('' Iteration no.:'',i3)')nit
END IF
iflag=0
DO i=1,ngp
   npt=npt+1
   DO n=1,nd
      a(n)=ainf(n)+(icoord((i-1)*nd+n)-1)*delta(n)
      asave(npt,n)=a(n)
   END DO
   DO j=1,npt-1
      equal=.TRUE.
      DO n=1,nd
         equal=equal.AND.(asave(j,n) == a(n))
         IF(.NOT.equal)GOTO 62
      END DO
      f=fsave(j)
      GOTO 66
62 END DO
   f=func(a)
66 fsave(npt)=f
   IF(f <= fmin)THEN
      iflag=1
      DO n=1,nd
         imin(n)=icoord((i-1)*nd+n)
         amin(n)=a(n)
      END DO
      fmin=f
   END IF
END DO
xtrm=.FALSE.
DO n=1,nd
   xtrm=xtrm.or.(imin(n).eq.1.or.imin(n).eq.negrid(n))
END DO
xtrm=xtrm.and.(iflag.ne.0)
rep=.FALSE.
DO n=1,nd
   rep=rep.OR.(dabs(delta(n)) > tol(n))
END DO
rep=rep.OR.xtrm
IF(rep)THEN
   IF(xtrm)THEN
      IF(nit.eq.1)GOTO 99
      out=.FALSE.
      DO n=1,nd
         out=out.or.(amin(n).lt.ainf0(n).or.amin(n).gt.asup0(n))
      END DO
      IF(out)GOTO 99
      DO  n=1,nd
         delta0(n)=delta(n)
         IF(iflag /= 0)THEN
            ainf(n)=amin(n)-delta(n)*(DBLE(negrid(n))-1)/2.d0
         ELSE
            IF(dabs(delta(n)).gt.tol(n))THEN
               ainf(n)=amin(n)-delta(n)
               delta(n)=2.d0*delta(n)/(DBLE(negrid(n))-1)
            END IF
         END IF
      END DO
      nit=nit+1
      GOTO 200
   ELSE
      DO n=1,nd
         delta0(n)=delta(n)
         IF(delta(n).gt.tol(n))THEN
            ainf(n)=amin(n)-delta(n)
            delta(n)=2.d0*delta(n)/(dble(negrid(n))-1)
         END IF
      END DO
      nit=nit+1
      GOTO 200
   END IF
END IF
99 CONTINUE
IF(verbose)THEN
   DO n=1,nd
      print*,'parameter no. ',n,': ',REAL(amin(n)),' (tolerance: ',REAL(delta(n)),')'
   END DO
   print*,'Minimum value of the function:',fmin
END IF
fmin=1.d300
END SUBROUTINE minim

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE coord(ndim,ndiv,npts,icrd)

!     Used to determine the values of the adjustable parameters
!     on the points of the ndim-ensional grid defined in subroutine 
!     minim.

!     Output:
!        npts: number of grid points
!        icrd(i): ordinals associated to each grid point.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
INTEGER, INTENT(IN) :: ndim
INTEGER, DIMENSION(0:ndim), INTENT(IN) :: ndiv
INTEGER, INTENT(OUT) :: npts,icrd(:)
INTEGER :: n,kmax,imax,k,j,i,ipos

npts=1
DO n=1,ndim
   npts=npts*ndiv(n)
END DO
kmax=1
imax=npts
DO n=1,ndim
   kmax=ndiv(n-1)*kmax
   imax=imax/ndiv(n)
   DO k=1,kmax
      DO j=1,ndiv(n)
         DO i=1,imax
            ipos=ndim*(i-1+(j-1)*imax+(k-1)*(npts/kmax))+n
            icrd(ipos)=j
         END DO
      END DO
   END DO
END DO
END SUBROUTINE coord

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE fourier(ne,y,ns,fy,dt,ind)

!  Calculates the direct or inverse Fourier transform (Bracewell system 1 
!  or frequency) to the input value of IND (+1 or -1, respectively), of a 
!  real symmetric function Y (input) known at NE (input) points separated 
!  by DT (input) after filling with 0's up to NS (input, power of 2 + 1, 
!  NS.LE.4097) points. The result is FY (output) always of NS points. On 
!  output, DT is the bin of the transform.                                            
                                                                          
!  Vectors Y and FY may be the same in the calling program.                
                                                                             
!  REQUIRED SUBROUTINES: FFT                                               

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 

IMPLICIT NONE
INTEGER, INTENT(IN) :: ne,ns,ind
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: Y
REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: FY
REAL(KIND=8), INTENT(INOUT) :: dt

INTEGER :: I,NU,ND,ND2
REAL(KIND=8), DIMENSION(2*SIZE(Y)) :: FY0,FYSYM

! Calculates the power of 2
NU=INT(LOG(DBLE(NS))/LOG(2.d0))  

! Multiplies the data by the interval (necessary for the transform)

DO I=1,NE
   FY(I)=Y(I)*DT
END DO

! If there are NS points, simetrizes. Otherwise fills with 0's

IF (NE.EQ.NS) GO TO 3

! Fills with 0's

DO I=NE+1,NS
   FY(I)=0.d0
END DO
3 CONTINUE

! Simetrizes

ND=2*(NS-1)
ND2=NS+NS
NU=NU+1
FYSYM(1:NS)=FY(1:NS)
DO I=NS+1,ND
   FYSYM(I)=FY(ND2-I)
END DO

! Imaginari part equal to 0

DO I=1,ND
   FY0(I)=0.
END DO

! Transform

CALL FFT(ND,FYSYM,FY0,NU,IND)
FY(1:NS)=FYSYM(1:NS)
! Interval of the transform

DT=1.D0/DT/ND

END SUBROUTINE fourier

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE fourier_odd(ne,y,ns,fy,dt,ind)

!  Calculates the direct or inverse Fourier transform (Bracewell system 1 
!  or frequency) to the input value of IND (+1 or -1, respectively), of a 
!  real symmetric function Y (input) known at NE (input) points separated 
!  by DT (input) after filling with 0's up to NS (input, power of 2 + 1, 
!  NS.LE.4097) points. The result is FY (output) always of NS points. On 
!  output, DT is the bin of the transform.                                            
                                                                          
!  Vectors Y and FY may be the same in the calling program.                
                                                                             
!  REQUIRED SUBROUTINES: FFT                                               

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 

IMPLICIT NONE
INTEGER, INTENT(IN) :: ne,ns,ind
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: Y
REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: FY
REAL(KIND=8), INTENT(INOUT) :: dt

INTEGER :: I,NU,ND,ND2
REAL(KIND=8), DIMENSION(2*SIZE(Y)) :: FY0,FYSYM

! Calculates the power of 2
NU=INT(LOG(DBLE(NS))/LOG(2.d0))  

! Multiplies the data by the interval (necessary for the transform)

DO I=1,NE
   FY(I)=Y(I)*DT
END DO

! If there are NS points, simetrizes. Otherwise fills with 0's
IF (NE.EQ.NS) GO TO 3

! Fills with 0's

DO I=NE+1,NS
   FY(I)=0.d0
END DO
3 CONTINUE

! Anti-simetrizes

ND=2*(NS-1)
ND2=NS+NS
NU=NU+1
FYSYM(1:NS)=FY(1:NS)
DO I=NS+1,ND
   FYSYM(I)=-FY(ND2-I)
END DO

! Imaginary part equal to 0

DO I=1,ND
   FY0(I)=0.
END DO

! Transform

CALL FFT(ND,FYSYM,FY0,NU,IND)
FY(1:NS)=FY0(1:NS)

! Interval of the transform

DT=1.D0/DT/ND

END SUBROUTINE fourier_odd


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE FFT(N,XREAL,XIMAG,NU,IND)   
                                                                   
!  Fast Fourier transform, direct or inverse according to value of IND 
!  (+1 or -1, respectively). N is the number of equidistant points, a  
!  equal to a power of two NU (also input) and XREAL and XMAG are the  
!  real and imaginary parts of the input or output functions.          
                                                                      
IMPLICIT NONE
INTEGER, INTENT(IN) :: n,nu,ind
REAL(KIND=8),DIMENSION(N), INTENT(INOUT) ::  XREAL,XIMAG

INTEGER :: N2,NU1,K,K1,K1N2,L,I
REAL(KIND=8) :: P,ARG,C,S,TREAL,TIMAG
REAL(KIND=8),PARAMETER :: pi=3.141592653589793d0, dospi=2.d0*pi

N2=N/2
NU1=NU-1
K=0
DO L=1,NU
102 CONTINUE
   DO I=1,N2
      P=DBLE(IBITR(K/2**NU1,NU))
      ARG=DOSPI*P/N
      C=COS(IND*ARG)
      S=SIN(IND*ARG)
      K1=K+1
      K1N2=K1+N2
      TREAL=XREAL(K1N2)*C+XIMAG(K1N2)*S
      TIMAG=XIMAG(K1N2)*C-XREAL(K1N2)*S
      XREAL(K1N2)=XREAL(K1)-TREAL
      XIMAG(K1N2)=XIMAG(K1)-TIMAG
      XREAL(K1)=XREAL(K1)+TREAL
      XIMAG(K1)=XIMAG(K1)+TIMAG
      K=K+1
   END DO
   K=K+N2
   IF(K.LT.N) GO TO 102
   K=0
   NU1=NU1-1
   N2=N2/2
END DO
DO K=1,N
   I=IBITR(K-1,NU)+1
   IF(I.LE.K) CYCLE
   TREAL=XREAL(K)
   TIMAG=XIMAG(K)
   XREAL(K)=XREAL(I)
   XIMAG(K)=XIMAG(I)
   XREAL(I)=TREAL
   XIMAG(I)=TIMAG
END DO
END SUBROUTINE FFT

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION IBITR(J,NU)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
INTEGER, INTENT(IN) :: J,NU
INTEGER :: IBITR,I,J1,J2
J1=J
IBITR=0
DO I=1,NU
   J2=J1/2
   IBITR=IBITR*2+(J1-2*J2)  
   J1=J2
END DO
END FUNCTION IBITR

END MODULE math_lib
