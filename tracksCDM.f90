!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
MODULE massrange

IMPLICIT NONE

INTEGER, PARAMETER :: nm=10        ! # of log-bins in 1 dex of halo masses 

! Before compilation, comment/uncomment the blocks below to choose the halo mass range

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!LOGICAL, PARAMETER :: lowm=.TRUE.  ! halo masses ranging from 10^-6 M_o to 10^7 M_o
!INTEGER, PARAMETER :: ndx=35
!INTEGER, PARAMETER :: nmi=38+3*nm+2
!REAL(KIND=8), PARAMETER :: hmup=5.d12
!REAL(KIND=8), PARAMETER :: gmup=5.d12
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
LOGICAL, PARAMETER :: lowm=.FALSE.  ! halo masses ranging from 10^8 M_o to 10^16 M_o
INTEGER, PARAMETER :: ndx=30
INTEGER, PARAMETER :: nmi=38+3*nm+6
REAL(KIND=8), PARAMETER :: hmup=5.d18
REAL(KIND=8), PARAMETER :: gmup=5.d16
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

END MODULE massrange

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
MODULE global_defs

USE tools
USE massrange

IMPLICIT NONE

! MAIN DIMENSIONS (interpolation matrix and output arrays):
INTEGER, PARAMETER :: nz=31    ! # of z-rows in redshift interval [zmax,zobs]
INTEGER, PARAMETER :: nt=2     ! # of equiprobable formation time intervals
INTEGER, PARAMETER :: nr=2     ! # of radial bins in halos at ct (.ge. nrf+1)

! OTHER DIMENSIONS
INTEGER, PARAMETER :: nrf=1    ! 1 if only one region inside R_f or 2 if inner region (with mass fraction xi) plus remaining region
INTEGER, PARAMETER :: net=5
INTEGER, PARAMETER :: nai=7
INTEGER, PARAMETER :: npi=8
INTEGER, PARAMETER :: npp=10
INTEGER, PARAMETER :: npt=18
INTEGER, PARAMETER :: nph=20
INTEGER, PARAMETER :: npm=24
INTEGER, PARAMETER :: npn=50
INTEGER, PARAMETER :: npc=91
INTEGER, PARAMETER :: npl=220
INTEGER, PARAMETER :: nrv=150

REAL(KIND=8) :: xmax
REAL(KIND=8) :: hmlow                 ! for zmax,zobs, see module cosmology

! CORRESPONDING STEPS
REAL(KIND=8) :: stpm,stpb

! SWITCHES
INTEGER :: option    !used to decide if we find the sigma_th-sigma_g relation or to generate profiles and fit NFW/Einasto
INTEGER :: chmass    !used to choose halo mass range 
INTEGER :: mdef      !used to choose mass definition
INTEGER :: prof_cons !used to choose constrained/unconstrained profile fits
INTEGER :: cosmo     !used to choose the cosmology, must change also file .par where the cosmological parameters are stored

REAL(KIND=8) :: prof,adcon,molec,nigm,hcool,op3s,loss,heat,dins,tvir,tfal,orbit,capt,cstb,strip,tides,trans,satmom,smerg,agn,bhseed,agnsat,agncool,diff,frame  
DATA prof,adcon,molec,nigm,hcool,op3s,loss,heat,dins,tvir,tfal,orbit,capt,cstb,strip,tides,trans,satmom,smerg,agn,bhseed,agnsat,agncool,diff,frame/25*0.d0/
! see program amiga for the actual values of switches

! OTHER INTEGERS
INTEGER, PARAMETER :: ip=2
INTEGER, PARAMETER :: ni=nz+2+ip
INTEGER, PARAMETER :: ez=ni-ip
INTEGER, PARAMETER :: nme=ndx+1
INTEGER, PARAMETER :: nmh=nM*ndx+1
INTEGER, PARAMETER :: nmh1=nmh+1
INTEGER, PARAMETER :: nmr=nmh+nMi+2*nM
INTEGER, PARAMETER :: nmt=nM*16
INTEGER, PARAMETER :: npp2=npp+npt+1

! RELATED INTEGERS

! MATHEMATICAL CONSTANTS
REAL(KIND=8), PARAMETER :: r13=1.d0/3.d0
REAL(KIND=8), PARAMETER :: r23=2.d0/3.d0
REAL(KIND=8), PARAMETER :: r43=2.d0*r23
REAL(KIND=8), PARAMETER :: r53=5.d0/3.d0
REAL(KIND=8), PARAMETER :: r56=5.d0/6.d0
REAL(KIND=8), PARAMETER :: pi=3.141592653589793d0                   ! pi and related constants
REAL(KIND=8), PARAMETER :: pi2=pi*2.d0
REAL(KIND=8), PARAMETER :: pi4=pi2*2.d0
REAL(KIND=8), PARAMETER :: pi43=pi*r43
REAL(KIND=8), PARAMETER :: sqpi=1.77245385d0                        ! sqrt(pi)
REAL(KIND=8), PARAMETER :: sq2pi=0.797884561d0                      ! sqrt(2/pi)
REAL(KIND=8), PARAMETER :: sq2=1.414213562d0                        ! sqrt(2)
REAL(KIND=8), PARAMETER :: lpi2=1.837877067d0                       ! ln(2pi)
REAL(KIND=8), PARAMETER :: l2=0.693147181d0                         ! ln(2)
REAL(KIND=8), PARAMETER :: l3=1.098612289d0                         ! ln(3)
REAL(KIND=8), PARAMETER :: l05=-0.6931472d0                         ! ln(.5)
REAL(KIND=8), PARAMETER :: loge=1.d0/2.302585092994046d0            ! log10(e)
REAL(KIND=8), PARAMETER :: log3h=0.17609126d0                       ! log10(3/2)
REAL(KIND=8), PARAMETER :: lgaus=LOG(pi2**1.5d0)                    ! log factor gaussian vol

! PHYSICAL CONSTANTS
REAL(KIND=8), PARAMETER :: mpc=2.93892d67                           ! Mpc^3, in MKS
REAL(KIND=8), PARAMETER :: mpcc=mpc*1.d6                            ! Mpc^3, in CGS
!REAL(KIND=8), PARAMETER :: gyr=9.7789439d2                          ! Gyr, in (km/s/Mpc)^{-1}
REAL(KIND=8), PARAMETER :: gyr=9.778131302d2                        ! Gyr, in (km/s/Mpc)^{-1}
REAL(KIND=8), PARAMETER :: gyrc=gyr*1.d5                            ! Gyr, in (cm/s/Mpc)^{-1} 
REAL(KIND=8), PARAMETER :: gyrs=3.15576d16                          ! Gyr, in s
REAL(KIND=8), PARAMETER :: loeg=7.537929d61                         ! 1 Lo, in eV/Gyr

REAL(KIND=8), PARAMETER :: cnor=0.322183635d0                       ! norm constant in lnPS_cos
REAL(KIND=8), PARAMETER :: cu=16.16938409d0                         ! to convert units
REAL(KIND=8), PARAMETER :: mo=1.989d33                              ! M_o, in g
REAL(KIND=8), PARAMETER :: gcgs=6.67428d-8                           ! G, in CGS
REAL(KIND=8), PARAMETER :: fgcgs=0.64460721d9*gcgs                  ! 0.64460721d9 times G in CGS
!REAL(KIND=8), PARAMETER :: gcos=4.49835961d-15                      ! G, in Mpc^3/Gyr^2/M_o
REAL(KIND=8), PARAMETER :: gcos=4.499450941d-15                     ! G, in Mpc^3/Gyr^2/M_o
REAL(KIND=8), PARAMETER :: gkm=gcos*gyr*gyr                         ! G, in (Km/s)^2*Mpc/M_o
REAL(KIND=8), PARAMETER :: gcm=gkm*mo*1.d10                         ! G, in (cm/s)^2 Mpc/gr
REAL(KIND=8), PARAMETER :: sqrg=6.70698115d-8                       ! sqrt of G in Mpc^3/Gyr^2/M_o
REAL(KIND=8), PARAMETER :: kb=1.38060505d-16                        ! Boltzmann's k, in erg/K
REAL(KIND=8), PARAMETER :: vc=2.99792458d10                         ! c, in CGS
REAL(KIND=8), PARAMETER :: lk=-15.85993054d0                        ! log10(kb), in erg/K
REAL(KIND=8), PARAMETER :: jfac=2.2913217062d0
REAL(KIND=8), PARAMETER :: jh=6.62606876d-27
REAL(KIND=8), PARAMETER :: f21=1.d21
REAL(KIND=8), PARAMETER :: k53=5d0*kb/3.d0                          ! 5*k/3 in erg/K
REAL(KIND=8), PARAMETER :: k32=1.5D0*kb                             ! 3*k/2 in erg/K
REAL(KIND=8), PARAMETER :: lo=3.826d33                              ! L_o in CGS
REAL(KIND=8), PARAMETER :: eno=lo*gyrs                              ! energy rad. by sun in 1 Gyr, in ergs 
REAL(KIND=8), PARAMETER :: pa=.315970346d0                          ! for primordial abundance
REAL(KIND=8), PARAMETER :: sqra=0.840832920d0                       ! ??
REAL(KIND=8), PARAMETER :: lbolo=1.9588d0                           ! 1 L_o, in L_o_B
REAL(KIND=8), PARAMETER :: pm=1.6726d-24                            ! proton mass, in CGS
REAL(KIND=8), PARAMETER :: pmo=pm/mo                                ! proton mass, in M_o

! PHYSICAL PARAMETERS
REAL(KIND=8), PARAMETER :: delc0=1.6864702D0                       ! density contrast sph. collapse
REAL(KIND=8), PARAMETER :: delm=0.26D0                              ! threshold in DM/M for merger
REAL(KIND=8), PARAMETER :: gpi=1.2d0                                ! polytropic index

!FORMALISM PARAMETERS
REAL(KIND=8) :: param_d,param_s0,param_s1,param_s2,param_A

! COMMONS
INTEGER :: i,m,idif,ndif                                            !/im/
INTEGER :: mi,it                                                    !/fixind/
INTEGER :: mb,sf,cf,dhh,ac,ad,ir                                    !/varind/
INTEGER :: nra                                                      !/ccap/ 
INTEGER :: nsmr                                                     !/csmr/
INTEGER :: ist,ita                                                  !/satind/
INTEGER :: ii,iio,ihe                                               !/intoz/
INTEGER :: s4,s6                                                    !/intmg/
INTEGER :: k1,k2                                                    ! nou
INTEGER :: iprof                                                    !/cprf/
INTEGER :: n0                                                       !/zbrak/
INTEGER :: ispe                                                     !/choice/
INTEGER :: iuni                                                     !/cospr/
INTEGER :: imf                                                      !/cimf/
INTEGER :: nord                                                     !/dfunc/
INTEGER, DIMENSION(nt) :: mup,mminio                                !/cmmin/
INTEGER, DIMENSION(nt) :: gtmax,sdmax,mbmax,abmax,acmax,admax,bhmax,dhmax !/cacc2/
REAL, DIMENSION(nmh) :: rlm,rln                                     !/cenvi/
REAL(KIND=8) :: rvar                                                !/dfunc/
REAL(KIND=8) :: rvarg                                               !/dfunc/
REAL(KIND=8) :: zmax,zobs                                           ! nou
REAL(KIND=8) :: ft,hmf,xsf                                          !/hafor/
REAL(KIND=8) :: lrhopi43,lrb                                        !/cosdn/
REAL(KIND=8) :: profcen,rfac,delv                                   !/cnfwc/
REAL(KIND=8) :: lhmst                                               !/chmst/
REAL(KIND=8) :: zcrit                                               !/stzz/
REAL(KIND=8) :: omm,lam0                                            !/cospr/
REAL(KIND=8) :: deld,deli                                           !/gamrg/
REAL(KIND=8) :: hrf                                                 !/hafor/
REAL(KIND=8) :: lrcom,dccom                                         !/csmr/
REAL(KIND=8) :: gdens                                               !/creac/
REAL(kind=8) :: cc,fcc,gcc                                          !/ccfg/
REAL(KIND=8) :: lambdam ! sigma0 (param) i satmom (data)            !/haspin/
REAL(KIND=8) :: ahe ! smm (param)                                   !/gahern/
REAL(KIND=8) :: hr                                                  !/harfc/
REAL(KIND=8) :: rs                                                  !/hascl/
REAL(KIND=8) :: hmc,hrc                                             !/chmt/
REAL(KIND=8) :: ccf,ffb,rb,w,aw,ncon                                !/cacpar/
REAL(KIND=8) :: edb                                                 !/cdmb/
REAL(KIND=8) :: st,ct,dt,lft,lthgf,hmm                              !/cct/ i nou
REAL(KIND=8) :: lt1                                                 !/clt1/
REAL(KIND=8) :: lozc,lozf                                           !/cesp/
REAL(KIND=8) :: rho                                                 !/crho
REAL(KIND=8) :: rhoc                                                !/hascl/
REAL(KIND=8) :: hmn,ixsc,sqrfc                                      !/chmt/
REAL(kind=8) :: ldelm,idelm1                                        !/hamgr/          
REAL(KIND=8) :: ztop,zbot,zh                                        !/stzz/
REAL(KIND=8) :: rcfac                                               !/harfc/
REAL(KIND=8) :: rcfacc                                              !/harad/
REAL(KIND=8) :: xi                                                  !/hainn/
REAL(KIND=8) :: xt                                                  !/hatrunc/
REAL(KIND=8) :: cn                                                  !/ccn/
REAL(KIND=8) :: cn2   
REAL(KIND=8) :: de 
REAL(KIND=8) :: yprob
REAL(KIND=8) :: trq                                                 !/ctrq/
REAL(KIND=8) :: mhgf,mhgc,mhgt                                      !/hahot/
REAL(KIND=8) :: q2,cc1,fcc1                                         !/chmr/
REAL(KIND=8) :: lrhgf                                               !/hahot/
REAL(KIND=8) :: hreal,gam                                           !/sfunc/
REAL(KIND=8) :: tn,kco                                              !/choice/
REAL(KIND=8) :: lrbot                                               !/rangs/
REAL(KIND=8) :: ct1                                                 !/clt1/
REAL(KIND=8) :: potf,ktn                                            !/cnpol/
REAL(KIND=8) :: rv2min,stprv2                                       !/cstrip/
REAL(KIND=8) :: bmcg,potb,rmed,rhalmed,pothal,ether                 !/sreg/
REAL(KIND=8) :: hstp,acnsat                                         !/cacc1/
REAL(KIND=8) :: mhg2,rad2                                           ! nou
REAL(KIND=8) :: mbh2                                                !/cmbh/
REAL(KIND=8) :: hbmf,hbmt                                           !/habar/
REAL(KIND=8) :: lnhgf                                               !/hahot/
REAL(KIND=8) :: fac,cca                                             !/cfdr/
REAL(KIND=8) :: stm,stz                                             !/csat/
REAL(KIND=8) :: nesta                                               !/intng/
REAL(KIND=8) :: tobs                                                !/samzb/
REAL(KIND=8) :: tbfac,tp,tpa                                        !/ctbhm/
REAL(KIND=8) :: fch,fche                                            ! nou
REAL(KIND=8) :: ih0,d0,tomm,tommt023,dc0                            !/cospr/
REAL(KIND=8) :: ch0 ! dlmin (param)                                 !/cdlmin/
REAL(KIND=8) :: u1,u2                                               !/zbrac/
REAL(KIND=8) :: hm1,rvir,alph,r_2,rhos,alpha,rNFW,lms
REAL(KIND=8) :: halom                                               !halo mass found from the delta-Rf track
REAL(KIND=8) :: intmass                                             !Total mass integrated from the final density profile
REAL(KIND=8) :: NFWmass                                             !Total mass integrated from the NFW density profile
REAL(KIND=8) :: delta0
REAL(KIND=8) :: dm
REAL(KIND=8) :: ozdiff
REAL(KIND=8), DIMENSION(1) :: ham                                   !/cflt/
REAL(KIND=8), DIMENSION(:), POINTER :: xb1,xb2                      !/zbrak/
REAL(KIND=8), DIMENSION(nz) :: lct                                  !/intct/
REAL(KIND=8), DIMENSION(ni) :: slt,mslt                             !/intst/
REAL(KIND=8), DIMENSION(ni) :: oz,loz,mloz,iof                      !/intoz/
REAL(KIND=8), DIMENSION(ni) :: lrcfac,mlrcfac                       !/intrc/
REAL(KIND=8), DIMENSION(ni) :: mau                                  !/intnum/
REAL(KIND=8), DIMENSION(ni) :: ldel,ltddel,mldel,mltddel            !/intdel/
REAL(KIND=8), DIMENSION(ni) :: lbf,lcz                              !/intigm/
REAL(KIND=8), DIMENSION(nt) :: bsmin                           !/cacc2/
REAL(KIND=8), DIMENSION(nr) :: rad                                  !/harad/
REAL(KIND=8), DIMENSION(nr) :: radm,sigm                            !/haint/
REAL(KIND=8), DIMENSION(npt) :: mgs                                 !/cacz/
REAL(KIND=8), DIMENSION(npt) :: amgs                                !/cacc1/
REAL(KIND=8), DIMENSION(npt) :: stp,lml,del,tddel                   ! nou
REAL(KIND=8), DIMENSION(npi) :: lz                                  !/cflt/
REAL(KIND=8), DIMENSION(npn) :: lhm1                                !/clhm1/
REAL(KIND=8), DIMENSION(nrv) :: lxt,lxg,mlxt
REAL(KIND=8), DIMENSION(npc) :: tm
REAL(KIND=8), DIMENSION(nmr) :: alrcom                              !/ints0/
REAL(KIND=8), DIMENSION(nmr) :: sls0,lmh,mlmh                       !/intmh/
REAL(KIND=8), DIMENSION(nmr) :: ls0,ldls0,mls0,mldls0               !/ints0/
REAL(KIND=8), DIMENSION(nmr) :: lrcomg,lsg0,lsg1,lsg2,mlsg0,mlsg1,mlsg2,ldlsg0,mldlsg0
REAL(KIND=8), DIMENSION(nmh) :: aux                                 !/cmass/
REAL(KIND=8), DIMENSION(nmh) :: lmr                                 !/clmr/
REAL(KIND=8), DIMENSION(nmh) :: chm,hmas                            !/cmass/
REAL(KIND=8), DIMENSION(nmh1) :: lrx                                !/inthm/
REAL(KIND=8), DIMENSION(npp2) :: rro,mro,mmro                       !/camom/
REAL(KIND=8), DIMENSION(npp2-1) :: gpot,ndm,nbm,ixcv                !/cfdr/
REAL(KIND=8), DIMENSION(nmh,npt) :: lm,ls                           !/clm/ modificat
REAL(KIND=8), DIMENSION(ni,nmh1) :: lhm,mlhm                        !/inthm/
REAL(KIND=8), DIMENSION(ni,nmh1) :: lfr,mlfr                        !/intfr/
REAL(KIND=8), DIMENSION(ni,nmh1) :: fri,mfri                        !/intfri/
REAL(KIND=8), DIMENSION(-npp:npt) :: hg                             !/chaz/
REAL(KIND=8), DIMENSION(nr,nmh,nt) :: radmc,sigmc                   !/haintc/
REAL(KIND=8), DIMENSION(nmh,nz,nt) :: lhft,lhmf                     !/intcg/
REAL(KIND=8), DIMENSION(1024+1) :: xxx2,dmdr,dr,sigma2p,sigma2t,dedmp,vp,dvpdmp,dlmdlr,dledlmp, &
                                   lhmass,lldr,letar,ldledlmp,ldlrdlm,sigma2tilde,hmass_2,lrh,lrhoh
LOGICAL :: hac                                                      !/chac/x
LOGICAL :: success                                                  !/zbrac/
LOGICAL :: calc
INTEGER :: kcont,imass,jmass
REAL(KIND=8) :: mvir,rv,mv,mvps,x2,x3,igam,h_z,ih_z,barx


INTEGER :: nitt,nit1,indi,indf
REAL(KIND=8) :: z,a1,a2,a3,b1,b2,a1min,a2min,a3min,a1max,a2max,a3max,b1min,b1max,b2min,b2max,ll,ul,c1,c1min,c1max,m1,m2
REAL(KIND=8) :: qapprox,deltapk,qm,cnn
REAL(KIND=8), DIMENSION(3) :: bd1,bd2,tol,amin,erra,bd11,bd12,tol1,amin1,erra1
INTEGER, DIMENSION(3) :: npg,npg1
LOGICAL :: xtrm,rep,xtrm1,rep1
INTEGER, PARAMETER :: nfit=5,npn1=npn+1,nrr=250,npr=1025,npf=1025,npf2=npf*2,nrr2=2**14+1
REAL(KIND=8), DIMENSION(3*nrr2) :: xxxn,sg02n,sg02n_rp,deltazi,deltam,ldeltam,deltac,qrf,etar,etarh,hmass,lrp,lrp2,lrhop
REAL(KIND=8), DIMENSION(2*nrr-1) :: xf2,yf2,sg02n_conv

DATA w,aw /0.8D0,0.85D0/
DATA hac /.FALSE./

END MODULE global_defs

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

MODULE cosmology

USE global_defs

IMPLICIT NONE

REAL(KIND=8) :: betav                                               !/betav/
!REAL(KIND=8), DIMENSION(nmr) :: ls0,ldls0,mls0,mldls0               !/ints0/


!     subroutine specmom_cos
!     subroutine specmomg_cos
!     subroutine norm_cos
!     subroutine delz_cos
!     function dtdoz_cos
!     subroutine hal_cos
!     subroutine dlrdoz_cos
!     subroutine dozdr_cos
!     subroutine derla_cos
!     subroutine dlrom_cos
!     subroutine intnm_cos
!     function lftime_cos
!     function lmprog_cos
!     function dfrdt_cos
!     function indmp_cos
!     function CDMth_cos
!     function CDMga_cos
!     function CDM_cos
!     function th_cos
!     function capd_cos
!     function cosmict_cos
!     function cosmicts_cos
!     function sn2th_cos
!     function sn2ga_cos
!     function sfth_cos
!     function dvbn_cos
!     function dldvdz_cos
!     function foz_cos
!     function mar_cos
!     function smr_cos
!     function smrlc1_cos
!     function smrlc2_cos
!     function scr_cos
!     function lnPS_cos
!     function lnST_cos
!     function deltac_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

INTERFACE th_cos
   MODULE PROCEDURE th_cos,ths_cos
END INTERFACE

INTERFACE smr_cos
   MODULE PROCEDURE smr_cos,smrs_cos
END INTERFACE

INTERFACE lnPS_cos
   MODULE PROCEDURE lnPS_cos,lnPSs_cos
END INTERFACE

CONTAINS

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE specmom_cos(dc0,s8,hmup,stpm,lrlow,lrup)

!     Spectral moments and related quantities

!     Uses: norm_cos,sn2th_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : det5,spl,spline
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: dc0    ! critical overdensity for collpase
REAL(KIND=8), INTENT(IN) :: s8     ! normalization factor sigma_8
REAL(KIND=8), INTENT(IN) :: hmup   ! upper halo mass (z=0), in M_o
REAL(KIND=8), INTENT(IN) :: stpm   ! log mass step, in M_o
REAL(KIND=8), INTENT(OUT) :: lrlow ! log of minimum comoving scale (z=0), in Mpc

REAL(KIND=8), INTENT(OUT) :: lrup  ! log of maximum comoving scale (z=0), in Mpc
INTEGER :: j,klo
REAL(KIND=8) :: rnor,lcrnor,stp,yy

!--bounds in comoving scale needed for the wanted range in masses at z=0
lrlow=(LOG(hmup)-stpm*(nmh+nmi-1)-lrhopi43)/3.d0
lrup=(LOG(hmup)+stpm*2*nm-lrhopi43)/3.d0
!--normalization scale
rnor=8.d0/hreal
stp=stpm/3.d0
!--spectral moments for top hat window
alrcom=arth(lrlow,stp,nmr)
lmh=lrhopi43+3.d0*alrcom ! log mass (at z=0)
IF(ispe == 1)THEN
   lcrnor=-.5D0*(tn+3.d0)*LOG(rnor/s8) ! includes sigma_8
   DO j=1,nmr ! SF spectrum
      ls0(j)=-.5D0*(tn+3.d0)*alrcom(j)-lcrnor
!      IF(MOD(j,nm) == 1) WRITE(*,'(a,i3,a,i3)')'    Scale # ',j,' out of ',nmr
   END DO
ELSE
   CALL norm_cos(rnor,cn2)
   cn2=cn2*s8*s8 ! norm. includes sigma_8
   OPEN(19,file='moments_nn_lm.dat', status='old') ! 0th-order moment (not normalized) is read from datafile (lowmass)
   DO j=1,nmr ! CDM spectrum
      IF(lowm)THEN
         READ(19,*)rvar,yy
         ls0(j)=yy+0.5d0*LOG(cn2)
      ELSE
         rvar=EXP(alrcom(j)) ! for sn2th_cos
         ls0(j)=.5D0*LOG(sn2th_cos(0)*cn2)
      END IF
!      IF(MOD(j,nm) == 1) WRITE(*,'(a,i3,a,i3)')'    Scale # ',j,' out of ',nmr
   END DO
CLOSE(19)
END IF
sls0=-ls0
lrb=alrcom(1)
!--prepare interpolations
CALL det5(stp,ls0,ldls0) ! log derivative of ls0
CALL spline(alrcom,ls0,mls0) ! prepares interpolation
CALL spline(alrcom,ldls0,mldls0)
!--M_*(t0) = s0(dc0)
CALL spline(sls0,lmh,mlmh)
klo=1
lhmst=spl(sls0,lmh,mlmh,-LOG(dc0),klo)
!--log of upper scale at z=0
lrbot=alrcom(2) ! top lower scale at z=0
lrlow=alrcom(nmi+1)
lrup=alrcom(nmr-2*nm) ! upper scale at z=0 (see rangz)
END SUBROUTINE specmom_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


SUBROUTINE specmomg_cos(dc0,s8,hmup,stpm)

!     Spectral moments (Gaussian filter) and related quantities

!     Uses: norm_cos,sn2th_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : det5,spline
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: dc0    ! critical overdensity for collapse
REAL(KIND=8), INTENT(IN) :: s8     ! normalization factor sigma_8
REAL(KIND=8), INTENT(IN) :: hmup   ! upper halo mass (z=0), in M_o
REAL(KIND=8), INTENT(IN) :: stpm   ! log mass step, in M_o
INTEGER :: j,klo
REAL(KIND=8) :: rnor,lcrnor,stp,lrlow,lrup,yy

!--bounds in comoving scale needed for the wanted range in masses at z=0
lrlow=(LOG(hmup)-stpm*(nmh+nmi-1)-lrhopi43)/3.d0
lrup=(LOG(hmup)+stpm*2*nm-lrhopi43)/3.d0
!--normalization scale
rnor=8.d0/hreal
stp=stpm/3.d0
!--spectral moments for Gaussian window
lrcomg=arth(lrlow,stp,nmr)
IF(ispe == 1)THEN
   lcrnor=-.5D0*(tn+3.d0)*LOG(rnor)-LOG(s8) ! includes sigma_8
   DO j=1,nmr ! SF spectrum
      lsg0(j)=-.5D0*(tn+3.d0)*lrcomg(j)-lcrnor
      lsg1(j)=-.5D0*(tn+5.d0)*lrcomg(j)+LOG(.5D0*(tn+3.d0))-lcrnor
      lsg2(j)=-.5D0*(tn+7.d0)*lrcomg(j)+LOG(.5D0*(tn+3.d0)*.5D0*(tn+5.d0))-lcrnor
!      IF(MOD(j,nm) == 1) WRITE(*,'(a,i3,a,i3)')'    Scale # ',j,' out of ',nmr
   END DO
ELSE
   CALL norm_cos(rnor,cn2)
   cn2=cn2*s8*s8 ! norm. includes sigma_8
   DO j=1,nmr ! CDM spectrum
      rvar=EXP(lrcomg(j)) ! for sn2ga_cos
      lsg0(j)=.5D0*LOG(sn2ga_cos(0)*cn2)
      lsg1(j)=.5D0*LOG(sn2ga_cos(1)*cn2)
      lsg2(j)=.5D0*LOG(sn2ga_cos(2)*cn2)
!      IF(MOD(j,nm) == 1) WRITE(*,'(a,i3,a,i3)')'    Scale # ',j,' out of ',nmr
   END DO
END IF
!--prepare interpolations
CALL det5(stp,lsg0,ldlsg0) ! log derivative of ls0
CALL spline(lrcomg,lsg0,mlsg0) ! prepares interpolation
CALL spline(lrcomg,lsg1,mlsg1) ! prepares interpolation
CALL spline(lrcomg,lsg2,mlsg2) ! prepares interpolation
CALL spline(lrcomg,ldlsg0,mldlsg0)
END SUBROUTINE specmomg_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE norm_cos(rnor,c2)

!     Normalizes (to sigma_8=1 in spheres) the power spectrum at t0.

!     Uses: sn2th_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: rnor   ! normalization scale (in Mpc),i.e., s_g(rnor)=1.
REAL(KIND=8), INTENT(OUT) :: c2    ! square of the normalization factor.
REAL(KIND=8) :: c2th

rvar=DBLE(rnor)
IF(ispe < 2)THEN
   c2th=0.d0 ! for norm. with sfth_cos
   IF(tn == 0.d0)c2th=pi/6.d0
   IF(tn == -1.d0)c2th=0.25D0
   IF(tn == -2.d0)c2th=pi/15.d0
   IF(c2th == 0.d0)c2th=sn2th_cos(0)
   c2=rnor**(tn+3.d0)/c2th ! norm. fact. for analit. expr. Gauss. window
ELSE
   c2=1.d0/sn2th_cos(0) ! for norm. with CDMth_cos
END IF
END SUBROUTINE norm_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE delz_cos(oz,t,d,dtdoz,delta,ddeldt,at)

!     Calculates: delta_c(t) extrapolated to t_0 (with the factor delta_c0 calculated in fixcosmo and for a bias parameter b=1),
!     the absolute value of its time derivative (in Gyr**{-1}), as well as the time corresponding to z, the growth factor D(t),
!     and the derivative dt/d(1+z). The expansion factor, a(t), and its logarithmic time derivative, dlna/dlnt, are also calculated

!     Uses: cosmicts_cos,capd_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: oz      ! 1+z
REAL(KIND=8), INTENT(OUT) :: t      ! t(z), in Gyr
REAL(KIND=8), INTENT(OUT) :: d      ! D(t)
REAL(KIND=8), INTENT(OUT) :: dtdoz  ! 1+z derivative of t
REAL(KIND=8), INTENT(OUT) :: delta  ! delta for collapse at t extrapolated to t0
REAL(KIND=8), INTENT(OUT) :: ddeldt ! absolute value of the time derivative of the previous quantity
REAL(KIND=8), INTENT(OUT) :: at     ! cosmic expansion factor normalized to present value
REAL(KIND=8) :: f1,f2,oz2,oz3,dldz,foz,x,y

oz2=oz*oz
oz3=oz2*oz
!--time, omega(z) (Viana & Liddle 1996, Peebles 1980) and D(t)
t=cosmicts_cos(oz)
d=capd_cos(oz,MERGE(omm,omm*oz/(1.d0+omm*(oz-1.d0)),iuni == 1),iuni)
!-----delta_c(t) and its time derivative
IF(iuni == 1)THEN
   IF(lam0 <= zero)THEN ! flat universe
      delta=dc0*d0/d ! null Lambda
      ddeldt=r23*delta/t
      dtdoz=-1.5D0*t/oz
      at=1.d0/oz ! expansion factor
   ELSE
      y=omm/lam0 ! non-null Lambda
      delta=dc0*d0/d
      x=1.d0/(1.d0+y*oz3)
      dldz=3.d0*x*y*oz2*(x**r56*(1.d0-x)**(r23-1.d0)/betav-.5D0)
      foz=ih0/(oz*SQRT(lam0+omm*oz3))
      ddeldt=delta*dldz/foz
      dtdoz=-foz
      at=1.d0/oz ! expansion factor
   END IF
ELSE
   delta=dc0*d0/d ! open universe
   f1=SQRT(omm*oz3+(1.d0-omm)*oz2)/ih0
   f2=-.5D0*omm*oz3/(ih0*ih0)
   ddeldt=delta*(f2/f1-f1+2.5D0*omm/(ih0*ih0)*oz2/(f1*d))
   dtdoz=-ih0/(oz2*SQRT(1.d0+omm*(oz-1.d0)))
   at=1.d0/oz
END IF
END SUBROUTINE delz_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION deltac_cos(omm,oz)

! Calculates the value of delta_c(z) using the formula given by Henry et al.
!     Computes the density contrast for spherical collapse delta_c for a
!     flat universe using the equation given by Henry(2000)
!
!     Input:
!        omm: Omega matter
!        oz=1+z
!
!     Output:
!        dcz=delta_c(omm,z)
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  IMPLICIT NONE
  REAL(KIND=8), INTENT(IN) :: omm,oz
  REAL(KIND=8) :: deltac_cos
  REAL(KIND=8) :: x

  x=(1d0/omm-1d0)**r13/oz
  deltac_cos=3d0*(12d0*pi)**r23/20d0*(1d0-0.0123d0*log10(1+x**3d0))
      
END FUNCTION deltac_cos


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dtdoz_cos(oz)

!     Calculates the derivative dt/d(1+z). 

!     Uses: cosmict_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: oz ! 1+z
REAL(KIND=8), DIMENSION(SIZE(oz)) :: dtdoz_cos

dtdoz_cos=MERGE(MERGE(-1.5D0*cosmict_cos(oz)/oz,-ih0/(oz*SQRT(lam0+omm*oz**3)),lam0 <= zero),-ih0/(oz**2*SQRT(1.d0+omm*(oz-1.d0))),iuni == 1)
END FUNCTION dtdoz_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE hal_cos(lr,lhm,ln,vsmr,lmar,ldr)

!     Calculates halo clustering properties at a given redshift using the MPS clustering model.

!     Uses: lnST_cos,indmp_cos,smrlc2_cos,dsqrmspl

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qrmspl
IMPLICIT NONE
REAL(KIND=8), INTENT(INOUT), DIMENSION(nmh1) :: lr      ! array with log of comoving halo scales
REAL(KIND=8), INTENT(OUT), DIMENSION(ni,nmh1) :: lhm    ! array with log of corresponding halo masses, in M_o
REAL(KIND=8), INTENT(OUT), DIMENSION(ni,nmh1) :: ln     ! array with log of comoving density of halos N(M), in M_o^{-1} Mpc^{-3}
REAL(KIND=8), INTENT(OUT), DIMENSION(ni,nmh1) :: lmar   ! array with the mass accretion rate in M_o Gyr^{-1}
REAL(KIND=8), INTENT(OUT), DIMENSION(ni,nmh1) :: ldr    ! array with the destruction rate in Gyr^{-1}
REAL(KIND=8), INTENT(OUT), DIMENSION(nmh1,nmh1) :: vsmr ! array with the specific merger rate in M_o^{-1} Gyr^{-1}
INTEGER :: m,mp,mpi
REAL(KIND=8) :: lrv,lra,lrm,y
REAL(KIND=8), DIMENSION(nmh1) :: aux

DO m=nmh1,1,-1
   lhm(i,m)=lrhopi43+3.d0*lr(m) ! halo mass 
   ln(i,m)=lnst_cos(de,lr(m))+lr(m)-lhm(i,m)-l3 ! log N(M)
   !--specific merger rate (smr) per interval of ddelta and dR':
   lrv=lr(m)-idelm1/3.d0
   mpi=indmp_cos(m,lrv,lr,nmh1) ! lower mp for m and delm
   lra=lr(mpi)
   lrm=lr(m)
   lr(mpi)=lrv
   IF(mpi < nmh1)vsmr(m,mpi:nmh1)=smrlc2_cos(de,lrm,lr(mpi:nmh1))
   !--mass accretion rate
   lmar(i,m)=LOG(mar_cos(de,lrm))+ltddel(i)+lhm(i,m)
   !--destruction rate
   IF(mpi < nmh1)THEN
      aux(mpi:nmh1)=EXP(lr(mpi:nmh1))*vsmr(m,mpi:nmh1)
      aux(mpi:nmh1)=MERGE(1.d-200,aux(mpi:nmh1),aux(mpi:nmh1) < zero)
      ldr(i,m)=LOG(qrmspl(lr(mpi:nmh1),aux(mpi:nmh1),1))+ltddel(i)
   END IF
   lr(mpi)=lra
END DO
END SUBROUTINE hal_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE dlrdoz_cos(x,y,dydx)

!     Computes the (1+z)-derivative, dlr/doz, of the scale.

!     Uses: delz_cos,mar_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x     ! 1+z
REAL(KIND=8), INTENT(IN) :: y     ! log of scale at z, in Mpc
REAL(KIND=8), INTENT(OUT) :: dydx ! derivative function
REAL(KIND=8) :: t,d,dtdoz,delta,ddeldt,y1,y2,at

CALL delz_cos(x,t,d,dtdoz,delta,ddeldt,at)
IF(y-lrb > -zero)THEN 
   dydx=r13*mar_cos(delta,y)*ddeldt*dtdoz
ELSE
   y1=LOG(mar_cos(delta,lrb))
   y2=LOG(mar_cos(delta,lrb+1.d-8))
   dydx=r13*EXP((y2-y1)*1.d8*(y-lrb)+y1)*ddeldt*dtdoz
END IF
END SUBROUTINE dlrdoz_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE dozdr_cos(x,y,dydx)

!     Computes the scale derivative of the redshift, d(1+z)/dr.

!     Uses: dlrdoz_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x     ! scale at z, in Mpc
REAL(KIND=8), INTENT(IN) :: y     ! 1+z
REAL(KIND=8), INTENT(OUT) :: dydx ! derivative function
REAL(KIND=8) :: fx

CALL dlrdoz_cos(y,LOG(x),fx)
dydx=1.d0/(fx*x)
END SUBROUTINE dozdr_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE derla_cos(t,oz,dla,dla2)

!     Computes the first and second log time derivs of the expansion factor.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: t      ! cosmic time, in Gyr
REAL(KIND=8), INTENT(IN) :: oz     ! redshift (1+z)
REAL(KIND=8), INTENT(OUT) :: dla   ! first derivative dlna/dlnt
REAL(KIND=8), INTENT(OUT) :: dla2  ! second derivative d2lna/dlnt2
REAL(KIND=8) :: oz2,oz3,tih,tih2,fzz,fac1,fac2

oz2=oz*oz
oz3=oz*oz2
tih=t/ih0
tih2=tih*tih
IF(iuni == 1)THEN
   fzz=omm*oz3+lam0
ELSE
   fzz=omm*oz3+(1.d0-omm)*oz2
END IF
fac1=tih2*(lam0-.5D0*omm*oz3)
fac2=tih*SQRT(fzz)
dla=fac2
dla2=fac2*(1.d0-fac2)+fac1
END SUBROUTINE derla_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE dlrom_cos(t,oz,ez,dlro,dlro2)

!     Computes the first and second log time derivs of the cosmic mean density.

!     Uses: derla_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: t      ! cosmic time, in Gyr
REAL(KIND=8), INTENT(IN) :: oz     ! redshift (1+z)
REAL(KIND=8), INTENT(OUT) :: ez    ! function E(z) appearing in the FRW ecuations (Peebles 1993)
REAL(KIND=8), INTENT(OUT) :: dlro  ! first derivative dlnrho/dlnt
REAL(KIND=8), INTENT(OUT) :: dlro2 ! second derivative d2lnrho/dlnt2
REAL(KIND=8) :: dla,dla2

ez=oz**3
CALL derla_cos(t,oz,dla,dla2)
dlro=-3.d0*dla
dlro2=-3.d0*dla2
END SUBROUTINE dlrom_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE intnm_cos(mc,ldel,y)

!     Total mass fraction in halos of mass between M=0 to M=mc.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : errep,gammln,gammp,spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: mc     ! cutoff mass, in M_o
REAL(KIND=8), INTENT(IN) :: ldel   ! natural log of delta for collapse at t extrapolated to t0
REAL(KIND=8), INTENT(OUT) :: y     ! resulting mass fraction
INTEGER :: klo
REAL(KIND=8) :: x,x2,nu,lrth
CHARACTER(LEN=75) :: message

lrth=(LOG(mc)-lrhopi43)/3.d0
message='nmp too small in subroutine intnm_cos'
IF(lrth < alrcom(1))CALL errep(message)
klo=1
nu=EXP(ldel-spl(alrcom,ls0,mls0,lrth,klo))
! y=erf(nu/sq2) ! for usual PS expression
x=sqra*nu/sq2 ! for Sheth & Tormen
x2=x*x
y=cnor*(erf(x)+jfac*(x**.4D0*EXP(-x2)+EXP(gammln(1.2D0))*gammp(1.2D0,x2)))
END SUBROUTINE intnm_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION lftime_cos(m,it)

!     Natural log of the formation time (in Gyr) of a halo of a given mass at a given time.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : errep,lint,spl,spline
IMPLICIT NONE
INTEGER, INTENT(IN) :: m    ! halo mass index
INTEGER, INTENT(IN) :: it   ! index for the equiprobable time interval
INTEGER :: k,klo,iia=0,ma=0
REAL(KIND=8) :: lftime_cos,ct,st,stp,frik,lst,ly
REAL(KIND=8), DIMENSION(ni) :: mfria
REAL(KIND=8), DIMENSION(nmh1) :: vx,vy,mvy,xaux,yaux
CHARACTER(LEN=75) :: message='nmi too small or ip too large for the value of hmlow'
SAVE yaux,vx,vy,mvy,iia,ma

ly=1.d0/nt
lst=LOG((-.5d0+DBLE(it))*ly) ! smaller ft first
IF(ii /= iia)THEN
   ct=EXP(-slt(ii))
   st=EXP(-slt(ni))
   stp=(ct-st)/nmh
   yaux=-LOG(arth(ct,-stp,nmh1))
   vy=-LOG(arth(st,stp,nmh1))
   iia=ii
END IF
IF(m /= ma)THEN
   CALL spline(slt,fri(:,m),mfria)
   xaux(2:nmh1)=spl(slt,fri(:,m),mfria,yaux(2:nmh1))-fri(ii,m) ! integrated ft-distribution
   DO k=1,nmh
      vx(k)=xaux(nmh1-k+1)
   END DO
   IF(vx(1) > lst.AND.m > 1)CALL errep(message)
   vx(nmh1)=0.d0
   CALL spline(vx(1:nmh),vy(1:nmh),mvy(1:nmh))
   ma=m
END IF
klo=1
IF(lst < vx(nmh))THEN
   lftime_cos=MAX(-spl(vx(1:nmh),vy(1:nmh),mvy(1:nmh),lst,klo),-slt(ni)+zero)
ELSE
   lftime_cos=MAX(-lint(vx,vy,lst,klo),-slt(ni)+zero)
END IF
END FUNCTION lftime_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION lmprog_cos(lt,lm,lmpbot)

!     Natural log of the mass of the main progenitor of a newly formed halo of a given mass at a given formation time.

!     Uses: scr_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromb,spl,spline
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: lt      ! log of formation time, in Gy
REAL(KIND=8), INTENT(IN) :: lm      ! log of the halo mass at lt, in M_o
REAL(KIND=8), INTENT(IN) :: lmpbot  ! log of lower bound for mass of main progenitor
INTEGER :: k,klo
REAL(KIND=8) :: lmprog_cos,lmup,lmlow
REAL(KIND=8), DIMENSION(nmh1) :: vx,vy,mvy,vy1

lmup=lm+idelm1 ! upper bound for mp1
lmlow=lm+l05 ! lower bound for mp1
IF(lmlow < lmpbot)THEN
   lmprog_cos=.5D0*(lmup+lmlow) ! default value (prog mass not available yet)
ELSE
   lrcom=(lm-lrhopi43)/3.d0 ! comoving scale for M'
   klo=1
   dccom=EXP(spl(slt,ldel,mldel,-lt,klo))
   vy=arth(lmlow,(lmup-lmlow)/nmh,nmh1)
   vy1=vy(1)
   vx=qromb(scr_cos,vy1,vy)
   vx(2:nmh1)=LOG(vx(2:nmh1)/vx(nmh1))
   CALL spline(vx(2:nmh1),vy(2:nmh1),mvy(1:nmh))
   klo=1
   lmprog_cos=spl(vx(2:nmh1),vy(2:nmh1),mvy(1:nmh),l05,klo)
END IF
END FUNCTION lmprog_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dfrdt_cos(x)

!     Computes the x-derivative dy/dx of "y(t)=\int_(x_0)^x  FR[M(t),t] dt" along the accretion track of index m.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x   ! time, in Gyr
REAL(KIND=8), DIMENSION(SIZE(x)) :: dfrdt_cos

dfrdt_cos=EXP(spl(slt,lfr(:,m),mlfr(:,m),-LOG(x)))
END FUNCTION dfrdt_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION indmp_cos(m,lrv,lr,nmh1)

!     Nearest index mp for log R' (lrv) in a vector logR (lr) starting at m (log R'>= lr(m)).

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, INTENT(IN) :: m,nmh1
REAL(KIND=8), INTENT(IN) :: lrv
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: lr
INTEGER :: indmp_cos,mp

mp=m
DO WHILE(lrv-lr(mp) > -zero)
   mp=mp+1
   IF(mp > nmh1)EXIT
END DO
indmp_cos=mp-1
END FUNCTION indmp_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION CDMth_cos(x)

!     Integrand of the n order momentum for a CDM spectrum and top-hat window of scale r.

!     Uses: CDM_cos,th_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8) :: r2,r6
REAL(KIND=8), DIMENSION(SIZE(x)) :: CDMth_cos

r2=rvar*rvar
r6=r2*r2*r2
CDMth_cos=th_cos(rvar*x)**2*x**(2.d0*DBLE(nord-2))*CDM_cos(x)/r6
END FUNCTION CDMth_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION CDMga_cos(x)

!     Integrand of the n order momentum for a CDM spectrum and Gaussian window of scale r.

!     Uses: CDM_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: CDMga_cos

CDMga_cos=x**(2.d0*DBLE(nord+1))*CDM_cos(x)*EXP(-(rvar*x)**2)
END FUNCTION CDMga_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION CDMneg_cos(x)

!     Integrand of the -1 order momentum for a CDM spectrum and Gaussian window of scale r.

!     Uses: CDM_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: CDMneg_cos

CDMneg_cos=CDM_cos(x)*EXP(-(rvar*x)**2)
END FUNCTION CDMneg_cos
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION CDMMikheeva_cos(x)

!     Integrand of the -1 order momentum for a CDM spectrum and Gaussian window of scale r.

!     Uses: CDM_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
!INTEGER :: i
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: CDMMikheeva_cos

CDMMikheeva_cos=CDM_cos(x)*(1.d0-EXP(-(rvar*x)**2))**2

END FUNCTION CDMMikheeva_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION CDMks_cos(x)

!     Integrand of the n order momentum for a CDM spectrum and k-sharp window of scale r.

!     Uses: CDM_cos,th_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: CDMks_cos

CDMks_cos=x**(2.d0*DBLE(nord+1))*CDM_cos(x)
END FUNCTION CDMks_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION CDM_cos(x)

!     CDM spectrum by BBKS (1985).

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER :: i
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: CDM_cos,q,q234
 
q=x/(hreal*gam)
q234=2.34D0*q
DO i=1,SIZE(x)
   IF(q234(i) < 1.d-6)THEN
      CDM_cos(i)=x(i)**(1.d0+tn)*(1.d0-0.5d0*q234(i))**2/SQRT(1.d0+q(i)*(3.89D0+q(i)*(259.21D0+q(i)*(162.771336D0+q(i)*2027.16958081D0))))
   ELSE
      CDM_cos(i)=x(i)**(1.d0+tn)*(LOG(1.d0+q234(i))/q234(i))**2/SQRT(1.d0+q(i)*(3.89D0+q(i)*(259.21D0+q(i)*(162.771336D0+q(i)*2027.16958081D0))))
   END IF
END DO
END FUNCTION CDM_cos


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION th_cos(x)

!     Part of the Fourier transform of the top-hat window.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION th_cos(x)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8),DIMENSION(SIZE(x)) :: th_cos

th_cos=MERGE(SIN(x)-x*COS(x),x**3*(1.d0-x*x/10.d0)/3.d0,x > 1.d-3)
END FUNCTION th_cos

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION ths_cos(x)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: ths_cos

ths_cos=MERGE(SIN(x)-x*COS(x),x**3*(1.d0-x*x/10.d0)/3.d0,x > 1.d-3)
END FUNCTION ths_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION cosmict_cos(oz)

!     Cosmic time (in Gyr) corresponding to a redshift z (Viana & Liddle 1996, Peebles 1980).

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: oz    ! 1 + z
REAL(KIND=8) :: oomm,oommr
REAL(KIND=8), DIMENSION(SIZE(oz)) :: cosmict_cos,l0oz3,zomm,t1,t2

SELECT CASE ( iuni )
  CASE (    1)
     IF(lam0 == 0.d0)THEN ! flat universe
        cosmict_cos=r23*ih0/(oz*SQRT(oz)) ! null Lambda
     ELSE
        l0oz3=lam0/(oz**3) ! non-null Lambda
        cosmict_cos=r23*ih0*(LOG(SQRT(l0oz3)+SQRT(l0oz3+omm))-0.5D0*LOG(omm))/SQRT(lam0)
     END IF
  CASE (    2)
     zomm=(oz-1.d0)*omm ! open universe
     oomm=1.d0-omm
     oommr=SQRT(oomm)
     t1=(zomm-omm+2.d0)/(zomm+omm)
     t1=LOG(t1+SQRT(t1*t1-1.d0))
     t2=(2.d0*oommr*SQRT(zomm+1.d0))/(omm+zomm)
     cosmict_cos=0.5d0*ih0*omm*(t2-t1)/(oomm*oommr)
END SELECT
END FUNCTION cosmict_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION cosmicts_cos(oz)

!     Cosmic time (in Gyr) corresponding to a redshift z (Viana & Liddle 1996, Peebles 1980).

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: oz    ! 1 + z
REAL(KIND=8) :: cosmicts_cos,l0oz3,t1,t2,zomm,oomm,oommr

SELECT CASE ( iuni )
  CASE (    1)
     IF(lam0 == 0.d0)THEN ! flat universe
        cosmicts_cos=r23*ih0/(oz*SQRT(oz)) ! null Lambda
     ELSE
        l0oz3=lam0/(oz*oz*oz) ! non-null Lambda
        cosmicts_cos=r23*ih0*(LOG(SQRT(l0oz3)+SQRT(l0oz3+omm))-0.5D0*LOG(omm))/SQRT(lam0)
     END IF
  CASE (    2)
     zomm=(oz-1.d0)*omm ! open universe
     oomm=1.d0-omm
     oommr=SQRT(oomm)
     t1=(zomm-omm+2.d0)/(zomm+omm)
     t1=LOG(t1+SQRT(t1*t1-1.d0))
     t2=(2.d0*oommr*SQRT(zomm+1.d0))/(omm+zomm)
     cosmicts_cos=0.5d0*ih0*omm*(t2-t1)/(oomm*oommr)
END SELECT
END FUNCTION cosmicts_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION capd_cos(oz,om,iuni)

!     Growth factor, D(t), of the amplitude of fluctuations for an open or flat universe (Viana & Liddle 1996, Peebles 1980).
      
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : betai
IMPLICIT NONE
INTEGER, INTENT(IN) :: iuni    ! index = 1/2 for flat universe/for open universe
REAL(KIND=8), INTENT(IN) :: oz ! 1 + z
REAL(KIND=8), INTENT(IN) :: om ! value of Omega_m at t
REAL(KIND=8) :: capd_cos,oom,x,y,sqrty1,sqrty
REAL(KIND=8), PARAMETER :: bfun=1.7247397061532D0

oom=1.d0-om
SELECT CASE ( iuni )
CASE (    1)
   capd_cos=1.d0/oz ! flat universe
   IF(oom <= 0.0)RETURN
   y=om/oom ! non-null Lambda
   x=1.d0/(1.d0+y*oz*oz*oz)
   betav=betai(r56,r23,x)*bfun
   capd_cos=r56*betav*y**(r23/2.d0)/SQRT(x)
CASE (    2)
   y=oom/om ! open universe
   sqrty=SQRT(y)
   sqrty1=SQRT(y+1.d0)
   capd_cos=1.d0+3.d0*(1.d0+sqrty1*LOG(sqrty1-sqrty)/sqrty)/y
END SELECT
END FUNCTION capd_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION sn2th_cos(n)

!     Integral of the square of the spectral moment of order n for the CDM or WDM spectra or of order 0 for the SF one
!     (in this case the integrals of the moments of n order can be calculated analitically) for a top-hat window.
!     The total integral is obtained by adding the integrals calculated between successive zeroes of function th.

!     Uses: th_cos,sfth_cos,CDMth_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : dmidpnt,qromb,qromo,zbrent
IMPLICIT NONE
INTEGER, INTENT(IN) :: n
INTEGER :: k,nmax
REAL(KIND=8) :: sn2th_cos,stp,szero,nzero,x01,x02,x20,x1,x2,x2m,x20m,xl1,xl2,s
REAL(KIND=8), DIMENSION(:), ALLOCATABLE :: x

x01=pi
x02=x01+pi
nzero=zbrent(ths_cos,x01,x02,1.d-7)
x2=nzero/rvar
!--split the 1st integral to make it fast and accurate
! the 1st portion...
SELECT CASE ( ispe )
CASE (    1)
   x20=MIN(1.d-3,1.d-2*x2)
   s=(x20**(tn+3.d0)/(tn+3.d0)-x20**(tn+5.d0)/(tn+5.d0)/5.d0+x20**(tn+7.d0)/(tn+7.d0)/1.d2)/9.d0
CASE (    2)
   nord=n
   x20=1.d-4*x2
   xl1=1.d-4*x20
   xl2=1.d-2*x20
   s=qromo(CDMth_cos,0.d0,xl1,dmidpnt)
   s=s+qromo(CDMth_cos,xl1,xl2,dmidpnt)
   s=s+qromo(CDMth_cos,xl2,x20,dmidpnt)
CASE (    3)
   nord=n
   x20=1.d-2*x2
   x20m=MIN(x20,1.d-2*kco*kco)
   s=qromo(CDMth_cos,0.d0,x20m,dmidpnt)
END SELECT
x1=x20
nmax=10
DO 
   ALLOCATE(x(nmax))
   stp=EXP((LOG(x2)-LOG(x1))/nmax)
   szero=0.d0
   x=geop(x1,stp,nmax)
   x1=x(nmax)*stp
   SELECT CASE ( ispe )
   CASE (    1)
      szero=sum(qromb(sfth_cos,x,EOSHIFT(x,1,x1)))
   CASE (    2)
      szero=sum(qromb(CDMth_cos,x,EOSHIFT(x,1,x1)))
   CASE (    3)
      szero=sum(qromb(CDMth_cos,x,MIN(EOSHIFT(x,1,x1),kco)))
   END SELECT
   sn2th_cos=s
   IF(szero < s*1.d-9)RETURN
   s=s+szero
   x01=x02
   x02=x01+pi
   nzero=zbrent(ths_cos,x01,x02,1.d-7)
   x2=nzero/rvar
   DEAlLOCATE(x)
   nmax=1
END DO
END FUNCTION sn2th_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION sn2ga_cos(n)

!     Integral of the square of the spectral moment of order n for the CDM or WDM spectra or of order 0 for the SF one
!     (in this case the integrals of the moments of n order can be calculated analitically) for a top-hat window.
!     The total integral is obtained by adding the integrals calculated between successive zeroes of function th.

!     Uses: qromb,qromo,dmidpnt,CDMga_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromb,qromo,dmidpnt
IMPLICIT NONE
INTEGER, INTENT(IN) :: n
INTEGER :: nmax
REAL(KIND=8) :: sn2ga_cos,stp,x20,x1,x2,x20m,x2m,s,ss

IF(n==-1.AND.rvar==0.d0)THEN
   ! the 1st portion...
   x20=1.d2
   ss=qromo(CDM_cos,0.d0,x20,dmidpnt)
   ! the 2nd portion...
   nmax=30
   x1=x20
   x2=5.d10 
   stp=EXP((LOG(x2)-LOG(x1))/nmax)
   DO 
      x2=x1*stp
      IF(X2 >= 5.d10)STOP' Integral has exceeded the upper limit'
      s=qromb(CDM_cos,x1,x2)
      sn2ga_cos=ss
      if(s < ss*1.d-9)RETURN ! keep on integrating until condition is verified
      ss=ss+s               
      x1=x2
   END DO
ELSEIF(n==-1)THEN
   ! the 1st portion...
   !x20=1.d-5/rvar
   x20=1.d-7/rvar
   ss=qromo(CDMneg_cos,0.d0,x20,dmidpnt)
   ! the 2nd portion...
   nmax=30
   x1=x20
   !x2=3.d1/rvar ! integrate up to 15\sigma 
   x2=5.d3/rvar ! integrate up to 15\sigma 
   stp=EXP((LOG(x2)-LOG(x1))/nmax)
   DO 
      x2=x1*stp
      !   IF(X2 >= 3.d1/rvar)STOP' Integral has exceeded the upper limit'
      IF(X2 >= 5.d3/rvar)STOP' Integral has exceeded the upper limit'
      s=qromb(CDMneg_cos,x1,x2)
      sn2ga_cos=ss
      if(s < ss*1.d-9)RETURN ! keep on integrating until condition is verified
      ss=ss+s               
      x1=x2
   END DO
ELSE
! the 1st portion...
SELECT CASE ( ispe )
CASE (    1)
   STOP' power-law spectra not implemented yet'
CASE (    2)
   nord=n
   x20=1.d-9/rvar
   ss=qromo(CDMga_cos,0.d0,x20,dmidpnt)
CASE (    3)
   nord=n
   x20=1.d-6/rvar
   x20m=MIN(x20,1.d-2*kco*kco)
   ss=qromo(CDMga_cos,0.d0,x20m,dmidpnt)
END SELECT
! the 2nd portion...
nmax=30
x1=x20
x2=3.d1/rvar ! integrate up to 15\sigma
stp=EXP((LOG(x2)-LOG(x1))/nmax)
DO 
   x2=x1*stp
   IF(X2 >= 3.d1/rvar)STOP' Integral has exceeded the upper limit'
   SELECT CASE(ispe)
   CASE (    1)
   STOP' power-law spectra not implemented yet'
   CASE (    2)
      s=qromb(CDMga_cos,x1,x2)
   CASE (    3)   
      x2m=min(x2,kco)
      s=qromb(CDMga_cos,x1,x2m)
   END SELECT
   sn2ga_cos=ss
   if(s < ss*1.d-9)RETURN ! keep on integrating until condition is verified
   ss=ss+s               
   x1=x2
END DO
END IF
END FUNCTION sn2ga_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION sn2ks_cos(n)

!     Integral of the square of the spectral moment of order n for the CDM or WDM spectra or of order 0 for the SF one
!     (in this case the integrals of the moments of n order can be calculated analitically) for a top-hat window.
!     The total integral is obtained by adding the integrals calculated between successive zeroes of function th.

!     Uses: CDMks_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : dmidpnt,qromb,qromo
IMPLICIT NONE
INTEGER, INTENT(IN) :: n
INTEGER :: k,nmax
REAL(KIND=8) :: sn2ks_cos,stp,szero,x01,x02,x20,x1,x2,x2m,x20m,xl1,xl2,s,ss

!--split the 1st integral to make it fast and accurate
! the 1st portion...
SELECT CASE ( ispe )
CASE (    1)
STOP' power-law spectra not implemented yet'
CASE (    2)
   nord=n
   x20=1.d-3/rvar
   ss=qromo(CDMks_cos,0.d0,x2,dmidpnt)
CASE (    3)
   nord=n
   x20=1.d-3/rvar
   x20m=MIN(x20,1.d-2*kco*kco)
   ss=qromo(CDMks_cos,0.d0,x20m,dmidpnt)
END SELECT
x1=x20
nmax=10
x1=x20
x2=1.d0/rvar 
stp=EXP((LOG(x2)-LOG(x1))/nmax)
DO 
   x2=x1*stp
   SELECT CASE(ispe)
   CASE (    1)
      STOP' power-law spectra not implemented yet'
   CASE (    2)
      s=qromb(CDMks_cos,x1,x2)
   CASE (    3)   
      x2m=min(x2,kco)
      s=qromb(CDMks_cos,x1,x2m)
   END SELECT
   ss=ss+s               
   x1=x2
END DO
sn2ks_cos=ss
END FUNCTION sn2ks_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION sfth_cos(x)

!     Integrand for the normalization factor of a scale free spectrum with top-hat window.

!     Uses: th_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8), DIMENSION(SIZE(x))  :: sfth_cos

sfth_cos=x**(tn-4.d0)*th_cos(x)**2
END FUNCTION sfth_cos


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dvbn_cos(oz)

!     Analytical fits of the halo virial overdensity in units of the mean cosmic density (Bryan & Norman 1998, ApJ, 495, 80).

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: oz   ! 1+z
REAL(KIND=8) :: dvbn_cos,omz

IF(iuni == 1)THEN
   omz=oz**3/(oz**3+lam0/omm)
   dvbn_cos=(18.d0*pi**2+82.d0*(omz-1.d0)-39.d0*(omz-1.d0)**2)/omz
ELSE
   omz=omm*oz/(1.d0+omm*(oz-1.d0))
   dvbn_cos=(18.d0*pi**2+60.d0*(omz-1.d0)-32.d0*(omz-1.d0)**2)/omz
END IF
END FUNCTION dvbn_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dldvdz_cos(oz)

!     1+z derivative of the log of the function dvbn_cos (Bryan & Norman 1998, ApJ, 495, 80).

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: oz    ! 1+z
REAL(KIND=8) :: dldvdz_cos, x,dx,y

IF(iuni == 1)THEN
   x=oz**3/(oz**3+lam0/omm)-1.d0
   dx=3.d0*(lam0/omm)/oz/(oz**3+(lam0/omm))
   y=dvbn_cos(oz)
   dldvdz_cos=dx*(82.d0-78.d0*x-y)/y
ELSE
   x=omm*oz/(1.d0+omm*(oz-1.d0))-1.d0
   dx=(1.d0-omm)/oz/(1.d0+omm*(oz-1.d0))
   y=dvbn_cos(oz)
   dldvdz_cos=dx*(60.d0-64.d0*x-y)/y
END IF
END FUNCTION dldvdz_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION foz_cos(lm,itime)

!     1+z corresponding to the formation of a halo of a given mass at the current redshift.

!     Uses: dlrdoz_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : odeint,drkqs,spl,sp21,spline
IMPLICIT NONE
INTEGER, INTENT(IN) :: itime      ! formation time index
REAL(KIND=8), INTENT(IN) :: lm    ! log of current halo mass, in M_o
INTEGER :: k,klo
REAL(KIND=8) :: foz_cos,x1,x2,stp,lra,ly,lst,y,lrfoz,yy1(1)
REAL(KIND=8), DIMENSION(nmh1) :: vx,vy,mvy

ly=1.d0/nt
lst=LOG((-.5d0+DBLE(itime))*ly) ! smaller ft first
lrfoz=(lm-lrhopi43)/3.d0
x1=oz(ii)
x2=oz(ez)
stp=2.d-3*nz*(x2-x1)
CALL odeint(lrfoz,x1,x2,1.d-9,stp,0.d0,dlrdoz_cos,drkqs)
lra=lrx(2)
IF(lrfoz < lrx(1))THEN
   lra=lrfoz
   lrfoz=lrx(1)
END IF
yy1=sp21(oz,lrx,fri,mfri,(/x1/),lrfoz)
x2=oz(ni)
stp=(x1-x2)/nmh
vy(1:nmh)=arth(x2,stp,nmh)
vx(1:nmh)=sp21(oz,lrx(1:nmh),fri(:,1:nmh),mfri(:,1:nmh),vy(1:nmh),lrfoz)-yy1(1) ! integrated ft-distribution
vy(nmh1)=x1
vx(nmh1)=0.d0
IF(vx(1)-lst > -zero)THEN
   y=oz(ni)
ELSE
   CALL spline(vx,vy,mvy)
   klo=1
   y=spl(vx,vy,mvy,lst,klo) ! log of halo ft
   IF(lra < lrx(1))y=(oz(ez)-y)*(EXP(lrx(1))-EXP(lra))/EXP(lrx(1))+y ! linear extrapolation
END if
foz_cos=y
END FUNCTION foz_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION mar_cos(dc,ls)

!     Mass accretion rate per ddelta interval over M.

!     Uses: smr_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : midsql,qromo
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: dc       ! linear density contrast for collapse at t
REAL(KIND=8), INTENT(IN) :: ls       ! log of the scale of the accreting object at t, in Mpc
REAL(KIND=8) :: mar_cos

nsmr=1
lrcom=ls
dccom=dc
mar_cos=qromo(smr_cos,0.d0,delm,midsql)
END FUNCTION mar_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FNCTION smr_cos(dmm)

!     Specific merger rate (nsmr=2), times DeltaM/M (nsmr=1).

!     USE: smrlc2_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION smr_cos(dmm)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: dmm   ! merged mass over M
REAL(KIND=8), DIMENSION(SIZE(dmm)) :: smr_cos,lrp,fm,dmm1

dmm1=dmm+1.d0
lrp=lrcom+r13*LOG(dmm1)
fm=r13*EXP(lrp)/dmm1
smr_cos=smrlc2_cos(dccom,lrcom,lrp)*MERGE(fm*dmm,fm,nsmr == 1)
END FUNCTION smr_cos

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION smrs_cos(dmm)
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: dmm   ! merged mass over M
REAL(KIND=8) :: smrs_cos,dmm1,lrp,fm,yy1(1)

dmm1=dmm+1.d0
lrp=lrcom+r13*LOG(dmm1)
fm=r13*EXP(lrp)/dmm1
yy1=smrlc2_cos(dccom,lrcom,(/lrp/))*MERGE(fm*dmm,fm,nsmr == 1)
smrs_cos=yy1(1)
END FUNCTION smrs_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION smrlc1_cos(dc,lr1,lr2)

!     Lacy & Cole specific merger rate per dR' and ddelta intervals.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: dc    ! linear density contrast at t
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: lr1   ! log R
REAL(KIND=8), INTENT(IN) :: lr2   ! log R'
INTEGER :: k,klo
REAL(KIND=8) :: s2,y
REAL(KIND=8), DIMENSION(SIZE(lr1)) :: smrlc1_cos,omqs21

klo=1
y=spl(alrcom,ls0,mls0,lr2,klo)
s2=EXP(2.d0*y)
omqs21=1.d0-s2/EXP(2.d0*spl(alrcom,ls0,mls0,lr1))
smrlc1_cos=sq2pi*ABS(spl(alrcom,ldls0,mldls0,lr2,klo))/EXP(MIN(200.d0,.5D0*dc*dc*omqs21/s2)+y+lr2)/SQRT(omqs21**3)
END FUNCTION smrlc1_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION smrlc2_cos(dc,lr1,lr2)

!     Lacy & Cole specific merger rate per dR' and ddelta intervals.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: dc    ! linear density contrast at t
REAL(KIND=8), INTENT(IN) :: lr1   ! log R
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: lr2   ! log R'
INTEGER :: k,klo
REAL(KIND=8), DIMENSION(SIZE(lr2)) :: smrlc2_cos,s2,omqs21,y

y=spl(alrcom,ls0,mls0,lr2)
s2=EXP(2.d0*y)
klo=1
omqs21=1.d0-s2/EXP(2.d0*spl(alrcom,ls0,mls0,lr1,klo))
smrlc2_cos=sq2pi*ABS(spl(alrcom,ldls0,mldls0,lr2))/EXP(MIN(200.d0,.5D0*dc*dc*omqs21/s2)+y+lr2)/SQRT(omqs21**3)
END FUNCTION smrlc2_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION scr_cos(lm)

!     Specific capture rate times M over time-derivative of delta.
!
!     Uses: lnPS_cos,smrlc1_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: lm    ! log of captured mass
REAL(KIND=8), DIMENSION(SIZE(lm)) :: scr_cos,lr,lrdm

lr=(lm-lrhopi43)/3.d0 ! comoving scale for M
lrdm=lr-l3-lm ! dR --> dDM
scr_cos=smrlc1_cos(dccom,lr,lrcom)*EXP(lnPS_cos(dccom,lr)-lnPS_cos(dccom,lrcom)+lrdm+lm)
END FUNCTION scr_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

! FUNCTION lnPS_cos(dc,lrth)

!     Natural log of the original PS mass function N(R,t), in Mpc^{-4}.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION lnPS_cos(dc,lrth)
USE math_lib, ONLY : spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: dc     ! density contrast
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: lrth   ! log of top hat filtering scale
REAL(KIND=8), DIMENSION(SIZE(lrth)) ::  lnPS_cos,ls0v,ldls0v

ls0v=spl(alrcom,ls0,mls0,lrth)
ldls0v=spl(alrcom,ldls0,mldls0,lrth)
lnPS_cos=LOG(-ldls0v*dc*3.d0)-ls0v-.5D0*(dc/EXP(ls0v))**2-4.d0*lrth-1.5D0*lpi2
END FUNCTION lnPS_cos

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

FUNCTION lnPSs_cos(dc,lrth)
USE math_lib, ONLY : spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: dc     ! density contrast
REAL(KIND=8), INTENT(IN) :: lrth   ! log of top hat filtering scale
INTEGER :: klo
REAL(KIND=8) ::  lnPSs_cos,nu

klo=1
nu=dc/EXP(spl(alrcom,ls0,mls0,lrth,klo))
lnPSs_cos=LOG(-spl(alrcom,ldls0,mldls0,lrth,klo)*nu)-.5D0*nu**2-4.d0*lrth+l3-1.5D0*lpi2
END FUNCTION lnPSs_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION lnST_cos(dc,lrth)

!     Natural log of modified PS (Sheth & Tormen 1999) mass function N(R,t), in Mpc^{-4}, fitting N-body simulations.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: dc     ! density contrast
REAL(KIND=8), INTENT(IN) :: lrth   ! log of top hat filtering scale
INTEGER :: klo
REAL(KIND=8) :: lnST_cos,nu

klo=1
nu=sqra*dc/EXP(spl(alrcom,ls0,mls0,lrth,klo))
lnST_cos=LOG(-(spl(alrcom,ldls0,mldls0,lrth,klo))*nu*(1.d0+1.d0/nu**.6D0)*3.d0*cnor)-.5D0*nu**2-4.d0*lrth-1.5D0*lpi2
END FUNCTION lnST_cos

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

END MODULE cosmology


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

MODULE peak

USE global_defs

IMPLICIT NONE

! COMMONS

REAL(KIND=8) :: xx,xs,gma1
INTEGER :: indn

CONTAINS

!     FUNCTION xiga_pk
!     FUNCTION d2xiga_pk
!     FUNCTION xiCDMga_pk
!     FUNCTION dxiCDMga_pk
!     FUNCTION xiks_pk
!     FUNCTION d2xiks_pk
!     FUNCTION xiCDMks_pk
!     FUNCTION dxiCDMks_pk
!     FUNCTION theta_pk
!     FUNCTION theta2_pk
!     FUNCTION pdfxn_pk
!     FUNCTION xav_pk
!     FUNCTION g_pk
!     FUNCTION npeakd_pk
!     FUNCTION npeakr_pk
!     FUNCTION npeakcond_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xiga_pk(x)

! Correlation function for a CDM-like power spectrum (ispe=2) smoothed on scale R.
! It should be normalized using the output of subroutine norm
!
!     Uses: qromb,qromo,dmidpnt,xiCDMga_pk
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromo,qromb,dmidpnt
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x 
INTEGER :: nmax,i,icont
REAL(KIND=8) :: xiga_pk,stp,szero,zero,x20,x1,x2,x2m,x20m,s,ss

xx=x
nord=0
zero=pi
! split the 1st integral to make it fast and accurate
x2=zero/x
x20=1.d-4*x2
SELECT CASE(ispe) 
CASE(   1)   ! the 1st portion...
   STOP' power-law spectra not implemented yet'
CASE(   2)
   ss=qromo(xiCDMga_pk,0.d0,x20,dmidpnt)
CASE(   3)      
   x20m=min(x20,1.d-2*kco*kco)
   ss=qromo(xiCDMga_pk,0.d0,x20m,dmidpnt)
END SELECT
nmax=40      ! ...and the rest
x1=x20
icont=0
DO 
   icont=icont+1
   x2=7.d0*x2
   stp=exp((dlog(x2)-dlog(x1))/dble(nmax))
   szero=0.d0
   DO i=1,nmax
      x2=x1*stp
      SELECT CASE(ispe)
      CASE(   1)     
         STOP' power-law spectra not implemented yet'
      CASE(   2)
         s=qromb(xiCDMga_pk,x1,x2)
      CASE(   3)
         x2m=min(x2,kco)
         s=qromb(xiCDMga_pk,x1,x2m)
      END SELECT
      szero=szero+s
      x1=x2
      IF(ABS(s) < 1.d-10*ABS(szero))EXIT
   END DO
   ss=ss+szero                              
   xiga_pk=ss
   IF(ABS(szero) < 1.d-10*ABS(ss))RETURN ! keep on integrating until condition is verified
   zero=zero+pi
   x2=zero/x
   nmax=40       ! but do not subdivide remaining integrals
   IF(icont==nmax)STOP'icont=nmax 0'
END DO
END FUNCTION xiga_pk


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dxiga_pk(x)

! Correlation function for a CDM-like power spectrum (ispe=2) smoothed on scale R.
! It should be normalized using the output of subroutine norm
!
!     Uses: qromb,qromo,dmidpnt,dxiCDMga_pk
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromo,qromb,dmidpnt
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x 
INTEGER :: nmax,i,icont
REAL(KIND=8) :: dxiga_pk,stp,szero,zero,x20,x1,x2,x2m,x20m,s,ss

xx=x
nord=0
zero=pi
! split the 1st integral to make it fast and accurate
x2=zero/x
x20=1.d-4*x2
SELECT CASE(ispe) 
CASE(   1)   ! the 1st portion...
   STOP' power-law spectra not implemented yet'
CASE(   2)
   ss=qromo(dxiCDMga_pk,0.d0,x20,dmidpnt)
CASE(   3)      
   x20m=min(x20,1.d-2*kco*kco)
   ss=qromo(dxiCDMga_pk,0.d0,x20m,dmidpnt)
END SELECT
nmax=50      ! ...and the rest
x1=x20
icont=0
DO 
   icont=icont+1
   x2=7.d0*x2
   stp=exp((dlog(x2)-dlog(x1))/dble(nmax))
   szero=0.d0
   DO i=1,nmax
      x2=x1*stp
      SELECT CASE(ispe)
      CASE(   1)     
         STOP' power-law spectra not implemented yet'
      CASE(   2)
         s=qromb(dxiCDMga_pk,x1,x2)
      CASE(   3)
         x2m=min(x2,kco)
         s=qromb(dxiCDMga_pk,x1,x2m)
      END SELECT
      szero=szero+s
      x1=x2
      IF(ABS(s) < 1.d-10*ABS(szero))EXIT
   END DO
   ss=ss+szero                              
   dxiga_pk=ss
   IF(ABS(szero) < 1.d-10*ABS(ss))RETURN ! keep on integrating until condition is verified
   zero=zero+pi
   x2=zero/x
   nmax=50       ! but do not subdivide remaining integrals
   IF(icont==nmax)STOP'icont=nmax 1'
END DO
END FUNCTION dxiga_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION d2xiga_pk(x)

! Correlation function for a CDM-like power spectrum (ispe=2) smoothed on scale R.
! It should be normalized using the output of subroutine norm
!
!     Uses: qromb,qromo,dmidpnt,d2xiCDMga_pk
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromo,qromb,dmidpnt
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x 
INTEGER :: nmax,i,icont
REAL(KIND=8) :: d2xiga_pk,stp,szero,zero,x20,x1,x2,x2m,x20m,s,ss

xx=x
nord=0
zero=pi
! split the 1st integral to make it fast and accurate
x2=zero/x
x20=1.d-4*x2
SELECT CASE(ispe) 
CASE(   1)   ! the 1st portion...
   STOP' power-law spectra not implemented yet'
CASE(   2)
   ss=qromo(d2xiCDMga_pk,0.d0,x20,dmidpnt)
CASE(   3)      
   x20m=min(x20,1.d-2*kco*kco)
   ss=qromo(d2xiCDMga_pk,0.d0,x20m,dmidpnt)
END SELECT
nmax=60      ! ...and the rest
x1=x20
icont=0
DO 
   icont=icont+1
   x2=7.d0*x2
   stp=exp((dlog(x2)-dlog(x1))/dble(nmax))
   szero=0.d0
   DO i=1,nmax
      x2=x1*stp
      SELECT CASE(ispe)
      CASE(   1)     
         STOP' power-law spectra not implemented yet'
      CASE(   2)
         s=qromb(d2xiCDMga_pk,x1,x2)
      CASE(   3)
         x2m=min(x2,kco)
         s=qromb(d2xiCDMga_pk,x1,x2m)
      END SELECT
      szero=szero+s
      x1=x2
      IF(ABS(s) < 1.d-10*ABS(szero))EXIT
   END DO
   ss=ss+szero                              
   d2xiga_pk=ss
   IF(ABS(szero) < 1.d-10*ABS(ss))RETURN ! keep on integrating until condition is verified
   zero=zero+pi
   x2=zero/x
   nmax=60       ! but do not subdivide remaining integrals
   IF(icont==nmax)STOP'icont=nmax 2'
END DO
END FUNCTION d2xiga_pk


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xiCDMga_pk(k)
!
! Integrand of the correlation function for a CDM spectrum and Gaussian window of scale rvar
!
!     Uses: CDM_cos
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE cosmology, ONLY : CDM_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: k
REAL(KIND=8), DIMENSION(SIZE(k)) :: xicdmga_pk,fsinx,kx

kx=k*xx
WHERE(kx > 1.d-8)
   fsinx=SIN(kx)/kx
ELSEWHERE
   fsinx=1.d0-kx**2/6.d0
END WHERE
xiCDMga_pk=fsinx*CDM_cos(k)*k**2*EXP(-(k*rvar)**2)
END FUNCTION xiCDMga_pk

    
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dxiCDMga_pk(k)

! Integrand of a pìece of the derivative of the correlation function for a CDM spectrum and Gaussian
!  window of scale rvar.
!
!     Uses: CDM_cos
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE cosmology, ONLY : CDM_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: k
REAL(KIND=8), DIMENSION(SIZE(k)) :: dxiCDMga_pk,fcosx,kx

kx=k*xx
WHERE(kx > 1.d-8)
   fcosx=COS(kx)
ELSEWHERE
   fcosx=1.d0-kx**2/2.d0
END WHERE
dxiCDMga_pk=fcosx*cdm_cos(k)*k**2*EXP(-(k*rvar)**2)
END FUNCTION dxiCDMga_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION d2xiCDMga_pk(k)

! Integrand of the Laplacian of the correlation function for a CDM spectrum and Gaussian
!  window of scale r.
!
!     Uses: CDM_cos
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE cosmology, ONLY : CDM_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: k
REAL(KIND=8), DIMENSION(SIZE(k)) :: d2xiCDMga_pk,fsinx,kx

kx=k*xx
WHERE(kx > 1.d-8)
   fsinx=SIN(kx)/kx
ELSEWHERE
   fsinx=1.d0-kx**2/6.d0
END WHERE
d2xiCDMga_pk=fsinx*cdm_cos(k)*k**4*EXP(-(k*rvar)**2)
END FUNCTION d2xiCDMga_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xiks_pk(x)

! Correlation function for a CDM-like power spectrum (ispe=2) smoothed on scale R with a
! top-hat window. It should be normalized using the output of subroutine norm

!     Uses: qromb,qromo,dmidpnt,dzbrent,xiCDMks_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromo,qromb,dmidpnt
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x 
INTEGER :: nmax,i 
REAL(KIND=8) :: xiks_pk,stp,szero,zero,x20,x1,x2,x2m,x20m,s,ss

xx=x
nord=0
zero=pi
! split the 1st integral to make it fast and accurate
x2=MIN(zero/x,1.d0/rvar)
x20=1.d-2*x2
SELECT CASE(ispe) 
CASE(   1)   ! the 1st portion...
   STOP' power-law spectra not implemented yet'
CASE(   2)
   ss=qromo(xiCDMks_pk,0.d0,x20,dmidpnt)
CASE(   3)        
   x20m=min(x20,1.d-2*kco*kco)
   ss=qromo(xiCDMks_pk,0.d0,x20m,dmidpnt)
END SELECT      
nmax=10      ! ...and the rest
x1=x20
DO 
   stp=exp((dlog(x2)-dlog(x1))/dble(nmax))
   szero=0.d0
   DO i=1,nmax
      x2=x1*stp
      SELECT CASE(ispe)
      CASE(   1)     
         STOP' power-law spectra not implemented yet'
      CASE(   2)
         s=qromb(xiCDMks_pk,x1,x2)
      CASE(   3)
         x2m=min(x2,kco)
         s=qromb(xiCDMks_pk,x1,x2m)
      END SELECT
      szero=szero+s
      x1=x2
   END DO
   ss=ss+szero                             
   xiks_pk=ss
   IF(x2 >= 1.d0/rvar)RETURN  ! integral upper limit reached 
   zero=zero+pi
   x2=min(zero/x,1.d0/rvar)
   nmax=1      ! but do not subdivide remaining integrals 
END DO
end FUNCTION xiks_pk


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dxiks_pk(x)

! Correlation function for a CDM-like power spectrum (ispe=2) smoothed on scale R with a
! top-hat window. It should be normalized using the output of subroutine norm

!     Uses: qromb,qromo,dmidpnt,dzbrent,dxiCDMks_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromo,qromb,dmidpnt
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x 
INTEGER :: nmax,i 
REAL(KIND=8) :: dxiks_pk,stp,szero,zero,x20,x1,x2,x2m,x20m,s,ss

xx=x
nord=0
zero=pi
! split the 1st integral to make it fast and accurate
x2=MIN(zero/x,1.d0/rvar)
x20=1.d-2*x2
SELECT CASE(ispe) 
CASE(   1)   ! the 1st portion...
   STOP' power-law spectra not implemented yet'
CASE(   2)
   ss=qromo(dxiCDMks_pk,0.d0,x20,dmidpnt)
CASE(   3)        
   x20m=min(x20,1.d-2*kco*kco)
   ss=qromo(dxiCDMks_pk,0.d0,x20m,dmidpnt)
END SELECT      
nmax=10      ! ...and the rest
x1=x20
DO 
   stp=EXP((dlog(x2)-dlog(x1))/dble(nmax))
   szero=0.d0
   DO i=1,nmax
      x2=x1*stp
      SELECT CASE(ispe)
      CASE(   1)     
         STOP' power-law spectra not implemented yet'
      CASE(   2)
         s=qromb(dxiCDMks_pk,x1,x2)
      CASE(   3)
         x2m=min(x2,kco)
         s=qromb(dxiCDMks_pk,x1,x2m)
      END SELECT
      szero=szero+s
      x1=x2
   END DO
   ss=ss+szero                             
   dxiks_pk=-ss
   IF(x2 >= 1.d0/rvar)RETURN  ! integral upper limit reached 
   zero=zero+pi
   x2=min(zero/x,1.d0/rvar)
   nmax=1      ! but do not subdivide remaining integrals 
END DO
END FUNCTION dxiks_pk


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xiCDMks_pk(k)
!
! Integrand of the correlation function for a CDM spectrum and Gaussian window of scale r
!
!     Uses: CDM_cos
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE cosmology, ONLY : CDM_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: k
REAL(KIND=8), DIMENSION(SIZE(k)) :: xiCDMks_pk,fsinx,kx

kx=k*xx
WHERE(kx > 1.d-8)
   fsinx=SIN(kx)/kx
ELSEWHERE
   fsinx=1.d0-kx**2/6.d0
END WHERE
xiCDMks_pk=fsinx*CDM_cos(k)*k**2
END FUNCTION xiCDMks_pk

    
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dxiCDMks_pk(k)

! Integrand of the derivative of the correlation function for a CDM spectrum and Gaussian
!  window of scale r.
!
!     Uses: CDM_cos
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE cosmology, ONLY : CDM_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: k
REAL(KIND=8), DIMENSION(SIZE(k)) :: dxiCDMks_pk,fsinx,kx

kx=k*xx
WHERE(kx > 1.d-8)
   fsinx=SIN(kx)/kx
ELSEWHERE
   fsinx=1.d0-kx**2/6.d0
END WHERE
dxiCDMks_pk=fsinx*CDM_cos(k)*k**4
END FUNCTION dxiCDMks_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION frmed1_pk(rs,x)

! First piece of the expression for mean delta(r)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: rs ! smoothing scale (Mpc)
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: frmed1_pk,sg0,sg1,sg2,gam,qmom,psi,d2psi,ogm1s
INTEGER :: klo
rvar=rs
klo=1
sg0=EXP(spl(lrcomg,lsg0,mlsg0,LOG(rs),klo))
sg1=EXP(spl(lrcomg,lsg1,mlsg0,LOG(rs),klo))
sg2=EXP(spl(lrcomg,lsg2,mlsg0,LOG(rs),klo))
gam=sg1**2/(sg0*sg2)
qmom=sg2/sg0
psi=cn2*xiga_pk(x)/sg0**2
d2psi=cn2*d2xiga_pk(x)/sg0**2
d2psi=-d2psi/qmom
ogm1s=1.d0-gam*gam
frmed1_pk=(psi/gam + d2psi)*gam/ogm1s
END FUNCTION frmed1_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION frmed2_pk(rs,x)

! First piece of the expression for mean delta(r)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: rs ! smoothing scale (Mpc)
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: frmed2_pk,sg0,sg1,sg2,gam,qmom,psi,d2psi,ogm1s
INTEGER :: klo
rvar=rs
klo=1
sg0=EXP(spl(lrcomg,lsg0,mlsg0,LOG(rs),klo))
sg1=EXP(spl(lrcomg,lsg1,mlsg0,LOG(rs),klo))
sg2=EXP(spl(lrcomg,lsg2,mlsg0,LOG(rs),klo))
gam=sg1**2/(sg0*sg2)
qmom=sg2/sg0
psi=cn2*xiga_pk(x)/sg0**2
d2psi=cn2*d2xiga_pk(x)/sg0**2
d2psi=-d2psi/qmom
ogm1s=1.d0-gam*gam
frmed2_pk=(gam*psi + d2psi)/ogm1s
END FUNCTION frmed2_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION frdis_pk(rs,x)

!  delta(r) dispersion

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: rs ! smoothing scale (in Mpc)
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: frdis_pk,sg0,sg1,sg2,gam,qmom,psi,dpsi,d2psi,ogm1s,part1,part2,part3
INTEGER :: klo
rvar=rs
klo=1
sg0=EXP(spl(lrcomg,lsg0,mlsg0,LOG(rs),klo))
sg1=EXP(spl(lrcomg,lsg1,mlsg0,LOG(rs),klo))
sg2=EXP(spl(lrcomg,lsg2,mlsg0,LOG(rs),klo))
gam=sg1**2/(sg0*sg2)
qmom=sg2/sg0
psi=cn2*xiga_pk(x)/sg0**2
dpsi=(cn2*dxiga_pk(x)/sg0**2-psi)/x
d2psi=cn2*d2xiga_pk(x)/sg0**2
d2psi=-d2psi/qmom
ogm1s=1.d0-gam*gam
part1=(psi*psi + (2.d0*gam*psi + d2psi)*d2psi)/ogm1s
part2=3.d0*dpsi/(x*qmom) - d2psi
part3=3.d0*dpsi*dpsi/(gam*qmom)
frdis_pk=part1 + 5.d0*part2*part2 + part3
frdis_pk=MIN(frdis_pk,1.d0)         ! forces frdis <= 1.d0
END FUNCTION frdis_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION f_pk(x)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
!USE math_lib, ONLY : erf
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) ::  f_pk,x2,c1,t1,t2

IF(x < 0.2d0)THEN
   c1=3.d0**5*5**1.5d0/(7.d0*2**11*SQRT(2.d0*pi))
   f_pk=x**8/(13.2d0*(1.d0+0.625d0*x**2))
ELSE
   c1=SQRT(2.5d0)
   t1=0.5d0*(erf(c1*x)+erf(c1*x/2.d0))
   t2=(7.75d0*x**2 + 1.6d0)*EXP(-0.625d0*x**2) + (0.5d0*x**2 -1.6d0)*EXP(-2.5d0*x**2)
   f_pk=(x**3-3.d0*x)*t1+SQRT(0.4d0/pi)*t2
END IF
END FUNCTION f_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION fix_pk(x)

! f(x)/x. Used to evaluate <1/x>  (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
!USE math_lib, ONLY : erf
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) ::  fix_pk,x2,c1,t1,t2

IF(x < 2.d-1)THEN
   c1=3.d0**5*5**2.5d0/(7.d0*2**11*SQRT(2.d0*pi))
   fix_pk=x**7/(13.2d0*(1.d0+0.625d0*x**2))
ELSE
   c1=SQRT(2.5d0)
   t1=0.5d0*(erf(c1*x)+erf(c1*x/2.d0))
   t2=(7.75d0*x**2 + 1.6)*EXP(-0.625d0*x**2) + (0.5d0*x**2 -1.6)*EXP(-2.5d0*x**2)
   fix_pk=((x**3-3.d0*x)*t1+SQRT(0.4d0/pi)*t2)/x
END IF
END FUNCTION fix_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION theta_pk(y,x)

! Fitting function used to evaluate the peak density profile (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x,y
REAL(KIND=8) ::  theta_pk,x2,yy1,t1,t2
yy1=1.d0-y**2
x2=x/2.d0
t1=3.d0*yy1+(1.216d0-0.9d0*y**4)*EXP(-0.5d0*y*x2**2)
t2=SQRT(3.d0*yy1+.45d0+x2**2)+x2
theta_pk=t1/t2
END FUNCTION theta_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION theta2_pk(y,x)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromb
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: y,x
INTEGER, PARAMETER :: nmax=10
INTEGER :: i
REAL(KIND=8) :: theta2_pk,ss0,ss1,s,x1,x2,stp

ss0=0.d0
ss1=0.d0
xs=x
gma1=1.d0-y**2
IF(x > 0.d0)THEN
   x1=1.d-3*x
   x2=2.d0
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   DO i=1,nmax
      x1=x2/stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      IF(s < 1.d-9*ss0)EXIT
      x2=x1
   END DO
   x1=1.d-3*MAX(x,1.d-30)
   x2=2.d0
   DO i=1,nmax
      x1=x2/stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-8*ss1)EXIT
      x2=x1
   END DO
   x1=2.d0
   x2=1.d3*ABS(x)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   DO i=1,nmax
      x2=x1*stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      IF(s < 1.d-9*ss0)EXIT
      x1=x2
   END DO
   x1=2.d0
   x2=1.d3*ABS(x)
   DO i=1,nmax
      x2=x1*stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-9*ss1)EXIT
      x1=x2
   END DO
ELSE
   x1=0.d0
   x2=1.d0
   stp=(x2-x1)/DBLE(nmax)
   DO i=1,nmax
      x2=x1+stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      IF(s < 1.d-9*ss0)EXIT
      x1=x2
   END DO
   x1=0.d0
   x2=1.d0
   stp=(x2-x1)/DBLE(nmax)
   DO i=1,nmax
      x2=x1+stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-8*ss1)EXIT
      x1=x2
   END DO
   x1=1.d0
   x2=1.d3
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   DO i=1,nmax
      x2=x1*stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      IF(s < 1.d-9*ss0)EXIT
      x1=x2
   END DO
   x1=1.d0
   x2=1.d3
   DO i=1,nmax
      x2=x1*stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-9*ss1)EXIT
      x1=x2
   END DO
END IF
theta2_pk=ss1/ss0-x
END FUNCTION theta2_pk
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xtypinv_pk(y,x)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromb
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: y,x
INTEGER, PARAMETER :: nmax=20
INTEGER :: i,kc
REAL(KIND=8) :: xtypinv_pk,ss0,ss1,s,x1,x2,stp,xl

ss0=0.d0
ss1=0.d0
xs=x
gma1=1.d0-y**2
xl=1.d100 ! no limit in the integral over x 
IF(x > 0.d0)THEN
   x1=1.d-9*x
   x2=MIN(2.d0,xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   IF(stp <= 0.d0)GOTO 11
   kc=0
   DO i=1,nmax
      x1=x2/stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      x2=x1
      IF(s < 1.d-9*ss0)EXIT
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=1.d-9*x
   x2=MIN(2.d0,xl)
   kc=0
   DO i=1,nmax
      x1=x2/stp
      s=qromb(pdfxix_pk,x1,x2)
      ss1=ss1+s
      x2=x1
      IF(s < 1.d-8*ss1)EXIT
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=MIN(2.d0,xl)
   x2=MIN(MAX(1.d1,1.d6*ABS(x)),xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   IF(stp <= 0.d0)GOTO 11  
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      x1=x2
      IF(s < 1.d-9*ss0)EXIT
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=MIN(2.d0,xl)
   x2=MIN(MAX(1.d1,1.d6*ABS(x)),xl)
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(pdfxix_pk,x1,x2)
      ss1=ss1+s
      x1=x2
      IF(s < 1.d-9*ss1)EXIT
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
ELSE
   x1=0.d0
   x2=MIN(1.d0,xl)
   stp=(x2-x1)/DBLE(nmax)
   IF(stp <= 0.d0)GOTO 11
   DO i=1,nmax
      x2=x1+stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      x1=x2
      IF(s < 1.d-9*ss0)EXIT
   END DO
   x1=0.d0
   x2=MIN(1.d0,xl)
   stp=(x2-x1)/DBLE(nmax)
   DO i=1,nmax
      x2=x1+stp
      s=qromb(pdfxix_pk,x1,x2)
      ss1=ss1+s
      x1=x2
      IF(s < 1.d-8*ss1)EXIT
   END DO
   x1=MIN(1.d0,xl)
   x2=MIN(1.d3,xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   IF(stp <= 0.d0)GOTO 11
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      x1=x2
      IF(s < 1.d-9*ss0.OR.ss0 < 1.d-200)EXIT
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=MIN(1.d0,xl)
   x2=MIN(1.d3,xl)
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(pdfxix_pk,x1,x2)
      ss1=ss1+s
      x1=x2
      IF(s < 1.d-9*ss1.OR.ss1 < 1.d-200)EXIT
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
END IF
11 CONTINUE
ss1=MAX(ss1,2.3d-308)
ss0=MAX(ss0,2.3d-308)
IF(ss0 > 0.d0)THEN
   xtypinv_pk=ss1/ss0
ELSE
   xtypinv_pk=0.d0
END IF
END FUNCTION xtypinv_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xtyp_pk(y,x)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromb
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: y,x
INTEGER, PARAMETER :: nmax=30
INTEGER :: i,kc
REAL(KIND=8) :: xtyp_pk,ss0,ss1,s,x1,x2,stp,xl

ss0=0.d0
ss1=0.d0
xs=x
gma1=1.d0-y**2
xl=1.d100 ! no limit in the integral over x 
IF(x > 0.d0)THEN
   x1=1.d-2*x
   x2=MIN(2.d0,xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   IF(stp <= 0.d0)GOTO 11
   kc=0
   DO i=1,nmax
      x1=x2/stp
      s=qromb(pdfxn_pk,x1,x2)      
      ss0=ss0+s
      IF(s < 1.d-9*ss0)EXIT
      x2=x1
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=1.d-2*x
   x2=MIN(2.d0,xl)
   kc=0
   DO i=1,nmax
      x1=x2/stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-8*ss1)EXIT
      x2=x1
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=MIN(2.d0,xl)
   x2=MIN(MAX(1.d1,1.d6*ABS(x)),xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   IF(stp <= 0.d0)GOTO 11  
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      IF(s < 1.d-9*ss0)EXIT
      x1=x2
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=MIN(2.d0,xl)
   x2=MIN(MAX(1.d1,1.d6*ABS(x)),xl)
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-9*ss1)EXIT
      x1=x2
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
      goto 11
   END IF
ELSE
   x1=0.d0
   x2=MIN(1.d0,xl)
   stp=(x2-x1)/DBLE(nmax)
   IF(stp <= 0.d0)GOTO 11
   DO i=1,nmax
      x2=x1+stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      IF(s < 1.d-9*ss0)EXIT
      x1=x2
   END DO
   x1=0.d0
   x2=MIN(1.d0,xl)
   stp=(x2-x1)/DBLE(nmax)
   DO i=1,nmax
      x2=x1+stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-8*ss1)EXIT
      x1=x2
   END DO
   x1=MIN(1.d0,xl)
   x2=MIN(1.d3,xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   IF(stp <= 0.d0)GOTO 11
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(pdfxn_pk,x1,x2)
      ss0=ss0+s
      IF(s < 1.d-9*ss0.OR.ss0 < 1.d-200)EXIT
      x1=x2
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=MIN(1.d0,xl)
   x2=MIN(1.d3,xl)
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-9*ss1.OR.ss1 < 1.d-200)EXIT
      x1=x2
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
END IF
11 CONTINUE
IF(ss0 > 0.d0)THEN
   xtyp_pk=ss1/ss0
ELSE
   xtyp_pk=0.d0
END IF
END FUNCTION xtyp_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xtypn_pk(y,x,n)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromb
IMPLICIT NONE
INTEGER, INTENT(IN) :: n  
REAL(KIND=8), INTENT(IN) :: y,x
INTEGER, PARAMETER :: nmax=30
INTEGER :: i,kc
REAL(KIND=8) :: xtypn_pk,ss1,s,x1,x2,stp,xl

ss1=0.d0
xs=x
gma1=1.d0-y**2
indn=n
xl=1.d100 ! no limit in the integral over x 
IF(x > 0.d0)THEN
   x1=1.d-2*x
   x2=MIN(2.d0,xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   kc=0
   DO i=1,nmax
      x1=x2/stp
      s=qromb(xavn_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-8*ss1)EXIT
      x2=x1
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=MIN(2.d0,xl)
   x2=MIN(MAX(1.d1,1.d6*ABS(x)),xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(xavn_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-9*ss1)EXIT
      x1=x2
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
ELSE
   x1=0.d0
   x2=MIN(1.d0,xl)
   stp=(x2-x1)/DBLE(nmax)
   DO i=1,nmax
      x2=x1+stp
      s=qromb(xavn_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-8*ss1)EXIT
      x1=x2
   END DO
   x1=MIN(1.d0,xl)
   x2=MIN(1.d3,xl)
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(xavn_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-9*ss1.OR.ss1 < 1.d-200)EXIT
      x1=x2
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
END IF
11 CONTINUE
xtypn_pk=ss1
END FUNCTION xtypn_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION H_pk(y,x)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : qromb
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: y,x
INTEGER, PARAMETER :: nmax=30
INTEGER :: i,kc
REAL(KIND=8) :: H_pk,ss1,s,x1,x2,stp

ss1=0.d0
xs=x
gma1=1.d0-y**2
IF(x > 0.d0)THEN
   x1=1.d-3*x
   x2=2.d0
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   kc=0
   DO i=1,nmax
      x1=x2/stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-8*ss1)EXIT
      x2=x1
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
   x1=2.d0
   x2=MAX(1.d1,1.d6*ABS(x))
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-9*ss1)EXIT
      x1=x2
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
ELSE
   x1=0.d0
   x2=1.d0
   stp=(x2-x1)/DBLE(nmax)
   DO i=1,nmax
      x2=x1+stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-8*ss1)EXIT
      x1=x2
   END DO
   x1=1.d0
   x2=MAX(1.d2,1.d6*ABS(x))
   stp=EXP((LOG(x2)-LOG(x1))/DBLE(nmax))
   kc=0
   DO i=1,nmax
      x2=x1*stp
      s=qromb(xav_pk,x1,x2)
      ss1=ss1+s
      IF(s < 1.d-9*ss1.OR.ss1 < 1.d-200)EXIT
      x1=x2
      kc=kc+1
   END DO
   IF(kc == nmax)THEN
      success=.FALSE.
      RETURN
   END IF
END IF
H_pk=ss1/SQRT(pi2*gma1)
END FUNCTION H_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION pdfxn_pk(x)

! Conditional probability of finding a value of the peak curvature x for a given amplitude nu (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) ::  pdfxn_pk

pdfxn_pk=EXP(-0.5d0*(x-xs)**2/gma1)*f_pk(x)
END FUNCTION pdfxn_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xinv_pk(x)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) ::  xinv_pk

xinv_pk=pdfxn_pk(x)/x
END FUNCTION xinv_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xav_pk(x)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) ::  xav_pk

xav_pk=x*pdfxn_pk(x)
END FUNCTION xav_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION xavn_pk(x)

! Function used to evaluate the average value of the peak curvature x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) ::  xavn_pk
INTEGER :: m,k

m=DIM(indn,1)
k=indn-m
xavn_pk=((x-barx)**m)*(x**k)*pdfxn_pk(x)
END FUNCTION xavn_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION pdfxix_pk(x)

!  Function used to evaluate the average valueof 1/x (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) ::  pdfxix_pk

pdfxix_pk=EXP(-0.5d0*(x-xs)**2/gma1)*fix_pk(x)
END FUNCTION pdfxix_pk


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION g_pk(y,x)

! Fitting function used to evaluate the number density of peaks (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: x,y
REAL(KIND=8) ::  g_pk,y2,aa,bb,c1,c2,c3,t1,t2
y2=y*y
aa=2.5d0/(9.d0-5.d0*y2)
bb=432.d0/(SQRT(1.d1*pi)*(9.d0-5.d0*y2)**2.5d0)
c1=1.84d0+1.13d0*(1.d0-y2)**5.72d0
c2=8.91d0+1.27d0*EXP(6.51d0*y2)
c3=2.58d0*EXP(1.05d0*y2)
t1=x**3 - 3.d0*y2*x + (bb*x**2 +c1)*EXP(-aa*x**2)
t2=1.d0 + c2*EXP(-c3*x)
g_pk=t1/t2
END FUNCTION g_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION npeakd_pk(delta,lrpk)

! Number density of peaks with height on scale R with heights in the interval (nu, nu+dnu) in Mpc^{-3} (BBKS)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE cosmology, ONLY : sn2ga_cos
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: delta ! peak overdensity
REAL(KIND=8), INTENT(IN) :: lrpk ! natural log of peak scale (in Mpc)
REAL(KIND=8) :: npeakd_pk,sg0,sg1,sg2,rst,gam,nu,xst 

rvar=EXP(lrpk)
sg0=SQRT(sn2ga_cos(0))
sg1=SQRT(sn2ga_cos(1))
sg2=SQRT(sn2ga_cos(2))
rst=SQRT(3.d0)*sg1/sg2
gam=sg1**2/(sg0*sg2)
nu=delta/(SQRT(cn2)*sg0)
xst=gam*nu
npeakd_pk=EXP(-0.5d0*nu**2)*g_pk(gam,xst)/(pi2**2*rst**3)
END FUNCTION npeakd_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION npeakr_pk(delta,lrpk)

! Scale function of peaks: number density of peaks with height nu in the interval (R, R+dR) in Mpc^{-4}

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: delta ! peak overdensity
REAL(KIND=8), INTENT(IN) :: lrpk ! natural log of peak scale (in Mpc)
REAL(KIND=8) :: npeakr_pk,sg0,sg1,sg2,rst,gam,nu,xst,fac 
INTEGER :: klo

klo=1
! peak spectral quantities
sg0=EXP(spl(lrcomg,lsg0,mlsg0,lrpk,klo))
sg1=EXP(spl(lrcomg,lsg1,mlsg1,lrpk,klo))
sg2=EXP(spl(lrcomg,lsg2,mlsg2,lrpk,klo))
rst=SQRT(3.d0)*sg1/sg2
gam=sg1**2/(sg0*sg2)
nu=delta/sg0
xst=gam*nu
fac=EXP(lrpk)*sg2/sg0
npeakr_pk=EXP(-0.5d0*nu**2)*H_pk(gam,xst)*fac/(pi2**2*rst**3)
END FUNCTION npeakr_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION npeakcond_pk(delta,lrpk,deltab,lrpkb)

! Conditional density of peak: number density of peaks with density contrast delta in the interval (R, R+dR) 
! at points with density contrast detal_b on scales R_b, in Mpc^{-4}

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: delta ! peak overdensity
REAL(KIND=8), INTENT(IN) :: lrpk ! natural log of peak scale (in Mpc)
REAL(KIND=8), INTENT(IN) :: deltab ! background field overdensity
REAL(KIND=8), INTENT(IN) :: lrpkb ! natural log of background scale (in Mpc)
REAL(KIND=8) :: npeakcond_pk,lrpkh,sg0,sg1,sg2,sg0b,sg0h,sg1h,rst,gam,gamt,nu,nub,nut,xst,epsil,eps12,r1,r11,fac 
INTEGER :: klo

klo=1
lrpkh=0.5d0*LOG(0.5d0*(EXP(2.d0*lrpk)+EXP(2.d0*lrpkb)))
! peak spectral quantities
sg0=EXP(spl(lrcomg,lsg0,mlsg0,lrpk,klo))
sg1=EXP(spl(lrcomg,lsg1,mlsg1,lrpk,klo))
sg2=EXP(spl(lrcomg,lsg2,mlsg2,lrpk,klo))
rst=SQRT(3.d0)*sg1/sg2
gam=sg1**2/(sg0*sg2)
nu=delta/sg0
! background field spectral quantities
sg0b=EXP(spl(lrcomg,lsg0,mlsg0,lrpkb,klo))
nub=deltab/sg0b
! cross momenta and related quantities
sg0h=EXP(spl(lrcomg,lsg0,mlsg0,lrpkh,klo))
sg1h=EXP(spl(lrcomg,lsg1,mlsg1,lrpkh,klo))
epsil=sg0h**2/(sg0*sg0b)
r1=(sg1h*sg0/(sg0h*sg1))**2
eps12=1.d0-epsil**2
r11=1.d0-r1
IF(EPS12 == 0.D0)THEN
   npeakcond_pk=1.d-30
   RETURN
END IF
gamt=gam*SQRT(1.d0+(epsil*r11)**2/eps12)
nut=(gam/gamt)*(r11/eps12)*(nu*(1.d0-epsil**2*r1)/r11-epsil*nub)
xst=gamt*nut 
fac=EXP(lrpk)*sg2/sg0
IF(gamt > 0.9999d0)THEN
   npeakcond_pk=1.d-30
ELSE
   npeakcond_pk=EXP(-0.5d0*(nu-epsil*nub)**2/eps12)*H_pk(gamt,xst)*fac/(pi2**2*rst**3*SQRT(eps12))
END IF
END FUNCTION npeakcond_pk


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION npkconr_pk(delta,lrpk,deltab,lrpkb,lr)

! Conditional conditional number density of peaks with δ at scales between R and R + dR subject to being at a distance r
! in Mpc^{-4}

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE 
REAL(KIND=8), INTENT(IN) :: delta ! peak overdensity
REAL(KIND=8), INTENT(IN) :: lrpk ! natural log of peak scale (in Mpc)
REAL(KIND=8), INTENT(IN) :: deltab ! background field overdensity
REAL(KIND=8), INTENT(IN) :: lrpkb ! natural log of background scale (in Mpc)
REAL(KIND=8), INTENT(IN) :: lr ! distance from the peak center (in units of qR)
REAL(KIND=8) :: npkconr_pk,lrpkh,sg0,sg1,sg2,sg0b,sg0h,sg1h,rst,gam,gamt,nu,nub,nut,xst,epsil,r1,r11,fac,t1,t2,nur, &
     dnur,epsnur,epsr2,oepsr2,gr,rb,sg1b,sg2b,gamb,qrb,ss,yy
INTEGER :: klo

klo=1
lrpkh=0.5d0*LOG(0.5d0*(EXP(2.d0*lrpk)+EXP(2.d0*lrpkb)))
rb=EXP(lrpkb) ! filtering radius for the mean and rms density contrast at r form the background peak
ss=EXP(lr)
qrb=EXP(spl(lxg,lxt,mlxt,lrpkb,klo))

! peak spectral quantities
klo=1
sg0=EXP(spl(lrcomg,lsg0,mlsg0,lrpk,klo))
sg1=EXP(spl(lrcomg,lsg1,mlsg1,lrpk,klo))
sg2=EXP(spl(lrcomg,lsg2,mlsg2,lrpk,klo))
rst=SQRT(3.d0)*sg1/sg2
gam=sg1**2/(sg0*sg2)
nu=delta/sg0

! background field spectral quantities
sg0b=EXP(spl(lrcomg,lsg0,mlsg0,lrpkb,klo))
sg1b=EXP(spl(lrcomg,lsg1,mlsg1,lrpkb,klo))
sg2b=EXP(spl(lrcomg,lsg2,mlsg2,lrpkb,klo))
gamb=sg1b**2/(sg0b*sg2b)
nub=deltab/sg0b

! r functions
barx=0.d0
yy=xtypn_pk(gamb,gamb*nub,1)
barx=xtypn_pk(gamb,gamb*nub,2)/yy
t1=frmed1_pk(rb,qrb*ss)
t2=frmed2_pk(rb,qrb*ss)
nur=nub*t1-barx*t2
dnur=1.d0-frdis_pk(rb,qrb*ss)+(xtypn_pk(gamb,gamb*nub,3)/yy)*t2**2
gr=SQRT(1.d0-dnur) 

! cross momenta and related quantities
sg0h=EXP(spl(lrcomg,lsg0,mlsg0,lrpkh,klo))
sg1h=EXP(spl(lrcomg,lsg1,mlsg1,lrpkh,klo))
r1=(sg1h*sg0/(sg0h*sg1))**2
epsil=sg0h**2/(sg0*sg0b)

epsnur=epsil*nur
epsr2=(epsil*gr)**2
oepsr2=1.d0-epsr2
IF(OEPSR2 < ACCUR8)STOP' EPSR=1'
r11=1.d0-r1
gamt=gam*SQRT(1.d0+epsr2*r11**2/oepsr2)
nut=(gam/gamt)*(r11/oepsr2)*(nu*(1.d0-epsr2*r1)/r11-epsnur)

xst=gamt*nut 
fac=EXP(lrpk)*sg2/sg0
IF(gamt > 0.9999d0)THEN
   npkconr_pk=1.d-18
ELSE
   yy=EXP(-0.5d0*(nu-epsnur)**2/oepsr2)*H_pk(gamt,xst)*fac/(pi2**2*rst**3*SQRT(oepsr2))
   !WRITE(*,'(8(1x,g12.5))')ss,oepsr2,dnur,gamt,xst,H_pk(gamt,xst),nu-epsnur,yy
   npkconr_pk=yy
END IF
END FUNCTION npkconr_pk


END MODULE peak

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

MODULE profiles

USE global_defs

IMPLICIT NONE

! COMMONS
REAL(KIND=8) :: prfac,lhr                                           !/cdpp/
REAL(KIND=8) :: hm0                                                 !/cfoz/
REAL(KIND=8) :: edmf,entf,gmrc,fndv                                 !/cfktf/
REAL(KIND=8), DIMENSION(npn) :: ax,ay,az,may                        !/cfit/
REAL(KIND=8), DIMENSION(npn) :: mx,my,yf,mmy,myf                    !/cprof_ac/

CONTAINS

!     subroutine prep_pr
!     subroutine model_pr
!     subroutine fitNFW_pr
!     subroutine fitSer_pr
!     subroutine par_pr
!     function cNFW_pr
!     function foz1_pr
!     function foz2_pr
!     function prof_pr
!     function func_pr
!     function smr_pr
!     function ndv_pr
!     function ndv2_pr
!     function massr_pr
!     function kinpNFW_pr
!     function etherm_pr
!     function virin_pr
!     function ftro_pr
!     function fktf_pr
!     function flrs_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE prep_pr(zobs,lozmax,hmup)

!     Gives the scaling parameters of the SI (actually none) or the NFW density profile (at z_obs) for a range of halo masses.
!       
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : gammln,gammp,spl,spline,zbrac,zbrent
USE cosmology, ONLY : delz_cos,dvbn_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: zobs      ! current redshift
REAL(KIND=8), INTENT(IN) :: lozmax    ! log of maximum redshift
REAL(KIND=8), INTENT(IN) :: hmup      ! maximum,halo mass (in M_o)
INTEGER :: h,j,k,jj,mm
INTEGER, PARAMETER :: rini=30
REAL(KIND=8) :: stpz,stp,dtduz,tdde,x1,x2,pn,rn,in,n3,cns,ro0,stpm,hz,oztp,t,an,an1,fan,dcf,hr,di,de,at,dotm,rs,y,vy(1)
REAL(KIND=8), DIMENSION(npn) :: x,chi2
REAL(KIND=8), DIMENSION(npc) :: lhrm,lhmm,pr,nfw,soz,maux

!--singular isothermal profile
IF (iprof == 0) THEN
   PRINT*,'   Arbitrary scale radius'
   x1=1.d0+zobs
   PRINT*,'   Singular Isothermal profile prepared at all z'
!--NFW profile
ELSE
   x1=1.d0+zobs
   CALL delz_cos(x1,t,di,dtduz,de,tdde,at)
   hm0=EXP(lrhopi43+3.d0*alrcom(rini))
   IF(iprof == 1)THEN
   !--NFW rs-Ms correlation at z=0 (3rd-degree polynomial fitting)
      stpm=(3.d1*hmup/hm0)**(1.d0/(npn-1))
      lhm1=arth(LOG(hm0),LOG(stpm),npn)
      DO mm=1,npn
         hm0=EXP(lhm1(mm))
         u1=1.d0
         u2=EXP(lozmax)
         CALL zbrac(foz1_pr,u1,u2,success)
         y=EXP(lozmax)-1.d0
         IF(success)y=zbrent(foz1_pr,u1,u2,1.d-4)-1.d0
         CALL model_pr(0.d0,hm0,y,lhrm,lhmm,pr,soz)
         CALL fitNFW_pr(npc,lhrm,lhmm,pr,soz,rs)
         ax(mm)=LOG(rs)
         CALL spline(lhrm,lhmm,maux)
         ay(mm:mm)=spl(lhrm,lhmm,maux,ax(mm:mm))
      END DO
      PRINT*,'   NFW density profile prepared at all z'
   ELSE
      !--Sersic profile
      stpm=(2.d0*hmup/hm0)**(1.d0/(npn-1))
      lhm1=arth(LOG(hm0),LOG(stpm),npn)
      DO mm=1,npn
         hm0=EXP(lhm1(mm))
         u1=1.d0+zobs
         u2=1.d3
         CALL zbrac(foz2_pr,u1,u2,success)
         y=EXP(lozmax)-1.d0
         IF(success)y=zbrent(foz2_pr,u1,u2,1.d-4)-1.d0
         CALL model_pr(zobs,hm0,y,lhrm,lhmm,pr,soz)
         CALL fitSer_pr(npc,lhrm,lhmm,pr,soz,rn,pn)
         n3=3.d0*pn
         in=1.d0/pn
         cns=EXP(lhrm(npc))/rn
         ro0=x1**3*r13*dvbn_cos(x1)*cns**3*in/(EXP(gammln(n3))*gammp(n3,cns**in))
         ay(mm)=LOG(pn)
         az(mm)=LOG(ro0)
         ax(mm)=LOG(rn)
      END DO
      STOP' Sersic not available yet'
   END IF
   CALL spline(ax,ay,may)
END IF
END SUBROUTINE prep_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE model_pr(zobs,hm,zmax,lhr,lhm,pr,oz)

!     Calculates the halo density profile according to the mass growth model by Raig, Gonzalez-Casado & Salvador-Sole (2001) to be fitted by the NWF
!     expression. Virial radii are defined from the cosmic mean density.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : odeint,drkqs,spl,spline
USE cosmology, ONLY : mar_cos,dldvdz_cos,dvbn_cos,delz_cos,dlrom_cos,dlrdoz_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: zobs                 ! current redshift
REAL(KIND=8), INTENT(IN) :: hm                   ! halo mass, in M_o, at zobs
REAL(KIND=8), INTENT(IN) :: zmax                 ! Maximum redshift for halo profile
REAL(KIND=8), INTENT(OUT), DIMENSION(:) :: lhr   ! array with radial distances for halo profile (log Mpc)
REAL(KIND=8), INTENT(OUT), DIMENSION(:) :: lhm   ! array with inner halo masses (log M_o)
REAL(KIND=8), INTENT(OUT), DIMENSION(:) :: pr    ! array with the density profile (normalized to rho_m)
REAL(KIND=8), INTENT(OUT), DIMENSION(:) :: oz    ! array with redshifts
INTEGER :: k,kk,np
REAL(KIND=8) :: tc,t,at0,dlma,de,di,tdde,dtdoz,x1a,x2a,stpz,lrof,stpp,lr,dlro,dlro2,lmar,at,stp,lr0,oz1,oz2,x1,x2,y,ez,dldv
REAL(KIND=8), DIMENSION(SIZE(pr)) :: lx
REAL(KIND=8), DIMENSION(SIZE(pr),3) :: aux,maux

x1=1.d0+zobs
x2=1.d0+zmax+1.d0 ! increased zmax to fulfill
!--current time (mean density)
np=SIZE(lhr)
lhm(np)=LOG(hm)
CALL delz_cos(x1,tc,di,dtdoz,de,tdde,at0)
lr0=(lhm(np)-lrhopi43)/3.d0 ! comoving scale
CALL dlrom_cos(tc,x1,ez,dlro,dlro2) ! der log of rho_m
lrof=lrhopi43+LOG(dvbn_cos(x1)*ez) ! log dv*rho(t)*4*pi/3
lx(np)=(lhm(np)-lrof)/3.d0
lmar=LOG(mar_cos(de,lr0))+LOG(tdde)+lhm(np)
dlma=tc*EXP(lmar-lhm(np))
dldv=tc*dldvdz_cos(x1)/dtdoz
pr(np)=dvbn_cos(x1)*ez/(1.d0-(dlro+dldv)/dlma) ! rho(t)/rho_m0
oz(np)=x1
!--maximum redshift
stpz=(x2/x1)**(1.d0/net)
x1a=x1
lr=lr0
DO kk=1,net
   x2a=x1a*stpz
   stp=1.d-3*(x2a-x1a)
   CALL odeint(lr,x1a,x2a,1.d-8,stp,0.d0,dlrdoz_cos,drkqs)
   x1a=x2a
END DO
lhm(1)=lrhopi43+3.d0*lr ! halo mass
CALL delz_cos(x2,t,di,dtdoz,de,tdde,at)
lmar=LOG(mar_cos(de,lr))+LOG(tdde)+lhm(1)
dlma=t*EXP(lmar-lhm(1))
CALL dlrom_cos(t,x2,ez,dlro,dlro2) ! der log of rho_m
lrof=lrhopi43+LOG(dvbn_cos(x2)*ez) ! log dv*rho(t)*4*pi/3
lx(1)=(lhm(1)-lrof)/3.d0 ! log total radius
dldv=t*dldvdz_cos(x2)/dtdoz
pr(1)=dvbn_cos(x2)*ez/(1.d0-(dlro+dldv)/dlma) ! rho(t)/rho_m0
oz(1)=x2
!--log radial step
stpp=(x1/x2)**(1.d0/(np-1))
oz2=x2
DO k=2,np-1
   oz1=oz2*stpp      
   stpz=(oz1/oz2)**(1.d0/net)
   x1a=oz2
   DO kk=1,net
      x2a=x1a*stpz
      stp=1.d-3*(x2a-x1a) 
      CALL odeint(lr,x1a,x2a,1.d-8,stp,0.d0,dlrdoz_cos,drkqs)
      x1a=x2a
   END DO
   lhm(k)=lrhopi43+3.d0*lr ! halo mass
   CALL delz_cos(oz1,t,di,dtdoz,de,tdde,at)
   lmar=LOG(mar_cos(de,lr))+LOG(tdde)+lhm(k)
   dlma=t*EXP(lmar-lhm(k))
   CALL dlrom_cos(t,oz1,ez,dlro,dlro2) ! der log of rho_m
   lrof=lrhopi43+LOG(dvbn_cos(oz1)*ez)
   lx(k)=(lhm(k)-lrof)/3.d0 ! log total radius
   dldv=t*dldvdz_cos(oz1)/dtdoz
   pr(k)=dvbn_cos(oz1)*ez/(1.d0-(dlro+dldv)/dlma) ! rho(t)/rho_m0
   oz(k)=oz1
   oz2=oz1
END DO
aux(:,1)=lhm ! log of mass
aux(:,2)=LOG(pr) ! log of density
aux(:,3)=LOG(oz) ! log of 1+z
CALL spline(lx,aux(:,1),maux(:,1))
CALL spline(lx,aux(:,2),maux(:,2))
CALL spline(lx,aux(:,3),maux(:,3))
!--log step in radius in the interval (0.01,1)R_vir
!y=MERGE(LOG(1.d-2)+lx(np),LOG(1.d-6),iprof == 1)
y=MERGE(LOG(.25d-2)+lx(np),LOG(1.d-6),iprof == 1)
stp=(lx(np)-y)/(np-1)
lhr=arth(y,stp,np)
lhm=spl(lx,aux(:,1),maux(:,1),lhr)
pr=rho*EXP(spl(lx,aux(:,2),maux(:,2),lhr))
oz=EXP(spl(lx,aux(:,3),maux(:,3),lhr))
END SUBROUTINE model_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE fitNFW_pr(npc,lhrm,lhmm,pr,soz,rs)

!     Gives the scaling parameters of the SI (actually none) or the NFW density profile (at z_obs) for a range of halo masses.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : zbrac,zbrent
USE cosmology, ONLY : dvbn_cos
IMPLICIT NONE
INTEGER,INTENT(IN) :: npc                       ! dimension of input arrays  
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: lhrm  ! array with radial distances for halo profile (log Mpc)
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: lhmm  ! array with inner halo masses (log M_o) 
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: pr    ! array with the density profile (normalized to rho_m)
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: soz   ! array with redshifts
REAL(KIND=8), INTENT(OUT) :: rs                 ! scale factor of the NFW profile
INTEGER :: kj,jj,h
REAL(KIND=8) :: hr,stp,an,an1,fan,dcf,y
REAL(KIND=8), DIMENSION(npn) :: x,chi2
REAL(KIND=8), DIMENSION(SIZE(pr)) :: nfw

jj=npc
IF(MOD(jj,2) /= 0)jj=jj+1
delv=dvbn_cos(soz(jj/2))
rfac=EXP(lhmm(jj/2)-3.d0*lhrm(jj/2))/(pi43*delv)
profcen=pr(jj/2)
u1=1.d-10
u2=1.d3
CALL zbrac(cNFW_pr,u1,u2,success)
y=EXP(lhrm(jj/2))/zbrent(cNFW_pr,u1,u2,1.d-4*u2)
rfac=rho*soz(npc)**3
hr=EXP(lhrm(npc))
x(1)=y/hr ! corresponding xs at zobs
jj=1
DO WHILE(jj == npn.or.jj == 1)
   y=x(1)/5.d0
   x(1)=x(1)-y ! initial x to check
   stp=1.5d0**(1.d0/(npn-1)) ! log step 
   jj=1
   DO kj=1,npn
      chi2(kj)=0.d0
      x(kj)=x(1)*stp**(kj-1)
      an=x(kj)
      an1=1.d0+an
      fan=1.d0/(LOG(an1/an)-1.d0/an1)
      dcf=dvbn_cos(soz(npc))*fan/(3.d0*an**3)
      rs=an*hr
      DO h=1,npc
         IF(EXP(lhrm(h))/hr-1.d-2 > zero)THEN
            y=EXP(lhrm(h))/rs
            nfw(h)=dcf*rfac/(y*(1.d0+y)*(1.d0+y))
            chi2(kj)=chi2(kj)+(LOG(pr(h)/nfw(h))/LOG(0.25d0*nfw(h)))**2
         END IF
      END DO
      chi2(kj)=chi2(kj)/npc
      IF(chi2(kj)-chi2(jj) < zero)jj=kj
   END DO
   IF(jj == npn.or.jj == 1)x(1)=x(jj)
END DO
rs=x(jj)*hr
END SUBROUTINE fitNFW_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE fitSer_pr(npc,lhrm,lhmm,pr,soz,rn,pn)

!     Gives the scaling parameters of the SI (actually none) or the NFW density profile (at z_obs) for a range of halo masses.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : gammln,gammp
USE cosmology, ONLY : dvbn_cos
IMPLICIT NONE
INTEGER,INTENT(IN) :: npc                           ! dimension of input arrays  
REAL(KIND=8), INTENT(IN), DIMENSION(npc) :: lhrm    ! array with radial distances for halo profile (log Mpc)
REAL(KIND=8), INTENT(IN), DIMENSION(npc) :: lhmm    ! array with inner halo masses (log M_o) 
REAL(KIND=8), INTENT(IN), DIMENSION(npc) :: pr      ! array with the density profile (normalized to rho_m)
REAL(KIND=8), INTENT(IN), DIMENSION(npc) :: soz     ! array with redshifts
REAL(KIND=8), INTENT(OUT) :: rn,pn
INTEGER :: iaa,ial,kj,j1,j2,jj2
INTEGER, PARAMETER :: nxi=401,nyi=401  
REAL(KIND=8) :: cns,hm,aa,aa1,aa2,al,al1,al2,in,n3,aax,alx,xia,al01,al02,lron,lsers,stp1,stp2,oz1,mst0,dv
REAL(KIND=8) :: xi2aa(nxi),xi2(nxi,nyi)

mst0=EXP(lhmst)
hm=EXP(lhmm(npc))
oz1=soz(npc)
dv=dvbn_cos(oz1)
IF(hm > mst0)THEN
   aa1=LOG(1.d10)
   aa2=LOG(1.d26)
ELSE
   aa1=LOG(1.d4)
   aa2=LOG(1.d16)
END IF
j1=1
DO WHILE(j1 == 1)
   j2=1
   stp1=(aa2-aa1)/(nxi-1)
   aa2=aa1
   xia=1.d99
   !--loop for cn's (log) 
   DO iaa=1,nxi 
      aa=aa1
      cns=EXP(aa)
      xi2aa(iaa)=1.d99
      IF(hm > mst0)THEN
         al1=8.d0
         al2=20.d0
      ELSE
         al1=4.d0
         al2=12.d0
      END IF
      jj2=1
      DO WHILE(jj2 == 1) 
         stp2=(al2-al1)/(nyi-1)
         al2=al1
         !--loop for n's 
         DO ial=1,nyi
            al=al1
            n3=3.d0*al
            in=1.d0/al
            lron=LOG(oz1**3*r13*dv*cns**3*in/(EXP(gammln(n3))*gammp(n3,cns**in)))
            xi2(iaa,ial)=0.d0
            DO kj=1,npc
               lsers=lron-EXP((lhrm(kj)-lhrm(npc)+aa)*in)
               xi2(iaa,ial)=xi2(iaa,ial)+(LOG(pr(kj)/rhoc)-lsers)**2
            END DO
            xi2(iaa,ial)=xi2(iaa,ial)/(npc-1)
            IF(xi2(iaa,ial)-xi2aa(iaa) < zero)THEN
               xi2aa(iaa)=xi2(iaa,ial)
               j2=ial
               IF(xi2aa(iaa)-xia < zero)THEN
                  jj2=j2
                  alx=al2+(jj2-1)*stp2
                  xia=xi2aa(iaa)
               END IF
            END IF
            al1=al1+stp2
         END DO
         IF(jj2 == 1)al1=MAX(1.d0,al2-stp2*(nyi-1))
      END DO
      aa1=aa1+stp1
      IF(xi2aa(iaa)-xi2aa(j1) < zero)j1=iaa
      aax=aa2+(j1-1)*stp1
   END DO
   IF(j1 == 1)aa1=MAX(1.d0,aa2-stp1*(nxi-1))
END DO
!--2D subgrid to improve Sersic fit
aa1=aax-5.d0*stp1
aa2=aax+5.d0*stp1
stp1=(aa2-aa1)/(nxi-1)
al01=alx-3.d0*stp2
al02=alx+3.d0*stp2
j1=1
j2=1
aa2=aa1
xia=1.d99
!--loop for cn's (log) 
DO iaa=1,nxi
   aa=aa1
   cns=EXP(aa)
   xi2aa(iaa)=1.d99
   al1=al01
   al2=al02
   stp2=(al2-al1)/(nyi-1)
   al2=al1
   !--loop for n's
   DO ial=1,nyi 
      al=al1
      n3=3.d0*al
      in=1.d0/al
      lron=LOG(oz1**3*r13*dv*cns**3*in/(EXP(gammln(n3))*gammp(n3,cns**in)))
      xi2(iaa,ial)=0.d0
      DO kj=1,npc
         lsers=lron-EXP((lhrm(kj)-lhrm(npc)+aa)*in)
         xi2(iaa,ial)=xi2(iaa,ial)+(LOG(pr(kj)/rhoc)-lsers)**2
      END DO
      xi2(iaa,ial)=xi2(iaa,ial)/(npc-1)
      IF(xi2(iaa,ial)-xi2aa(iaa) < zero)THEN
         xi2aa(iaa)=xi2(iaa,ial)
         j2=ial
         IF(xi2aa(iaa)-xia < zero)THEN
            jj2=j2
            alx=al2+(jj2-1)*stp2
            xia=xi2aa(iaa)
         END IF
      END IF
      al1=al1+stp2
   END DO
   aa1=aa1+stp1
   IF(xi2aa(iaa)-xi2aa(j1) < zero)j1=iaa
   aax=aa2+(j1-1)*stp1
END DO
close(3)
rn=EXP(lhrm(npc)-aax)
pn=alx
cns=EXP(aax)
END SUBROUTINE fitSer_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE par_pr(lhm,lt,rs)

!     Find the parameters of the density profile for NFW (iprof=1) or Sersic (iprof=2) through time-free correlations
!
!     Uses: flrs_pr
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl,zbrent2
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: lhm    ! log of halo mass (in M_o)       
REAL(KIND=8), INTENT(IN) :: lt     ! log of cosmic time (in Gyr)   
REAL(KIND=8), INTENT(OUT) :: rs    ! scale radius in Mpc (NFW or Sersic)  
INTEGER :: klo

klo=1
lhr=(lhm-spl(slt,lrcfac,mlrcfac,-lt,klo))/3.d0
IF(iprof == 1)THEN     
   prfac=lhm+LOG(l2-0.5d0)
   rs=EXP(zbrent2(flrs_pr,ax(1),ax(npn),1.d-6))
ELSE
   STOP' Sersic law not implemented yet'
END IF
END SUBROUTINE par_pr


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION cNFW_pr(x)

!     Its root gives an approximate value of the NFW concentration for a halo of central mass.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: cNFW_pr,dc

dc=delv*func_pr(1,x)*x**3/3.d0
cNFW_pr=profcen-dc*rfac/4.d0
END FUNCTION cNFW_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION foz1_pr(x)

!     Auxiliar funtion used to find 1+z at which R(1+z)/R(1)=0.01, when building the inside-out profile (NFW fitting).     

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : odeint,drkqs
USE cosmology, ONLY : dvbn_cos,dlrdoz_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: foz1_pr,x1,stp,hmz,lrfoz

lrfoz=(LOG(hm0)-lrhopi43)*r13
x1=1.d0
stp=1.d-2*(x-x1)
CALL odeint(lrfoz,x1,x,1.d-9,stp,0.d0,dlrdoz_cos,drkqs)
hmz=EXP(lrhopi43+3.d0*lrfoz)
foz1_pr=.249d-2-(hmz*dvbn_cos(x1)/(hm0*dvbn_cos(x)))**r13/x
END FUNCTION foz1_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION foz2_pr(x)

!     Auxiliar funtion used to find 1+z at which R(1+z)=1 pc, when building the inside-out profile (Sersic fitting).

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : odeint,drkqs
USE cosmology, ONLY : dvbn_cos,dlrdoz_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: foz2_pr,x1,stp,lrfoz

lrfoz=(LOG(hm0)-lrhopi43)*r13
x1=1.d0+zobs
stp=1.d-2*(x-x1)
CALL odeint(lrfoz,x1,x,1.d-9,stp,0.d0,dlrdoz_cos,drkqs)
foz2_pr=1.d-6-EXP(lrfoz)/(x*dvbn_cos(x)**r13)
END FUNCTION foz2_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION prof_pr(in,cc,x)

!     Calcultes several radial profiles for halos endowed with the SI or NFW density profile of the halo according to the value of in.

!     Uses: kinpNFW_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : dmidpnt,qromo
IMPLICIT NONE
INTEGER, INTENT(IN) :: in     ! index to select the profile
                              !  (1) density profile normalized to rho_c
                              !  (2) mass profile normalized to M_v
                              !  (3) gravitational potential, in units of V_c**2=G*M**2/R
                              !  (4) log of hot gas density profile norm to the central density
                              !  (5) derivative of log of Hot gas density profile
                              !  (6) 1-D vel dispersion, in units of 2.d0*rs*dsqrt(pi*g*rhoc)
                              !  (7) gravitational energy, in units of V_c**2=G*M**2/R
                              !  (8) specific thermal energy profile in units of G*M/R*c*f(c)
REAL(KIND=8), INTENT(IN) :: cc! halo concentration (R_v/r_s)
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x ! radial distance normalized to R_v (SI) or r_s (NFW)
INTEGER :: itt,inprof,k
REAL(KIND=8) :: y,apres,x1,x2,fcc
REAL(KIND=8), DIMENSION(SIZE(x)) :: prof_pr,xx,xx1,ycf,potn,u,stp,aden

ycf=1.d0                                    ! contraction factor (rf/ri)
xx=x
xx1=1.d0+xx
inprof=iprof*8+in
SELECT CASE ( inprof )
!--Singular Isothermal profile (INDEPENDENT OF CC)
CASE (    1)
   ! density profile normalized to rho_char
   prof_pr=1.d0/xx**2/ycf**3
CASE (    2)
   !--mass profile normalized to M_v
   prof_pr=xx
CASE (    3)
   !--gravitational potential normalized to V_c**2
   prof_pr=(-1.d0+LOG(xx))/ycf
CASE (    4)
   !--log of hot gas density profile normalized to the density at R_v
   potn=(-1.d0+LOG(xx))/ycf
   u=(potf-potn)/ktn
   IF(gpi == 1.d0)THEN
      prof_pr=u*loge
   ELSE
      prof_pr=(1.d0/(gpi-1.d0))*LOG10(1.d0+u*(gpi-1.d0)/gpi)
   END IF
CASE (    5)
   !--derivative of log of Hot gas density profile
   potn=(-1.d0+LOG(xx))/ycf
   prof_pr=-1.d0/(gpi*ktn)
   IF(gpi > 1.d0)prof_pr=prof_pr/(1.d0+(gpi-1.d0)/gpi*(potf-potn)/ktn)
CASE (    6)
   !--1-D velocity dispersion in units of 2.d0*rs*dsqrt(pi*G*rhoc)
   prof_pr=1.d0/sq2/ycf
CASE (    7)
   !--gravitational energy in units of G*M**2/R
   prof_pr=-xx*(1.d0-0.5D0*LOG(xx))/ycf
CASE (    8)
   !--specific thermal energy profile in units of G*M/R*c*f(c)
   potn=-1.d0+LOG(xx)
   prof_pr=ktn+(gpi-1.d0)/gpi*(potf-potn)
!-----NFW profile
CASE (    9)
   !--density profile normalized to rho_char
   prof_pr=1.d0/(xx*xx1**2)/ycf**3
CASE (   10)
   !--mass profile normalized to M_v
   fcc=func_pr(1,cc)
   WHERE(xx < 1.d-5)
      prof_pr=fcc*xx*xx*(3.d0-4.d0*xx)/6.d0
   ELSEWHERE
      prof_pr=fcc*(LOG(xx1)-xx/xx1)
   END WHERE
CASE (   11)
   !--gravitational potential normalized to V_c**2
   fcc=func_pr(1,cc)
   WHERE(xx < 1.d-5)
      prof_pr=-cc*fcc*(1.d0-xx/2.d0+xx**2/3.d0)/ycf
    ELSEWHERE
      prof_pr=-cc*fcc*LOG(xx1)/xx/ycf
   END WHERE
CASE (   12)
   !--log of Hot gas density profile normalized to the density R_v
   WHERE(xx < 1.d-5)
      potn=-(1.d0-xx/2.d0+xx**2/3.d0)/ycf
    ELSEWHERE
      potn=-LOG(xx1)/xx/ycf
   END WHERE
   IF(gpi == 1.d0)THEN
      prof_pr=(potf-potn)/ktn*loge
   ELSE
      prof_pr=(1.d0/(gpi-1.d0))*LOG10(1.d0+(potf-potn)/ktn*(gpi-1.d0)/gpi)
   END IF
CASE (   13)
   !--derivative of log of hot gas density profile
   WHERE(xx < 1.d-5)
      potn=-(1.d0-xx/2.d0+xx**2/3.d0)/ycf
      prof_pr=-xx*(0.5D0-2.d0*xx/3.d0)/(gpi*ktn)
    ELSEWHERE
      potn=-LOG(xx1)/xx/ycf
      prof_pr=-(LOG(xx1)-xx/xx1)/(gpi*ktn*xx)
   END WHERE
   IF(gpi > 1.d0)prof_pr=prof_pr/(1.d0+(gpi-1.d0)/gpi*(potf-potn)/ktn)
CASE (   14)
   !--1-D velocity dispersion in units of 2.d0*rs*dsqrt(pi*G*rhoc)
   u=xx/(1.d0+xx)
   stp=(1.d0/u)**(1.d0/npt)
   DO k=1,SIZE(x)
      x1=u(k)
      apres=0.d0
      DO itt=1,npt
         x2=x1*stp(k)
         y=qromo(kinpNFW_pr,x1,x2,dmidpnt)
         apres=apres+y
         IF(ABS(y) < eps*ABS(apres))EXIT
         x1=x2
      END DO
   END DO
   aden=1.d0/(xx*xx1**2)
   prof_pr=SQRT(apres/aden/ycf)
CASE (   15)
   !--gravitational energy in units of G*M**2/R
   fcc=func_pr(1,cc)
   y=cc*fcc*fcc
   WHERE(xx < 1.d-5)
      prof_pr=-.5D0*(y*xx*xx/2.d0)/ycf
    ELSEWHERE
      prof_pr=-.5D0*(y*(xx-LOG(xx1))/xx1)/ycf
   END WHERE
CASE (   16)
   !--specific thermal energy profile in units of G*M/R*c*f(c)
   WHERE(xx < 1.d-5)
      potn=-(1.d0-xx/2.d0+xx**2/3.d0)
    ELSEWHERE
      potn=-LOG(xx1)/xx
   END WHERE
   prof_pr=ktn+(gpi-1.d0)/gpi*(potf-potn)
END SELECT
END FUNCTION prof_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION func_pr(in,cc)

!     Calcultes several concentration functions related to the SI or NFW density profile of the halo according to the value of in.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, INTENT(IN) :: in     ! index to select the function:
                              !  (1) f for halo density
                              !  (2) k for  halo potential
                              !  (3) g for gas temp
                              !  (4) sqrt of energy factor
REAL(KIND=8), INTENT(IN) :: cc! halo concentration (R_v/r_s)
INTEGER :: inprof
REAL(KIND=8) :: func_pr,cc1,y

inprof=iprof*4+in
cc1=1.d0+cc
SELECT CASE ( inprof )
!--Singular Isothermal profile (INDEPENDENT OF CC)
CASE (    1)
   func_pr=1.d0
CASE (    2)
   func_pr=1.d0
CASE (    3)
   func_pr=0.5D0
CASE (    4)
   func_pr=1.d0
!-----NFW profile
CASE (    5)
   func_pr=1.d0/(LOG(cc1)-cc/cc1) ! 1 function for halo dens
CASE (    6)
   func_pr=LOG(cc1)/(LOG(cc1)-cc/cc1) ! 2 function for halo pot
CASE (    7)
   func_pr=cc1/(1.d0+3.d0*cc) ! 3 function for gas temp
CASE (    8)
   y=cc*(1.d0-1.d0/cc1**2-2.d0*LOG(cc1)/cc1)/2.d0
   func_pr=SQRT(y)/(LOG(cc1)-cc/cc1) ! 4 square root NFW E factor
END SELECT
END FUNCTION func_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION smr_pr(dmm)

!     For a given value of nsmr, calculates M_hal (nsmr=1), M_gal/M (nsmr=3), M_Bstar/M (nsmr=4), M_bar/M (nsmr=5), M_hgas/M (nsmr=6), M_met hgas/M (nsmr=6), 
!     abundances of different molecular species (nsmr from 7 to 11), and mass losses (nsmr from 12 to 15) in accreted resolved halos.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : lint
USE cosmology, ONLY : smrlc2_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: dmm   ! merged mass over M
REAL(KIND=8), DIMENSION(SIZE(dmm)) :: smr_pr,y,dmm1,lrp

SELECT CASE ( nsmr )
  CASE (    1)
     y=dmm
  CASE (    2)
     STOP' Problem with nsmr in smr_pr'
END SELECT
dmm1=dmm+1.d0
lrp=lrcom+r13*LOG(dmm1)
smr_pr=smrlc2_cos(dccom,lrcom,lrp)*r13*EXP(lrp)/dmm1*y
END FUNCTION smr_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION ndv_pr(x)

!     Function x^2 times the normalized hot gas profile used to calculate the total mass of hot gas in the halo within x=r/r_vir.

!     Uses: prof_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: ndv_pr,lhg

lhg=prof_pr(4,cc,cc*x)
ndv_pr=x**2*10.d0**lhg
END FUNCTION ndv_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION ndv2_pr(x)

!
!     Function x^2 times the square normalized hot gas profile. x=r/r_vir.
!
!     Uses: prof_pr
!
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: x
REAL(KIND=8), DIMENSION(SIZE(x)) :: ndv2_pr,lhg

lhg=prof_pr(4,cc,cc*x)
ndv2_pr=(x*10.d0**lhg)**2
END FUNCTION ndv2_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION massr_pr(y)

!     Its root gives the radius (in units of rs) for which the inner mass is q2.

!     Uses: prof_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: y   ! distance to the halo center in rs units
REAL(KIND=8) :: massr_pr,yy1(1)

yy1=prof_pr(2,cc1,(/y/))-q2
massr_pr=yy1(1)
END FUNCTION massr_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION kinpNFW_pr(u)

!     Integrand for the dimensionless kinetic pressure of background halo particles in halos with NFW density profile.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: u   ! integration variable = y/(1+y) where y is the distance to the halo center in rs units.
REAL(KIND=8) , DIMENSION(SIZE(u)) :: kinpNFW_pr

kinpNFW_pr=(LOG(1.d0-u)+u)*(1.d0-1.d0/u)**3
END FUNCTION kinpNFW_pr


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION flrs_pr(x)

!
!     Auxiliary function to find NFW scale radius. Used by par_pr.
!
!     Uses: fit

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : fit,spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
INTEGER :: klo
REAL(KIND=8) :: flrs_pr,fcc,aa,bb,siga,sigb,chi2,q,y
REAL(KIND=8), DIMENSION(npn) :: sig

IF(x <= ax(npn))THEN
   klo=1
   y=spl(ax,ay,may,x,klo)
ELSE
   CALL fit(ax(npn-2:npn),ay(npn-2:npn),3,sig,0,aa,bb,siga,sigb,chi2,q)
   y=aa+bb*x
END IF
flrs_pr=y-(prfac+LOG(func_pr(1,EXP(lhr-x))))
END FUNCTION flrs_pr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


END MODULE profiles

MODULE massf

USE global_defs
CONTAINS

!----------------------------------------------------------------------------------------------------------------------
 SUBROUTINE CUSPMF(zz,mh,dndlm,nuc,fcusp)


!----------------------------------------------------------------------------------------------------------------------
   USE math_lib, ONLY : zbrent,spline,spl,qromb,fit
   USE cosmology, ONLY : delz_cos,sn2ga_cos
   USE peak, ONLY : frdis_pk,npeakr_pk,npeakcond_pk,xtypn_pk,npkconr_pk
   IMPLICIT NONE
   REAL(KIND=8), INTENT(IN) :: zz
   REAL(KIND=8), INTENT(OUT), DIMENSION(:) :: mh,dndlm,nuc,fcusp
   REAL(KIND=8) :: tc,dd,dtdoz,delc,ddeldt,at,delc_e,M_ast,lrh_1,lrh_2,s0_th,s0_g,ffr,lrg1,lrg2,stpr,stps,s1,s2,  &
        lx,lxb,lpk,delc_m,fcusp2,fwarren,sre,ff0a,sum,sth0,sg0,sg1,sg2,nus,gam,rg,ls,cc,yy,yc,q,siga,sigb,chi2
   INTEGER :: i,j,k,klo1,klo2,klor,kc
   REAL(KIND=8), DIMENSION(5) :: sig
   REAL(KIND=8), DIMENSION(nrv) :: ff0,ftry,ftry0,mlftry0,lx_spl,npkc_spl,npkcc_spl,mlnpkc_spl,rvstp,laux1,laux2, &
        aa,bb,vol
   REAL(KIND=8), DIMENSION(npn) :: ls_spl,mly_spl
   REAL(KIND=8), DIMENSION(npn,nrv) :: ly_spl
   ! Typical collapsing mass and critical overdensities
   CALL delz_cos(1.d0+zz,tc,dd,dtdoz,delc,ddeldt,at)
   delc_e=delc*(at**param_d/dd) ! ellipsoidal collapse overdensity 
   klo1=1
   IF(-LOG(delc) > sls0(1))THEN 
      M_ast=EXP(spl(sls0,lmh,mlmh,-LOG(delc),klo1)) ! typical collapsing mass at z (top hat filter)
   ELSE
      M_ast=EXP(lmh(1))
   END IF
   WRITE(*,'(1x,A,f5.2,g13.4)')'COLLAPSING MASS AT Zobs= ',zz,M_ast
   PRINT*,'CRITCAL OVERDENSITIES (TOP-HAT/ELLIPSIODAL)',REAL(delc),REAL(delc_e)

   M_ast=MAX(M_ast,1.d2)
   lrh_2=MAX(alrcom(nmr-1),(LOG(1.d6*M_ast)-lrhopi43)/3.d0)   ! top-hat scale corresponding to 10^6 M_*
   lrh_1=MAX(alrcom(3),(LOG(1.d-6*M_ast)-lrhopi43)/3.d0)  ! top-hat scale corresponding to 10^-6 M_*
   kc=0
   ff0a=1.d-99

!--Finding the scale range of the MF
   stpr=(lrh_2-lrh_1)/DBLE(nrv-1)
   lx=lrh_1
   DO i=1,nrv
      lxt(i)=lx
      ffr=r_sigma(lx)
      lxg(i)=zbrent(rfiltg,lrcomg(1),lrcomg(nmr-1),1.d-6) ! Gaussian scale
      ff0(i)=npeakr_pk(delc_e,lxg(i))
      kc=kc+1
      IF(ff0(i)/ff0a < 5.d-2*EXP(-stpr))EXIT ! maximum scale (large drop of N_pk in one R-step)
      ff0a=ff0(i)
      lx=lx+stpr
   END DO
   lrh_2=lxt(kc)
   lx=lrh_1
   WRITE(*,'(1x,A,4(1x,g12.5))')'MIN & MAX TOP-HAT SCALES ',EXP(lrh_1),EXP(lrh_2)
   lrg1=lxg(1) !  Minimum Gaussian scale 
   lrg2=lxg(kc) ! Maximum Gaussian scale 
   WRITE(*,'(1x,A,4(1x,g12.5))')'MIN & MAX GAUSSIAN SCALES',EXP(lrg1),EXP(lrg2)
   CALL spline(lxg(1:kc),lxt(1:kc),mlxt(1:kc))

!--Number density of peaks with delta per dR
   stpr=(lrg2-lrg1)/DBLE(nrv-1)
   lx=lrg1
   klor=1
   klo1=1
   klo2=1
   DO i=1,nrv
      laux1(i)=lx
      laux2(i)=spl(lxg(1:kc),lxt(1:kc),mlxt(1:kc),lx,klor)
      ff0(i)= npeakr_pk(delc_e,lx)
      yc=EXP(spl(lrcomg,lsg0,mlsg0,lx,klo1)) 
      yy=EXP(spl(alrcom,ls0,mls0,laux2(i),klo2)) 
      IF(MOD(I,5) == 0)WRITE(*,'(1x,i3,6(1x,g12.5))')i,EXP(lrhopi43+3.d0*laux2(i)),EXP(lx),EXP(laux2(i)-lx),yy,yc,ff0(i)
      lx=lx+stpr
   END DO
   lrg2=lxg(nrv)
   lxg=laux1
   lxt=laux2
   CALL spline(lxg,lxt,mlxt)

!--Solving Volterra for the scale function 
   eps=1.d-8
   kc=0
   ftry0=ff0
   lrg2=lxg(nrv-1)
   CALL spline(lxg,LOG(ftry0),mlftry0)
   sum=0.d0
   sig=0.d0
   loop_R: DO i=nrv-1,1,-1 ! Peak scale loop
      lpk=lxg(i) ! peak scale (Gaussian)
      rg=EXP(lpk)
      qm=EXP(lxt(i)-lxg(i)) ! Function q(M,t)
      vol(i)=pi43*(qm*rg)**3 ! Volume of the collapsing patch
      rvstp(i)=rg*vol(i)*stpr
!-----Preparing spline for conditional N_pk (for a given R) 
      lxb=LOG(1.001d0)+lpk ! background scale
      sre=1.d0
!-----Preparing values for the conditional number density depending on the distance (in units of top-hat scale) 
      DO k=i,nrv-1 ! background
         lxb=MERGE(lxb,lxg(k),k == i)
         lx_spl(k)=lxb
         s1=1.d-2 ! mix value of distance r (in units of the top-hat filtering scale)
         s2=3.5d1 ! max value of distance r (in units of the top-hat filtering scale)
         stps=(LOG(s2)-LOG(s1))/DBLE(npn-1)
         ls=LOG(s1)
         DO j=1,npn
            IF(j == npn)ls=LOG(s2)
            ls_spl(j)=ls
            ly_spl(j,k)=LOG(npkconr_pk(delc,lpk,delc,lxb,ls))
            ls=ls+stps
         END DO
         CALL fit(ls_spl(npn-2:npn),ly_spl(npn-2:npn,k),3,sig(1:3),0,aa(k),bb(k),siga,sigb,chi2,q)
         CALL spline(ls_spl,ly_spl(:,k),mly_spl)
         !npkc_spl(k)=npeakcond_pk(delc_e,lpk,delc_e,lxb)                  
         npkc_spl(k)=qromb(fnpkc,1.d-2,1.d0)
      END DO
!-----solves Volterra equation (point to point) for the scale function
      yy=ff0(i)
      npkc_spl(nrv-1)=0.5d0*npkc_spl(nrv-1)
      DO k=i+1,nrv-1         
         yy=yy-ftry0(k)*rvstp(k)*npkc_spl(k)
      END DO
      ftry0(i)=yy/(1.d0+0.5d0*rvstp(i)*npkc_spl(i))
      IF(i == nrv-1)ftry0(i)=ff0(i)
      yc=ftry0(i)
!-----Correction for overcounting host peaks
      DO k=i,nrv-1
         CALL spline(ls_spl,ly_spl(:,k),mly_spl)
         sre=1.d0/(vol(k)*ftry0(k))**r13 ! mean halo separation (in units of qR')
         IF(sre < s2)THEN
            yy=qromb(fnpkc,1.d-2,sre)
         ELSE
            yy=qromb(fnpkc,1.d-2,s2)+3.d0*EXP(aa(k))/(bb(k)+3.d0)*(sre**(bb(k)+3.d0)-s2**(bb(k)+3.d0))
         END IF
         cc=ff0(i)/(vol(k)*ftry0(k)*yy)
         npkcc_spl(k)=cc*npkc_spl(k)
      END DO
!-----solves Volterra equation (point to point) for the scale function (including overcounting correction)
      yy=ff0(i)
      npkcc_spl(nrv-1)=0.5d0*npkcc_spl(nrv-1)
      DO k=i+1,nrv-1         
         yy=yy-ftry0(k)*rvstp(k)*npkcc_spl(k)
      END DO
      ftry0(i)=yy/(1.d0+0.5d0*rvstp(i)*npkcc_spl(i))
      IF(i == nrv-1)ftry0(i)=ff0(i)
      WRITE(*,'(1x,a,i3,a,i3,6(1x,g12.5))')' STEP ',i,' OF ',nrv,EXP(lrhopi43+3.d0*lxt(i)),ff0(i),yc,ftry0(i)
   END DO loop_R
   pause
   klo1=1
   klo2=1
   delc_m=delc
   DO i=1,nrv
      sth0=EXP(spl(alrcom,ls0,mls0,lxt(i),klo1))
      nus=delc_m/sth0 ! nu top-hat
      sg0=EXP(spl(lrcomg,lsg0,mlsg0,lxg(i),klo2))
      sg1=EXP(spl(lrcomg,lsg1,mlsg1,lxg(i),klo2))
      sg2=EXP(spl(lrcomg,lsg2,mlsg2,lxg(i),klo2))
      yy=-spl(lrcomg,ldlsg0,mldlsg0,lxg(i),klo2)
      gam=sg1**2/(sg0*sg2)
      mh(i)=EXP(lrhopi43+3.d0*lxt(i))
      nuc(i)=delc_e/sg0 ! nu CUSP
      yc=EXP(lxt(i))
      dndlm(i)=r13*yc*ftry0(i)      
      fcusp(i)=(mh(i)/rho)*ftry0(i)*EXP(lxg(i))/yy      
      fwarren=0.7234d0*(sth0**-1.625d0 + 0.2538d0)*EXP(-1.1982d0/sth0**2)
      WRITE(*,'(1X,I3,6(1X,G12.5))')i,mh(i),ftry0(i),dndlm(i),LOG(nuc(i)),LOG(fcusp(i))
   END DO
RETURN
CONTAINS

!----------------------------------------------------------------------------------------------------------------------

FUNCTION ftry_int(x)

! To calculate the integral term of (integral) equation

!----------------------------------------------------------------------------------------------------------------------
  USE math_lib, ONLY : lint,spl
  IMPLICIT NONE
  REAL(KIND=8), INTENT(IN) :: x ! Gaussian filtering scale, in Mpc 
  INTEGER :: klo1,klo2
  REAL(KIND=8) :: ftry_int,lr,y1,y2,rvol

  lr=LOG(x)
  IF(ABS(lr-lxg(nrv)) < 1.d-14)lr=lxg(nrv) 
  IF(lr <= lxg(nrv))THEN
     klo1=1
     klo2=1
     y1=EXP(spl(lxg,LOG(ftry0),mlftry0,lr,klo1))
     y2=EXP(spl(lx_spl,LOG(npkc_spl),mlnpkc_spl,lr,klo2))
     klo1=1
     rvol=EXP(spl(lxg,lxt,mlxt,lr,klo1)) ! radius of the volume M(R)/rho 
     ftry_int=rvol**3*y1*y2
  ELSE
     print*,lr,lxg(nrv),lrg2
     STOP' interpolation out of range in function ftry_int'
  END IF
END FUNCTION ftry_int

!----------------------------------------------------------------------------------------------------------------------
FUNCTION r_sigma(lrth)

! This function relates the Gaussian 0th spectral moment with the top-hat one
! Common variables: param_s0,param_s2,param_A,param_s1,delc,at,s0; s0_th 

!----------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: lrth  ! natural log of top-hat filtering radius (in Mpc)
REAL(KIND=8) :: r_sigma,st
INTEGER :: klo
klo=1
s0_th=EXP(spl(alrcom,ls0,mls0,lrth,klo)) ! 0th spectral moment (top-hat filter)
st=param_s0 + param_s1*at + LOG10(at**param_s2/(1.d0 + at/param_A))
r_sigma=1.d0+st*(delc_e/s0_th)
END FUNCTION r_sigma
 
!----------------------------------------------------------------------------------------------------------------------

FUNCTION rfiltg(lx)

! Auxiliar function used to find the Gaussian filtering scale of a halo of mass M at z
! Common variables: ffr 

!----------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : spl
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: lx  ! natural log of Gaussian filtering radius (in Mpc)
REAL(KIND=8) :: rfiltg,s0_g
INTEGER :: klo
klo=1
rvar=EXP(lx)
s0_g=SQRT(sn2ga_cos(0)*cnn)
rfiltg=s0_g-s0_th*ffr
END FUNCTION rfiltg

!----------------------------------------------------------------------------------------------------------------------

FUNCTION fnpkc(x)

!----------------------------------------------------------------------------------------------------------------------
  USE math_lib, ONLY : spl
  IMPLICIT NONE
  REAL(KIND=8), INTENT(IN) :: x ! distance to the peak, in units of qR 
  REAL(KIND=8) :: fnpkc
  INTEGER :: klo
  klo=1
  fnpkc=3.d0*x**2*EXP(spl(ls_spl,ly_spl(:,k),mly_spl,LOG(x),klo))
END FUNCTION fnpkc

END SUBROUTINE CUSPMF

END MODULE massf


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

MODULE in_out

USE global_defs

CONTAINS

!     subroutine header_IO
!     subroutine param_IO
!     subroutine disfix_IO
!     subroutine dissam_IO
!     subroutine rangz_IO


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE header_IO(iuni,ispe,omm,lam0,h0,omb,tn,s8,zobs,unit,io)

!     Reads/writes the parameter values of the model from/in the file "./input/last.par" with appropriate headers.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, INTENT(IN) :: unit             ! unit identifier
INTEGER, INTENT(IN) :: io               ! 1 reads; 2 writes
INTEGER, INTENT(INOUT) :: iuni          ! 1 if flat universe (F);  2 if open universe (O)
INTEGER, INTENT(INOUT) :: ispe          ! 1 if scale-free spectrum (SF); 2 if cold-dark-matter spectrum (CDM); 3 if warm-dark-matter spectrum (WDM)
REAL(KIND=8), INTENT(INOUT) :: omm      ! 1 if flat universe (F); 2 if open universe (O)
REAL(KIND=8), INTENT(INOUT) :: lam0     ! lamda_0
REAL(KIND=8), INTENT(INOUT) :: h0       ! H_0, in km/s/Mpc
REAL(KIND=8), INTENT(INOUT) :: omb      ! Omega_baryonic
REAL(KIND=8), INTENT(INOUT) :: tn       ! spectral index (for SF) or tilt parameter epsilon (for CDM) or log10(M_cut off/M_o) (for WDM)
REAL(KIND=8), INTENT(INOUT) :: s8       ! spectral normalization sigma_8
REAL(KIND=8), INTENT(INOUT) :: zobs     ! redshift of the sample
CHARACTER(LEN=10) :: skip

IF(io == 1)THEN
   READ(unit,'(a)'),skip
   READ(unit,'(30x,i1)')iuni
   IF(iuni == 1)THEN
      READ(unit,'(10x,f4.2,12x,f4.2)')lam0,omm
   ELSE
      READ(unit,'(11x,f4.2)')omm
   END IF
   READ(unit,'(7x,f4.0/11x,f5.3/35x,i1)')h0,omb,ispe
   IF(ispe == 1)THEN
      READ(unit,'(18x,f6.3)')tn
   ELSE IF(ispe == 2)THEN
      READ(unit,'(8x,f6.3)')tn
   ELSE
      READ(unit,'(24x,f6.3)')tn
   END IF
   READ(unit,'(11x,f4.2)')s8
   READ(unit,'(30x,i1)')imf
   READ(unit,'(26x,f5.2)')zobs
ELSE
   WRITE(unit,'(''Fixed parameters:'')')
   WRITE(unit,'('' Flat (1)/Open (2) universe = '',I1)')iuni
   IF(iuni == 1)THEN
      WRITE(unit,'('' Lambda = '',f4.2, '' (Omega_m = '',f4.2,'')'')')lam0,omm
   ELSE
      WRITE(unit,'('' Omega_m = '',f4.2, '' (Lambda = 0.)'')')omm
   END IF
   WRITE(unit,'('' H_0 = '',f4.0,'' km/s/Mpc'')')h0
   WRITE(unit,'('' OMEGA_B = '',F5.3)')omb
   WRITE(unit,'('' SF (1)/CDM (2)/WDM (3) spectrum = '',I1)')ispe
   IF(ispe == 1)THEN
      WRITE(unit,'('' Spectral index = '',f4.1)')tn
   ELSE IF(ispe == 2)THEN
      WRITE(unit,'('' Tilt = '',F6.3)')tn
   ELSE
      WRITE(unit,'('' Log10(M_cut-off/M_O) = '',f6.3)')tn
   END IF
   WRITE(unit,'('' Sigma_8 = '',F4.2)')s8
   WRITE(unit,'(''Sample parameters:'')')
   WRITE(unit,'('' Redshift of the sample = '',F5.2)')zobs
END IF
END SUBROUTINE header_IO

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE param_IO(iuni,omm,lam0,h0,omb,ispe,tn,s8,zobs,unit,disp)

!     Reads and displays or WRITEs all necessary parameters.

!     Uses: header_IO,headerw_IO,disfix_IO,disfixw_IO,disgal_IO,dissta_IO,disagn_IO,dissam_IO

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, INTENT(IN) :: unit             ! unit identifier
INTEGER, INTENT(IN) :: disp             ! display/not display index (1=display; 2=not display)
INTEGER, INTENT(INOUT) :: iuni          ! 1 if flat universe (F);  2 if open universe (O)
INTEGER, INTENT(INOUT) :: ispe          ! 1 if scale-free spectrum (SF); 2 if cold-dark-matter spectrum (CDM); 3 if warm-dark-matter spectrum (WDM)
REAL(KIND=8), INTENT(INOUT) :: omm      ! 1 if flat universe (F); 2 if open universe (O)
REAL(KIND=8), INTENT(INOUT) :: lam0     ! lamda_0
REAL(KIND=8), INTENT(INOUT) :: h0       ! H_0, in km/s/Mpc
REAL(KIND=8), INTENT(INOUT) :: omb      ! Omega_baryonic
REAL(KIND=8), INTENT(INOUT) :: tn       ! spectral index (for SF) or tilt parameter epsilon (for CDM) or log10(M_cut off/M_o) (for WDM)
REAL(KIND=8), INTENT(INOUT) :: s8       ! spectral normalization sigma_8
REAL(KIND=8), INTENT(INOUT) :: zobs     ! redshift of the sample

!-----fill with default parameters (WMAP values)
iuni=1                    ! type of universe (1 flat, 2 open)
omm=.27D0                 ! Omega_m
lam0=.73D0                ! lambda_0
h0=71.D0                  ! Ho in km/s/Mpc
omb=.045D0                ! Omega_b
ispe=2                    ! type of power spectrum (1 scale free, 2 CDM)
tn=0.D0                   ! n index (scale free) or 10*eps (tilted CDM)
s8=0.84D0                 ! bias factor
zobs=0.D0                 ! redshift of the sample
!-----read parameters from header of input file (if available)
CALL header_IO(iuni,ispe,omm,lam0,h0,omb,tn,s8,zobs,unit,1)
!-----display parameters read
IF(disp == 1)THEN
   PRINT*,'   Fixed parameters:'
   CALL disfix_IO(iuni,ispe,omm,lam0,h0,omb,tn,s8) 
   CALL dissam_IO(zobs)
END IF
END SUBROUTINE param_IO


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE disfix_IO(iuni,ispe,omm,lam0,h0,omb,tn,s8)

!     Displays the values of the cosmological model parameters.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER, INTENT(IN) :: iuni          ! 1 if flat universe (F);  2 if open universe (O)
INTEGER, INTENT(IN) :: ispe          ! 1 if scale-free spectrum (SF); 2 if cold-dark-matter spectrum (CDM); 3 if warm-dark-matter spectrum (WDM)
REAL(KIND=8), INTENT(IN) :: omm      ! 1 if flat universe (F); 2 if open universe (O)
REAL(KIND=8), INTENT(IN) :: lam0     ! lamda_0
REAL(KIND=8), INTENT(IN) :: h0       ! H_0, in km/s/Mpc
REAL(KIND=8), INTENT(IN) :: omb      ! Omega_baryonic
REAL(KIND=8), INTENT(IN) :: tn       ! spectral index (for SF) or tilt parameter epsilon (for CDM) or log10(M_cut off/M_o) (for WDM)
REAL(KIND=8), INTENT(IN) :: s8       ! spectral normalization sigma_8
INTEGER :: ior

!-----displaying parameter values
ior=1
IF(iuni == 1)THEN
   WRITE(*,'(''       ['',I1,''] TYPE = Flat (F)'')')ior
   ior=ior+1
   WRITE(*,'(''       ['',I1,''] LAMBDA = '',f4.2,'' (OMEGA_m = '',f4.2,'')'')')ior,lam0,omm
ELSE
   WRITE(*,'(''       ['',I1,''] TYPE = Open (O)'')')ior
   ior=ior+1
   WRITE(*,'(''       ['',I1,''] OMEGA_m = '',f4.2,'' (LAMBDA = .00)'')')ior,omm
END IF
hreal=h0/100.D0
ior=ior+1
WRITE(*,'   (''       ['',I1,''] H_0 = '',f4.0,'' km/s/Mpc'')') ior,h0
ior=ior+1
WRITE(*,'   (''       ['',I1,''] OMEGA_b = '',f5.3,$)')ior,omb

IF(ispe > 1)THEN
   IF(omb > 0.5D0*omm)THEN
      WRITE(*,'(''    (OMEGA_b < '',f5.3,'')'')')omb
   ELSE
      gam=omm*hreal*EXP(-omb-SQRT(hreal/.5D0)*omb/omm)
      WRITE(*,'('' (SHAPE PARAMETER = '',f4.2,'')'')')gam
   END IF
END IF
IF(ispe == 1)THEN
   ior=ior+1
   WRITE(*,'(''       ['',I1,''] TYPE = Scale-free (SF)'')')ior
   ior=ior+1
   WRITE(*,'(''       ['',I1,''] SPECTRAL INDEX (n_s) = '',f4.1)') ior,tn
ELSE IF(ispe == 2)THEN
   ior=ior+1
   WRITE(*,'(''       ['',I1,''] TYPE = Cold Dark Matter (CDM)'' )')ior
   ior=ior+1
   WRITE(*,'(''       ['',I1,''] TILT (departure from n=1) = '', f6.3)')ior,tn
ELSE
   ! wave numberm cutt-off
   kco=2.D0*pi*(omm*h0**2/(2.D0*gkm*10.D0**tn))**r13
   ior=ior+1
   WRITE(*,'(''       ['',I1,''] TYPE = WARM DARK MATTER (WDM)'' )')ior
   ior=ior+1
   WRITE(*,'(''       ['',I1,''] LOG10(M_CO/MO) = '',f6.3)')ior,tn
END IF
ior=ior+1
WRITE(*,'(''       ['',I1,''] SIGMA_8 = '', f4.2)')ior,s8
ior=ior+1
IF(ispe == 2.AND.omb > omm)WRITE(*,'('' WARNING: SELECTED VALUES LEAD TO OMEGA_b > OMEGA_m'')')
END SUBROUTINE disfix_IO

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SUBROUTINE dissam_IO(zobs)

!     Displays the characteristics of the studied sample.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: zobs     ! redshift of the sample
INTEGER :: ior

!--displaying parameter values
ior=0
ior=ior+1
WRITE(*,'(''       ['',I1,''] REDSHIFT OF THE SAMPLE = '',F5.2)') ior,zobs
END SUBROUTINE dissam_IO

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE rangz_IO(lrlow,lrup,zobs,zmax,lozmin,lozmax)

!     Upper and lower values of 1+z. The upper value is obtained by integrating doz/dR along the accretion track starting from lrlow up to lrbot (see specmom_cos). 
!     Displays the redshift range.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE math_lib, ONLY : odeint,drkqs
USE cosmology, ONLY : dozdr_cos,dlrdoz_cos
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: zobs      ! reference redshift
REAL(KIND=8), INTENT(INOUT) :: lrlow  ! log of lower scale at z=0 (input) or at zmax (output), in Mpc
REAL(KIND=8), INTENT(INOUT) :: lrup   ! log of upper scale at z=0 (input) or at zmax (output), in Mpc
REAL(KIND=8), INTENT(OUT) :: zmax     ! maximum 1+z
REAL(KIND=8), INTENT(OUT) :: lozmin   ! log of minimum 1+z (zmin=zobs)
REAL(KIND=8), INTENT(OUT) :: lozmax   ! log of maximum 1+z
REAL(KIND=8) :: x1,x2,stp,y

!--calculation of 1.+zmax
x1=EXP(lrlow) ! lower scale at z_obs
x2=EXP(lrbot) ! lower scale at z_max
stp=1.d-2*(x2-x1)
y=1.D0
CALL odeint(y,x1,x2,1.d-7,stp,0.D0,dozdr_cos,drkqs)
lozmax=LOG(y)
lozmin=LOG(1.D0+zobs)
stp=lozmax/11.D0
lozmax=MIN(lozmax-ip*stp,LOG(1.d2)) ! ip first points for buffer
!--calculation of lrlow(zmax) and lrup(zmax)
x1=1.D0
x2=EXP(lozmax)
stp=1.d-2*(x2-x1)
CALL odeint(lrlow,x1,x2,1.d-7,stp,0.d0,dlrdoz_cos,drkqs)
CALL odeint(lrup,x1,x2,1.d-7,stp,0.d0,dlrdoz_cos,drkqs)
!--output of z range
zmax=x2-1.d0
zmax=1000.d0
WRITE(*,'(a,i3,a,f7.2,a,f5.2,a)')'    Redshift:',nz-1,' log-steps in the range [',zmax,',',zobs,']'
PRINT*,'   (To start at a higher z increase nmi, and conversely)'
END SUBROUTINE rangz_IO

END MODULE in_out

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
MODULE delta_track 

!    Computes the density profile of the protoobject at t_i from the energy profile at
!    turn-around. Compares the \delta(R_f) track (central overdensity as a function of 
!    the filtering scale) for protoobjects to the same track for peaks of the smoothed
!    linear density field 


!     Basic assumptions of the model (see Manrique et al. 2003a and references therein):
!     - halos grow through mergers of different amplitude (accretion and mergers) according to some assumed hierarchical cosmology,
!     - their inner structure follows from the way they form (in binary mergers of progenitors with similar masses) and grow (inside-out
!     during accretion), which gives rise to NFW-like density profiles with the correct mass-density relation at any z,


!     PART I. DARK-MATTER HALO EVOLUTION:

!     Using the modified extended PS clustering model by Salvador-Sole et al. (1998), Raig et al. (1999, 2001) & Manrique et al. (2003b)
!     it calculates, in any given arbitrary hierarchical cosmogony:
!       - the mass of halos in M_o (lhm);
!       - the comoving density of haloes in M_o^{-1} (comv Mpc^{-3}) (ln);
!       - the specific merger rate in M_o^{-1} Gyr^{-1} (smr);
!       - the mass accretion rate in M_o Gyr^{-1} (lmar);
!       - the destruction rate in Gyr^{-1} (ldr);
!       - the formation rate in Gyr^{-1} (lfr),
!         and its integral along accretion tracks (fri),
!       - the halo density profile and the corresponding best
!         analytical fit.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
USE global_defs
USE math_lib, ONLY : errep,lnbchar,qromb,eqromb,drkqs,oderomb,odeint,spl,spline,splie2,zbrent,zbrent2,fit,lfit,qromo,dmidpnt_s,det5,zbrac,fourier_odd,dzbrak,lint,dmidpnt,drkqs_v,sp21,gammp,gammq,gammln,minim
USE cosmology, ONLY : specmom_cos,specmomg_cos,hal_cos,delz_cos,dlrdoz_cos,dfrdt_cos,capd_cos,scr_cos,cosmicts_cos,dvbn_cos,mar_cos,capd_cos,sn2ga_cos,sn2th_cos,lnPS_cos,CDMMikheeva_cos,deltac_cos
USE peak, ONLY : theta2_pk,f_pk,H_pk,xtyp_pk,xtypinv_pk,theta_pk
USE profiles, ONLY : prof_pr,func_pr
USE in_out, ONLY : param_IO,header_IO,rangz_IO
USE massf
CONTAINS

SUBROUTINE delta(imass)
IMPLICIT NONE
REAL(KIND=8),DIMENSION(100) :: xcp,ycp
INTEGER, INTENT(IN) ::imass
INTEGER :: k,kk,control,i1,i2,i3,km,kq,kc,kh,t,kz,unit,z3,nz3,nz4,mn,klo,j,kzn,kzn2,inb,kzc,nsa,nmax,noio,nbad,n
INTEGER, PARAMETER :: ma=3
INTEGER, DIMENSION(ma) :: ia

REAL :: ry,ryq,rys,ryy,ryyy
REAL, DIMENSION(nr,nmh) :: rr
REAL, DIMENSION(nz,nmh) :: rrs

REAL(KIND=8) :: t0,nlmf,h0,omb,s8,bf,bfi,tommt0,ddelm1,bm1d,lmf,stpr,stpx,stpy,stpz,stpqr,lmup,lmlow,lrup,lrlow,ozmin,lozmin,lozmax,di,tdde,et,z,loz1,loz2,dtdoz,ddldz,yfac,dffr, &
     x1,xx,x2,lmstar,mst,bmt,igal,iagn,inbm,inbmi,cbmi,ntbm,ntbmi,lbtbin,lmlbin,mgmin,mqmin,y,y1,y2,yy,yy0,cvir,lqth,delpk,stpa,s1,s2,diff2,aa,ab,eaa,ab2,aa1,ab1,aa3,ab3,hh,mass1_fit,mass2_fit,eta1_fit,eta2_fit,xstoremin,xstoremax,xactual,xfirst,xup,xdown,deltap1_fit,deltap2_fit,deltam1_fit,deltam2_fit,sg,zz
REAL(KIND=8), DIMENSION(nmh1) :: lrr
REAL(KIND=8), DIMENSION(nmh1,nmh1) :: vsmr
REAL(KIND=8), DIMENSION(ni,nmh1) :: ln,lmar,ldr

REAL(KIND=8) :: lr,ch,ch1,sh,sh1,rth,rvar0,rfz,fhalo,en,ozi,oz0,ozc,psd,pow,sgr2,fc,qq,wr,a1,del1,b1,a2,b2,a3,b3,a0,b0,siga,sigb,chi2,chisq,q,hi,mcl,mcla,fmr,dlmdld, &
     err,omz,gma,gma1,rst,sg0,sg1,sg2,sg0n,sg1n,sg2n,nu,mh,mh0,fd,peak0_pr,dcz,rhozi,ihz,phi,d2phi,thet0,thet1,e_norm,cni2,fmass,mhi,mhf,d1,d2,r200,m200,c200,&
     r0,sol1,rab,dens1,dens2,rbn,dth,dethm,dedm,rfil,ozv,dltz,lrth,lrpk,sum,ss,xl,x0,rhom,rhor,frho,xbound,mfil,fc1,fm1,fdel,massx,massc,radc,limdmdr,etac,drc, &
     dedmc,gmreta,rhop0,bfac,rmax,mmax,rpmax,rfmax,qrfmax,rv(2),sigma2,rhoFOF,deltacmax,deltammax,mbkg,Ep,finalrp
REAL(KIND=8) :: r1(3),lmv(3),yy1(1),a(ma),covar(ma,ma),ca0(3),ca1(3),ca2(3)
REAL(KIND=8), DIMENSION(npn) :: lrhal,lmhal,densp,ozarr
REAL(KIND=8), DIMENSION(nrr) :: mm,eta,dledlm,rp,dlrdlm,mrhop,lrhlmh,lmhlrh,sig,xf,yf,xcv,ycv,mycv,rm
REAL(KIND=8), DIMENSION(nrr2) :: myf2
REAL(KIND=8), DIMENSION(npr) :: by,mby,cx,cy,cz,mcy,mcz,xphi,xd2phi,frg
REAL(KIND=8), DIMENSION(npr) :: bx,dltp,mdltp
REAL(KIND=8), DIMENSION(npr) :: rab_1,tabel_1,mtabel_1,tabel_i1,mtabel_i1,rab_2,tabel_2
REAL(KIND=8), DIMENSION(npf) :: sfo,tfour_1,tfour,mtfour,rhop2,myf,rhoh,rp3,mlrho,rhohspl
REAL(KIND=8), DIMENSION(npf2) :: aux1,aux2,aux3,laux1
REAL(KIND=8), DIMENSION(npp) :: amh,ars,mars
REAL(KIND=8), DIMENSION(nrr) :: fmh
REAL(KIND=8), DIMENSION(nrr2) :: xf3,yf3,yf4,myf3,myf4
REAL(KIND=8), DIMENSION(nrr2+2) :: xxx,sg02,auxn1,auxn2,convdif,decondif,xdif,recondif
REAL(KIND=8), DIMENSION(3*nrr2) :: auxxx,sax,say,msay
REAL(KIND=8), DIMENSION(nrv) :: hm,dndlm,nuc,fcusp
REAL(KIND=8), DIMENSION(133) :: lmhalo,lmms,alf,malf
REAL(KIND=8) :: delpk1(10),delpk2(10),delpk3(10),rvar01(20),rvar02(20),rvar03(20)
REAL(KIND=8) :: stpy1,stpy2,stpy3,stpz1,stpz2,stpz3
REAL(KIND=8) :: yydr1(10,20),yydr2(10,20),yydr3(10,20),myfdr1(10,20),myfdr2(10,20),myfdr3(10,20)
REAL(KIND=8) :: auxx,auxy,firstx,firsty,paramx,paramy,qth
REAL(KIND=8) :: nucusp,deltam0,delta_try,ad1,ad2,ad3,nuc0,xyx
REAL(KIND=8) :: mhal,rhal
LOGICAL:: skip
LOGICAL:: first
CHARACTER(LEN=1) :: nit
CHARACTER(LEN=75) :: message
CHARACTER(LEN=40) :: status
CHARACTER(LEN=50) :: model
CHARACTER(LEN=4) :: modcos
CHARACTER(LEN=1) :: cband0
CHARACTER(LEN=30) :: char_m,char_j,char_q

DATA skip/.FALSE./
DATA sig/nrr*1.d0/
DATA ca0/0.0048076d0,0.017921d0,0.064533d0/
DATA ca1/0.19231d0,0.19023d0,0.18062d0/
DATA ca2/0.807643d0,0.66029d0,0.51906d0/

!--SWITCHES

! turn off by comenting line             on<>off
prof=1.d0                                  !  NFW dens prof (off: S.I.)

!--PRELIMINARY CHEKS

!--status of the task
PRINT*
unit=2
SELECT CASE(cosmo)
CASE(0)
   modcos='wmap' ! WMAP cosmological parameters
CASE(1)
   modcos='plnk'! Planck cosmological parameters
END SELECT
OPEN(unit,FILE='param_'//modcos//'.par')
READ(unit,'(a40)',END=9)status
READ(unit,*,END=9)
IF(status(1:7) == 'running')THEN
   GO TO 999
ELSE
   GO TO 999
END IF
9  PRINT*,'There is no new model ready to be fixed.'
PRINT*
CLOSE(unit)
STOP
99 PRINT*,'There is already one model being fixed.'
PRINT*
REWIND(unit)
CLOSE(unit)
STOP
!--array lengths
999 message='nz smaller than 10'
control=10
IF(nz < control)CALL errep(message)
message='nt greater than 9'
control=9
IF(nt > control)CALL errep(message)
message='nt smaller than 2'
control=2
IF(nt > control)CALL errep(message)
message='nb smaller than 3'
control=3
IF(MOD(nm,2) /= 0)CALL errep(message)
message='nrf must be smaller than or equal to 2'
IF(nrf > 2)CALL errep(message)
!--initialize
!--INPUT

!--primary parameters
! read name of output ".dat" file
REWIND(unit)
READ(unit,'(a)')model
PRINT*,'Model name: ',model
PRINT*
PRINT*,'Model characteristics...'
! read primary parameters
CALL param_IO(iuni,omm,lam0,h0,omb,ispe,tn,s8,zobs,unit,1)
REWIND(unit)
WRITE(unit,'(a)')'running'
CALL header_IO(iuni,ispe,omm,lam0,h0,omb,tn,s8,zobs,unit,2) 
CLOSE(unit)
WRITE(*,'(a)')'    Switches:'
WRITE(*,'(a,f2.0)')'       prof:   ',prof
WRITE(*,'(a,f2.0)')'       adcon:  ',adcon
WRITE(*,'(a,f2.0)')'       tvir:  ',tvir
PRINT*

eps=1.D-9 ! precision in integrations
message='fmbh+yld3 greater than 1.d0'
!--secondary parameters
! universe
bf=omb/omm ! initial cosmic bar I fraction
kz=1
ih0=gyr/h0 ! H_0^{-1} in Gyr
ch0=3.d5/h0 ! c H_0^{-1} in Mpc
t0=cosmicts_cos(1.d0) ! present cosmic time
tobs=cosmicts_cos(1.d0+zobs) ! observing cosmic time
rho=omm*1.d0/(2.d0*pi43*gcos*ih0**2) ! comov cosmol dens, Mo/Mpc^3
h_z=SQRT(h0**2d0*(omm*(1d0+zobs)**3+(1d0-omm)))
ih_z=gyr/h_z
rhoc=1.d0/(2.d0*pi43*gcos*ih_z**2)
PRINT*,RHO,OMM
IF(iuni == 1)THEN ! flat case
   d0=capd_cos(1.d0,omm,iuni) ! linear growing factor at t0
   dc0=delc0
ELSE ! open case
   d0=capd_cos(1.d0,omm,iuni) ! linear growing factor at t0
   ! other constants for open case
   tomm=pi*omm*ih0/((1.d0-omm)*SQRT(1.d0-omm))
   tommt0=tomm/t0
   tommt023=tommt0**(2.d0/3.d0)
   ! critical overdensity for collapse in the open case
   dc0=1.5D0*d0*(1.d0+tommt023)*delc0/1.686470200D0
END IF
lrhopi43=LOG(rho*pi43)
! halos
hmlow=hmup/10.d0**ndx
stpm=LOG(hmup/hmlow)/(nmh-1)
message='nm too small for the value of Delta_m'
IF(stpm > delm)CALL errep(message)
ldelm=LOG(delm)
idelm1=-LOG(1.d0+delm)
ddelm1=ldelm+idelm1
iprof=NINT(prof)
!--spectral moments
PRINT*,'Preparing spectral moments...'
PRINT*,'Top Hat'
CALL specmom_cos(dc0,s8,hmup,stpm,lrlow,lrup)
PRINT*,'Gaussian'
CALL specmomg_cos(dc0,s8,hmup,stpm)
PRINT*
!--ranges and binnings
PRINT*,'Preparing ranges and binnings...'
WRITE(*,'(a,i3,a,g9.3,a,g9.3,a)')'    Halo mass (z=0) in M_o: ',nmh-1,' log-steps in the range [',hmlow,',',hmup,']'
CALL rangz_IO(lrlow,lrup,zobs,zmax,lozmin,lozmax)
stpz=(lozmax-lozmin)/(ni-ip-3)
st=MAX(0.d0,cosmicts_cos(1.d0+zmax)) ! initial time, Gyr
et=cosmicts_cos(1.d0+zobs) ! final time, Gyr
message='mi too small to warrant initial non-ionized IGM'
IF(zmax-5.8d0 < zero)CALL errep(message)
! constant binning of (comoving) halo filtering scale at z_max
stpy=(lrup-lrlow)/(nmh-1)
lrx(1)=lrlow
DO m=2,nmh1
   lrx(m)=lrx(m-1)+stpy
END DO
cnn=cn2/9.d0
PRINT*

!--PART I: PREPARE HALO MASS GROWTH AND INNER STRUCTURE

!--loop for decreasing z's (z_obs corresponds to iz and z_max to ez)
loz2=lozmax+2.d0*stpz ! 1st step from zmax to loz2 (we keep a buffer for z of two bins each side)
loz1=lozmax    
lrr=lrx ! constant scale step at zmax
PRINT*,'Preparing DM halos...'
DO i=ni,1,-1 ! part I time arrays
   ! some functions of z !    (z grows with growing i)
   loz(i)=lozmin+(i-3)*stpz
   oz(i)=EXP(loz(i))
   CALL delz_cos(oz(i),y,di,dtdoz,de,tdde,yy)
   slt(i)=-LOG(y)
   ii=ni-i-ip+1
   IF(ii >= 1.AND.ii <= nz)lct(ii)=-slt(i)
   delv=dvbn_cos(oz(i))
   lrcfac(i)=lrhopi43+LOG(delv*oz(i)**3) ! ln[ 4pi/3*rho(z)*Del_v(z)]
   ldel(i)=LOG(de)
   ltddel(i)=LOG(tdde)
   !--loop for increasing halo masses M
   ! R(z) by integration of dlogR/doz using accretion rate
   x1=EXP(loz1)
   x2=EXP(loz2)
   stpy=2.d-3*nz*(x2-x1)
   DO m=1,nmh1
      CALL odeint(lrr(m),x1,x2,1.d-8,stpy,0.d0,dlrdoz_cos,drkqs)
      IF(lrr(1) < lrb)lrr(1)=lrb
   END DO
   loz1=loz2
   loz2=loz1-stpz
   ! formation rate (lfr) from specific capture rate
   DO m=nmh,1,-1
      lmup=lhm(i,m)+idelm1 ! upper bound for capt mass
      lmlow=lhm(i,m)+ddelm1 ! lower bound for capt mass
      y=lhm(i,m)+l05 ! intermediate lower bound
      IF(lmlow > lhm(ni,1))THEN ! int over the whole range
         lrcom=(lhm(i,m)-lrhopi43)/3.d0
         lfr(i,m)=LOG(.5D0*qromb(scr_cos,lmlow,lmup))+ltddel(i)
      ELSE IF(y > lhm(ni,1))THEN ! int over high half range
         lrcom=(lhm(i,m)-lrhopi43)/3.d0
         lfr(i,m)=LOG(qromb(scr_cos,y,lmup))+ltddel(i)
      ELSE ! interpolation
         lfr(i,m)=0.d0
         IF(lhm(i,m+2)-lhm(i,m+1) /= 0.d0)lfr(i,m)=(lfr(i,m+2)-lfr(i,m+1))/(lhm(i,m+2)-lhm(i,m+1))*(lhm(i,m)-lhm(i,m+1))+lfr(i,m+1)
      END IF
   END DO
END DO
PRINT*,'   Mass function prepared'
PRINT*,'   Mass accretion rate prepared'
PRINT*,'   Destruction rate prepared' ! not mandatory
PRINT*,'   Formation rate prepared'
!--integral of the formation rate along accretion tracks (fri):
DO m=1,nmh
   CALL spline(slt,lfr(:,m),mlfr(:,m))
   x2=EXP(-slt(1))
   y=0.d0
   DO i=1,ni
      x1=x2
      x2=EXP(-slt(i))
      call oderomb(y,x1,x2,dfrdt_cos)
      fri(i,m)=y
   END DO
   fri(1,m)=0.d0
END DO
PRINT*,'   Distribution of formation times prepared'
PRINT*,'   Distribution of progenitor masses prepared'
!--scale parameters of halo density profiles at z=0.

PRINT*
!--prepare interpolations for part III
CALL spline(slt,lrcfac,mlrcfac) ! 4*pi/3*rho(z)*Delta_v(z)
CALL spline(slt,loz,mloz) ! log(1+z)
CALL spline(slt,ldel,mldel) ! delta
CALL spline(slt,ltddel,mltddel) ! dt/ddelta
CALL spline(sls0,lmh,mlmh) ! sig_0(M)
CALL spline(loz,slt,mslt) ! t(1+z)
CALL splie2(lrx,lhm,mlhm) ! halo mass M(R,t)
CALL splie2(lrx,fri,mfri) ! integral of r


ozdiff=1d0+zobs
dm=deltac_cos(omm,ozdiff)/capd_cos(ozdiff,omm,iuni)/ozdiff**param_d  !delta_c definition, taken from the best value of the MF (relation 1)
PRINT*,'DELTA_Z0=',DM,param_d

IF(option==0)THEN
   bd11(1)=1.50d0                            ! boundaries for q(M,t), which will be used to find the proper value of sigma_g for each mass
   bd12(1)=2.50d0                            !
   tol1(1)=1.d-9                             ! desired accuracies in a1
   npg1(1)=16                                ! no. of points along a1
   
   jmass=1                                   !M=jmass*10^imass
   rvar=((3*jmass*10d0**imass)/(4.d0*pi*rho))**r13/2.5d0
   PRINT*,rvar,SQRT(sn2ga_cos(0)*cn2)/3d0

   CALL minim_1(1,npg1,bd11,bd12,tol1,amin1,erra1,nit1,xtrm1,rep1,diff_q)

   rvar=((3*jmass*10d0**imass)/(4.d0*pi*rho))**r13/amin1(1)
   sg0=SQRT(sn2ga_cos(0)*cn2)
   rvar=((3*jmass*10d0**imass)/(4.d0*pi*rho))**r13
   WRITE(50,*)SQRT(sn2th_cos(0)*cn2),sg0
ELSEIF(option == 1)THEN
   jmass=1
   xyx=gen_prof(jmass)
   jmass=2
   xyx=gen_prof(jmass)
   jmass=3
   xyx=gen_prof(jmass)
   jmass=5
   xyx=gen_prof(jmass)
   jmass=7
   xyx=gen_prof(jmass)
   jmass=9
   xyx=gen_prof(jmass)
ELSE
   zz=0.d0
   CALL CUSPMF(zz,hm,dndlm,nuc,fcusp)
   STOP
END IF

PRINT*
PRINT*,'***********************************************************************************************'
PRINT*,'Log(M)=',imass
print*,' Best value for q=',amin1(1)
PRINT*,'***********************************************************************************************'
print*

RETURN
CONTAINS
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
!  FUNCTION diff_q
!  FUNCTION gen_prof
!  FUNCTION em_fit
!  FUNCTION ddeldr_pk
!  SUBROUTINE dddr_pk
!  SUBROUTINE drdd_pk
!  FUNCTION mar_pk
!  SUBROUTINE dlrdoz_pk
!  FUNCTION fnr200
!  FUNCTION fnr50
!  FUNCTION fnr_nfw
!  FUNCTION fgconv

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION diff_q(a)
!
!     Given one value of a1 ,a(1) this program measures by means of the chi-square merit 
!     function how well the total mass integrated from a generated density
!     profile using the peak formalism with the theoretical one.
!
!
!-----------------------------------------------------------------------
      IMPLICIT NONE

      INTEGER i,ncp,k
      INTEGER, PARAMETER :: ns=250

      REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a

      DOUBLE PRECISION diff_q
      DOUBLE PRECISION intmass,halom,q0,rg,sth,hmspl
      DOUBLE PRECISION,DIMENSION(ns):: ms,qs,auxs

      deltapk=dm*capd_cos(zmax,omm,iuni)/capd_cos(ozdiff,omm,iuni) !Extrapolation of \delta_c
      q0=a(1)                                                                                                                                               
      call trackq(deltapk,q0,intmass,halom)

      diff_q=0.d0
      IF(intmass>1.d25)THEN
         diff_q=1.d30
      ELSE
         diff_q=ABS(intmass-halom)
      END IF

      PRINT*,'Q value=',q0,intmass,halom
      print*,'Mass diference and %=',diff_q,100.d0*diff_q/halom
      diff_q=ABS(diff_q-100d0)
    END FUNCTION diff_q
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION gen_prof(a)
!
!     Given one value of a1 ,a(1) this program measures by means of the chi-square merit 
!     function how well the total mass integrated from a generated density
!     profile using the peak formalism with the theoretical one.
!
!
!-----------------------------------------------------------------------
      USE math_lib, ONLY : spl
      IMPLICIT NONE

      INTEGER i,ncp,k,klo
      INTEGER, PARAMETER :: ns=250

      INTEGER, INTENT(IN) :: a
      DOUBLE PRECISION gen_prof,at
      DOUBLE PRECISION intmass,halom,q0,rg,sth,hmspl
      DOUBLE PRECISION,DIMENSION(ns):: ms,qs,auxs

      PRINT*,'DELTA_Z0=',DM
      at=1.d0/(1.d0+zobs)
      DO i=1,ns
         ms(i)=EXP(LOG(1.d-9)+LOG((1.d16/1.d-9))/(ns-1)*(i-1))
         rvar=(3*ms(i)/(4.d0*pi*rho))**r13
         sth=SQRT(sn2th_cos(0)*cn2)
         sg=sth+dm*(param_s0+param_s1*at+LOG10(at**param_s2/(1d0+at/param_A)))  !RELATION
         rg=zbrent(rgroot,1.d-2*rvar,1.d2*rvar,1d-9)
         qs(i)=1.d0/(rg*(3*ms(i)/(4.d0*pi*rho))**-r13)
         !WRITE(*,'(1X,I3,6(1X,G12.5))')I,MS(I),RVAR,STH,SG,RG,QS(I)
      END DO
      CALL spline(ms,qs,auxs)
      hmspl=dble(jmass)*10**(DBLE(imass))
      IF(hmspl > 1.d16)STOP' COMPUTATION FINISHED'
      klo=1
      q0=spl(ms,qs,auxs,hmspl,klo)
      deltapk=dm*capd_cos(zmax,omm,iuni)/capd_cos(1.d0+zobs,omm,iuni)
      call trackq(deltapk,q0,intmass,halom)
      gen_prof=0.d0
      IF(intmass>1.d25)THEN
         gen_prof=1.d30
      ELSE
         gen_prof=ABS(intmass-halom)
      END IF

      PRINT*,'Q value=',q0,intmass,halom

      gen_prof=ABS(gen_prof-100d0)

    END FUNCTION gen_prof

!--PART II: PROTOHALO ENERGY AND DENSITY PROFILE
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE trackq(a01,qth,intmass,halom)

REAL(KIND=8), INTENT(IN) :: qth,a01
REAL(KIND=8), INTENT(OUT) :: halom,intmass

CALL delz_cos(1.d0,y,di,dtdoz,de,tdde,yy)
klo=1
lmstar=spl(sls0,lmh,mlmh,-LOG(de),klo)
mst=EXP(lmstar)
print*,'M*',real(mst)

ozi=zmax                             ! init epoch (linear regime)
oz0=1.d0                             ! present epoch
ozc=zobs+1.d0                        ! collapse time
fd=d0/capd_cos(ozi,omm,iuni)         ! to extrapolate to zi
rhozi=rho*ozi**3                     ! cosmic density at zi
omz=ozi**3/(ozi**3+lam0/omm)         ! cosmic density parameter at zi 
dcz=dc0*d0/capd_cos(ozc,omm,iuni)/fd ! coll overdensity at z collapse
ihz=ih0/SQRT(omm*ozi**3+lam0)        ! inverse Hubble parameter at zi    
delpk=1.d0*dc0                       ! normalized peak overdensity for collapse 
CLOSE(2)

!--PART III: GAUSSIAN CONVOLUTION OF THE PROTOHALO PROFILE & PEAK PROFILE

!--- Sampling the \delta(R_f) track for peaks for given initial conditions
! Peak trajectory formalism
mhi=dble(jmass)*10**(DBLE(imass))
halom=mhi
hm1=mhi
lqth=LOG(qth)

!--- Creating strings to store all the created files
WRITE(char_m,*)imass
WRITE(char_j,*)jmass
WRITE(char_q,'(1(1x,g13.5))')qth
char_m=adjustl(char_m)
char_j=adjustl(char_j)
char_q=adjustl(char_q)

fc=d0/capd_cos(ozc,omm,iuni)/fd
fc=capd_cos(ozi,omm,iuni)/capd_cos(1.d0,omm,iuni)

delpk=a01/fc  ! delta taken to z=0
print*
PRINT*,'INPUTS',real(MHI),a01,qth
PRINT*,'delta_track',delpk

rvar=(3.d0*mhi/(pi4*rhozi))**(r13)*ozi
y=sqrt(sn2th_cos(0)*cn2)

rvar0=(3.d0*mhi/(pi4*rhozi))**(r13)*ozi/qth  ! comoving filtering scale
rvar=rvar0
PRINT*,'moment',SQRT(sn2ga_cos(0)*cn2),y
PRINT*,'rfil',RVAR
PRINT*,'nu', delpk/(SQRT(sn2ga_cos(0)*cn2)),y/(SQRT(sn2ga_cos(0)*cn2))

OPEN(1,FILE='tracks/m_'//trim(char_m)//'/conv_'//modcos//'_mst_q2.75'//trim(char_q)//'.dat')
OPEN(2,FILE='tracks/m_'//trim(char_m)//'/track_bbks_mi_q2.75'//trim(char_q)//'.dat')
OPEN(4,file='tracks/m_'//trim(char_m)//'/track_work'//trim(char_q)//'.dat')
OPEN(5,file='tracks/m_2d'//trim(char_m)//'track.dat')
kc=2*nrr-1
!GOTO 31 !To avoid track and deconvolution calculus (only if it has yet been done)
x1=delpk
x2=1.d-8 
y=rvar0
x1=rvar0
x2=5.d-8*rvar0
stpx=(x2/x1)**(1.d0/dble(nrr-1))
y=delpk
y1=delpk
PRINT*,'INITIAL CONDITIONS',real(mhi),real(rvar0),real(y),dcz/fc
xf2(nrr)=rvar0
yf2(nrr)=y
xx=x2
r0=-100.d0
klo=1
DO i2=1,nrr-1
   x2=MERGE(xx,x1*stpx,i2 == nrr-1)
   CALL dddr_pk(x1,y1,r0)
   CALL rk4(y1,r0,x1,x2-x1,y1,dddr_pk)
   xf2(nrr-i2)=x2
   yf2(nrr-i2)=y1
   x1=x2
END DO
x1=rvar0
x2=rvar0*3.d1 ! filt radius at which the density contrast is (almost) null
stpx=(x2/x1)**(1.d0/dble(nrr-1))
xx=x2
y=delpk
y1=delpk
DO i2=1,nrr-1
   x2=MERGE(xx,x1*stpx,i2 == nrr)
   IF(r0 /= 0.d0)THEN
      CALL dddr_pk(x1,y1,r0)
      CALL rk4(y1,r0,x1,x2-x1,y1,dddr_pk)
   END IF
   xf2(nrr+i2)=x2
   yf2(nrr+i2)=y1
   x1=x2
END DO
kc=2*nrr-1
FORALL(i2=1:kc)aux1(i2)=xf2(kc-i2+1)/ozi 
FORALL(i2=1:kc)xf2(i2)=aux1(i2) 
FORALL(i2=1:kc)aux1(i2)=yf2(kc-i2+1)*fc
FORALL(i2=1:kc)yf2(i2)=aux1(i2) 
DO i2=1,kc
   y=yf2(i2)
   IF(lowm.AND.yf2(i2)<-0.95d0)yf2(i2)=-0.95d0
   !!! Output: col(1) counter, col(2) filtering scale (Mpc) at t_i, 
   !!!         col(3) extrapolated peak overdensity, col(4) peak overdensity
   write(2,'(1X,I3,4(1X,G12.5))')I2,XF2(I2),y/fc,y
   write(4,*)xf2(i2),yf2(i2)
   write(5,*)log10(xf2(i2)*ozi),yf2(i2)/fc
END DO
CLOSE(1)
CLOSE(2)
CLOSE(4)
CLOSE(5)

OPEN(4,FILE='tracks/m_'//trim(char_m)//'/track_work'//trim(char_q)//'.dat')
DO i2=1,kc
   READ(4,*)xf2(i2),yf2(i2)
END DO
CLOSE(4)

xf3(1:kc)=xf2(1:kc)                     ! Rf
yf3(1:kc)=yf2(1:kc)                     ! Peak overdensity
PRINT*,'TRACK DONE'
kzn=0
yfac=1.d0
skip=.FALSE.
DO j=1,60
   IF(mod(j,5)==0)PRINT*,'MOSAIC=',j
   CALL laplacei(xf3(1:kc),2.d0+yf3(1:kc),auxn1,auxn2)
   IF(skip==.TRUE.)EXIT
   IF(j==1)THEN
!      print*,auxn1(1:nrr2)
      DO i=nrr2,2,-20
         kzn=kzn+1
         xxxn(kzn)=auxn1(i)
         sg02n(kzn)=auxn2(i)
         yfac=5.d-1
         !PRINT*,j,kzn,xxxn(kzn)!,sg02n(kzn)
      END DO
   ELSEIF(j>=2)THEN
      DO i=nrr2-20,1,-5
         IF(auxn1(i)<xxxn(kzn))THEN
            kzn=kzn+1
            xxxn(kzn)=auxn1(i)
            sg02n(kzn)=auxn2(i)
!            PRINT*,j,xxxn(kzn)
         END IF
      END DO
      IF(xxxn(kzn)<1.d-10)THEN
         print*,'Rf < 1.d-10',xxxn(kzn),kzn
         EXIT
      ELSE
         yfac=yfac*5.d-1
      END IF
   END IF
END DO
OPEN(2,file='tracks/m_'//trim(char_m)//'/deconvolution_work'//trim(char_q)//'.dat') !!! File with the uncorrected data.
WRITE(2,*)kzn
DO i2=1,kzn
    WRITE(2,*)xxxn(i2),sg02n(i2)
ENDDO
CLOSE(2)

OPEN(4,FILE='tracks/m_'//trim(char_m)//'/track_work'//trim(char_q)//'.dat')
DO i2=1,kc
   READ(4,*)xf2(kc-i2+1),yf2(kc-i2+1)
END DO
CLOSE(4)

OPEN(2,file='tracks/m_'//trim(char_m)//'/deconvolution_work'//trim(char_q)//'.dat')
READ(2,*)kzn
DO i2=1,kzn
   READ(2,*)xxxn(kzn-i2+1),sg02n(kzn-i2+1)
END DO
CLOSE(2)

!--  boundaries for \delta 
IF(lowm)THEN
   bd1(1)=0.65d0
   bd1(2)=0.95d0
   bd2(1)=1.00d0
   bd2(2)=1.20d0 
ELSE
   IF(imass>13)THEN
      bd1(1)=0.65d0                           
      bd1(2)=1.06d0                         
      bd2(1)=0.90d0                         
      bd2(2)=1.25d0                         
   ELSE
      bd1(1)=0.85d0                         
      bd1(2)=0.95d0                         
      bd2(1)=1.05d0                         
      bd2(2)=1.15d0                         
   END IF
END IF
!-- accuracies for a1, a2
tol(1)=0.001d0         
tol(2)=0.001d0
!-- number of points along a1, a2         
npg(1)=21              
npg(2)=21 

ndif=3
if(imass>13)ndif=2
DO idif=1,ndif
   first=.true.   
   CALL minim(2,npg,bd1,bd2,tol,amin,erra,nitt,xtrm,rep,diff3)
   
   xxxn(1:kzn)=amin(1)*xxxn(1:kzn)    ! lineal
   sg02n(1:kzn)=amin(2)*sg02n(1:kzn)  ! lineal
   IF(idif==1)first=.false.
   PRINT*,'rfmax',rfmax
   CALL gaussconv0(xxxn(1:kzn),sg02n(1:kzn),xf2,sg02n_conv)
  
   IF(idif==ndif)kzn=kzn2
   IF(idif==ndif)EXIT

   DO i=1,kc
      yf4(i)=yf2(i)
      convdif(i)=sg02n_conv(i)-yf4(i)
   END DO

!--  boundaries for \delta 
   IF(lowm)THEN
      bd1(1)=0.90d0         
      bd1(2)=0.90d0         
      bd2(1)=1.20d0         
      bd2(2)=1.20d0         
!-- number of points along a1, a2   
      npg(1)=21                                
      npg(2)=21    
   ELSE
      bd1(1)=0.80d0           
      bd1(2)=0.80d0           
      bd2(1)=1.15d0           
      bd2(2)=1.05d0        
      IF(imass>13.AND.idif>1)THEN
         bd1(1)=0.65d0     
         bd1(2)=0.95d0     
         bd2(1)=0.85d0     
         bd2(2)=1.20d0     
      END IF
!-- number of points along a1, a2   
      npg(1)=31 
      npg(2)=31 
   END IF

   !Deconvolution (with mosaic) of the difference between the original track and the convolved-deconvolution
   PRINT*,xf3(1),xf3(kc),kc
   PRINT*,'STARTING DECONVOLUTION'
   kzn2=0
   yfac=1.d0
   skip=.FALSE.
   DO j=1,60
      IF(mod(j,5)==0)PRINT*,'MOSAIC2=',j
      CALL laplacei(xf3(1:kc),2.d0+convdif(1:kc),auxn1,auxn2)
      IF(skip==.TRUE.)EXIT
      IF(j==1)THEN
         DO i=nrr2,2,-20
            kzn2=kzn2+1
            xdif(kzn2)=auxn1(i)
            decondif(kzn2)=auxn2(i)
            yfac=5.d-1
         END DO
      ELSEIF(j>=2)THEN
         DO i=nrr2-20,1,-5
            IF(auxn1(i)<xdif(kzn2))THEN
               kzn2=kzn2+1
               xdif(kzn2)=auxn1(i)
               decondif(kzn2)=auxn2(i)
            END IF
         END DO
         IF(xdif(kzn2)<1.d-10)THEN
            print*,'Rf < 1.d-10',xf2(kzn2),kzn2
            EXIT
         ELSE
            yfac=yfac*5.d-1
         END IF
      END IF
   END DO
   PRINT*,'Rfmax2',rfmax
   print*,'kzns',kzn,kzn2
   
   OPEN(2,file='tracks/m_'//trim(char_m)//'/deconvolution_diff'//trim(char_q)//'.dat') !!! File with the uncorrected data.                                                          
   WRITE(2,*)kzn2
   DO i2=1,kzn2
      WRITE(2,*)xdif(i2),decondif(i2)
   ENDDO
   CLOSE(2)
   
   OPEN(2,file='tracks/m_'//trim(char_m)//'/deconvolution_diff'//trim(char_q)//'.dat')
   READ(2,*)kzn2
   DO i2=1,kzn2
      READ(2,*)xdif(kzn2-i2+1),decondif(kzn2-i2+1)
   END DO
   CLOSE(2)
   
   DO I=1,KZN2
      xxxn(i)=xdif(i)
      sg02n(i)=sg02n(i)-0.25d0*decondif(i)
   END DO
END DO

OPEN(2,FILE='tracks/m_'//trim(char_m)//'/final_decon'//trim(char_q)//'.dat')  !!! File with the corrected data to check results.
WRITE(2,*)kzn
DO i2=1,kzn
   WRITE(2,*)xxxn(i2),sg02n(i2)
ENDDO
CLOSE(2)

!31 CONTINUE
OPEN(4,FILE='tracks/m_'//trim(char_m)//'/track_work'//trim(char_q)//'.dat')
DO i2=1,kc
   READ(4,*)xf2(kc-i2+1),yf2(kc-i2+1)
END DO
CLOSE(4)

OPEN(5,FILE='tracks/m_'//trim(char_m)//'/final_decon'//trim(char_q)//'.dat')  !!! File with the corrected data to check results.
READ(5,*)kzn
DO i2=1,kzn
    READ(5,*)xxxn(i2),sg02n(i2)
ENDDO
CLOSE(5)
delpk=dc0*fc

DO i2=1,kzn
   lrhop(i2)=log((1.d0+sg02n(i2))*rhozi)
   lrp(i2)=log(xxxn(i2))
END DO
CLOSE(5)

CALL spline(lrp(1:kzn),lrhop(1:kzn),myf3(1:kzn))

CALL fit(lrp(1:10),lrhop(1:10),10,sig(1:10),0,aa,ab,siga,sigb,chi2,y)

! Sampling the protohalo density profile
eaa=EXP(aa)
qrfmax=(3.d0*mhi/(pi4*rhozi))**(r13)

!-------------------- Mass of the protohalo for each rp -------------------- 
PRINT*,'Sampling protohalo mass'
stpr=(log(xxxn(2))-log(xxxn(1)))/15.d0
i1=0
DO i2=1,kzn
   xxxn(i2)=EXP(log(xxxn(1))+stpr*(i2-1))
   IF(xxxn(i2)>xxxn(kzn))EXIT
   lrp2(i2)=log(xxxn(i2))
   deltac(i2)=(eaa*xxxn(1)**(ab+3.d0)/(ab+3.d0)+qromb(protoprofint,xxxn(1),xxxn(i2)))/rhozi-xxxn(i2)**3/3.d0
   deltam(i2)=pi4*rhozi*deltac(i2)
   hmass(i2)=pi43*rhozi*xxxn(i2)**3+deltam(i2)
   mbkg=pi43*rhozi*xxxn(i2)**3
   ldeltam(i2)=log(deltam(i2)/mbkg+1.d0)            ! Adding the mass to avoid logs of negative values.
   IF(ldeltam(i2)>0.d0)i1=i1+1
END DO
!stop
i2=i2-1
i1=I2

CALL spline(lrp2(1:i1),ldeltam(1:i1),myf2(1:i1))

!-------------------- Eta by integration -------------------- 
PRINT*,'Eta calculus'
eps=1.d-9
kc=0
DO i2=1,i1
   etarh(i2)=pi4*r53*qromb(eint,0.d0,xxxn(i2))
END DO

eps=1.d-9
! Second part: adding random density fluctuations of any scale smaller than Rp
DO i2=1,i1
   etar(i2)=etarh(i2)
ENDDO

!------------------- dlnE/dlnM -------------------- 
DO i2=1,i1
   dedmp(i2)=-gcos*r53*deltam(i2)/xxxn(i2)
   dledlmp(i2)=dedmp(i2)*hmass(i2)/etar(i2) 
   lhmass(i2)=LOG(hmass(i2))
END DO

!--------------------  profile -------------------- 
print*,'Profile calculation',rvir,i1
DO i2=1,i1
   massc=hmass(i2)
   etac=ABS(etar(i2))
   y=3.d-1*gcos*massc**2/etac
   IF(rroot(1.d-1*y)>0.d0.AND.rroot(1.d1*y)>0.d0)EXIT
   IF(rroot(1.d-1*y)<0.d0.AND.rroot(1.d1*y)<0.d0)EXIT
   IF(dledlmp(i2)>2.d0)EXIT
   xxx2(i2)=zbrent(rroot,1.d-1*y,1.d1*y,1.d-6*y)
ENDDO

i1=i2-1
DO i2=1,i1
   dmdr(i2)=hmass(i2)/xxx2(i2)/(2.d0-dledlmp(i2))
ENDDO

DO i2=1,i1
   rhoh(i2)=dmdr(i2)/(pi4*xxx2(i2)**2)
   mbkg=pi43*rho*xxx2(i2)**3
   lrh(i2)=log(xxx2(i2))
   lrhoh(i2)=log(rhoh(i2))
END DO

IF(mdef==0)THEN
   rvir=(mhi/(pi43*dvbn_cos(ozc)*rho))**r13/ozc
ELSE
   rvir=(mhi/(pi43*200d0*rhoc))**r13
END IF

OPEN(19,file='profiles/'//trim(char_j)//'e'//trim(char_m)//'.dat')

DO i2=1,i1
   IF(xxx2(i2)/rvir>1.d0)EXIT
   write(19,*)xxx2(i2),rhoh(i2)
END DO
klo=1
CALL spline(lrh(1:i1),lrhoh(1:i1),aux1(1:i1))
WRITE(19,*)rvir,EXP(spl(lrh(1:i1),lrhoh(1:i1),aux1(1:i1),log(rvir),klo))
CLOSE(19)

CALL fit(lrh(1:10),lrhoh(1:10),10,sig(1:1),0,aa,ab,siga,sigb,chi2,y)
mass1_fit=aa    ! rhoh=aa*rp^ab
mass2_fit=ab
CALL spline(lrh(1:i1),lrhoh(1:i1),aux1(1:i1))
IF(rvir>1.005d0*xxx2(i1))THEN
   intmass=1.d30
   RETURN
ELSE
   intmass=qromb(profint,1.d-2*(MIN(xxx2(i1),rvir)),MIN(xxx2(i1),rvir))*pi4
END IF
PRINT*,'CHECK!=',intmass

IF(option==0)return

kc=MAX(kc,i1)

PRINT*,'Sampling halo density profile',kc
kzc=100
! Sampling the halo density profile to kzc points (Einasto)
stpx=(LOG(MIN(xxx2(kc),rvir))-LOG(1.d-2*rvir))/DBLE(kzc-1) !1e9  Mo

klo=1
OPEN(20,file='profiles/'//trim(char_j)//'e'//trim(char_m)//'_short.dat')
DO i2=1,kzc
   aux1(i2)=LOG(1.d-2*rvir)+stpx*DBLE(i2-1)
   aux2(i2)=lint(LOG(xxx2(1:kc)),LOG(rhoh(1:kc)),aux1(i2),klo) ! rho(r)
   WRITE(20,'(1X,2(1X,G12.5))')EXP(aux1(i2)),EXP(aux2(i2))
ENDDO
CLOSE(20)

if(option==0)RETURN                       !We don't want to fit the NFW & Einasto profiles when searching the sigma relation

IF(prof_cons==0)THEN
   
   !goto 345                               ! UNCOMMENT TO AVOID FITTING THE EINASTO PROFILE
   IF(lowm)THEN
!-- fitting range for rhos (in log10)
      bd1(1)=14.d0                           
      bd2(1)=18.d0
!-- fitting range for rs (in log10)
      bd1(2)=-3.0d0                          
      bd2(2)=-9.0d0
   ELSE
!-- fitting range for rhos (in log10)
      bd1(1)=12.d0                        
      bd2(1)=16.d0
!-- fitting range for rs (in log10)
      bd1(2)=2.0d0                        
      bd2(2)=-4.0d0
   END IF
!-- fitting range for alpha
   bd1(3)=0.31d0                       
   bd2(3)=0.16d0
   
   tol(1)=0.001d0                             ! desired accuracies in a1
   tol(2)=0.001d0                             ! desired accuracies in a2
   tol(3)=0.001d0                             ! desired accuracies in a3
   
   npg(1)=51                                  ! no. of points along a1
   npg(2)=51 
   npg(3)=65 
   CALL minim(3,npg,bd1,bd2,tol,amin,erra,nitt,xtrm,rep,Einastodiff)

   PRINT*,'EINASTO rho_s,r_s:',10**amin(1),10**amin(2)
   PRINT*,'EINASTO ALPHA=',amin(3),halom
   PRINT*,'EINASTO PARAMETERS in h units:',10**amin(1)/0.71**2,10**amin(2)*0.71
   PRINT*,'CONCENTRATION:',rvir/10**amin(2)
   
   rhos=10d0**amin(1)
   rs=10d0**amin(2)
   alpha=amin(3)
   
   x2=2d0/alpha
   x3=3d0/alpha
   igam=gammq(x3,x2)*gamma(x3)
   WRITE(84,'(1X,6(1X,G12.5))')mhi,rhos,rs,rvir/rs,pi2*x2**(1d0-x3)*exp(x2)*(gamma(x3)-igam)*rhos*rs**3,alpha    !M,rhos,rs,c,Ms,alpha
   PRINT*
   PRINT*
   
345 continue
   PRINT*,'Sampling halo density profile',kc
   PRINT*
   kzc=100
   ! Sampling the halo density profile to kzc points (NFW)
   stpx=(LOG(MIN(xxx2(kc),rvir))-LOG(1.d-2*rvir))/DBLE(kzc-1) !1e9  Mo
   
   klo=1
   OPEN(20,file='profiles/'//trim(char_j)//'e'//trim(char_m)//'_short.dat')
   DO i2=1,kzc
      aux1(i2)=LOG(1.d-2*rvir)+stpx*DBLE(i2-1)
      aux2(i2)=lint(LOG(xxx2(1:kc)),LOG(rhoh(1:kc)),aux1(i2),klo) ! rho(r)
      WRITE(20,'(1X,2(1X,G12.5))')EXP(aux1(i2)),EXP(aux2(i2))
   ENDDO
   CLOSE(20)
   IF(lowm)THEN
!-- fitting range for rhos (in log10)   
      bd1(1)=12.d0                    
      bd2(1)=18.d0
!-- fitting range for rs (in log10)
      bd1(2)=-2.0d0                   
      bd2(2)=-9.0d0
   ELSE
!-- fitting range for rhos (in log10)   
      bd1(1)=10.d0                          
      bd2(1)=16.d0
!-- fitting range for rs (in log10)
      bd1(2)=2.0d0                          
      bd2(2)=-5.0d0
   END IF
   tol(1)=0.001d0                             ! desired accuracies in a1
   tol(2)=0.001d0                             ! desired accuracies in a2
   npg(1)=71                                  ! no. of points along a1
   npg(2)=71 
   
   CALL minim(2,npg,bd1,bd2,tol,amin,erra,nitt,xtrm,rep,NFWdiff)
   rhos=10.d0**amin(1)
   rs=10.d0**amin(2)
  
   PRINT*,'NFW PARAMETERS:',10**amin(1),10**amin(2)
   PRINT*,'NFW PARAMETERS in h units:',10**amin(1)/0.71**2,10**amin(2)*0.71
   PRINT*,'CONCENTRATION:',rvir/10**amin(2)
   WRITE(74,'(1X,5(1X,G12.5))')mhi,10**amin(1),10**amin(2),rvir/10**amin(2),16*pi*(LOG(2d0)-0.5d0)*rhos*rs**3 !M,rhos,rs,c
   PRINT*
   RETURN
ELSE
bd1(1)=log10(rvir/100d0)                  ! rs fit_range
bd2(1)=log10(rvir*10.0d0)

bd1(2)=0.30d0                             !alpha fit_range
bd2(2)=0.16d0

tol(1)=0.001d0                             ! desired accuracies in a1
tol(2)=0.001d0                             ! desired accuracies in a2

npg(1)=101                                  ! no. of points along a1
npg(2)=101 

CALL minim(2,npg,bd1,bd2,tol,amin,erra,nitt,xtrm,rep,Einastodiffc)

rs=10d0**amin(1)
alpha=amin(2)
x2=2d0/alpha
x3=3d0/alpha
igam=gammq(3d0/alpha,2d0/alpha*(rvir/rs)**alpha)*gamma(3d0/alpha)
rhos=mhi*alpha/pi4*exp(-2d0/alpha)/rs**3*(2d0/alpha)**(3d0/alpha)/(gamma(3d0/alpha)-igam)

PRINT*,'EINASTO rho_s,r_s:',rhos,rs
PRINT*,'EINASTO ALPHA=',alpha
PRINT*,'CONCENTRATION:',rvir/rs

igam=gammq(3d0/alpha,2d0/alpha)*gamma(3d0/alpha)
WRITE(84,'(1X,6(1X,G12.5))')mhi,rhos,rs,rvir/rs,pi2*x2**(1d0-x3)*exp(x2)*(gamma(x3)-igam)*rhos*rs**3,alpha  !Ms,rhos,rs,c,Ms,alpha
PRINT*
PRINT*

PRINT*,'Sampling halo density profile',kc
PRINT*
kzc=100
! Sampling the halo density profile to kzc points (NFW)
stpx=(LOG(MIN(xxx2(kc),rvir))-LOG(1.d-2*rvir))/DBLE(kzc-1) !1e9  Mo

klo=1
OPEN(20,file='profiles/'//trim(char_j)//'e'//trim(char_m)//'_short.dat')
DO i2=1,kzc
   aux1(i2)=LOG(1.d-2*rvir)+stpx*DBLE(i2-1)
   aux2(i2)=lint(LOG(xxx2(1:kc)),LOG(rhoh(1:kc)),aux1(i2),klo) ! rho(r)
   WRITE(20,'(1X,2(1X,G12.5))')EXP(aux1(i2)),EXP(aux2(i2))
ENDDO
CLOSE(20)

IF(lowm)THEN
!-- fitting range for rs (in log10)
   bd1(1)=-2.0d0                   
   bd2(1)=-9.0d0
ELSE
!-- fitting range for rs (in log10)
   bd1(1)=2.0d0                          
   bd2(1)=-5.0d0
END IF
tol(1)=0.001d0                             ! desired accuracies in a1
npg(1)=71                                  ! no. of points along a1

CALL minim(1,npg,bd1,bd2,tol,amin,erra,nitt,xtrm,rep,NFWdiffc)

rs=10d0**amin(1)
rhos=mhi/16d0/pi/rs**3/(LOG((rs+rvir)/rs)-rvir/(rvir+rs))

PRINT*,'NFW PARAMETERS:',rhos,rs
PRINT*,'CONCENTRATION:',rvir/rs
WRITE(74,'(1X,5(1X,G12.5))')mhi,rhos,rs,rvir/rs,16*pi*(LOG(2d0)-0.5d0)*rhos*rs**3 !M,rhos,rs,c,Ms
PRINT*
END IF
RETURN

END SUBROUTINE trackq

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION em_fit(x,y,k)

!
!     Fitting function for E(M)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x  ! inner halo mass normalized to the halo mass at t_0
REAL(KIND=8), INTENT(IN) :: y  ! halo mass at t_0 in units of M_*
INTEGER, INTENT(IN) :: k
REAL(KIND=8) :: em_fit,a0,a1

IF(k == 1)THEN
   STOP' Not implemented for SCDM'
ELSE
   a0=0.02974d0*y**0.7418d0
   a1=1.1785d0*y**0.13855d0
   em_fit=-mhi*a0*x**1.22d0/(x**2+a1)
END IF
END FUNCTION em_fit

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION ddeldr_pk(delta,lrf)

!
!     Computes the R_f-derivative, ddel/dR_f, of the density contrast for peaks

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: delta ! linear density contrast for collapse at t
REAL(KIND=8), INTENT(IN) :: lrf   ! ln of the filtering scale in Mpc
REAL(KIND=8) :: ddeldr_pk,sg_0,sg_1,sg_2,gm,nu,xav,lx
REAL(KIND=8) :: xfact(1),yy1(1)
INTEGER :: klo

klo=1
IF(lrf < lrcomg(1))print*,lrf,lrcomg(1)
lx=MAX(lrcomg(1),lrf)
sg_0=EXP(spl(lrcomg,lsg0,mlsg0,lx,klo))
sg_1=EXP(spl(lrcomg,lsg1,mlsg1,lx,klo))
sg_2=EXP(spl(lrcomg,lsg2,mlsg2,lx,klo))
gm=sg_1**2/(sg_0*sg_2)  
nu=delta/sg_0
xav=1.d0/xtypinv_pk(gm,gm*nu)
!xav=xtyp_pk(gm,gm*nu)
!xav=gm*nu+theta_pk(gm,gm*nu)
ddeldr_pk=xav*sg_2*EXP(lrf)
END FUNCTION ddeldr_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE dddr_pk(x,y,dydx)

!
!     Computes the (1+z)-derivative, dlr/doz, of the scale for peaks

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x ! filtering scale in Mpc
REAL(KIND=8), INTENT(IN) :: y ! peak density contrast 
REAL(KIND=8), INTENT(OUT) :: dydx ! derivative function


dydx=-ddeldr_pk(y,LOG(x))
END SUBROUTINE dddr_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE drdd_pk(x,y,dydx)

!
!     Computes the (1+z)-derivative, dlr/doz, of the scale.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x ! peak density contrast 
REAL(KIND=8), INTENT(IN) :: y ! filtering scale en Mpc
REAL(KIND=8), INTENT(OUT) :: dydx ! derivative function

dydx=-1.d0/ddeldr_pk(x,LOG(y))
END SUBROUTINE drdd_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION mar_pk(mh,oz)

!
!     Computes the mass accretion rate (M_o/Gyr) for peaks

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: mh  ! halo mass in M_o
REAL(KIND=8), INTENT(IN) :: oz ! 1+z
REAL(KIND=8) :: mar_pk,t,di,dtdoz,de,tdde,at,dmdr,lrf,delta

CALL delz_cos(oz,t,di,dtdoz,de,tdde,at)
delta=de*delpk/dc0 
tdde=tdde*delta/dc0
lrf=(LOG(mh)-lrhopi43)*r13-lqth
dmdr=3.d0*mh/EXP(lrf)
mar_pk=tdde*dmdr/(ddeldr_pk(delta,lrf))
END FUNCTION mar_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE dlrdoz_pk(x,y,dydx)

!
!     Computes the (1+z)-derivative, dlr/doz, of the scale for peaks

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x ! 1+z
REAL(KIND=8), INTENT(IN) :: y ! log of scale at z, in Mpc
REAL(KIND=8), INTENT(OUT) :: dydx ! derivative function
REAL(KIND=8) :: t,di,dtdoz,de,tdde,at,delta

CALL delz_cos(x,t,di,dtdoz,de,tdde,at)
delta=de*delpk/dc0 
tdde=tdde*delpk/dc0
dydx=dtdoz*tdde/(EXP(y)*ddeldr_pk(delta,y))
END SUBROUTINE dlrdoz_pk

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION fnr200(x)

! auxiliary function to find the halo radius containing an overdensity 200 times the critical one 

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: fnr200

fnr200=200.d0-fmass/(func_pr(1,x/rs)*x**3)
END function fnr200

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION fnr50(x)

! auxiliary function to find the halo radius containing an overdensity 50 times the critical one 

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: fnr50

fnr50=50.d0-fmass/(func_pr(1,x/rs)*x**3)
END function fnr50

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION fnr_nfw(x)

! auxiliary function to find the radius containing a mass x for a given energy E_ta

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: fnr_nfw,yy1(1)

yy1=prof_pr(2,cvir,(/x/))
fnr_nfw=fmass-yy1(1)
END function fnr_nfw

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION find0(x)

! auxiliary function to find the filter radius yielding a null convolution for the protoobject profile

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: find0
INTEGER :: klo

klo=1
find0=spl(xcv,ycv,mycv,x,klo)
END function find0

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION mprot(x)

! integrand for the inner mass of the protoobject (over 4*pi*rho_i)

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: mprot
INTEGER :: klo

klo=1
mprot=x**2*EXP(spl(xf,yf,myf,LOG(x),klo))
END function mprot

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION findr(x)

! auxiliary function to find the radius of the protoobject containing the final mass

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: findr,y

klo=1
IF(x < EXP(xf(1)))THEN
   y=EXP(a1)*x**(b1+3.d0)/(b1+3.d0)
ELSE
   y=EXP(a1)*EXP(xf(1))**(b1+3.d0)/(b1+3.d0)+qromb(mprot,EXP(xf(1)),x)
END IF
massx=pi4*rhozi*y
findr=fmass-massx
END function findr

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION fgconv(x)

! Integrand for Gaussian convolution

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: fgconv,y

IF(x < EXP(xf(1)))THEN
   y=EXP(a1)*x**b1-1.d0  ! power-law extrapolation
ELSEIF(x > EXP(xf(nrr)))THEN
   y=-1.d0
ELSE
   klo=1
   y=EXP(spl(xf,yf,myf,LOG(x),klo))-1.d0
END IF
fgconv=x**2*y*EXP(-0.5d0*(x/rvar)**2) ! Gaussian window
END FUNCTION fgconv


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE gaussconv0(ax,ay,rf,fr)

! Gaussian convolution of a tabulated function at r=0
! Uses: fgc0
! Common variables: a0,b0,siga,sigb,sig,sax,say,msay,nsa,rvar

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE

REAL(KIND=8), INTENT(IN), DIMENSION(:) :: ax  ! tabulated input function: array with r_p values
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: ay  ! tabulated input function: array with 1+delta values
REAL(KIND=8), INTENT(IN), DIMENSION(:) :: rf  ! array with filtering radius (Mpc)
REAL(KIND=8), INTENT(OUT), DIMENSION(SIZE(rf)) :: fr ! Gaussian convolution at r=0 
INTEGER :: i,kk,nsf
INTEGER, PARAMETER :: nstp=150 
REAL(KIND=8) :: stpy,y,y1,y2,sum,ss,xl
REAL(KIND=8), DIMENSION(SIZE(ax)) :: may

nsf=SIZE(rf)
nsa=SIZE(ax)
sax(1:nsa)=ax(1:nsa)
say(1:nsa)=ay(1:nsa)+1.d0  ! to avoid slightly negative values of input function
CALL fit(LOG(sax(1:5)),LOG(say(1:5)),5,sig(1:5),0,a0,b0,siga,sigb,chi2,y)
CALL spline(sax(1:nsa),say(1:nsa),msay(1:nsa))
IF(mhi > 5.d12)eps=1.d-8

kcont=0
DO i=1,nsf
   rvar=rf(i)
   xl=9.d0*rvar
   IF(xl>rfmax)EXIT
!--- Convolution integral to evaluate the central density of the smoothed profile
   sum=qromo(fgc0,0.d0,MIN(1.d-3,xl),1.d-7,dmidpnt_s)
   y1=MIN(1.d-3,xl)! Second part of the convolution integral (decomposed in nstp intervals)
   y2=xl
   stpy=EXP((LOG(y2)-LOG(y1))/DBLE(nstp))
   DO kk=1,nstp
      y2=MERGE(xl,y1*stpy,kk == nstp)
      ss=qromb(fgc0,y1,y2)
      sum=sum+ss
      IF(ABS(ss) < 1.d-9*ABS(sum))EXIT
      y1=y2
   END DO
   fr(i)=sqrt(2.d0/pi)*sum/rvar**3
   IF(first)THEN
      if(fr(i) <= 0.d0)EXIT
   END IF
   kcont=kcont+1
END DO
eps=1.d-9
END SUBROUTINE gaussconv0

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION fgc0(x)

! Integrand for Gaussian convolution

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: fgc0,y

IF(x < sax(1))THEN
   y=EXP(a0)*x**b0-1.d0  ! power-law extrapolation
ELSEIF(x > sax(nsa))THEN
   PRINT*,x,sax(1),sax(nsa)
   stop' Beyond limit'
ELSE
   klo=1
   y=spl(sax(1:nsa),say(1:nsa),msay(1:nsa),x,klo)-1.d0
END IF
fgc0=x**2*y*EXP(-0.5d0*(x/rvar)**2) ! Gaussian window
END FUNCTION fgc0


!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION fgconv2(x)

! Integrand for Gaussian convolution

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: fgconv2,y

IF(x < xxxn(1))THEN
   y=EXP(LOG(sg02n(1))+(LOG(x)-LOG(xxxn(1)))*(LOG(sg02n(2))-LOG(sg02n(1)))/(LOG(xxxn(2))-LOG(xxxn(1))))-1.d0
ELSEIF(x > xxxn(kzn))THEN
   PRINT*,'WARNING !!!!!!'
   y=-1.d0
ELSE
   klo=1
   y=lint(xxxn(1:kzn),sg02n(1:kzn),x,klo)-1.d0
END IF
fgconv2=x**2*y*EXP(-0.5d0*(x/rvar)**2) ! Gaussian window
END FUNCTION fgconv2

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

 SUBROUTINE rk4(y,dydx,x,h,yout,derivs)

!     Subroutine adapted from Numerical Recipes.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: y
REAL(KIND=8), INTENT(IN) :: dydx
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8), INTENT(IN) :: h
REAL(KIND=8), INTENT(OUT) :: yout
INTEGER i
REAL(KIND=8) :: h6,hh,xh,dym,dyt,yt

INTERFACE
   SUBROUTINE derivs(x,y,dydx)
     IMPLICIT NONE
     REAL(KIND=8), INTENT(IN) :: x
     REAL(KIND=8), INTENT(IN) :: y
     REAL(KIND=8), INTENT(OUT) :: dydx
   END SUBROUTINE derivs
END INTERFACE

hh=h*0.5d0
h6=h/6.d0
xh=x+hh
yt=y+hh*dydx
call derivs(xh,yt,dyt)
yt=y+hh*dyt
call derivs(xh,yt,dym)
yt=y+h*dym
dym=dyt+dym
call derivs(x+h,yt,dyt)
yout=y+h6*(dydx+dyt+2.d0*dym)
END SUBROUTINE rk4

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION hmassroot(x)

IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x 
REAL(KIND=8) :: hmassroot,y

y=qromb(mint,xxxn(1),x)
hmassroot=pi4*(eaa*xxxn(1)**(ab+3.d0)/(ab+3.d0)+Y)-mhi
END FUNCTION hmassroot

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION rmaxroot(x)

! Function to find rmax

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x ! rp
REAL(KIND=8) :: rmaxroot,y,mmax,hh

klo=1
y=lint(xxxn(1:kzn),sg02n(1:kzn),x,klo)
rmaxroot=y-rhozi

END FUNCTION rmaxroot

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION mint(x)

! Integrand for Mass integration

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: mint,y

IF(x > xxxn(kzn))THEN
   y=sg02n(kzn)
   stop
ELSE
   klo=1
   y=exp(spl(lrp(1:kzn),lrhop(1:kzn),myf3(1:kzn),log(x),klo))
END IF
mint=y*x**2
if(mint < 0.d0)then
   print*,x,y
   stop
end if
END FUNCTION mint

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION mint2(x)

! Integrand for Mass integration

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: mint2,y

IF(x > xxxn(i1))THEN
   y=rhop2(i1)
ELSE
   klo=1
   y=EXP(spl(LOG(xxxn(1:i1)),LOG(rhop2(1:i1)),myf(1:i1),LOG(x),klo))
END IF
mint2=y*x**2

END FUNCTION mint2

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION eint(x)

! Integrand for Eta integration

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER :: klo
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: eint,y,massx,hh,deltax,z

IF(x == 0)THEN
   eint=0.d0
   RETURN
ELSEIF(x < xxxn(1))THEN
   y=eaa*x**ab
   deltax=pi4*(eaa*x**(ab+2.d0)/(ab+3.d0)-rhozi*x**2/3.d0)
ELSEIF(x > xxxn(i1))THEN
   print*,'Warning1'
   y=exp(lrhop(i1))
   deltax=deltam(i1)/x
ELSE
   klo=1
   z=log(x)
   y=exp(spl(lrp(1:kzn),lrhop(1:kzn),myf3(1:kzn),z,klo))
   mbkg=pi43*rhozi*x**3
   klo=1
   deltax=(exp(spl(lrp2(1:i1),ldeltam(1:i1),myf2(1:i1),z,klo))-1.d0)*mbkg/x
END IF
eint=-y*x**2*gcos*deltax
END FUNCTION eint

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION profint(x)

! Integrand to find the total mass of a given profile up to a maximum radius.

!----------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8) :: x
REAL(KIND=8) :: profint

klo=1
IF(x<xxx2(1))THEN
   profint=EXP(mass1_fit)*x**(mass2_fit+2.d0)
ELSE
   profint=EXP(spl(lrh(1:i1),lrhoh(1:i1),aux1(1:i1),log(x),klo))*x**2
END IF

END FUNCTION profint
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    

FUNCTION NFWdiff(a)

! Function used in the minim subroutine in order to compute the best fit of a NFW-like profile to the real one via chi-squared.

REAL(KIND=8),DIMENSION(:),INTENT(IN) :: a
DOUBLE PRECISION rhos,rs,r,diff,rhoNFW,NFWdiff

NFWdiff=0.d0
rhos=10**(a(1))
rs=10**(a(2))

DO i=1,kzc
   r=EXP(aux1(i))
   rhoNFW=4d0*rhos*rs**3/r/(r+rs)**2
   diff=aux2(i)-log(rhoNFW)
   NFWdiff=NFWdiff+diff**2
END DO
NFWdiff=NFWdiff/kzc
END FUNCTION NFWdiff

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    

FUNCTION Einastodiff(a)

! Function used in the minim subroutine in order to compute the best fit of a Einasto-like profile to the real one via chi-squared.

REAL(KIND=8),DIMENSION(:),INTENT(IN) :: a
DOUBLE PRECISION rhos,rs,r,diff,rhoEinasto,Einastodiff,alpha

Einastodiff=0.d0
rhos=10**(a(1))
rs=10**(a(2))
alpha=a(3)

DO i=1,kzc
   r=EXP(aux1(i))
   rhoEinasto=rhos*EXP(-2.d0/alpha*((r/rs)**alpha-1.d0))
   IF(rhoEinasto<1d-30)THEN
      Einastodiff=1d30
      RETURN
   END IF
   diff=aux2(i)-log(rhoEinasto)
   Einastodiff=Einastodiff+diff**2
END DO
Einastodiff=Einastodiff/kzc
END FUNCTION Einastodiff

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    

FUNCTION NFWdiffc(a)

! Function used in the minim subroutine in order to compute the best fit of a NFW-like profile to the real one via chi-squared.

REAL(KIND=8),DIMENSION(:),INTENT(IN) :: a
DOUBLE PRECISION rhos,rs,r,diff,rhoNFW,NFWdiffc

NFWdiffc=0.d0
rs=10**(a(1))
rhos=mhi/16d0/pi/rs**3/(LOG((rs+rvir)/rs)-rvir/(rvir+rs))

DO i=1,kzc
   r=EXP(aux1(i))
   rhoNFW=4d0*rhos*rs**3/r/(r+rs)**2
   diff=aux2(i)-log(rhoNFW)
   NFWdiffc=NFWdiffc+diff**2
END DO
NFWdiffc=NFWdiffc/kzc
END FUNCTION NFWdiffc

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION Einastodiffc(a)

! Function used in the minim subroutine in order to compute the best fit of a Einasto-like profile to the real one via chi-squared.

REAL(KIND=8),DIMENSION(:),INTENT(IN) :: a
DOUBLE PRECISION rhos,rs,r,diff,rhoEinasto,Einastodiffc,alpha,igam

Einastodiffc=0.d0
rs=10**(a(1))
alpha=a(2)
igam=gammq(3d0/alpha,2d0/alpha*(rvir/rs)**alpha)*gamma(3d0/alpha)
rhos=mhi*alpha/pi4*exp(-2d0/alpha)/rs**3*(2d0/alpha)**(3d0/alpha)/(gamma(3d0/alpha)-igam)

DO i=1,kzc
   r=EXP(aux1(i))
   rhoEinasto=rhos*EXP(-2.d0/alpha*((r/rs)**alpha-1.d0))
   if(rhoEinasto<=0.d0)THEN
      Einastodiffc=1d30
      RETURN
   end if
   diff=aux2(i)-log(rhoEinasto)
   Einastodiffc=Einastodiffc+diff**2
END DO
Einastodiffc=Einastodiffc/kzc
END FUNCTION Einastodiffc

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------  
FUNCTION NFWint(x)

! Integrand to find the total mass of a given profile up to a maximum radius.

!----------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8) :: x
REAL(KIND=8) :: NFWint

NFWint=4*rhos*rs**3/(x*(rs+x)**2)*x**2

END FUNCTION NFWint
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------    

FUNCTION protoprofint(x)

! Integrand to find the total mass of a given profile up to a maximum radius.                                                                                                      

!----------------------------------------------------------------------------------------------------------------------------------------------------------                        
IMPLICIT NONE
REAL(KIND=8) :: x
REAL(KIND=8) :: protoprofint

klo=1
IF(log(x)<lrp(1))THEN
   protoprofint=EXP(spl(lrp(1:kzn),lrhop(1:kzn),myf3(1:kzn),lrp(1),klo))*x**2
ELSE
   protoprofint=EXP(spl(lrp(1:kzn),lrhop(1:kzn),myf3(1:kzn),log(x),klo))*x**2
END IF

END FUNCTION protoprofint

!----------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION Dint(x)

! Integrand for D(R) integration

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
INTEGER :: klo
REAL(KIND=8), INTENT(IN) :: x  ! radius
REAL(KIND=8) :: Dint,mass,y,lx

lx=LOG(x)
IF(x < xxx2(1))THEN
   STOP'Dint Beyond inf. limit'
ELSEIF(x > xxx2(i1))THEN
   STOP'Dint Beyond sup. limit'
ELSE
   klo=1
   mass=EXP(spl(LOG(xxx2(1:i1)),laux1(1:i1),myf3(1:i1),lx,klo))
END IF
if(mass < 0.d0)PRINT*,'Warning: mass < 0'
Dint=((mass/massc)**2)*((radc/x)**2)/radc
END FUNCTION Dint
      
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SUBROUTINE laplacei(xf,yf,rp,dp)
IMPLICIT NONE
REAL*8,DIMENSION(:),INTENT(IN) :: xf,yf
REAL*8,DIMENSION(:),INTENT(OUT) :: rp,dp
REAL*8,DIMENSION(SIZE(xf)) :: myf,aux1,aux2
REAL*8,DIMENSION(size(rp)) :: xi2,tfour1
REAL*8 :: x1,stp,y,fac
INTEGER :: kc,kz,klo

kc=SIZE(xf)
kz=SIZE(rp)
aux1=LOG(xf)
aux2=LOG(yf)
CALL spline(aux1,aux2,myf)
stp=1.d0/(pi4*(yfac*MAX(xf(1),xf(kc))**2))          ! Fixed Step in Fourier space (Defined by the greatest Rf) 
xi2(1:kz)=arth(stp,stp,kz)                          ! prepare xi variable sampled with a constant step
DO i2=1,kz
   klo=1
   rp(i2)=1.d0/(SQRT(pi4*xi2(i2)))                  ! New Rf's sampled with a constant step in xi
   IF(rp(i2)<MIN(xf(1),xf(kc)))THEN
      print*,rp(i2),MIN(xf(1),xf(kc)),i2,kz
      skip=.TRUE.
      RETURN
   END IF
   dp(i2)=EXP(spl(aux1,aux2,myf,LOG(rp(i2)),klo))-2.d0    ! interpolate delta(Rf) to find delta(xi)
ENDDO
IF(j==1)rfmax=MAX(rp(1),rp(kz))

DO i2=1,kz
   tfour1(i2)=dp(i2)/(SQRT(2.d0)*pi2*(xi2(i2)**1.5d0))   ! g(xi)
END DO
IF(tfour1(kz) > 0.d0)THEN                                 ! prepare antisymmetric form
   tfour1(1:kz)=tfour1(1:kz)-tfour1(kz)
ELSEIF(tfour1(kz) < 0.d0)THEN
   tfour1(1:kz)=tfour1(1:kz)+ABS(tfour1(kz))
END IF

CALL fourier_odd(kz,tfour1,nrr2,tfour1,stp,-1)  ! f(y)

stp=SQRT(1+pi2)*stp       ! To mimic the truncation of g(xi) with a step function (approx a Gaussian).
fac=((1+pi2)/pi2)**1.5d0
DO i2=2,kz
   y=(i2-1)*stp
   rp(i2)=sqrt(y)
   dp(i2)=tfour1(i2)*fac/sqrt(y)
END DO

END SUBROUTINE laplacei

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION dvdmpint(x)

! Integrand for dvdm integration

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x
REAL(KIND=8) :: dvdmpint,y

klo=1
y=EXP(spl(LOG(xxxn(1:i1)),LOG(rhop2(1:i1)),myf,LOG(x),klo))

dvdmpint=y*x**4

END FUNCTION dvdmpint
!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNCTION rroot(x)

IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x 
REAL(KIND=8) :: rroot,y

rroot=(gcos*massc-lam0*x**3/(2.d0*ih0**2))*massc-etac*x/3.d-1
END FUNCTION rroot

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNCTION rroot_p(x)

IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: x 
REAL(KIND=8) :: rroot_p,y

PRINT*,(gcos*massc-lam0*x**3/(2.d0*ih0**2))*massc-etac*x/3.d-1,gcos*massc*massc-etac*x/3.d-1,((gcos*massc-lam0*x**3/(2.d0*ih0**2))*massc-etac*x/3.d-1-(gcos*massc*massc-etac*x/3.d-1))
rroot_p=(gcos*massc-lam0*x**3/(2.d0*ih0**2))*massc-etac*x/3.d-1

END FUNCTION rroot_p

FUNCTION mv_einas(m200)

IMPLICIT NONE
REAL(KIND=8), INTENT(IN) :: m200 
INTEGER :: klo
REAL(KIND=8) :: mv_einas,lrf,nu

r200=(m200/(pi43*200.d0*(rho/omm)))**r13
lrf=(LOG(m200)-lrhopi43)*r13
klo=1
nu=delc0/EXP(spl(alrcom,ls0,mls0,lrf,klo)) ! z=0, use value del_c(z) for other epochs
alph=0.155+0.0095*nu**2
c200=10.d0**2.646d0*m200**(-0.138d0)       ! valid for z=0
y=m200/(pi43*95.4d0*(rho/omm)*gammp(3.d0/alph,(2.d0/alph)*c200**alph))
print*,m200,r200,alph
r_2=r200/c200
mv_einas=y*gammp(3.d0/alph,(2.d0/alph)*(c200*rvir/r200)**alph)/rvir**3-1.d0
END FUNCTION mv_einas

!?!?!?!?
!-----------------------------------------------------------------------
      FUNCTION diff3(a)
!
!     Given one value of a1 ,a(1), one value of b1, a(2), and one value 
!     of b2, a(3), this program measures by means of the chi-square merit 
!     function how well the Ms-rs from the M03 model agrees with the 
!     universal Ms-rs one.
!
!
!-----------------------------------------------------------------------

IMPLICIT NONE

INTEGER i,k

REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a
DOUBLE PRECISION diff3
DOUBLE PRECISION a1,a2,a3,b1,b2,c1
DOUBLE PRECISION z
DOUBLE PRECISION, DIMENSION(:), POINTER :: adx,ady,xcp,ycp,xcomp,ycomp
DOUBLE PRECISION dinf,diff,y,rs,cvir

PARAMETER (dinf=1.d30)
ALLOCATE(adx(kzn),ady(kzn),xcp(kzn),ycp(kzn),xcomp(kzn),ycomp(kzn))

a1=a(1)
a2=a(2)

adx(1:kzn)=a1*xxxn(1:kzn)     ! lineal
ady(1:kzn)=a2*sg02n(1:kzn)    ! lineal

CALL gaussconv0(adx,ady,xf2,ycp)

! Differences are made for positive values of \delta

IF(kcont == 0)THEN
   diff3=1.d30
   DEALLOCATE(adx,ady,xcp,ycp,xcomp,ycomp)
   RETURN
END IF
DO i=1,kcont
   xcomp(i)=xf2(i)
   ycomp(i)=yf2(i)
   IF(ycomp(i) < 0.d0)EXIT  
END DO
kcont=i-1

!-----comparison between convolved protohalo delta profile and the peak track
diff3=0.d0      
DO i=1,kcont
   IF(ycp(i) <= 0.D0.OR.ycomp(i) <= 0.D0)PRINT*,I,KCONT,ycp(i),ycomp(i)
   diff=LOG10(ycp(i))-LOG10(ycomp(i))
   diff3=diff3+diff*diff  ! chi-square
END DO
diff3=diff3/(kcont)         ! average square difference per point
DEALLOCATE(adx,ady,xcp,ycp,xcomp,ycomp)
END FUNCTION diff3


!-------------------------------------------------------------------------------------------------
FUNCTION diff3_x(a)

IMPLICIT NONE

INTEGER i,k

REAL(KIND=8), DIMENSION(:), INTENT(IN) :: a
DOUBLE PRECISION diff3_x
DOUBLE PRECISION a1,a2,b1,b2,c1
DOUBLE PRECISION z
DOUBLE PRECISION, DIMENSION(:), POINTER :: adx,ady,xcp,ycp,xcomp,ycomp
DOUBLE PRECISION dinf,diff,y,rs,cvir

PARAMETER (dinf=1.d30)
a1=a(1)

DO i=1,kzn
   IF((xf2(i)<1.d-11).OR.(xxxn(i)<1.d-11)) EXIT
   diff=LOG10(xf2(i))-LOG10(a1*xxxn(i))
   diff3_x=diff3_x+diff*diff
END DO
diff3_x=diff3_x/kzn


END FUNCTION diff3_x

!-------------------------------------------------------------------------------------------------
FUNCTION rgroot(x)

IMPLICIT NONE

DOUBLE PRECISION x,r,rgroot

rvar=x
rgroot=sg-SQRT(sn2ga_cos(0)*cnn)
END FUNCTION

END SUBROUTINE delta

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE minim_1(nd,neg,bd1,bd2,tol,amin,delta,nit,xtrm,rep,func)   

!     Given a double precision parametric function func with nd
!     free parameters a with values in a range bracketed by the user, 
!     this routine isolates the local minimum of func.
!     The minimum is searched by calculating the value of func  
!     in neg(1)*...*neg(nd) points of a nd-imensional grid of 
!     parameter values. This is done iteratively by reducing at each 
!     step the grid boundaries until the desired accuracy is reached.
!     The minimum function value is saved as fmin. 
!     The nd parameter values at the minimum and their accuracy 
!     are returned by the vectors amin and delta, respectively. 
!     nit returns the number of iterations required.

!     Input:
!        nd: number of dimensions of the grid, i.e., number of free
!            parameters of func (nitmx is the max. expected number 
!            of iterations)
!        neg(i): number of grid elements along each dim. (>=4)
!        ainf0(i) = min(bd1(n),bd2(n))
!        asup0(i) = max(bd1(n),bd2(n)): boundaries of parameter i,
!                   delta(i)=(asup0(i)-ainf0(i))/(neg(i)-1) is the grid
!                   element size along each dimension
!        tol(i): upper limit to the precision of the parameter values. 

!     Uses coord

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
INTEGER, INTENT(IN) :: nd
INTEGER, DIMENSION(:),INTENT(IN) :: neg
REAL(KIND=8), DIMENSION(:), INTENT(IN) :: bd1,bd2,tol 
REAL(KIND=8), DIMENSION(:), INTENT(OUT) :: amin,delta
INTEGER, INTENT(OUT) :: nit
LOGICAL, INTENT(OUT) :: rep,xtrm
INTEGER, PARAMETER :: ndmx=2,        &       ! expected no. of params
                      ncmx=400*ndmx, &       ! max. expected size of icoord
                      nitmx=3,      &       ! max. expected no. of iterations
                      nptmx=ndmx*nitmx*ncmx  ! max. expected no. of points where the minimum is calculated
INTEGER :: ngp,npt,iflag,n,i,j,negrid(0:ndmx),imin(ndmx),icoord(ncmx)
REAL(KIND=8) ::  f,fmin,y
REAL(KIND=8), DIMENSION(ndmx) :: a,ainf,ainf0,asup0,delta0(ndmx)
REAL(KIND=8), DIMENSION(nptmx) :: fsave
REAL(KIND=8), DIMENSION(nptmx,ndmx) :: asave
LOGICAL :: out,equal,verbose
DATA fmin,negrid(0)/1.d+300,1/

INTERFACE
   FUNCTION func(x)
     IMPLICIT NONE
     REAL(KIND=8), DIMENSION(:) :: x
     REAL(KIND=8) :: func
   END FUNCTION func
END INTERFACE

verbose=.TRUE.
DO n=1,nd
   ainf0(n)=min(bd1(n),bd2(n))
   asup0(n)=max(bd1(n),bd2(n))
   negrid(n)=neg(n)
   delta0(n)=(asup0(n)-ainf0(n))/dble(negrid(n)-1)
END DO
DO n=1,nd
   delta(n)=delta0(n)
   ainf(n)=ainf0(n)
   amin(n)=0.d0
   imin(n)=1
END DO

!-----determines the coordinates of the grid
CALL coord_1(nd,negrid,ngp,icoord)

!-----calculates the minimum of the values taken by func  
!     at the points of the grid. Repeats, reducing delta(i), until 
!     tol(i) is larger than delta(i).
npt=0
nit=1
200 IF(nit.gt.nitmx)THEN
   nit=nit-1
   goto 99
END IF
IF(verbose)THEN
   IF(nit > 1)THEN
      DO n=1,nd
         print*,'parameter no. ',n,': ',REAL(amin(n)),' (tolerance: ',REAL(delta0(n)),')'
      END DO
      print*,'Minimum value of the function:',fmin
   END IF
   write(*,'('' Iteration no.:'',i3)')nit
END IF
iflag=0
DO i=1,ngp
   npt=npt+1
   DO n=1,nd
      a(n)=ainf(n)+(icoord((i-1)*nd+n)-1)*delta(n)
      asave(npt,n)=a(n)
   END DO
   DO j=1,npt-1
      equal=.TRUE.
      DO n=1,nd
         equal=equal.AND.(asave(j,n) == a(n))
         IF(.NOT.equal)GOTO 62
      END DO
      f=fsave(j)
      GOTO 66
62 END DO
   f=func(a)
66 fsave(npt)=f
   IF(f <= fmin)THEN
      iflag=1
      DO n=1,nd
         imin(n)=icoord((i-1)*nd+n)
         amin(n)=a(n)
      END DO
      fmin=f
   END IF
END DO
xtrm=.FALSE.
DO n=1,nd
   xtrm=xtrm.or.(imin(n).eq.1.or.imin(n).eq.negrid(n))
END DO
xtrm=xtrm.and.(iflag.ne.0)
rep=.FALSE.
DO n=1,nd
   rep=rep.OR.(dabs(delta(n)) > tol(n))
END DO
rep=rep.OR.xtrm
IF(rep)THEN
   IF(xtrm)THEN
      IF(nit.eq.1)GOTO 99
      out=.FALSE.
      DO n=1,nd
         out=out.or.(amin(n).lt.ainf0(n).or.amin(n).gt.asup0(n))
      END DO
      IF(out)GOTO 99
      DO  n=1,nd
         delta0(n)=delta(n)
         IF(iflag /= 0)THEN
            ainf(n)=amin(n)-delta(n)*(DBLE(negrid(n))-1)/2.d0
         ELSE
            IF(dabs(delta(n)).gt.tol(n))THEN
               ainf(n)=amin(n)-delta(n)
               delta(n)=2.d0*delta(n)/(DBLE(negrid(n))-1)
            END IF
         END IF
      END DO
      nit=nit+1
      GOTO 200
   ELSE
      DO n=1,nd
         delta0(n)=delta(n)
         IF(delta(n).gt.tol(n))THEN
            ainf(n)=amin(n)-delta(n)
            delta(n)=2.d0*delta(n)/(dble(negrid(n))-1)
         END IF
      END DO
      nit=nit+1
      GOTO 200
   END IF
END IF
99 CONTINUE
IF(verbose)THEN
   DO n=1,nd
      print*,'parameter no. ',n,': ',REAL(amin(n)),' (tolerance: ',REAL(delta(n)),')'
   END DO
   print*,'Minimum value of the function:',fmin
END IF
fmin=1.d300
END SUBROUTINE minim_1

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

SUBROUTINE coord_1(ndim,ndiv,npts,icrd)

!     Used to determine the values of the adjustable parameters
!     on the points of the ndim-ensional grid defined in subroutine 
!     minim.

!     Output:
!        npts: number of grid points
!        icrd(i): ordinals associated to each grid point.

!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IMPLICIT NONE
INTEGER, INTENT(IN) :: ndim
INTEGER, DIMENSION(0:ndim), INTENT(IN) :: ndiv
INTEGER, INTENT(OUT) :: npts,icrd(:)
INTEGER :: n,kmax,imax,k,j,i,ipos

npts=1
DO n=1,ndim
   npts=npts*ndiv(n)
END DO
kmax=1
imax=npts
DO n=1,ndim
   kmax=ndiv(n-1)*kmax
   imax=imax/ndiv(n)
   DO k=1,kmax
      DO j=1,ndiv(n)
         DO i=1,imax
            ipos=ndim*(i-1+(j-1)*imax+(k-1)*(npts/kmax))+n
            icrd(ipos)=j
         END DO
      END DO
   END DO
END DO
END SUBROUTINE coord_1

END MODULE delta_track
